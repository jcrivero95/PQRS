//PRELOADER

(function(e){var t={position:"bottom",height:"5px",col_1:"#159756",col_2:"#da4733",col_3:"#3b78e7",col_4:"#fdba2c",fadeIn:200,fadeOut:200};e.materialPreloader=function(n){var r=e.extend({},t,n);$template="<div id='materialPreloader' class='load-bar' style='height:"+r.height+";display:none;"+r.position+":0px'><div class='load-bar-container'><div class='load-bar-base base1' style='background:"+r.col_1+"'><div class='color red' style='background:"+r.col_2+"'></div><div class='color blue' style='background:"+r.col_3+"'></div><div class='color yellow' style='background:"+r.col_4+"'></div><div class='color green' style='background:"+r.col_1+"'></div></div></div> <div class='load-bar-container'><div class='load-bar-base base2' style='background:"+r.col_1+"'><div class='color red' style='background:"+r.col_2+"'></div><div class='color blue' style='background:"+r.col_3+"'></div><div class='color yellow' style='background:"+r.col_4+"'></div> <div class='color green' style='background:"+r.col_1+"'></div> </div> </div> </div>";e("body").prepend($template);this.on=function(){e("#materialPreloader").fadeIn(r.fadeIn)};this.off=function(){e("#materialPreloader").fadeOut(r.fadeOut)}}})(jQuery)

preloader = new $.materialPreloader({
			position: 'top',
			height: '5px',
			col_1: '#FFC900',
			col_2: '#3b78e7',
			col_3: '#159756',
			col_4: '#3b78e7',
			fadeIn: 200,
			fadeOut: 200
		});
		
//window.alert=function(msg){generate('',msg);}
var _ResquestAJAX='';
var _location=window.location.hostname+($.trim(window.location.port)!=''?':'+window.location.port:'');
function utf8_decode(str_data) {
  var tmp_arr = [],
    i = 0,
    ac = 0,
    c1 = 0,
    c2 = 0,
    c3 = 0,
    c4 = 0;

  str_data += '';

  while (i < str_data.length) {
    c1 = str_data.charCodeAt(i);
    if (c1 <= 191) {
      tmp_arr[ac++] = String.fromCharCode(c1);
      i++;
    } else if (c1 <= 223) {
      c2 = str_data.charCodeAt(i + 1);
      tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
      i += 2;
    } else if (c1 <= 239) {
      // http://en.wikipedia.org/wiki/UTF-8#Codepage_layout
      c2 = str_data.charCodeAt(i + 1);
      c3 = str_data.charCodeAt(i + 2);
      tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
      i += 3;
    } else {
      c2 = str_data.charCodeAt(i + 1);
      c3 = str_data.charCodeAt(i + 2);
      c4 = str_data.charCodeAt(i + 3);
      c1 = ((c1 & 7) << 18) | ((c2 & 63) << 12) | ((c3 & 63) << 6) | (c4 & 63);
      c1 -= 0x10000;
      tmp_arr[ac++] = String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF));
      tmp_arr[ac++] = String.fromCharCode(0xDC00 | (c1 & 0x3FF));
      i += 4;
    }
  }

  return tmp_arr.join('');
}



function DesktopNotification(img,message,title) {
	if (Notification) {
		if (Notification.permission !== "granted") {
			Notification.requestPermission();
		}
		var title = (title==''?"Notificaciones Sisma Asistencial":title);
		var extra = {
			icon: (img==''?'ImagenesZeus/Simbolo_Zeus.png':img),
			body: message
		}
		var noti = new Notification( title, extra);
		noti.onclick = {
		// Al hacer click
		}
		noti.onclose = {
		// Al cerrar
		}
		//setTimeout( function() { noti.close() }, 5000);
	}
}


_permiteAtencion='';
$(function(){
	cargarDatePickerCitas();
});
function cargarDatePickerCitas(){
	try{	
				var dias_citas=get('dias_citas').value;
				var events=new Array();
				var nro_dias=dias_citas.split("|"); 
				
				for(var i=0;i<nro_dias.length;i++){
					events[i]=nro_dias[i];	
				}
				
				var dias_festivos=get('dias_festivos').value;
				var eventsFestivos=new Array();
				var nro_diasFestivos=dias_festivos.split("|"); 
				
				for(var i=0;i<nro_diasFestivos.length;i++){
					eventsFestivos[i]=nro_diasFestivos[i];	
				}
				
				///alert(events.length);
				/*events[0]='03-10-2012';
				events[1]='22-8-2012';
				events[2]='21-8-2012';
				events[3]='20-8-2012';*/
	
			//	events=['03-8-2012','20-10-2012','10-8-2012'];
				//alert(events.length);
				var myDate = new Date();
				myDate.setMonth(myDate.getMonth() + 0, 1);
				$('#datepicker').datepicker({
					inline: true,
					defaultDate:myDate,
	
					onSelect: function(dateText, inst) { 
					  seleccionarFechaCita(dateText);
					 
					},
					 onChangeMonthYear: function(year, month, inst) {
						var myDate = new Date();
						myDate.setMonth(myDate.getMonth() + 3, 1);
						$('#datepicker2').datepicker('option', {defaultDate: myDate}); 
						/*$('#datepicker2').datepicker({
						inline: true,
						defaultDate:myDate});*/
						
					},
					
					
					beforeShowDay: function(date) {
					
						var current = $.datepicker.formatDate('dd-m-yy', date);
						if(jQuery.inArray(current, events) == -1){
							//return [true, ''];
							if(jQuery.inArray(current, eventsFestivos) != -1){
								return [true, 'ui-festivo', ''];	
							}else{
								return [true, ''];	
							}
							
							
						}else{
							if(jQuery.inArray(current, eventsFestivos) != -1){
								return [true, 'ui-state-hover ui-festivo', ''];	
							}else{
								return [true, 'ui-state-hover', ''];	
							}
						}
						//return jQuery.inArray(current, events) == -1 ? [true, ''] : [true, 'ui-state-hover ui-festivo', ''];
					}
	
				});
				
				
				$('#datepicker').datepicker('option', {dateFormat: 'yy/mm/dd'}); 
				
				//events=['03-9-2012','20-9-2012','10-9-2012'];
				var myDate = new Date();
				myDate.setMonth(myDate.getMonth() + 1, 1);
				$('#datepicker2').datepicker({
					inline: true,
					defaultDate:myDate,
	
					onSelect: function(dateText, inst) { 
					  seleccionarFechaCita(dateText);
					},
					/* onChangeMonthYear: function(year, month, inst) {
	
					}*/
					
					
					beforeShowDay: function(date) {
						var current = $.datepicker.formatDate('dd-m-yy', date);
						if(jQuery.inArray(current, events) == -1){
							//return [true, ''];
							if(jQuery.inArray(current, eventsFestivos) != -1){
								return [true, 'ui-festivo', ''];	
							}else{
								return [true, ''];	
							}
							
							
						}else{
							if(jQuery.inArray(current, eventsFestivos) != -1){
								return [true, 'ui-state-hover ui-festivo', ''];	
							}else{
								return [true, 'ui-state-hover', ''];	
							}
						}
						//return jQuery.inArray(current, events) == -1 ? [true, ''] : [true, 'ui-state-hover', ''];
					}
	
				});
				
				
				$('#datepicker2').datepicker('option', {dateFormat: 'yy/mm/dd'}); 
				
							
			//	events=['03-10-2012','20-10-2012','10-10-2012'];
				var myDate = new Date();
				myDate.setMonth(myDate.getMonth() + 2, 1);
				$('#datepicker3').datepicker({
					inline: true,
					defaultDate:myDate,
	
					onSelect: function(dateText, inst) { 
					  seleccionarFechaCita(dateText);
					},
					/* onChangeMonthYear: function(year, month, inst) {
	
					}*/
					
					
					beforeShowDay: function(date) {
						var current = $.datepicker.formatDate('dd-m-yy', date);
						if(jQuery.inArray(current, events) == -1){
							//return [true, ''];
							if(jQuery.inArray(current, eventsFestivos) != -1){
								return [true, 'ui-festivo', ''];	
							}else{
								return [true, ''];	
							}
							
							
						}else{
							if(jQuery.inArray(current, eventsFestivos) != -1){
								return [true, 'ui-state-hover ui-festivo', ''];	
							}else{
								return [true, 'ui-state-hover', ''];	
							}
						}
						//return jQuery.inArray(current, events) == -1 ? [true, ''] : [true, 'ui-state-hover', ''];
					}
	
				});
				
				
				$('#datepicker3').datepicker('option', {dateFormat: 'yy/mm/dd'}); 
	
	
	}catch(e){}
}			


function ajaxFunction() {
  var xmlHttp;
  
  try {
   
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
  } catch (e) {
    
    try {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
    } catch (e) {
      
	  try {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
      } catch (e) {
        alert("Tu navegador no soporta AJAX!");
        return false;
      }}}
}


function ajax(_pagina, contenedor,metodo,loading){
	crearLoadingJX();
	try{
		if(loading!=''){
			document.getElementById(loading).style.visibility="visible";
		}
	}catch(e){}
	var f = document.getElementById("formulario");
	var ajax;
	ajax = ajaxFunction();
	ajax.open("POST", _pagina, true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.onreadystatechange = function() {
		if (ajax.readyState == 4) {
			eliminaLoadingJX();
			if(contenedor!=''){
				document.getElementById(contenedor).innerHTML=ajax.responseText; 
			}
			if(loading!=''){
				document.getElementById(loading).style.visibility="hidden";
			}
			if(metodo!=''){
				setTimeout(metodo,0);
			}
		}
	}
	ajax.send(null);
}

function crearLoadingJX(titulo){
	var strTitulo='Cargando...';
	if(titulo!=undefined && titulo!='undefined' && $.trim(titulo)!=''){
		strTitulo=titulo;
	}
	$css={
		width:'100%',
		height:'100%',
		backgroundColor:'#000',
		position:'fixed',
		top:'0px',
		left:'0px',	
		opacity:'0.0',
		filter: 'alpha(opacity=00)',
		zIndex:'99999999'
	}
	
	$css2={
		/*width:'100px',*/
		height:'30px',
		position:'fixed',
		bottom:'1%',
		left:'0.1%',
		zIndex:'99999999',
		fontSize:'12px',
		backgroundColor:'#1D71B6',
		borderRadius:'3px',
		borderWidth:'1px',
		paddingTop:'7px',
		paddingLeft:'2px',
		color:'#fff !important'
	}
	
	$div=$('<div id="divLoading">');
	$div.css($css);
	$div2=$('<div id="lbldivLoading" class="letraDisplay"><table><tr><td><img src="http://'+ _location + '/SismaSalud/ips/App/ImagenesZeus/loading.gif"></td><td style="color:#fff !important"><b>'+strTitulo+'</b></td></tr></table></div>');
	$div2.css($css2);	
	$div.appendTo("body");
	$div2.appendTo("body");	
	//$div2.css('margin-left',(($div2.width()/2)*-1));
 	preloader.on();
}

function eliminaLoadingJX(){
	$("#divLoading").remove();
	$("#lbldivLoading").remove();	
	preloader.off();	
}

function ajaxPOST(_pagina, contenedor,metodo,loading,param,tituloLoading){
	//console.log(contenedor);
	crearLoadingJX(tituloLoading);
	
	try{
		if(loading!=''){
			document.getElementById(loading).style.visibility="visible";
		}
	}catch(e){}
	var f = document.getElementById("formulario");
	var ajax;
	ajax = ajaxFunction();
	ajax.open("POST", _pagina, true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.onreadystatechange = function() {
		if (ajax.readyState == 4) {
			
			eliminaLoadingJX();
			
			_ResquestAJAX=ajax.responseText;
			if(contenedor!=''){

				document.getElementById(contenedor).innerHTML=ajax.responseText; 
			}
			if(loading!=''){
				document.getElementById(loading).style.visibility="hidden";
			}
			if(metodo!=''){
				setTimeout(metodo,0);
			}
		}
	}
	ajax.send(param);
}

function get(id){
	return document.getElementById(id);
}

function trim (myString)
{
	
	try{
		 myString=myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
	}catch(e){
		myString= '';
	}
	return myString;
}

function seleccionarFechaCita(fechacita){
	get('codigo_medico').value='';
	get('fecha').value=fechacita;
	get('horacita').innerHTML='';
	cargarProgramacion('','',1);	
}


function abrirFormularioPaciente(){

	if($("#chincheStatus").val()=='off'){
		get('estado').value="1";
		$('#link').trigger("click");	
	}

}
function cerrarFormularioPaciente(){
	get('estado').value="0";
	$('#link').trigger("click");	
}


function showMedicoCitaAsig(codMedico,nombre_medico,fecha){
	ajaxPOST('../../controlador/citas/citas.php?operacion=getCitasAsignadasMedico','divCitaAsignadasMedico','setCitasAsignadasMedico("'+nombre_medico+'")','','codMedico='+codMedico+'&fecha='+fecha);
}

function setCitasAsignadasMedico(nombre_medico){
	get('nomMedicoCitaAsig').innerHTML=nombre_medico;
	get('nroCitasAsignadas').innerHTML=getValue('rs_citas_asignadasMed');
	get('tdNroCitaAsignadaMedico').style.display='';
	
}

function setCita(id,fecha){
	//try{
		var td=document.getElementById(id);
		$(td).click();
		if($.trim(getValue('fecha'))!=$.trim(fecha)){
			get('fecha').value=fecha;
			cargarProgramacion('',"setCita('"+id+"','"+fecha+"');");
		}
		closeModal();
	//}catch(e){}	
}

function validarFechaMenorActual(date){
	  date=date.replace("-", "/");
	  date=date.replace("-", "/");

      var x=new Date();
      var fecha = date.split("/");
      x.setFullYear(fecha[0],fecha[1]-1,fecha[2]);
      var today = new Date();
 
      if (x >= today)
        return false;
      else
        return true;
}
function obtener_idprog(id){
	$("#IdProgMedDeta").val(id);
}

$_esAdicional=0;
$muestraBtnConfirmar=0;
function selecionarHora(hora,meridiano,fecha,id_medico,nombre_medico,estado_cita,id_cita,classCita,fecha_usuario_desea_cita,
	formaSolicitud,id_programacion,lugarAtencion,asuntoCita,esAdicional,muestraBtnConfirmar,confirmada_tem = null,selecionarHora=null,){
	$_esAdicional=0;
	if(esAdicional==1){
		$_esAdicional=1;
	}
	$('#tipo_examen').val("");
	$('#nombre_otro_tipo_examen').val("");
	
	$muestraBtnConfirmar=muestraBtnConfirmar;
	validarTipoApartarCita()
	

	$("#modificaCedula").attr("checked",false);
	
	try{
		$(this).ZS_CleanReferencias();
	}catch(e){};
	if($("#selCitaAplazar").val()!='ok'){
		get('id_asunto_cita_aplazar').value='';
	}
	showMedicoCitaAsig(id_medico,nombre_medico,fecha);
	
	get('id_programacion').value=id_programacion;
	
	abrirFormularioPaciente();
	
	try{
	var claseAnt='disponible';

	var esMismo=false;
	for(var i=0;i<document.getElementsByClassName('seleccionado').length;i++){
		
		if(get('estado_cita').value=='P'&&!validarFechaMenorActual(fecha)){
			claseAnt+=' citaPendiente ';
		}else if(get('estado_cita').value=='A'){
			claseAnt+=' citaAtendida ';
		}else if(get('estado_cita').value=='CC'){
			
			if(get('confirmada_temp_hiden').value==1){
				claseAnt+=' citaCconfirmadaTemp ';
			}else{
				claseAnt+=' citaConfirmada ';
			}
		}else if(get('estado_cita').value=='I'||(get('estado_cita').value=='P'&&validarFechaMenorActual(fecha))){
			claseAnt+=' citaIncumplida ';
		}
		if(document.getElementsByClassName('seleccionado').item(i).id=='td'+hora+meridiano+id_medico){
			esMismo=true;
		}

		document.getElementsByClassName('seleccionado').item(i).className=''+claseAnt;	
		
	}	
	}catch(e){//alert("aqui.: "+e);
	}
	if(estado_cita=='P'){
		//if(!esMismo){
			get('td'+hora+meridiano+id_medico).className='seleccionado borderSel';	
		/*}else{
			id_cita='';	
		}*/
	}else{
		//if(!esMismo){
			get('td'+hora+meridiano+id_medico).className='seleccionado';		
		/*}else{
			id_cita='';	
		}*/
	}
	
	get('horacita').innerHTML=fecha+' '+hora+' '+meridiano+' Medico: '+nombre_medico;
	
	var n=hora.split(":"); 
	get('hora').value=n[0];
	get('minutos').value=n[1];
	get('meridiano').value=meridiano;
	get('medico').value=nombre_medico;
	get('codigo_medico').value=id_medico;
	
	if(get('fecha_usuario_desea_cita').value==''){
		get('fecha_usuario_desea_cita').value=fecha;
	}
	/*if(fecha_usuario_desea_cita!=''&&get('fecha_usuario_desea_cita').value==''){
		console.log(fecha_usuario_desea_cita);
		get('fecha_usuario_desea_cita').value=fecha_usuario_desea_cita;
	}*/
	$("#fecha_usuario_desea_cita").val(fecha_usuario_desea_cita);
	get("confirmada_temp_hiden").value= confirmada_tem;
	get('fecha_cita').value=fecha;	
	get('estado_cita').value=estado_cita;
	get('id_cita').value=id_cita;
	get('obs').value="";
	if(trim(id_cita)!=""&&estado_cita=='P'&&get('id_cita_aplazar').value=="0"){
		get('id_cita_aplazar').value=id_cita;
		//get('id_asunto_cita_aplazar').value=asuntoCita;
	}
	
		
	if(formaSolicitud!=''&&formaSolicitud!='0'){
		
		get('formaSolicitud').value=formaSolicitud;
	}
	
	try{
		get('lugarAtencion').value=lugarAtencion;
	}catch(e){
		//alert(e);
	}
	/*if(estado_cita=='P'){
		cargarDatosCita(id_cita);
	}*/
	//alert("ok");
	cod_contrato = $("#id_contrato").val();
	cargarAsuntosMedico(id_medico,asuntoCita,cod_contrato);
	$("#id_cita").val(id_cita);
}

function cargarAsuntosMedico(id_medico,asuntoCita,cod_contrato){
	deshabilitarToolBar();

	autoid = $('#autoid').val()
	var esCitaMultiple = $("#esCitaMultiple").val();
	if($.trim(id_medico) != ""){

		if(esCitaMultiple == 1){
			ajaxPOST("../../controlador/citas/citas.php","","setAsuntosMedico('"+asuntoCita+"', 'Asuntos_multiple');habilitarToolBar();","","operacion=cargarAsuntosMedico&id_medico="+id_medico+"&asuntoCita="+asuntoCita+"&contrato_paci="+cod_contrato+"&autoid="+autoid+"&c_multiple=1");

		}else{
			ajaxPOST("../../controlador/citas/citas.php",""
			,"setAsuntosMedico('"+asuntoCita+"', 'Asuntos_multiple');habilitarToolBar();","",
			"operacion=cargarAsuntosMedico&id_medico="+id_medico+"&asuntoCita="+asuntoCita+"&contrato_paci="+cod_contrato+"&autoid="+autoid);

		}
    }else{
		//No perder funcionalidades
        habilitarToolBar(); //Habilitar barras superiores de menus
        validarContrato(); // Validación de contrato del cliente
    }
}

function setAsuntosMedico(asuntoCita, Asuntos_multiple){
	$("#nomservicio option").remove();
	var resp=JSON.parse(_ResquestAJAX);

	$("#nomservicio").append('<option value="">[SELECCIONE]</option>');
	for(var i in resp) {

		if(resp[i]["deshabilitar"] == true){
			$("#nomservicio").append('<option style="background-color: #E8E8E8; color: grey;" disabled="true" value="'+resp[i]["id"]+'">'+resp[i]["nombre"]+'</option>');
		}else{
			$("#nomservicio").append('<option value="'+resp[i]["id"]+'">'+resp[i]["nombre"]+'</option>');
		}

		//$("#nomservicio").append('<option value="'+resp[i]["id"]+'">'+resp[i]["nombre"]+'</option>');
	}
	
	$("#nomservicio").val(asuntoCita);
	
	if($.trim(asuntoCita)==''){
		$("#nomservicio").val($_asuntoTemp);
	}
	
	try{
		if(get('estado_cita').value!='P'&&get('id_cita_aplazar').value!='0'){
			$("#nomservicio").val($('#id_asunto_cita_aplazar').val());
		}
	}catch(e){}
	
	validarContrato();
}



function validarContrato(){
	contrato=$.trim($("#id_contrato").val());

	if(contrato==''){
		return false;
	}

	respValidarContrato=function(){
		resp=JSON.parse(_ResquestAJAX);
		$("#confirmarCitaEPS_backup").val(resp[0]["confirmaCitaMedico"]);
	}

	ajaxPOST("../../controlador/citas/citas.php","","respValidarContrato()","","operacion=validarContrato&Contrato="+contrato);
}

function selecionarHoraTrasladar(id){
	$("#tdTraslado"+id).toggleClass('borderSelTraslado');
	/*if($("#tdTraslado"+id).hasClass('borderSelTraslado')){
		$("#tdTraslado"+id).removeClass('citaPendiente ');
	}else{
		$("#tdTraslado"+id).addClass('citaPendiente ');	
	}*/
	
}

function confirmarTraslado(clase){
	
	var sel=document.getElementsByClassName(clase+' borderSelTraslado');
//	var Fecha_Agenda=get('Fecha_Agenda').value;
	var Fecha_Agenda=((clase=='TrasIzq')?get('Fecha_Destino').value:get('Fecha_Agenda').value);
	var citasSel='';
	
	var codMedicoDestino=((clase=='TrasIzq')?get('codigoMedicoB').value:get('codigoMedicoA').value);
	for(i=0;i<sel.length;i++){
		try{
			id=sel.item(i).id;
			citasSel+=$("#"+id).attr('valueIdCita')+',';
		}catch(e){}
	}
	if(trim(Fecha_Agenda)==''){
		notify("Seleccione la fecha a trasladar","error");
		return false;
	}
	if(trim(get('Fecha_Destino').value)==''){
		notify("Seleccione la fecha Destino","error");
		return false;
	}
	if(trim(citasSel)==''){
		notify("No ha seleccionado ninguna cita","error");
	}else{
		ajax("../../controlador/citas/citas.php?operacion=trasladarAgenda&codMedDestino="+codMedicoDestino+"&Fecha_Agenda="+Fecha_Agenda+"&citas="+citasSel, "divTrasladoAgenda",'cargarProgramacionMedicoA();cargarProgramacionMedicoB();cargarProgramacion(\'\');','');
	}
}

function cargarDatosCita(id_cita,tipo_servicio,id_stock,nom_stock,primera_vez_control,observacion,ExisteDocVencidos){
	cargaProcedimientosCita(id_cita);
	//cargaCanastasCita(id_cita);
	
	get('tipo_servicio').value=tipo_servicio;
	get('id_stock').value=id_stock;
	get('stock').value=nom_stock;
	get('primera_vez_control').value=primera_vez_control;
	get('obs').value=observacion;
	$('#existe_vencidos').val(ExisteDocVencidos);
	$("#id_cita").val(id_cita);
}

function verLogCitaModificada(IdCita){
	var widget = new ModalContentZeusSalud("Modificaciones de Citas",
		"../../controlador/citas/citas.php", {operacion:'verLogCitas',IdCita:IdCita}, "general", "", "", "");
		widget.process();	
}

function cargaProcedimientosCita(id_cita){
	console.log('Cirugia 33');
		ajax("../../controlador/citas/citas.php?operacion=cargar_procedimientos_cita&id_cita="+id_cita, "contenidoProcedimientos",'cargaCanastasCita('+id_cita+')','');

}
function cargaCanastasCita(id_cita){
		ajax("../../controlador/citas/citas.php?operacion=cargar_canastas_cita&id_cita="+id_cita, "contenidoCanasta",'limpiarCanastaInput();limpiarProcedimientoInput();verificarAseguradoraCita();','');

}

function verificarAseguradoraCita(){

	respVerificacionAseguradora=function(){
		var resp=JSON.parse(_ResquestAJAX);

		if($.trim(resp[0]["aseguradora"])=='1' ){
			$("#trPoliza").css({display:''});

			window.top.fnCargarSelectPoliza($("#autoid").val(),$("#codigo_empresa").val(),$("#no_poliza"),resp[0]["NumeroPoliza"]);
		}else{
			$("#trPoliza").css({display:'none'});
			
		}
	}
	
	ajaxPOST("../../controlador/citas/citas.php", "",'respVerificacionAseguradora()','',$.param({operacion:"verificarAseguradoraCita",IdCita:$("#id_cita").val(),CodEmpresa:$("#codigo_empresa").val()}));

}

function addPoliza(){
	window.top.fnAddPoliza($("#autoid").val(),$("#codigo_empresa").val(),$("#no_poliza"));
}

/*
function cargarProcedimientos(){
	var id_procedimiento=get('id_procediento_realizar').value;
    var id_contrato=get('id_contrato').value;
	
	ajax("../../controlador/citas/citas.php?operacion=cargar_procedimientos&id_procedimiento="+id_procedimiento+"&id_contrato="+id_contrato, "contenidoProcedimientos",'limpiarProcedimientoInput()','');

}
*/

function traerPaciente() {
	$("#buscaPaciente").val("");
	var tipo=get('tipo').value;
	var ident=get('ident').value;
	setMedicos('','');
	get('obs').value='';
	$("#medico").val();
	$("#codigo_medico").val();
	try{
		get('tipo_servicio').value="";
		get('stock').value="";
		get('id_stock').value="";
		

		cargaProcedimientosCita(0);
		cargaCanastasCita(0);	
	}catch(e){}
		
	$("#sede").focus();
	ajax("../../controlador/citas/citas.php?operacion=getPaciente&tipo="+tipo+"&ident="+ident, "res_paciente",'setPaciente();','');
}

function setMedicos(codigo_medico,medico){
	get('medico').value=medico;
	get('codigo_medico').value=codigo_medico;
	closeModal();
}
function modificarCedula(){
	if(!get('modificaCedula').checked){
		cargarProgramacion('');	
		traerPaciente();
		
	}
}

function buscarMedico() {
	showWindowFind('codigo, nombre,especialidad,tiempo', 'sis_medi', 'es_medico | 1', 
		'Codigo, Nombre', '80, 300', 'cod_medico#nom_medico', 'M&eacute;dicos','cargarProgramacion()')
}

function seleccionar(id){
	$("#"+id).select();	
}

function replaceTipoSangre(tipo_sangre){
	tipo_sangre=tipo_sangre.replace("+","|43;");
	tipo_sangre=tipo_sangre.replace("-","|45;"); 	
	return tipo_sangre;
}

function validarTipoApartarCita(){
	try{

		if(trim(get('confirmarCitaEPS').value)=='S'){
			get('btnConfirmarFlotante').style.display='none';
			$("#Column05").css({display:'none'});
		}else{
			get('btnConfirmarFlotante').style.display='';
			$("#Column05").css({display:''});
		}

		if($.trim($("#nro_procedimientos_agregadas").val())==0 && trim(get('confirmarCitaEPS_backup').value)=='S'){
			get('btnConfirmarFlotante').style.display='none';
			$("#Column05").css({display:'none'});
		}else if($.trim($("#nro_procedimientos_agregadas").val())>0){
			get('btnConfirmarFlotante').style.display='';
			$("#Column05").css({display:''});
		}

		if( $("#confirmada_agrupada").val() ==1 ){
			get('btnConfirmarFlotante').style.display='';
			$("#Column05").css({display:''});
		}
		get('btnConfirmarFlotante').style.display='';
	
	}catch(e){}
	//get('confirmarCitaEPS').value=get('confirmarCitaEPS_backup').value;
	//alert(get('procedimiento_noqx').checked);
	/*if(get('procedimiento_noqx').checked || $_esAdicional==1 || $muestraBtnConfirmar==1){
		get('confirmarCitaEPS').value='N';
		get('btnConfirmarFlotante').style.display='';
	}*/
	
	
}

function ValidaTelefonoCita(){

	var valida=false;
	if($.trim(getValue('validar_telefono'))=='S' && $.trim(getValue('telefono'))==''){
		valida=true;
	}
	return valida;
}

function ValidaEmailCita(){

	var valida=false;
	if($.trim(getValue('validar_correo'))=='S' && $.trim(getValue('email'))==''){
		valida=true;
	}
	return valida;
}

function ver_planes_pyp(){
	var sexo = $('#sexo').val()
	var edad = calcularEdad($('#fechanaci').val());
	var errorCita = getValue('error_citas');
 

	if(errorCita == 'N'){
		
		$.ajax({
			type: 'post',
			url: '../../controlador/citas/citas.php?operacion=planes_pyp_para_usuario',
			 data:  {
				edad: edad, 
				sexo:sexo
			},
			 success: function(data)
			{
				 
				 if(data.length > 0){
					showWindowFind('codigo,nombre','pyp_planes',' edad_inicial <= '+edad+' and edad_final >= '+edad+' and (sexo = \'U\' or sexo = \''+sexo+'\' )','Codigo,Nombre','80,500','','Planes PYP disponibles para el paciente','','',null,null,'showBuscarEmpresas');
				 }
	 
			},
			error: function(e)
			{
			   alert(e.message);
			}
		});
	}

}
function calcularEdad(fecha) {
    var hoy = new Date();
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }

    return edad;
}

function apartarCita(){
	var autoid = $('#autoid').val();
	var asunto = $("#nomservicio").val();
	var id_contrato_ = $('#id_contrato').val();
	alerta_asunto_procedimiento=function(){
		var resp=JSON.parse(_ResquestAJAX);
		
		var numero_autorizacion =0;
		if(resp.alerta == 1){
				var widget2233 = new ModalContentZeusSalud("Alerta",
				"../../Vistas/citas/alerta_asunto_procedimiento.php?idcontrato="+id_contrato_+"&asunto="+asunto+"&autoid="+autoid,  
				{}, "general", "", "400px", "",null,null,false,"id_ModalMultipleProcs"); 
				widget2233.imgs_data = { 
					close_icon:{ 
						src:"../../ImagenesZeus/salir 01.png",  
						_w:32,  
						_h:32,  
						_class:"icon close", 
						_title:"Cerrar", 
						id:"cerrar" 
					}, 
						
					confirmar_icon:{
						src:"../../ImagenesZeus/confirmar citas 01.png",  
						_w:32,  
						_h:32,  
						_class:"icon", 
						_title:"Guardar", 
						id:"Guardar", 
						fnAction:function(){ 
							numero_autorizacion =	$('#autorizacion').val();
							numero_autorizacion = $.trim(numero_autorizacion);
							if(numero_autorizacion ==""){
								notify("Debe digitar un numero de autorizacion","error");
								return false;
							}else{
								apartar_cita_datos(numero_autorizacion);
								closeModal();
							}
							
						}
					}
				}; 
				widget2233.process(); 
			}else if(resp.alerta == 2){
				var w = new ModalContentZeusSalud("Alerta", 
				'<div style="color: #fff; background-color:#EE6262; font-size: 13px; padding: 5px;"><strong>Uno de los Cups en la cita no esta contratado. CUPS: ('+resp.cups_no_contratados+')</strong></div>'
				, {}, "content", "", null, null, null,null,false, 'alertaut_cita');
				 var icons = {}
				$.extend(w.imgs_data, icons);
				w.process();
			}else{
				apartar_cita_datos();
				closeModal();
			} 
			
		}
		ajaxPOST("../../controlador/citas/citas.php","","alerta_asunto_procedimiento()","","operacion=alerta_asunto_procedimiento&idcontrato="+id_contrato_+"&idcita="+$("#id_cita").val()+"&asunto="+asunto);
}

function apartar_cita_datos(numero_autorizacion =null){
	
	if($("#nro_procedimientos_agregadas").val()=="0"){
		$("#confirmarCitaEPS").val($("#confirmarCitaEPS_backup").val());
	}else{
		$("#confirmarCitaEPS").val("N");
	}
	
	var idCita=get('id_cita').value;
	var confirmada_agrupada = $("#confirmada_agrupada").val();
	var esCitaMultiple = $("#esCitaMultiple").val();
	var estado_cita=get('estado_cita').value;
	var idprodmedidetalle  = $("#IdProgMedDeta").val();
	if($.trim(estado_cita)!='' &&  confirmada_agrupada != 1){
		notify("No se puede apartar la cita en este espacio.","error");
		return false;
	}

	if(ValidaTelefonoCita()){
		if(  esCitaMultiple != 2){
			notify("Ingrese el numero telefonico del paciente.","error");
			return false;
		}
	}
	if(ValidaEmailCita()){
		notify("Ingrese el Correo del paciente.","error");
		return false;
	}
	existeVencidos();
	validarAtencion();
	validarTipoApartarCita();
	
	if(trim(get('autoid').value)=='' &&  esCitaMultiple != 2 ){
		notify("Guarde los datos del paciente antes de apartar una cita.","error");
		return false;
	}
	if(get('bloqueaAtencionUsuario').value=='1'&&getValue('ManejaProcesosEPS')=='S'){	
		alertGlobal(_permiteAtencion);
		return false;
	}
	
	get('txtCitaMarcada').innerHTML='';
	var estado_cita=get('estado_cita').value;
	if(estado_cita=='P' && confirmada_agrupada != 1){
		notify("Ya se ha apartado esta cita.","error");	
	}else if(estado_cita=='CC'){
		notify("Ya se ha confirmado esta cita.","error");	
	}else if(estado_cita=='A'){
		notify("Ya se ha atendido esta cita.","error");	
	}else{
		
		var nro_procedimientos_agregadas=0;
		try{
			nro_procedimientos_agregadas=get('nro_procedimientos_agregadas').value;
		}catch(e){}
		
		var id_programacion=get('id_programacion').value;
		
		var tipo_id=get('tipo').value;
		var num_id=get('ident').value;
		var pnombre=encodeURIComponent(get('pnombre').value);
		var snombre=encodeURIComponent(get('snombre').value);
		var papellido=encodeURIComponent(get('papellido').value);
		var sapellido=encodeURIComponent(get('sapellido').value);
		var sexo=get('sexo').value;
		var fecha_naci=get('fechanaci').value;
		var tipo_sangre=replaceTipoSangre(get('tipo_sangre').value);
		var tipo_afiliacion=$('#tipo_afiliacion').val();
		var codigo_departamento=get('codigo_departamento').value;
		var codigo_ciudad=get('codigo_ciudad').value;
		var codigo_barrio=get('codigo_barrio').value;
		var direccion=encodeURIComponent(get('direccion').value);
		var telefono=get('telefono').value;
		var contrato=get('id_contrato').value;

		var NumeroPoliza=$("#no_poliza").val();


		var email=get('email').value;
		var sede=get('sede').value;
		
		var recordar_cita='0';
		
		if(get('recordar_cita').checked){
			recordar_cita='1';
		}
		
		
		var responsable_nombre=(get('responsable_nombre').value);
		var responsable_apellido=(get('responsable_apellido').value);
		var responsable_parentesco=(get('responsable_parentesco').value);
		var responsable_direccion=(get('responsable_direccion').value);
		var responsable_telefono=(get('responsable_telefono').value);
		
	
		
		var paramPersonal='&tipo_id='+tipo_id+'&num_id='+num_id+'&pnombre='+pnombre+'&snombre='+snombre+'&papellido='+papellido+'&sapellido='+sapellido+'&sexo='+sexo+'&fecha_naci='+fecha_naci+'&direccion='+direccion+'&telefono='+telefono+'&tipo_sangre='+tipo_sangre+'&codigo_departamento='+codigo_departamento+'&codigo_ciudad='+codigo_ciudad+'&codigo_barrio='+codigo_barrio+"&contrato="+contrato+"&sede="+sede+"&email="+email+'&recordar_cita='+recordar_cita+'&responsable_nombre='+responsable_nombre+'&responsable_apellido='+responsable_apellido+'&responsable_parentesco='+responsable_parentesco+'&responsable_direccion='+responsable_direccion+'&responsable_telefono='+responsable_telefono+'&id_programacion='+id_programacion+"&tipo_afiliacion="+tipo_afiliacion;
		
		
		
		var lugarAtencion=getValue('lugarAtencion');
		var autoid=get('autoid').value;
		var cod_medico=get('codigo_medico').value;
		var fecha_usuario_desea_cita=get('fecha_usuario_desea_cita').value;
		var fecha_cita=get('fecha_cita').value;
		var hora=get('hora').value;
		var minutos=get('minutos').value;
		var meridiano=get('meridiano').value;
		var primera_vez_control=get('primera_vez_control').value;
		
		var estado='P';
		var asunto=get('nomservicio').value;
		var observacion=encodeURIComponent(get('obs').value);
		var codigo_empresa=get('codigo_empresa').value;
		var id_stock=get('id_stock').value;
		var id_procediento_realizar=get('id_procediento_realizar').value;
		var tipo_servicio=get('tipo_servicio').value;
		var tipo_examen = get('tipo_examen').value;
		var nombre_tipo_de_examen = get('nombre_tipo_de_examen').value;
		var nombre_otro_tipo_examen = get('nombre_otro_tipo_examen').value;
		if(nombre_tipo_de_examen == ""){
			nombre_tipo_de_examen = nombre_otro_tipo_examen;	
		}
 
		
		
		
		var nro_canastas_agregadas=0;
		
		try{
			nro_canastas_agregadas=get('nro_canastas_agregadas').value;
		}catch(e){
			nro_canastas_agregadas=0;
		}
		
		/*var estrato=get('id_estrato').value;
		var es_terapia=get('es_terapia').value;
		var tipo_historia=get('tipo_historia').value;*/
	
		/*alert(autoid+" | "+cod_medico+" | "+fecha_cita+" | "+hora+":"+minutos+" "+meridiano+" | "+estado+" | "+asunto+" | "+observacion+" | "+codigo_empresa);*/
		if(!get('procedimiento_noqx').checked){
			tipo_servicio='';
		}
		
		if(trim(tipo_id)=="" &&  esCitaMultiple != 2 ){
			notify("Seleccione un tipo de documento","error");		
		}else if(trim(num_id)==""  &&  esCitaMultiple != 2  ){
			notify("Ingrese numero de documento","error");
			seleccionar('ident');
		}else if(trim(pnombre)==""  &&  esCitaMultiple != 2  ){
			notify("Ingrese primer nombre del paciente","error");
			seleccionar('pnombre');
		}else if(trim(papellido)==""  &&  esCitaMultiple != 2 ){
			notify("Ingrese primer apellido del paciente","error");
			seleccionar('papellido');
		}else if(trim(sexo)=="" &&get('citasRapidas').value!='S'  &&  esCitaMultiple != 2 ){
			notify("Seleccione el sexo","error");
		}else if(trim(fecha_naci)==""&&get('citasRapidas').value!='S'  &&  esCitaMultiple != 2  ){
			notify("Seleccione la fecha de nacimiento del paciente","error");
			seleccionar('fechanaci');
		}else if($.trim(tipo_afiliacion)==""&&get('citasRapidas').value!='S'   &&  esCitaMultiple != 2  ){
			notify("Seleccione el tipo de afiliacion","error");
			seleccionar('tipo_afiliacion');
		}else if(trim(cod_medico)=="" && $('#esCitaMultiple').val() != 1  &&  esCitaMultiple != 2  ){
			notify("Seleccione un medico","error");
		}else if(trim(fecha_usuario_desea_cita)==""&&get('citasRapidas').value!='S'   &&  esCitaMultiple != 2 ){
			notify("Seleccione una fecha para la cual el usuario desea la cita","error");
		}else if(trim(get('formaSolicitud').value)=="0"&&get('citasRapidas').value!='S'  &&  esCitaMultiple != 2 ){
			notify("Seleccione la Forma de Solicitud de cita","error");
		}else if(trim(fecha_cita)=="" &&  $('#esCitaMultiple').val() != 1  &&  esCitaMultiple != 2  ){
			notify("Seleccione una fecha para la cita","error");
		}else if(trim(hora)=="" && $('#esCitaMultiple').val() != 1   &&  esCitaMultiple != 2  ){
			notify("Seleccione una hora para la cita","error");
		}else if((trim(codigo_empresa)==""&&get('citasRapidas').value!='S')||(trim(codigo_empresa)==""&&trim(get('confirmarCitaEPS').value)=='S') ){
			notify("Seleccione una empresa","error");
		}else if((trim(contrato)==""&&get('citasRapidas').value!='S')||(trim(codigo_empresa)==""&&trim(get('confirmarCitaEPS').value)=='S')){
			notify("Seleccione un contrato","error");
		}/*else if(trim(estrato)==""){
			alert("Seleccione un nivel");
		}else if(trim(es_terapia)==""){
			alert("Seleccione si es terapia");
		}else if(trim(tipo_historia)==""){
			alert("Seleccione un tipo de historia");
		}*/else if(get('procedimiento_noqx').checked&&tipo_servicio==''){
			notify("Seleccione un tipo de servicio","error");
		}else if(get('procedimiento_noqx').checked&&id_stock==''&&nro_canastas_agregadas>0){
			notify("Seleccione un stock","error");
		}else if(get('procedimiento_noqx').checked&&nro_procedimientos_agregadas=='0'&& $('#esCitaMultiple').val() !=1 ){
			notify("Seleccione un procedimiento a realizar","error");
		}else if(get('procedimiento_noqx').checked&&get('id_contrato').value==''){
			notify("Seleccione un contrato","error");
		}else if(trim(asunto)=="" && $('#esCitaMultiple').val() !=1 && (($('#esCitaMultiple').val() == 2 && estado_cita != 'P') || $('#esCitaMultiple').val() == 0)){
			notify("Seleccione un asunto","error");
			
		}else if($("#esMontoPcte").val()==1&&$("#tipoMontoPcte").val()==2&&$("#actividad").val()==''&&$('#actividad option').length>1){
			notify("Seleccione una actividad","error");
		}else{
		//	ver_planes_pyp();
		//get('cod_medico').value='';

	//	
	 
	 var apartarC=true;

	if(trim(get('confirmarCitaEPS').value)=='S'){
		var widget = new ModalContentZeusSalud("Asignar Cita",
	"../../Vistas/citas/asignarCita.php?idCita=1&tip_ser=1&autoid="+autoid+"&codigo_empresa="+codigo_empresa+"&contrato="+contrato+"&asunto="+asunto, {}, "general", "", "750px", "2%",null,null,false,'idShowAsignarCita');
															
		widget.imgs_data = {	
							
							close_icon:{
							src:"../../ImagenesZeus/salir 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon close",
							_title:"Cerrar",
							id:"cerrar"
							},
							
							confirmar_icon:{
							src:"../../ImagenesZeus/confirmar citas 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon",
							_title:"Apartar Cita",
							id:"confirmar",
							fnAction:function(){
								
								var parametros="&autoid="+autoid+"&cod_medico="+cod_medico+"&fecha_cita="+fecha_cita+"&fecha_usuario_desea_cita="+fecha_usuario_desea_cita+"&hora="+hora+"&minutos="+minutos+"&primera_vez_control="+primera_vez_control+"&estado="+estado+"&asunto="+asunto+"&observacion="+observacion+"&codigo_empresa="+codigo_empresa+"&meridiano="+meridiano+"&id_stock="+id_stock+'&id_procediento_realizar='+id_procediento_realizar+"&tipo_servicio="+tipo_servicio+'&formaSolicitud='+get('formaSolicitud').value+"&vlrVariable="+$("#vlrPagoVariable").val()+"&tipo_afiliacion="+tipo_afiliacion;/*+"&estrato="+estrato+"&es_terapia="+es_terapia+"&tipo_historia="+tipo_historia*/
								//alert("../../controlador/citas/citas.php?operacion=apartar_cita"+parametros+paramPersonal);
								if(trim(get('id_cita').value)==''){
									
var tip_ser = get('tipo_servicio').value;
var validaCerrarHistoria=false;
var motivo_cerrar_historia="null";
var acompanante=get('acompanante').value;
var telefono_acompanante=get('telefono_acompanante').value;
var nro_autorizacion=get('nro_autorizacion').value;
	
try{
	motivo_cerrar_historia=get('motivo_cerrar_historia').value;
	validaCerrarHistoria=true;
}catch(e){
	validaCerrarHistoria=false;
}

var idCita=get('id_cita').value;
var estrato=get('id_estrato').value;

if($.trim(estrato)==''){
	notify("Seleccione un estrato","error");
	return false;
}

var es_terapia=get('es_terapia').value;
var tipo_historia=get('tipo_historia').value;
var via_ingreso=get('via_ingreso').value;
var causa_ext=get('causa_ext').value;
var ufuncional=$('#ufuncional').val();

if($.trim(ufuncional)==''){
	notify("Seleccione la unidad funcional","error");
	return false;
}

var id_cama=$('#id_cama').val();
var tipo_cama=$('#tipo_cama').val();
var tipo_admision=$('#tipo_admision').val();
var contador = 0;
var conf=true;


var subsidio=get('subsidio'+estrato).value;
var vlr_maximo=get('maximo'+estrato).value;
var contrato=get('id_contrato').value;	

var vlr_tipo_servicio='';
try{
	vlr_tipo_servicio=get('vlr_tipo_servicio').value;
}catch(e){}
	
if(trim(estrato)==""){
	alert("Seleccione estrato");
	contador =+ 1;	
	conf=false;
}

if(trim(es_terapia)==""){
	alert("Seleccione si es terapia");
	contador =+ 1;	
	conf=false;
}

if($.trim($("#pagos_en_citas").val())=='N'&&$.trim($("#vlrPagoVariable").val())==''&&$.trim($("#subsidio"+$("#id_estrato").val()).val())=='-1') {
	notify("Ingrese el valor variable.","error");
	contador =+ 1;	
	conf=false;
	return false;
}

if(trim(tip_ser)!=8){
if(trim(tipo_historia)==""){
	alert("seleccione un tipo de historia");
	conf=false;
	contador =+ 1;	
	}
	}else{
	tipo_historia == -1;
	
	}
	
if(validaCerrarHistoria&&trim(motivo_cerrar_historia)==""){
	alert("Ingrese el motivo por el cual se va a cerrar la historia anterior");
	contador =+ 1;	
	conf=false;
}



if(contador == 0){
//	idCita=438;
	get('id_cita_aplazar').value="0";
}	
/*		alert("../../controlador/citas/citas.php?operacion=confirmar_cita&idCita="+idCita+"&tipo_historia="+tipo_historia+"&tipo_admision="+tipo_admision+"&es_terapia="+es_terapia+"&estrato="+estrato+"&via_ingreso="+via_ingreso+"&causa_ext="+causa_ext+"&ufuncional="+ufuncional+"&autoid="+autoid);*/
if(conf){
//	deshabilitarToolBar(true);
	deshabilitarToolBar(true);
	mostrarCargando(true);
	var getReferencias='';
	try{
		getReferencias=JSON.stringify($(this).ZS_get_referencias());
	}catch(e){}
	
		respContratoMedFamiliar=function(){	
			var resp=JSON.parse(_ResquestAJAX);
			$("#habilitarMedicoAsignado").val(resp[0]["ManejaMedicoFamiliar"]);

			//alert(resp[0]["ManejaMedicoFamiliar"]);

			if($("#habilitarMedicoAsignado").val()=="S" && $.trim($("#MedicoAsignadoPcte").val())=='' && $("#TipoMedicoFiltro").val()=='General'){
					alertify.labels.ok = "SI";
					alertify.labels.cancel = "NO";
					alertify.confirm("Desea asignar este medico, como medico familiar del paciente y de sus familiares relacionados?", function (e) {
					if (e) {
						$("#MedicoAsignadoPcte").val($('#codigo_medico').val());
						
						ajax("../../controlador/citas/citas.php?operacion=apartar_cita_y_confirmar&relacionaMedicoPcte=1&tipo_historia="+tipo_historia+"&tipo_admision="+tipo_admision+"&es_terapia="+es_terapia+"&estrato="+estrato+"&via_ingreso="+via_ingreso+"&causa_ext="+causa_ext+"&ufuncional="+ufuncional+"&autoid="+autoid+"&motivo_cerrar_historia="+motivo_cerrar_historia+"&acompanante="+acompanante+"&telefono_acompanante="+telefono_acompanante+"&nro_autorizacion="+nro_autorizacion+'&subsidio='+subsidio+'&vlr_maximo='+vlr_maximo+'&contrato='+contrato+'&lugarAtencion='+lugarAtencion+'&vlr_tipo_servicio='+vlr_tipo_servicio+parametros+paramPersonal+'&id_cama='+id_cama+'&tipo_cama='+tipo_cama+"&JSON_Referencias="+getReferencias+"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion, "res_confirmacion",'habilitarToolBar(true);mostrarCargando(false);cargarProgramacion();','');	
					}else{
						ajax("../../controlador/citas/citas.php?operacion=apartar_cita_y_confirmar&relacionaMedicoPcte=0&tipo_historia="+tipo_historia+"&tipo_admision="+tipo_admision+"&es_terapia="+es_terapia+"&estrato="+estrato+"&via_ingreso="+via_ingreso+"&causa_ext="+causa_ext+"&ufuncional="+ufuncional+"&autoid="+autoid+"&motivo_cerrar_historia="+motivo_cerrar_historia+"&acompanante="+acompanante+"&telefono_acompanante="+telefono_acompanante+"&nro_autorizacion="+nro_autorizacion+'&subsidio='+subsidio+'&vlr_maximo='+vlr_maximo+'&contrato='+contrato+'&lugarAtencion='+lugarAtencion+'&vlr_tipo_servicio='+vlr_tipo_servicio+parametros+paramPersonal+'&id_cama='+id_cama+'&tipo_cama='+tipo_cama+"&JSON_Referencias="+getReferencias+"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion, "res_confirmacion",'habilitarToolBar(true);mostrarCargando(false);cargarProgramacion();','');	
					}
					}).set('labels', {ok:'SI', cancel:'NO'});



			}else{
				ajax("../../controlador/citas/citas.php?operacion=apartar_cita_y_confirmar&relacionaMedicoPcte=0&tipo_historia="+tipo_historia+
				"&tipo_admision="+tipo_admision+"&es_terapia="+es_terapia+"&estrato="+estrato+"&via_ingreso="+via_ingreso+
				"&causa_ext="+causa_ext+"&ufuncional="+ufuncional+"&autoid="+autoid+"&motivo_cerrar_historia="+motivo_cerrar_historia+
				"&acompanante="+acompanante+"&telefono_acompanante="+telefono_acompanante+"&nro_autorizacion="+nro_autorizacion+
				'&subsidio='+subsidio+'&vlr_maximo='+vlr_maximo+'&contrato='+contrato+'&lugarAtencion='+lugarAtencion+'&vlr_tipo_servicio='+
				vlr_tipo_servicio+parametros+paramPersonal+'&id_cama='+id_cama+'&tipo_cama='+tipo_cama+"&JSON_Referencias="+getReferencias+
				"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion+"&idCita="+idCita,
				 "res_confirmacion",'habilitarToolBar(true);mostrarCargando(false);cargarProgramacion();','');	
			}
		}
	ajaxPOST("../../controlador/citas/citas.php","","respContratoMedFamiliar()","","operacion=ContratoHabilitaMedFamiliar&id_contrato="+$("#id_contrato").val()+"&cod_medico="+cod_medico);
	
}
 						
									
}else{
//confirmar_cita();
}
								
							}
							}
					};
		widget.process();
		apartarC=false;
		
	}

//ajaxPOST("../../controlador/citas/citas.php?operacion=asunto_procedimiento&autoid="+autoid,'','cargarMultiplesCitas()', ''); 


	if(apartarC){
		deshabilitarToolBar();
		mostrarCargando(true);
		var getReferencias='';
		var esCitaMultiple = get('esCitaMultiple').value;
		try{
			getReferencias=JSON.stringify($(this).ZS_get_referencias());
		}catch(e){}
		var parametros="&autoid="+autoid+"&cod_medico="+cod_medico+"&fecha_cita="+fecha_cita+"&fecha_usuario_desea_cita="+fecha_usuario_desea_cita+"&hora="+hora+"&minutos="+minutos+"&primera_vez_control="+primera_vez_control+"&estado="+estado+"&asunto="+asunto+"&observacion="+observacion+"&codigo_empresa="+codigo_empresa+"&meridiano="+meridiano+"&id_stock="+id_stock+'&id_procediento_realizar='+id_procediento_realizar+"&lugarAtencion="+lugarAtencion+"&tipo_servicio="+tipo_servicio+'&formaSolicitud='+get('formaSolicitud').value+"&JSON_Referencias="+getReferencias+"&ActividadAsunto="+$("#actividad").val()+"&ActividadServicio="+$("#actividadCheck").val()+"&id_tipos_examen="+tipo_examen+"&nombre_tipo_examen="+nombre_tipo_de_examen;/*+"&estrato="+estrato+"&es_terapia="+es_terapia+"&tipo_historia="+tipo_historia*/
		parametros += "&esCitaMultiple="+esCitaMultiple;
			// RUTAS DE ATENCION
			var object = {};
			if($("input[name='id_riesgos_asign_procs[]']").length > 0){
				$("input[name='id_riesgos_asign_procs[]']").each(function(key, value){
					// console.log($(value).val());
					object[key] = $(value).val();
				});
			}else{
				object = null;
			}
			
			var object2 = {};
			if($("input[name='id_riesgos_asign_asuntos[]']").length > 0){
				$("input[name='id_riesgos_asign_asuntos[]']").each(function(key, value){
					object2[key] = $(value).val();
				});
			}else{
				object2 = null;
			}
			parametros+="&id_riesgos_asign_procs="+JSON.stringify(object)+"&id_riesgos_asignados_asuntos="+JSON.stringify(object2);
		respContratoMedFamiliar=function(){	
			var resp=JSON.parse(_ResquestAJAX);
			$("#habilitarMedicoAsignado").val(resp[0]["ManejaMedicoFamiliar"]);
			
			// CITA MULTIPLE
			if(esCitaMultiple == 1){
  
				
				if($("#habilitarMedicoAsignado").val()=="S" && $.trim($("#MedicoAsignadoPcte").val())=='' && $("#TipoMedicoFiltro").val()=='General'){
					alertify.labels.ok = "SI";
					alertify.labels.cancel = "NO";
					alertify.confirm("Desea asignar este medico, como medico familiar del paciente y de sus familiares relacionados?", function (e) {
					if (e) {
						$("#MedicoAsignadoPcte").val($('#codigo_medico').val());
						ajax("../../controlador/citas/citas.php?operacion=apartar_cita&relacionaMedicoPcte=1"+parametros+paramPersonal+"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion, "txtCitaMarcada",'cargarProgramacion();habilitarToolBar();mostrarCargando(false);comprobar_citamultiple();','');
						closeModal();
					}else{
						ajax("../../controlador/citas/citas.php?operacion=apartar_cita&relacionaMedicoPcte=0"+parametros+paramPersonal+"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion, "txtCitaMarcada",'cargarProgramacion();habilitarToolBar();mostrarCargando(false);alerta();comprobar_citamultiple();','');
						closeModal();
					}
					});
				}else{
					
					ajax("../../controlador/citas/citas.php?operacion=apartar_cita&relacionaMedicoPcte=0"+parametros+paramPersonal+"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion, "txtCitaMarcada",'cargarProgramacion();habilitarToolBar();mostrarCargando(false);alerta();comprobar_citamultiple();','');
					closeModal();
				}
				 // CITA NORMAL
			}else if(esCitaMultiple == 0){
				if($("#habilitarMedicoAsignado").val()=="S" && $.trim($("#MedicoAsignadoPcte").val())=='' && $("#TipoMedicoFiltro").val()=='General'){
					alertify.labels.ok = "SI";
					alertify.labels.cancel = "NO";
					alertify.confirm("Desea asignar este medico, como medico familiar del paciente y de sus familiares relacionados?", function (e) {
					if (e) {
						$("#MedicoAsignadoPcte").val($('#codigo_medico').val());
						ajax("../../controlador/citas/citas.php?operacion=apartar_cita&relacionaMedicoPcte=1"+parametros+paramPersonal+"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion, "txtCitaMarcada",'cargarProgramacion();habilitarToolBar();mostrarCargando(false);','');
					}else{
						ajax("../../controlador/citas/citas.php?operacion=apartar_cita&relacionaMedicoPcte=0"+parametros+paramPersonal+"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion, "txtCitaMarcada",'cargarProgramacion();habilitarToolBar();mostrarCargando(false);alerta();','');
					}
					});
				}else{
					ajax("../../controlador/citas/citas.php?operacion=apartar_cita&relacionaMedicoPcte=0"+parametros+paramPersonal+"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion, "txtCitaMarcada",'cargarProgramacion();habilitarToolBar();mostrarCargando(false);alerta();','');
				}
				// CITA AGRUPADA
			}else if( esCitaMultiple ==2){
				if($("#habilitarMedicoAsignado").val()=="S" && $.trim($("#MedicoAsignadoPcte").val())=='' && $("#TipoMedicoFiltro").val()=='General'){
					alertify.labels.ok = "SI";
					alertify.labels.cancel = "NO";
					alertify.confirm("Desea asignar este medico, como medico familiar del paciente y de sus familiares relacionados?", function (e) {
					if (e) {
						$("#MedicoAsignadoPcte").val($('#codigo_medico').val());
						ajax("../../controlador/citas/citas.php?operacion=apartar_cita&relacionaMedicoPcte=1"+parametros+paramPersonal+"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion, "txtCitaMarcada",'cargarProgramacion();habilitarToolBar();mostrarCargando(false);','');
					}else{
						ajax("../../controlador/citas/citas.php?operacion=apartar_cita&relacionaMedicoPcte=0"+parametros+paramPersonal+"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion, "txtCitaMarcada",'cargarProgramacion();habilitarToolBar();mostrarCargando(false);alerta();','');
					}
					});
				}else{
					ajax("../../controlador/citas/citas.php?operacion=apartar_cita&relacionaMedicoPcte=0"+parametros+paramPersonal+
					"&NumeroPoliza="+NumeroPoliza+"&autorizacion="+numero_autorizacion+"&idCita="+idCita+"&idprodmedidetalle="+idprodmedidetalle+"&estado_cita="+estado_cita, "txtCitaMarcada",
					'delete_cita_grupal("");cargarProgramacion();habilitarToolBar();mostrarCargando(false);alerta();','');
				}
			}
		}

		ajaxPOST("../../controlador/citas/citas.php","","respContratoMedFamiliar()","","operacion=ContratoHabilitaMedFamiliar&id_contrato="+$("#id_contrato").val()+"&cod_medico="+cod_medico);

		}
		
	}

	//	ajaxPOST("../../controlador/citas/citas.php","","alerta_asunto_procedimiento()","","operacion=alerta_asunto_procedimiento&idcita="+$("#id_cita").val()+"&asunto="+asunto);


	}
}


function comprobar_citamultiple(){
	var comprobar_citamultiple = $('#comprobar_citamultiple').val();
	deleteAllCitaMultiple();
	if(comprobar_citamultiple == 'S'){
		$("#tbodyy").html('');
		$("#esCitaMultiple").val(0);
		get('opc_proc_noqx').style.display="none";
		$("#stock").val("");	
		$("#id_stock").val("");
		$("#tipo_servicio").val("");
		$('input[name=canasta]').removeAttr('disabled');
		$('input[name=canasta]').removeClass('normDisabled');
		$('input[name=stock]').removeAttr('disabled');
		$('input[name=stock]').removeClass('normDisabled');
		cargaProcedimientosCita(0);
	}

}

function confirmar_citaEPS(){
	
	if(get('error_citas').value=='N'){
		get('id_cita').value=$("#error_citas").attr('idCita');
		confirmar_cita();
	}
}

function mostrarCargando(mostrar){
	if(mostrar){
		get('cargando').style.display="";
	}else{
		get('cargando').style.display="none";
	}
}

function showCancelarCitas(){
	var estado_cita=get('estado_cita').value;
	var tipo_cita = $("#tipo_cita").val();
	
	if( (estado_cita=='P'||estado_cita=='CC') && tipo_cita != 2){
		var widget = new ModalContentZeusSalud("Cancelar Cita",
		"../../controlador/citas/citas.php?operacion=show_cancelar_cita&estado_cita="+estado_cita, {}, "general", "", "400px", "10%",null,null,true);
	widget.process();	
	}else{	
		/*if(estado_cita=='CC'){
			notify("No se puede cancelar una cita confirmada","error");
		}else*/ 
		if(estado_cita=='A'){
			notify("No se puede cancelar una cita atendida","error");
		}else{
			notify("No se puede cancelar.","error");
		}
	}
}

function showCitaIncumplida(){
	var estado_cita=get('estado_cita').value;
	if(estado_cita=='P'){
		var widget = new ModalContentZeusSalud("Incumplir Cita",
		"../../controlador/citas/citas.php?operacion=show_incumplir_cita&estado_cita="+estado_cita, {}, "general", "", "400px", "10%",null,null,true);
	widget.process();	
	}else{	
		if(estado_cita=='CC'){
			notify("No se puede incumplir una cita confirmada","error");
		}else if(estado_cita=='A'){
			notify("No se puede incumplir una cita atendida","error");
		}else{
			notify("No se puede incumplir la cita.","error");
		}
	}
}

function closeMenuBar(){
	$("#menuBarFloat").hide(500,null,function(a){
		$("#menuFlotante").show(200);
	});
}

function showMenuBar(){
	$("#menuBarFloat").show(500,null,function(a){
		$("#menuFlotante").hide(200);
	});
}


function motivoCancelaCita(){
	if(get('motivo').value=='5'){
		get('div_motivoCancelaCita').style.display="";
	}else{
		get('div_motivoCancelaCita').style.display="none";	
	}
}

function motivoIncumpleCita(){
	if(get('motivo').value=='5'){
		get('div_motivoIncumpleCita').style.display="";
	}else{
		get('div_motivoIncumpleCita').style.display="none";	
	}
}


function anularCita(){
	var id_cita=get('id_cita').value;
	var estado_cita=get('estado_cita').value;
	if(estado_cita=='P'||estado_cita=='C'){
		ajax("../../controlador/citas/citas.php?operacion=anular_cita&id_cita="+id_cita+"&estado_cita="+estado_cita, "txtCitaMarcada",'cerrarDiv();cargarProgramacion();','');
	}else{	
		if(estado_cita=='CC'){
			notify("No se puede anular una cita confirmada","error");
		}else if(estado_cita=='A'){
			notify("No se puede anular una cita atendida","error");
		}else{
			notify("No se puede anular.","error");
		}
	}
}


function cerrarDiv(){
	$("div.zs-toolbar img.close").trigger("click");
}

function cancelarCita(){
	var id_cita=get('id_cita').value;
	var motivo=get('motivo').value;
	var motivoCancelaCita=get('motivoCancelaCita').value;
	var estado_cita=get('estado_cita').value;
	try{
		get('id_cita_aplazar').value="0";
	}catch(e){}
	if(trim(id_cita)!=""){		
//		alert(id_cita+' '+motivo);	

		/*get('cod_medico').value='';*/
		ajax("../../controlador/citas/citas.php?operacion=cancelar_cita&id_cita="+id_cita+"&motivo="+motivo+"&motivoCancelaCita="+motivoCancelaCita+"&estado_cita="+estado_cita, "txtCitaMarcada",'cerrarDiv();cargarProgramacion();','');
	
	}
}

function MarcarAtendida(){
	var id_cita=get('id_cita').value;
	var estado_cita=get('estado_cita').value;

	if($.trim(id_cita)==''||$.trim(estado_cita)!='CC'){
		notify("Seleccione una cita confirmada","error");
	}else{
		if(confirm("Seguro desea marcar la cita como atendida?")){
			ajaxPOST("../../controlador/citas/citas.php","","msgAtendida()","","operacion=marcarAtendida&idCita="+id_cita+"&estadoCita="+estado_cita);
		}
	}
}

function msgAtendida(){
	resp=JSON.parse(_ResquestAJAX)
	if(resp["tipo"]=='OK'){
		notify(resp["msg"]);
		cargarProgramacion('');
	}else{
		notify(resp["msg"],"error");	
	}
	
}

function incumplirCita(){
	var id_cita=get('id_cita').value;
	var motivo=get('motivo').value;
	var motivoCancelaCita=get('motivoIncumpleCita').value;
	var estado_cita=get('estado_cita').value;
	try{
		get('id_cita_aplazar').value="0";
	}catch(e){}
	if(trim(id_cita)!=""){		
//		alert(id_cita+' '+motivo);	

		get('cod_medico').value='';
		ajax("../../controlador/citas/citas.php?operacion=incumplir_cita&id_cita="+id_cita+"&motivo="+motivo+"&motivoCancelaCita="+motivoCancelaCita+"&estado_cita="+estado_cita, "txtCitaMarcada",'cerrarDiv();cargarProgramacion();','');
	
	}
}

function alerta(){
	var cedula=$('#ident').val();
	' ../../Vistas/citas/confirmarCita.php'
	$.ajax({
		url : '../../Vistas/Pyp/CtrlPyp.php?operacion=alertas',
		data : { 
			 cedula : cedula
		 },
		type : 'post',
 
		success : function(json) {
			
			var  aux =0;

			if( json.success.length >0 ){
				var aux = 0;
				var respuesta = 0;
			 for (let i = 0; i <  json.success.length; i++) {
				aux =  json.success[i]['frecuencia'] -  json.success[i]['n_asistenci'];
				if (aux > 0) {
					respuesta = aux;
				}
			 }
			
			 if ( respuesta >0) {
					var widget = new ModalContentZeusSalud("Alertas pyp",
					"../../Vistas/citas/Alertas_pyp.php?cedula="+cedula, {}, "general", "", "470px", "2%",null,null,false,'idShowModalConfirmarCita',setContratoConfirmacion);

					widget.imgs_data = {		
						close_icon:{
							src:"../../ImagenesZeus/salir 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon close",
							_title:"Cerrar",
							id:"cerrar"
						}
						
	
					};
					widget.process()
				}

 			}else{

 			}
			
		},
		error : function(xhr, status) {
				alert('Disculpe, existió un problema');
		}
	});

	


}


function confirmarall(asistencia,llamado){

	if($("#confirmaDatos").val()=='S'){
		if(!confirm('Los datos basicos del paciente se encuentran actualizados?')){
			return false;
		}
	}

	
   	var tipo_id=get('tipo').value;
	var num_id=get('ident').value;
	var nroCarnet=(get('nroCarnet').value);
	var pnombre=(get('pnombre').value);
	var snombre=(get('snombre').value);
	var papellido=(get('papellido').value);
	var sapellido=(get('sapellido').value);
	var sexo=get('sexo').value;
	var fecha_naci=get('fechanaci').value;
	var tipo_sangre=replaceTipoSangre(get('tipo_sangre').value);
	var tipo_afiliacion=$('#tipo_afiliacion').val();
	var departamento=get('departamento').value;
	var codigo_departamento=get('codigo_departamento').value;
	var codigo_ciudad=get('codigo_ciudad').value;
	var ciudad=get('ciudad').value;
	var codigo_barrio=get('codigo_barrio').value;
	var barrio=get('barrio').value;
	var direccion=(get('direccion').value);
	var telefono=(get('telefono').value);
	var autoid=get('autoid').value;
	var codigo_empresa=get('codigo_empresa').value;
	var contrato=get('id_contrato').value;

	var estado_cita=get('estado_cita').value;
	var confirmada_temp_hiden = $('#confirmada_temp_hiden').val();
 
	if(estado_cita!='P' && confirmada_temp_hiden!=1  ){
		notify("No se puede confirmar esta cita.","error");
		return false;
	} 

	
	var idCita=get('id_cita').value;
	var tip_ser = get('tipo_servicio').value;
	var estado_cita=get('estado_cita').value;
	if($.trim(idCita)==''){
		
		notify("No ha seleccionado una cita.","error");
		return false;
	}

	if(trim(tipo_id)==""){
		notify("Seleccione un tipo de documento","error");
		return false;
	}else if(trim(num_id)==""){
		notify("Ingrese numero de documento","error");
		seleccionar('ident');
		return false;
	}else if(trim(pnombre)==""){
		notify("Ingrese primer nombre del paciente","error");
		seleccionar('pnombre');
		return false;
	}else if(trim(papellido)==""){
		notify("Ingrese primer apellido del paciente","error");
		seleccionar('papellido');
		return false;
	}else if(trim(sexo)==""){
		notify("Seleccione el sexo","error");
		return false;
	}else if(trim(fecha_naci)==""){
		notify("Seleccione la fecha de nacimiento del paciente","error");
		seleccionar('fechanaci');
		return false;
	}else if(trim(tipo_afiliacion)==""){
		notify("Seleccione el tipo de afiliacion","error");
		seleccionar('tipo_afiliacion');
		return false;
	}else if(trim(departamento)==""){
		notify("Seleccione el departamento","error");
		return false;
	}else if(trim(ciudad)==""){
		notify("Seleccione la ciudad","error");
		return false;
	}else if(trim(barrio)==""){
		notify("Seleccione el barrio","error");
		return false;
	}else if(trim(codigo_empresa)==""){
		notify("Seleccione una empresa","error");
		return false;
	}

	if($.trim($("#confirmarModificacionCitas").val())=='S'&&llamado!=1){
		validDatosCitas(idCita);
		_callMetodo='confirmarCitas(null,1)';
		 
	}else{
		
		
		//validDatosCitas(idCita);
		if($.trim($("#tipo_afiliacion").val())==''){
			notify("No ha seleccionado el tipo de afiliacion del paciente.","error");	
			return false;
		}
		
		if(tip_ser == 8){
			$('#tipo_historia').attr('disabled', 'disabled');
			}
		if(estado_cita!='P' ){
		 
			if( confirmada_temp_hiden==1  && estado_cita != 'C'){
				var widget = new ModalContentZeusSalud("Confirmar Cita",
					"../../Vistas/citas/confirmarCita_multiple.php?idCita="+idCita+"&tip_ser="+tip_ser+'&asistencia='+asistencia, {}, "general", "", "750px", "2%",null,null,false,'idShowModalConfirmarCita',setContratoConfirmacion);
					
					
					widget.imgs_data = {	
										
										close_icon:{
										src:"../../ImagenesZeus/salir 01.png", 
										_w:32, 
										_h:32, 
										_class:"icon close",
										_title:"Cerrar",
										id:"cerrar"
										},
										
										confirmar_icon:{
										src:"../../ImagenesZeus/confirmar citas 01.png", 
										_w:32, 
										_h:32, 
										_class:"icon",
										_title:"Confirmar Cita",
										id:"confirmar",
										fnAction:function(){
											confirmar_cita_m();	
										}
										}
								 };
					widget.process();
					return false;
			}else{
				notify("No se puede confirmar esta cita.","error");	
			}
	//		return;
		}else{
	
			/*
		ajax("../../controlador/citas/citas.php?operacion=confirmar_cita&id_cita="+idCita, "txtCitaMarcada",'cargarProgramacion();','');*/
			
		/*** Cita Normal ***/
		if(get('citasRapidas').value!="S" ){

				var widget = new ModalContentZeusSalud("Confirmar Cita",
				"../../Vistas/citas/confirmarCita.php?idCita="+idCita+"&tip_ser="+tip_ser+'&asistencia='+asistencia, {}, 
				"general", "", "750px", "2%",null,null,false,'idShowModalConfirmarCita',setContratoConfirmacion);
				
				
				widget.imgs_data = {	
									
					close_icon:{
					src:"../../ImagenesZeus/salir 01.png", 
					_w:32, 
					_h:32, 
					_class:"icon close",
					_title:"Cerrar",
					id:"cerrar"
					},
					
					confirmar_icon:{
					src:"../../ImagenesZeus/confirmar citas 01.png", 
					_w:32, 
					_h:32, 
					_class:"icon",
					_title:"Confirmar Cita",
					id:"confirmar",
					fnAction:function(){
						confirmar_cita();	
					}
					}
					};
				widget.process();	
 
			}else{
			
				confirmarCitasRapidas(asistencia);
			}
		}
	}
}

function confirmarCitas(asistencia,llamado){
	var idCita=get('id_cita').value;
	if($.trim(idCita)==''){
		
		notify("No ha seleccionado una cita.","error");
		return false;
	}
	var tip_ser = get('tipo_servicio').value;
	var estado_cita=get('estado_cita').value;
	 
	var estado_cita = $("#estado_cita").val();
	if( estado_cita == 'CC'){
		return 0;
	}

 
	if($("#confirmada_agrupada").val() ==1 ){
		var widget = new ModalContentZeusSalud("Confirmar Cita",
		"../../Vistas/citas/confirmarCita_grupal.php?idCita="+idCita+"&tip_ser="+tip_ser+'&asistencia='+asistencia, {},
		 "general", "", "750px", "2%",null,null,false,'idShowModalConfirmarCita',setContratoConfirmacion);
					
		
		widget.imgs_data = {	
							
			close_icon:{
			src:"../../ImagenesZeus/salir 01.png", 
			_w:32, 
			_h:32, 
			_class:"icon close",
			_title:"Cerrar",
			id:"cerrar"
			},
			
			confirmar_icon:{
			src:"../../ImagenesZeus/confirmar citas 01.png", 
			_w:32, 
			_h:32, 
			_class:"icon",
			_title:"Confirmar Cita",
			id:"confirmar",
			fnAction:function(){
				confirmar_cita_grupal();	
			}
				}
			};
			widget.process();
	}else{
		confirmarall(asistencia,llamado);
	}
}

/* Funcion para confirmar las citas Agrupadas o Grupales  */
function confirmar_cita_grupal(){

	var error = false; 
	var idCita=get('id_cita').value;
	var autoid=get('autoid').value;
	
	var es_terapia=get('es_terapia').value;
	var tipo_historia=get('tipo_historia').value;
	var via_ingreso=get('via_ingreso').value;
	var causa_ext=get('causa_ext').value;
	var ufuncional=get('ufuncional').value;
	var tipo_admision=get('tipo_admision').value;
	
	if(trim(es_terapia)==""){
		notify("Seleccione si es terapia","error");
		error = true;
	}

	if(trim(tipo_historia)==""){
		notify("Seleccione el tipo de historia","error");
		error = true;
	}

	var estratos = new Array();
	$("input[class=estrato]:checked").each(function(){	
		if( estratos != "" ){
			estratos.push($(this).val()); 
		} 
    });

	var paciente_asociado = new Array();
	var pacientes_estrato = new Array();
	$("input[class=check_paciente_asociado]:checked").each(function(){
		paciente = $(this).val();
		paciente_asociado.push(paciente); 
		
		estratopaciente= $("#id_estrato_"+paciente).val();
		 
		if( estratopaciente != "" ){
			pacientes_estrato.push({"autoid": paciente,"estrato": estratopaciente });
		}else{
			notify("Seleccione los estratos","error");
			error = true;
		}
    });

	if( paciente_asociado.length >0 && error == false ){
		ajaxPOST("../../controlador/citas/citas.php","res_confirmacion_grupal","finalizarConfirmaCita();habilitarToolBar(true);mensaje_confirmar_cita_grupal();",""
		,"operacion=confirmar_cita&idCita="+idCita+"&tipo_admision="+tipo_admision+
		"&via_ingreso="+via_ingreso+"&causa_ext="+causa_ext+"&ufuncional="+ufuncional+"&autoid="+autoid+
		'&contrato='+contrato+'&asistencia='+get('asistencia').value+"&cita_grupal=1"+"&tipo_historia="+tipo_historia+"&es_terapia="+es_terapia+
		"&paciente_asociado="+JSON.stringify(paciente_asociado)+"&pacientes_estrato="+JSON.stringify(pacientes_estrato));
		//,"operacion=confirmar_cita&cita_grupal=1&idCita="+idCita+"&paciente_asociado="+JSON.stringify(paciente_asociado));
	}else{
		notify("Complete los campos obligatorios","error");	
	}
	
}

function mensaje_confirmar_cita_grupal(){
	var error_conf_cita = $("#error_conf_cita").val();
 
	if( error_conf_cita == "ok"  && error_conf_cita != undefined ){
		// Imprime el mensaje
		$("#msg_grupal").html( "<table cellpadding='0' cellspacing='0' width='100%'>"
		+"<tbody><tr height='30px;'>"
			+"<td align='center' bgcolor='#B5D699' style='border:solid; border-color:#74B33E; color:#21432B'>Se ha confirmado la cita correctamente " 
			+"<input type='hidden' name='citaconf' id='citaconf' value='ok'>"
			+"<input type='hidden' name='estudio_cita' id='estudio_cita' value='269194'></td>"	
		+"</tr>"
	  +"</tbody></table>");
		// Cierra el modal.
	  $("div.zs-toolbar img.close").trigger("click");	
		
	} 
}
 

function setContratoConfirmacion(){
	$(".seleccionado").find('#NameEmpresa').html($("#empresa").val()); 
}

function confirmarCitasRapidas(asistencia){

		var tipo_id=get('tipo').value;
		var num_id=get('ident').value;
		var pnombre=(get('pnombre').value);
		var snombre=(get('snombre').value);
		var papellido=(get('papellido').value);
		var sapellido=(get('sapellido').value);
		var sexo=get('sexo').value;
		var fecha_naci=get('fechanaci').value;
		var tipo_sangre=replaceTipoSangre(get('tipo_sangre').value);
		var tipo_afiliacion=$('#tipo_afiliacion').val();
		var departamento=get('departamento').value;
		var codigo_departamento=get('codigo_departamento').value;
		var codigo_ciudad=get('codigo_ciudad').value;
		var ciudad=get('ciudad').value;
		var codigo_barrio=get('codigo_barrio').value;
		var barrio=get('barrio').value;
		var direccion=(get('direccion').value);
		var telefono=(get('telefono').value);
		var autoid=get('autoid').value;
		var codigo_empresa=get('codigo_empresa').value;
		var contrato=get('id_contrato').value;
		var email=get('email').value;
		var id_stock=get('id_stock').value;
		var tipo_servicio=get('tipo_servicio').value;
		var asunto=get('nomservicio').value;
		var primera_vez_control=get('primera_vez_control').value;
		
		var responsable_nombre=(get('responsable_nombre').value);
		var responsable_apellido=(get('responsable_apellido').value);
		var responsable_parentesco=(get('responsable_parentesco').value);
		var responsable_direccion=(get('responsable_direccion').value);
		var responsable_telefono=(get('responsable_telefono').value);
		
		
		var paramPersonal='&tipo_id='+tipo_id+'&num_id='+num_id+'&pnombre='+pnombre+'&snombre='+snombre+'&papellido='+papellido+'&sapellido='+sapellido+'&sexo='+sexo+'&fecha_naci='+fecha_naci+'&direccion='+direccion+'&telefono='+telefono+'&tipo_sangre='+tipo_sangre+'&codigo_departamento='+codigo_departamento+'&codigo_ciudad='+codigo_ciudad+'&codigo_barrio='+codigo_barrio+"&codigo_empresa="+codigo_empresa+"&autoid="+autoid+"&contrato="+contrato+"&id_stock="+id_stock+"&tipo_servicio="+tipo_servicio+"&asunto="+asunto+"&primera_vez_control="+primera_vez_control+"&email="+email+'&responsable_nombre='+responsable_nombre+'&responsable_apellido='+responsable_apellido+'&responsable_parentesco='+responsable_parentesco+'&responsable_direccion='+responsable_direccion+'&responsable_telefono='+responsable_telefono+"&tipo_afiliacion="+tipo_afiliacion;
		
		var cod_medico=get('cod_medico').value;
		var fecha_cita=get('fecha_cita').value;
		var hora=get('hora').value;
		var minutos=get('minutos').value;
		var meridiano=get('meridiano').value;
		
		
		var estado='P';
		var asunto=get('nomservicio').value;
		var observacion=(get('obs').value);
		var id_procediento_realizar=get('id_procediento_realizar').value;
		
		var recordar_cita='0';
		
		if(get('recordar_cita').checked){
			recordar_cita='1';
		}
		
		
		/*alert(autoid+" | "+cod_medico+" | "+fecha_cita+" | "+hora+":"+minutos+" "+meridiano+" | "+estado+" | "+asunto+" | "+observacion+" | "+codigo_empresa);*/
		if(trim(tipo_id)==""){
			notify("Seleccione un tipo de documento","error");		
		}else if(trim(num_id)==""){
			notify("Ingrese numero de documento","error");
			seleccionar('ident');
		}else if(trim(pnombre)==""){
			notify("Ingrese primer nombre del paciente","error");
			seleccionar('pnombre');
		}else if(trim(papellido)==""){
			notify("Ingrese primer apellido del paciente","error");
			seleccionar('papellido');
		}else if(trim(sexo)==""){
			notify("Seleccione el sexo","error");
		}else if(trim(fecha_naci)==""){
			notify("Seleccione la fecha de nacimiento del paciente","error");
			seleccionar('fechanaci');
		}else if(trim(departamento)==""){
			notify("Seleccione el departamento","error");
		}else if(trim(ciudad)==""){
			notify("Seleccione la ciudad","error");
		}else if(trim(barrio)==""){
			notify("Seleccione el barrio","error");
		}else if(get('validar_telefono').value=='S'&&trim(get('telefono').value)==''){
			notify("Ingrese un numero telefonico","error");
		}else if(trim(codigo_empresa)==""){
			notify("Seleccione una empresa","error");
		}else if(trim(contrato)==""){
			notify("Seleccione un contrato","error");
		}else if(get('validar_correo').value=='S'&&trim(get('email').value)==''){
			notify("Ingrese un correo electronico","error");
		}else{
			var idCita=get('id_cita').value;
			
			RespCitasRapidas(asistencia);
			/*ajaxPOST("../../controlador/citas/citas.php", "res_paciente",'RespCitasRapidas("'+asistencia+'")','',"operacion=guardarPaciente"+paramPersonal+"&idCita="+idCita+'&recordar_cita='+recordar_cita);*/
		
		}
}


function sesioncheckEsCitaMultiple(id, combo){ 
	
	var es_cita_multiple = $("#esCitaMultiple").val();
	
	if(es_cita_multiple == 1){
		
		if($('#checkEsCitaMultiple_'+id).attr("checked")){
			ajaxPOST("../../controlador/citas/citas.php", '','setearCitas()','',"operacion=addCitaMultiple&idcita="+id);
			 
		}else{
			ajaxPOST("../../controlador/citas/citas.php", '','setearCitas()','',"operacion=deleteCitaMultiple&idcita="+id);		
		}
	}
	
}

function deleteCitaMultiple(id){
	ajaxPOST("../../controlador/citas/citas.php", '','setearCitas()','',"operacion=deleteCitaMultiple&idcita="+id);		
	$('#checkEsCitaMultiple_'+id).attr('checked',false );
}

function deleteAllCitaMultiple(){
	ajaxPOST("../../controlador/citas/citas.php", '','setearCitas()','',"operacion=delete_all_agrupar_cita_multiple");		
	$('.checkEsCitaMultiple').attr('checked',false );
}

function setearCitas(){	
	var resp=JSON.parse(_ResquestAJAX);
	 
	var html ="";
	if( resp.citas.length > 0){
		for (let index = 0; index < resp.citas.length; index++) {
			var contenido = resp.citas[index];
			
			html +="<tr><td align='center'>"+ contenido[0].fecha+" </td> <td align='center'> "+contenido[0].nombre+" </td><td align='center'> <a href='javascript:deleteCitaMultiple("+contenido[0].id+");'><img src='../../Imagenes/Eliminar.png' border='0'></a> </td></tr>" ;

		}
	}
	$("#tbodyy").html(html);
}


function eliminarcitamultiple(id){
	
	ajaxPOST("../../controlador/citas/citas.php", '','setearCitas()','',"operacion=addCitaMultiple&idcita="+id);
}

function RespCitasRapidas(asistencia){
	//if(get('resp_msg').value=='ok'){
		var idCita=get('id_cita').value;
		var tip_ser = get('tipo_servicio').value;
		var estado_cita=get('estado_cita').value;
		
		var confirmada_temp_hiden = $("#confirmada_temp_hiden").val(); 
		if(tip_ser == 8){
			$('#tipo_historia').attr('disabled', 'disabled');
			}
		if(estado_cita!='P' && confirmada_temp_hiden!=1 ){
			 
			notify("No se puede confirmar esta cita.","error");	
	//		return;
		}else{
			CargaValidarCitas = function () {
				res = JSON.parse(_ResquestAJAX); 
				//if(res.esPrimeraConfirmacion){
					var widget = new ModalContentZeusSalud("Confirmar Cita",
					"../../Vistas/citas/confirmarCita.php?idCita="+idCita+"&tip_ser="+tip_ser+'&asistencia='+asistencia, {}, "general", "", "750px", "2%",null,null);
					
					widget.imgs_data = {
						close_icon:{
							src:"../../ImagenesZeus/salir 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon close",
							_title:"Cerrar",
							id:"cerrar"
						},				
						confirmar_icon:{
							src:"../../ImagenesZeus/confirmar citas 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon",
							_title:"Confirmar Cita",
							id:"confirmar",
							fnAction:function(){
								confirmar_cita();	
							}
						}
					};
					widget.process();
				// }else{
				// 	alert("Ya existe multiple cita asociada confirmada.");
				// 	// ajaxPOST("../../controlador/citas/citas.php", "res_confirmacion","finalizarConfirmaCita();habilitarToolBar(true);",'',"operacion=confirmar_cita&idCita="+idCita+"&tipo_historia="+tipo_historia+"&tipo_admision="+tipo_admision+"&es_terapia="+es_terapia+"&estrato="+estrato+"&via_ingreso="+via_ingreso+"&causa_ext="+causa_ext+"&ufuncional="+ufuncional+"&autoid="+autoid+"&motivo_cerrar_historia="+motivo_cerrar_historia+"&acompanante="+acompanante+"&telefono_acompanante="+telefono_acompanante+"&nro_autorizacion="+nro_autorizacion+'&subsidio='+subsidio+'&vlr_maximo='+vlr_maximo+'&contrato='+contrato+'&vlr_tipo_servicio='+vlr_tipo_servicio+cadTercero+'&asistencia='+get('asistencia').value+'&vlrVariable='+$("#vlrPagoVariable").val()+'&id_cama='+id_cama+'&tipo_cama='+tipo_cama);
				// }				
			}
			ajaxPOST("../../controlador/citas/citas.php?operacion=validarMultiplesCitas&idCita="+idCita+"&tip_ser="+tip_ser+'&asistencia='+asistencia,'','CargaValidarCitas()', ''); 		
		}
 // 	}	
}

function confirmar_cita_m(){
	
	var estado_cita = $("#estado_cita").val();
	var confirmada_temp_hiden = $("#confirmada_temp_hiden").val();
	var idCita=get('id_cita').value;
 	if( estado_cita == 'CC' &&  confirmada_temp_hiden == 1 && idCita !="" ){
		deshabilitarToolBar(true);
		ajaxPOST("../../controlador/citas/citas.php", "res_confirmacion","mensaje_multiple();habilitarToolBar(true);",'',"operacion=confirmar_cita_multiple&idCita="+idCita);
	}
 	
}

function mensaje_multiple(){
	var resp=_ResquestAJAX;
	
	cargarProgramacion('');
}

// confirma cita normal
function confirmar_cita(){
	var tip_ser = get('tipo_servicio').value;
	var validaCerrarHistoria=false;
	var motivo_cerrar_historia="null";
	var acompanante=get('acompanante').value;
	var telefono_acompanante=get('telefono_acompanante').value;
	var nro_autorizacion=get('nro_autorizacion').value;
	var es_terapia=get('es_terapia').value;
		
	try{
		motivo_cerrar_historia=get('motivo_cerrar_historia').value;
		validaCerrarHistoria=true;
	}catch(e){
		validaCerrarHistoria=false;
	}
	var idprogramMultiplesCitas = "";
	$(".check_multiples").each(function(){
		if(this.value != "" && $(this).attr('checked')){
			idprogramMultiplesCitas += this.value +",";
		}
		
	});

	var idCita=get('id_cita').value;
	 
	var autoid=get('autoid').value;
	var estrato=get('id_estrato').value;
	var es_terapia=get('es_terapia').value;
	var tipo_historia=get('tipo_historia').value;
	var via_ingreso=get('via_ingreso').value;
	var causa_ext=get('causa_ext').value;
	var ufuncional=get('ufuncional').value;
	var id_cama=get('id_cama').value;
	var tipo_cama=get('tipo_cama').value;
	var tipo_admision=get('tipo_admision').value;
	var contador = 0;
	var conf=true;
	
	try{
		var subsidio=get('subsidio'+estrato).value;
		var vlr_maximo=get('maximo'+estrato).value;
	}catch(e){}
	var contrato=get('id_contrato').value;	
	
	var vlr_tipo_servicio='';
	try{
		vlr_tipo_servicio=get('vlr_tipo_servicio').value;
	}catch(e){}
	
	if(trim(estrato)==""){
		notify("Seleccione estrato","error");
		contador =+ 1;	
		conf=false;
		return false;
	}
	
	if($.trim($("#pagos_en_citas").val())=='N'&&$.trim($("#vlrPagoVariable").val())==''&&$.trim($("#subsidio"+$("#id_estrato").val()).val())=='-1') {
		notify("Ingrese el valor variable.","error");
		contador =+ 1;	
		conf=false;
		return false;
	}
	
	if(trim(es_terapia)==""){
		notify("Seleccione si es terapia","error");
		contador =+ 1;	
		conf=false;
		return false;
	}
	
	if(trim(tip_ser)!=8){
	  if(trim(tipo_historia)==""){
		notify("seleccione un tipo de historia","error");
		conf=false;
		contador =+ 1;	
		return false;
	  }
	}else{
		tipo_historia == -1;	
	}
	
	var cadTercero='';
	//alert($.trim($("#GenerarFacturasCopagosCM").val()));
	if(trim(get('tipo_contrato_confirmar').value)=='5' || $.trim($("#GenerarFacturasCopagosCM").val())=='S'){
		if(trim($('#res_tipo_identificacion').val())==''){
			notify("Seleccione el tipo de identificacion al tercero","error");
			contador =+ 1;	
			conf=false;
			return false;
		}else if(trim(get('res_identificacion').value)==''){
			notify("Ingrese numero de identificacion al tercero","error");
			contador =+ 1;	
			conf=false;
			return false;
		}else if(trim(get('res_primer_nom').value)==''){
			notify("Ingrese primer nombre al tercero","error");
			contador =+ 1;	
			conf=false;
			return false;
		}else if(trim(get('res_primer_ape').value)==''){
			notify("Ingrese primer apellido del tercero","error");
			contador =+ 1;	
			conf=false;
			return false;
		}else if(trim(get('res_ciudad').value)==''){
			notify("Ingrese la ciudad del tercero","error");
			contador =+ 1;	
			conf=false;
			return false;
		}else if(trim(get('res_direccion').value)==''){
			notify("Ingrese direccion al tercero","error");
			contador =+ 1;	
			conf=false;
			return false;
		}else if(trim(get('res_telefono').value)==''){
			notify("Ingrese numero de telefono al tercero","error");
			contador =+ 1;	
			conf=false;
			return false;
		}
		cadTercero+='&res_tipo_identificacion='+get('res_tipo_identificacion').value;
		cadTercero+='&res_identificacion='+get('res_identificacion').value;
		cadTercero+='&res_primer_nom='+get('res_primer_nom').value;
		cadTercero+='&res_segundo_nom='+get('res_segundo_nom').value;
		cadTercero+='&res_primer_ape='+get('res_primer_ape').value;
		cadTercero+='&res_segundo_ape='+get('res_segundo_ape').value;
		cadTercero+='&res_ciudad='+get('res_ciudad').value;
		cadTercero+='&res_direccion='+get('res_direccion').value;
		cadTercero+='&res_telefono='+get('res_telefono').value;
	}
		
	if(validaCerrarHistoria&&trim(motivo_cerrar_historia)==""){
		notify("Ingrese el motivo por el cual se va a cerrar la historia anterior","error");
		contador =+ 1;	
		conf=false;
		return false;
	}
	
	
	
	if(contador == 0){
	//	idCita=438;
		get('id_cita_aplazar').value="0";
	}	
/*		alert("../../controlador/citas/citas.php?operacion=confirmar_cita&idCita="+idCita+"&tipo_historia="+tipo_historia+"&tipo_admision="+tipo_admision+"&es_terapia="+es_terapia+"&estrato="+estrato+"&via_ingreso="+via_ingreso+"&causa_ext="+causa_ext+"&ufuncional="+ufuncional+"&autoid="+autoid);*/
	if(conf){
		deshabilitarToolBar(true);
		ajaxPOST("../../controlador/citas/citas.php", "res_confirmacion","finalizarConfirmaCita();habilitarToolBar(true);",'',"operacion=confirmar_cita&idCita="+idCita+"&citas_m="+idprogramMultiplesCitas+"&tipo_historia="+tipo_historia+"&tipo_admision="+tipo_admision+"&es_terapia="+es_terapia+"&estrato="+estrato+"&via_ingreso="+via_ingreso+"&causa_ext="+causa_ext+"&ufuncional="+ufuncional+"&autoid="+autoid+"&motivo_cerrar_historia="+motivo_cerrar_historia+"&acompanante="+acompanante+"&telefono_acompanante="+telefono_acompanante+"&nro_autorizacion="+nro_autorizacion+'&subsidio='+subsidio+'&vlr_maximo='+vlr_maximo+'&contrato='+contrato+'&vlr_tipo_servicio='+vlr_tipo_servicio+cadTercero+'&asistencia='+get('asistencia').value+'&vlrVariable='+$("#vlrPagoVariable").val()+'&id_cama='+id_cama+'&tipo_cama='+tipo_cama);
	}
//		LLAMAR METODO cargarProgramacion('') DESPUES DE CONFIRMAR_CITA
	
}

function finalizarConfirmaCita(){
	var tipo_cita = $("#tipo_cita").val();
	try{
		if(get('citaconf').value=="ok"){			
			if($.trim($("#impresionDeAdmisionConfirmarCitas").val())=='S' && tipo_cita != 2 ){
				invocarReporte("reporte_admision.php?estudio="+get('estudio_cita').value);
			}
			
			recargarPagos(get('estudio_cita').value);
			cargarProgramacion('');	
			if(get('asistencia').value=='Asistencia'){
				irFacturacion();
			}
		}
	}catch(e){
		//alert(e);
	}
}

function irFacturacion(){
	var ident=get('ident').value;
	var tipo=get('tipo').value;
	abrirOpcion('../Facturacion/CtrlFacturarCE.php?operacion=abrir&ident='+ident+"&tipo="+tipo+'&estudio='+get('estudio_cita').value, 'f');	
}

function recargarPagos(estudio){
	ajaxPOST("../../controlador/citas/citas.php", 'contenidoPago','','',"operacion=recargarPagos&estudio="+estudio);		
}

function cerrarHistoria(){
	var idCita=get('id_cita').value;
	var tipo_historia=get('tipo_historia').value;
	var tipo_admision=get('tipo_admision').value;
	var es_terapia=get('es_terapia').value;
	var estrato=get('id_estrato').value;
	var via_ingreso=get('via_ingreso').value;
	var causa_ext=get('causa_ext').value;
	var ufuncional=get('ufuncional').value;
	var autoid=get('autoid').value;
	
	
	var parametos="?idCita="+idCita+"&tipo_historia="+tipo_historia+"&tipo_admision="+tipo_admision+"&es_terapia="+es_terapia+"&estrato="+estrato+"&via_ingreso="+via_ingreso+"&causa_ext="+causa_ext+"&ufuncional="+ufuncional+"&autoid="+autoid;
	
	ajax("../../Vistas/citas/cerrarHistoria.php"+parametos, "contenidoConfirmarCitas",'','');
	get('res_confirmacion').innerHTML="";
}


function cargarAutocompletarEstrato(){

$("#estrato").autocomplete({
						urlOrData:"../../controlador/citas/citas.php?",
						params:function(){
							return "&id_empresa="+get('codigo_empresa').value+"&operacion=cargarEstratos";
						},
						
						ids:"id_estrato#"			
					});	
					
					
}


function confirmarCitas_x() {
	//alert("aqui");
    var destino = "";
    var medico = get('medico').value;
    var codigoMedico = get('cod_medico').value;
    var servicio = get('nomservicio').value;
	var idCita=get('id_cita').value;
	var tipo=get('tipo').value;
    var ident = get('ident').value;
	
	if(trim(idCita)==""){		
        alert("Operacion invalida. No ha seleccionado una cita.");
        return;
    }
	
	
    destino = "../Facturacion/CtrlAdmitirCE.php?tipo=" + tipo
        + "&ident=" + ident + "&medico=" + medico
        + "&codigo_medico=" + codigoMedico + "&idcita=" + idCita+ "&codigo_servicio=" + servicio;

	//alert(destino);
    reDirect(destino);
}


function ConsultarImagenAplicacionExterna(autoid,div,width="100",height="75",otrosattr="",html=true) {
	procesador.procesar = function (response){
		if(response.foto != null && response.foto != ""){
			if(html){
				$(div).html('<img width="'+width+'" height="'+height+'" '+otrosattr+' src="data:image/png;base64,'+response.foto+'">');
			}else{
				$(div).attr("src",'data:image/png;base64,'+response.foto);
			}			
		}
	};
	getJSONAjax({operacion:'ConsultarFotoPaciente',autoid:autoid}, "../../Vistas/Facturacion/CtrlBasicos.php", procesador, "GET", true, "");
}

$_asuntoTemp='';
function setearPaciente(autoid,tipo_id,num_id,primer_nombre,segundo_nombre,primer_ape,segundo_ape,sexo,fecha_nac,
	direccion,telefono,cod_empresa,empresa,tipo_sangre,cod_departamento,departamento,cod_ciudad,ciudad,cod_barrio,barrio,
	zona,asunto,codContrato,nomContrato,cod_estrato,estrato,es_terapia,tipo_historia,email,recordar_cita,nom_responsable,
	ape_responsable,parentesco_responsable,telefono_responsable,direccion_responsable,nomEstadoAfld,permiteAtencion,
	puntoAtencion,tipoAfiliado,carnet,tipo_afilia,esMontoPcte,tipoMontoPcte,ActividadServicio,ActividadAsunto,confirmaCitaMedico,
	contratoConfirmaMedico,NivelPago,ManejaTablaLQ_Pagos,MedicoAsignado,NomMedicoAsignado,manejaPGP,tipocita= null){
	cargarAsuntosMedico($("#cod_medico").val(),asunto, trim(codContrato));
	/* Si es una cita grupal no cargarle los datos del paciente */
	if(tipocita == 2){
		get('tabla_cita_agrupada').style.display="";
	   return 0;
	}
	fnEtiquetaPcte(autoid);
	$("#ImgPcteFotoDiv").html('<img width="100" height="75" src="'+$('#CarpetaArchivosRead').val()+'/Pacientes/'+autoid+'/FotoPaciente/'+autoid+'.jpg" width="100px">');
	
	ConsultarImagenAplicacionExterna(autoid,"#ImgPcteFotoDiv","100","75","", true);
    
        $("#MedicoAsignadoPcte").val(MedicoAsignado);
        $("#MedicoFamiliarAsignado").val(NomMedicoAsignado);
            
		if($.trim(confirmaCitaMedico)!=''){
			$('#confirmarCitaEPS').val(confirmaCitaMedico);
			//alert($('#confirmarCitaEPS').val());
			validarTipoApartarCita();
		}


		$("#confirmarCitaEPS_backup").val(contratoConfirmaMedico);

		if($_esAdicional==1){
			$("#confirmarCitaEPS").val("N");
			$("#confirmarCitaEPS_backup").val("N");
		}

		get('autoid').value=(autoid);
		get('tipo').value=trim(tipo_id);
		get('ident').value=trim(num_id);
		get('nroCarnet').value=$.trim(carnet);
		get('pnombre').value=trim(primer_nombre);
		get('snombre').value=trim(segundo_nombre);
		get('papellido').value=trim(primer_ape);
		get('sapellido').value=trim(segundo_ape);
		get('sexo').value=trim(sexo);
		get('fechanaci').value=trim(fecha_nac);
		get('direccion').value=trim(direccion);
		get('telefono').value=trim(telefono);
		get('codigo_empresa').value=trim(empresa);
		get('departamento').value=trim(departamento);
		get('codigo_departamento').value=trim(cod_departamento);
		get('ciudad').value=trim(ciudad);
		get('codigo_ciudad').value=trim(cod_ciudad);
		get('barrio').value=trim(barrio);
		get('codigo_barrio').value=trim(cod_barrio);
		get('empresa').value=trim(empresa);
		get('codigo_empresa').value=trim(cod_empresa);
		get('tipo_sangre').value=trim(tipo_sangre);
		get('tipo_afiliacion').value=$.trim(tipo_afilia);
		get('nomservicio').value=trim(asunto);
		$_asuntoTemp=asunto;
		get('punto_atencion').value=trim(puntoAtencion);
		get('tipo_afiliado').value=trim(tipoAfiliado);
		
        $("#hidden_NivelPago").val(NivelPago);
        $("#pcte_NivelPago").val(NivelPago);
		
        $("#contrato_ManejaTablaLQ_Pagos").val(ManejaTablaLQ_Pagos);
    
        if($("#contrato_ManejaTablaLQ_Pagos").val()==1){
            $(".ClassManejaTablaLiquidacion").css({display:''});
            cargarNivelPagosPcte();
        }else{
            $(".ClassManejaTablaLiquidacion").css({display:'none'});
        }
		
		if(autoid!=""){
			
			get('id_contrato').value=trim(codContrato);
			
			if($.trim($("#id_contrato").val())!=''){
				cargarAsuntosMedico($("#cod_medico").val(),asunto,$("#id_contrato").val());
				
			}
			get('contrato').value=trim(nomContrato);

			get('manejaPGP').value=trim(manejaPGP);

			if(getValue('modificaEmpresa') == '0'){
				$('#empresa').attr('readonly',true)
				$('#empresa').removeClass('norm').addClass('normDisabled')

				}
		}
		try{
			get('email').value=trim(email);
			if($.trim(email)==''){
				get('email').value=getValue('rs_email_paci');
			}
		}catch(e){}
		
		try{
			if(recordar_cita=='1'){
				get('recordar_cita').checked=true;
			}else{
				get('recordar_cita').checked=false;			
			}
		}catch(e){};
		
	/*	get('id_estrato').value=cod_estrato;
		get('estrato').value=estrato;
		get('es_terapia').value=es_terapia;
		get('tipo_historia').value=tipo_historia;*/
		try{
			get('responsable_nombre').value=nom_responsable;
			get('responsable_apellido').value=ape_responsable;
			get('responsable_parentesco').value=parentesco_responsable;
			get('responsable_telefono').value=telefono_responsable;
			get('responsable_direccion').value=direccion_responsable;
		}catch(e){}
		
		try{
			if(trim(nomEstadoAfld)!=''){
				get('nombreestado').value=nomEstadoAfld;
				get('bloqueaAtencionUsuario').value=0;
				get('pacientePermiteAtencion').value=permiteAtencion;
				//alert("111");
				if(permiteAtencion!=1){
					//alert("sdsd");
					get('bloqueaAtencionUsuario').value='1';
				}
			}
		}catch(e){};
		
		try{
			$("#esMontoPcte").val(esMontoPcte);
			$("#tipoMontoPcte").val(tipoMontoPcte);	
			if(esMontoPcte==1&&tipoMontoPcte==2){
				$("#trActividadesPlanEspecial").css("display","");
				$("#trActividadesPlanEspecialCheck").css("display","");
			}else{
				$("#trActividadesPlanEspecial").css("display","");
				$("#trActividadesPlanEspecialCheck").css("display","");
			}
			if($("#esMontoPcte").val()==1&&$("#tipoMontoPcte").val()==2){
				$("#trProcedimientoRealizar").css("display","none");
			}else{
				$("#trProcedimientoRealizar").css("display","");
			}
			/*$("#actividadCheck").val(ActividadServicio);	
			$("#actividad").val(ActividadAsunto);			
			camposEsPlanEspecial(2,ActividadServicio,ActividadAsunto);*/
		}catch(e){}

	
}

function setear_tipo_examen(id_tipos_examen, nombre_tipo_examen, otro){
	get('tipo_examen').value=trim(id_tipos_examen);
	if( otro == 1){
		get('nombre_otro_tipo_examen').value=trim(nombre_tipo_examen);
		$("#otro_tipo_examen").css("display", "");
		
	}else{
		get('nombre_tipo_de_examen').value=trim(nombre_tipo_examen);
	}

	if(id_tipos_examen == ""){
		get('nombre_otro_tipo_examen').value="";
	}
}

function buscarPacientes(){	
	autoid=get('autoid').value;
    $("#ImgPcteFotoDiv").html('<img width="100" height="75" src="'+$('#CarpetaArchivosRead').val()+'/Pacientes/'+autoid+'/FotoPaciente/'+autoid+'.jpg" width="100px">');
	ajax("../../controlador/citas/citas.php?operacion=buscar_paciente&autoid="+autoid, "res_paciente",'setPaciente()','');
	//console.log("aqui");
	
}

function setPaciente(){
	
    $("#MedicoAsignadoPcte").val("");
	get('bloqueaAtencionUsuario').value='0';
	
	
	if(getValue('PlanAfiliacion')!=getValue('id_contrato')){
		get('bloqueaAtencionUsuario').value=0;
	}else{			
		try{				
			if(trim(get('pacientePermiteAtencion').value)!="1"&&trim(get('pacientePermiteAtencion').value)!=""){
				if(trim(get('bloqueaAtencionEstadoAfiliado').value)=='S'){
					_permiteAtencion='<b>Paciente no puede ser atendido, verifique su estado de afiliacion.</b>';
					get('bloqueaAtencionUsuario').value='1';
				}
			}
		}catch(e){
			alert(".: "+e);
		}	
	}
	
	if(getValue('PlanAfiliacion')!=getValue('id_contrato')){
		get('bloqueaAtencionUsuario').value=0;
	}else{
		try{
			if(trim(get('rs_PermiteAtencion').value)!="1"&&trim(get('pacientePermiteAtencion').value)!=""){
				if(trim(get('bloqueaAtencionEstadoAfiliado').value)=='S'){
					_permiteAtencion='<b>Paciente no puede ser atendido, verifique su estado de afiliacion.</b>';
					get('bloqueaAtencionUsuario').value='1';
				}
			}
		}catch(e){
		
		}
	}
	
	try{
		
		if(trim(get('estadoUltCita').value)=='P' || trim(get('estadoUltCita').value)=='I'){
			alertGlobal("El paciente <b>"+get('buscaPaciente').value+"</b> ha incumplido su ultima cita.</br>"+_permiteAtencion);
		}
		else if(trim(get('rs_PermiteAtencion').value)!="1"&&getValue('ManejaProcesosEPS')=='S'){
			if(getValue('PlanAfiliacion')!=getValue('id_contrato')){
				get('bloqueaAtencionUsuario').value=0;
			}else{
					alertGlobal(_permiteAtencion);			
			}
		}
	}catch(e){}
	
	var param=get('info_paci').value
	if($.trim(param) == ""){
		$.each($("#formularioPaciente").find("input:not('.fileRips')"), function(){
			if(this.id != "ident" && this.id != "medico" && this.id != "codigo_medico" 
				&& this.id != "fecha_cita" && this.id != "hora" && this.id != "minutos" && this.id != "meridiano"
				&& this.id != "btnGuardar" && this.id != "btnAsignar" && this.value != "Lector de Documento"){
				$(this).val("");
			}			
		});
		$("#tipo_sangre").val("No Aplica");
		$("#sexo").val("");
		$("#nomservicio").val("");
		$("#opc_proc_noqx").hide();
		try{
			get('recordar_cita').checked=false;
		}catch(e){}

		return;
	}
	var cad=param.split("|"); 

	var rs_email_paci=get('rs_email_paci').value;
	var rs_recordar_cita=get('rs_recordar_cita').value;
	get('nombreestado').value=get('rs_EstadoAfiliacion').value;
	
	
		
	var rs_autoid=get('rs_autoid').value;

	var rs_tipo_id=get('rs_tipo_id').value;
	var rs_num_id=get('rs_num_id').value;
	var rs_carnet=get('rs_carnet').value;
	var rs_primer_nom=get('rs_primer_nom').value;
	var rs_segundo_nom=get('rs_segundo_nom').value;
	var rs_primer_ape=get('rs_primer_ape').value;
	var rs_segundo_ape=get('rs_segundo_ape').value;
	var rs_sexo=get('rs_sexo').value;
	var rs_fecNac=get('rs_fecNac').value;
	var rs_direccion=get('rs_direccion').value;
	var rs_telefono=get('rs_telefono').value;
	var rs_codEmp=get('rs_codEmp').value;
	var rs_nomEmp=get('rs_nomEmp').value;
	var rs_tipo_sangre=get('rs_tipo_sangre').value;
	var rs_tipo_afilia=get('rs_tipo_afilia').value;
	var rs_codDep=get('rs_codDep').value;
	var rs_nomDep=get('rs_nomDep').value;
	var rs_codMun=get('rs_codMun').value;
	var rs_nomMun=get('rs_nomMun').value;
	var rs_codBarrio=get('rs_codBarrio').value;
	var rs_nomBarrio=get('rs_nomBarrio').value;
	var rs_zona=get('rs_zona').value;
	var rs_codCont=get('rs_codCont').value;
	var rs_nomCont=get('rs_nomCont').value;
	var rs_nom_responsable=get('rs_nom_responsable').value;
	var rs_ape_responsable=get('rs_ape_responsable').value;
	var rs_parentesco_responsable=get('rs_parentesco_responsable').value;
	var rs_telefono_responsable=get('rs_telefono_responsable').value;
	var rs_direccion_responsable=get('rs_direccion_responsable').value;
	var rs_EstadoAfiliacion=get('rs_EstadoAfiliacion').value;
	var rs_PermiteAtencion=get('rs_PermiteAtencion').value;
	var rs_PuntoAtencion=getValue('rs_PuntoAtencion');
	var rs_TipoAfiliado=getValue('rs_TipoAfiliado');
	var rs_esMontoPcte=getValue('rs_esMontoPcte');
	var rs_tipoMontoPcte=getValue('rs_tipoMontoPcte');
    var rs_NivelPago=$("#rs_NivelPago").val();
    var rs_ManejaTablaLQ_Pagos=$("#rs_ManejaTablaLQ_Pagos").val();
    var rs_MedicoAsignado=$("#rs_MedicoAsignado").val();
    var rs_NomMedicoAsignado=$("#rs_NomMedicoAsignado").val();
	var rs_manejaPGP=$("#rs_manejaPGP").val();

    cod_contrato = $("#id_contrato").val();
	cod_medico = $("#cod_medico").val();

	var asuntoCita;
	if(cod_medico != ""){
		cargarAsuntosMedico(cod_medico,asuntoCita,cod_contrato);
	}	
    
	
	try{
		get('pacientePermiteAtencion').value=get('rs_PermiteAtencion').value;
	}catch(e){}

 

	setearPaciente(rs_autoid,rs_tipo_id,rs_num_id,rs_primer_nom,rs_segundo_nom,rs_primer_ape,rs_segundo_ape,rs_sexo,rs_fecNac,rs_direccion,rs_telefono,rs_codEmp,rs_nomEmp,rs_tipo_sangre,rs_codDep,rs_nomDep,rs_codMun,rs_nomMun,rs_codBarrio,rs_nomBarrio,' ',' ',rs_codCont,rs_nomCont,'','','','','','',rs_nom_responsable,rs_ape_responsable,rs_parentesco_responsable,rs_telefono_responsable,rs_direccion_responsable,rs_EstadoAfiliacion,rs_PermiteAtencion,rs_PuntoAtencion,rs_TipoAfiliado,rs_carnet,rs_tipo_afilia,rs_esMontoPcte,rs_tipoMontoPcte,null,null,null,null,rs_NivelPago,rs_ManejaTablaLQ_Pagos,rs_MedicoAsignado,rs_NomMedicoAsignado,rs_manejaPGP);
    
	
	if(getValue('PlanAfiliacion')!=getValue('id_contrato')){
		get('bloqueaAtencionUsuario').value=0;
	}else{			
		try{				
			if(trim(get('pacientePermiteAtencion').value)!="1"&&trim(get('pacientePermiteAtencion').value)!=""){
				if(trim(get('bloqueaAtencionEstadoAfiliado').value)=='S'){
					_permiteAtencion='<b>Paciente no puede ser atendido, verifique su estado de afiliacion.</b>';
					get('bloqueaAtencionUsuario').value='1';
				}
			}
		}catch(e){
			alert(".: "+e);
		}	
	}

	
	$("#confirmarCitaEPS").val($("#rs_confirmaCitaMedico").val());
	$("#confirmarCitaEPS_backup").val($("#confirmarCitaEPS").val());

	try{
		existeVencidos();
	}catch(e){}
	//setearPaciente(cad[0],cad[1],cad[2],cad[3],cad[4],cad[5],cad[6],cad[7],cad[8],cad[9],cad[10],cad[11],cad[12],cad[13],cad[14],cad[15],cad[16],cad[17],cad[18],cad[19],cad[20],' ',cad[21],cad[22],'','','','',rs_email_paci,rs_recordar_cita,cad[23],cad[24],cad[25],cad[26],cad[27]);
	//setearPaciente(autoid,tipo_id,num_id,primer_nombre,segundo_nombre,primer_ape,segundo_ape,sexo,fecha_nac,direccion,telefono,cod_empresa,empresa,tipo_sangre,cod_departamento,departamento,cod_ciudad,ciudad,cod_barrio,barrio,zona,asunto,codContrato,nomContrato,cod_estrato,estrato,es_terapia,tipo_historia,email,recordar_cita,nom_responsable,ape_responsable,parentesco_responsable,telefono_responsable,direccion_responsable,nomEstadoAfld,permiteAtencion)

	cargarBateriaRutasAtencion(get('autoid').value);
	try{
		if( $("#esCitaMultiple").val() != 2 ){
			cargarProgramacion('');
			get('tipo_servicio').value='';
			cargaProcedimientosCita(0);
			cargaCanastasCita(0);
			cargaProcedimientosCirugia(0);
			cargaCanastasCirugia(0);
		}

	}catch(e){}
}

function cargarCanasta(){
	var id_canasta=get('id_canasta').value;

	ajax("../../controlador/citas/citas.php?operacion=cargar_contenido_canasta&id_canasta="+id_canasta, "contenidoCanasta",'limpiarCanastaInput()','');

}

// creamos una función auxiliar para que solo recargue los procedimientos ya cargados en la tabla
function return_true_proc(aux){
	return aux;
}

function cargarProcedimientos2(reemplazar_procs = 0, aux2){
	console.log('Cirugia 22');
	//get('tipo_servicio').value = "";
	console.log(get('tipo_servicio_temp').value);

	get('tipo_servicio').value = get('tipo_servicio_temp').value;
	if((get('tipo_servicio').value != "")){
		var id_procedimiento=get('id_procediento_realizar').value;
		var id_contrato=get('id_contrato').value;
		var id_servicio=get('tipo_servicio').value;
		ajax("../../controlador/citas/citas.php?operacion=cargar_procedimientos&id_procedimiento="+id_procedimiento+"&id_contrato="+id_contrato+"&id_servicio="+id_servicio+"&reemplazar_procs="+reemplazar_procs, "contenidoProcedimientos",'limpiarProcedimientoInput()','');
	}else{
		var id_procedimiento=get('id_procediento_realizar').value;
		var id_contrato=get('id_contrato').value;
		var id_servicio=get('tipo_servicio').value;
		get('tipo_servicio').value = '';
		get('tipo_servicio_temp').value = '';
		get('id_procediento_realizar').value = '';
		get('procediento_realizar').value = '';

		if(aux2 == true && id_servicio == ""){
			notify("Servicio no configurado.", 'error');
		}else{
			ajax("../../controlador/citas/citas.php?operacion=cargar_procedimientos&id_procedimiento="+id_procedimiento+"&id_contrato="+id_contrato+"&id_servicio="+id_servicio+"&reemplazar_procs="+reemplazar_procs, "contenidoProcedimientos",'limpiarProcedimientoInput()','');
		}
		
	}
}

function cargarProcedimientos(reemplazar_procs = 0){
	console.log('Cirugia 1');
	//get('tipo_servicio').value = "";
	get('tipo_servicio').value = get('tipo_servicio_temp').value;

	alert('Servicio: '+ get('tipo_servicio').value);
	if((get('tipo_servicio').value != "")){
		var id_procedimiento=get('id_procediento_realizar').value;
		var id_contrato=get('id_contrato').value;
		var id_servicio=get('tipo_servicio').value;
		ajax("../../controlador/citas/citas.php?operacion=cargar_procedimientos&id_procedimiento="+id_procedimiento+"&id_contrato="+id_contrato+"&id_servicio="+id_servicio+"&reemplazar_procs="+reemplazar_procs, "contenidoProcedimientos",'limpiarProcedimientoInput()','');
	}else{
		get('tipo_servicio').value = '';
		get('tipo_servicio_temp').value = '';
		get('id_procediento_realizar').value = '';
		get('procediento_realizar').value = '';
		notify("Servicio no configurado.", 'error');
	}
}


function limpiarCanastaInput(){
	get('canasta').value='';
	get('id_canasta').value='';	
	
}

function addDivList(_CodigoReferencia,_CodigoUnico,_Descripcion){
	$cssDiv={
		
		backgroundColor:'#FFF',
		border:'solid',
		float:'left',
		margin:'2px',
		borderWidth:'1px',
		borderColor:"#1a8090",
		padding:'2px',
		
	}
	$cssLabel={
		fontWeight:'bold'
	}
	
	$cssDelete={
		fontWeight:'bold',
		color:'#920000',
		cursor:'pointer',
	}
	
	$div=$('<div class="get-referencias-variables" data-codigo_referencia="'+_CodigoReferencia+'" data-codigo_unico_item="'+_CodigoUnico+'">');
	$label=$('<label class="letraDisplay">');
	$label.append(_Descripcion+' ');
	$delete=$('<span title="Eliminar">');
	$delete.append('[X]');
	$delete.data('div',$div);
	$delete.click(function(){
		$(this).data('div').remove();
	});
	$delete.css($cssDelete);
	$label.append($delete);
	$label.css($cssLabel);
	$div.append($label);
	$div.css($cssDiv);	
	return $div;
}

function limpiarProcedimientoInput(){
	cargarReferenciasProcedimientos();
	
	
	get('procediento_realizar').value='';
	get('id_procediento_realizar').value='';	
	try{
		
		if(get('nro_procedimientos_agregadas').value>0){
			get('procedimiento_noqx').checked=true;
			get('opc_proc_noqx').style.display="";
		}else{
			get('procedimiento_noqx').checked=false;
			get('opc_proc_noqx').style.display="none";
		}	
	}catch(e){}
	try{
		validarTipoApartarCita();
	}catch(e){}
	
}


function cargarReferenciasProcedimientos(){
	try{
	var $arrayTemp=new Array();
	//$(this).ZS_CleanReferencias();
	$(this).ZS_set_referencia();
	
	$($.parseJSON($("#obj_referencias").val())).each(function(indexP, elementP) {
		
		$(".set-attr-referencia").each(function(index, element) {
			//alert($(this).data("codigo_unico")+' '+elementP.CodProcedimiento);
            if($(this).data("codigo_unico")==elementP.CodProcedimiento){
				$divP=addDivList(elementP.IdReferencia,$(this).data("codigo_unico"),elementP.Descripcion);
				$(this).parent().find(".set-attr-referencia").children("div").append($divP);
				
			}
        });
    });
	}catch(e){console.log(e);}
}


function eliminarCanasta(id_canasta){
	ajax("../../controlador/citas/citas.php?operacion=quitar_canasta&id_canasta="+id_canasta, "contenidoCanasta",'','');
	
}

function eliminarProcedimiento(id_procedimiento){
	ajax("../../controlador/citas/citas.php?operacion=quitar_procedimiento&id_procedimiento="+id_procedimiento, "contenidoProcedimientos",'$(this).ZS_set_referencia();validarTipoApartarCita();','');
	
}

function eliminarProcedimiento_paraclinico(id_procedimiento){
	ajax("../../controlador/citas/citas.php?operacion=quitar_procedimiento_paraclinico&id_procedimiento="+id_procedimiento, "contenidoProcedimientosParaclinico",'','');
	
}

function cleanProcsSelected(){
	ajax("../../controlador/citas/citas.php?operacion=cleanProcedimientosSelected", "contenidoProcedimientos",'$(this).ZS_set_referencia();','');
}

function mostrarDivInfoCanasta(id,cancelar){
	var nro_canastas_agregadas='';
	if(cancelar=='detalleHistorial'||trim(cancelar)!=''&&trim(cancelar)!='1'){
		nro_canastas_agregadas=get('nro_canastas_agregadas2').value;
	}else{
		nro_canastas_agregadas=get('nro_canastas_agregadas').value;	
	}
	
	if(trim(cancelar)==''||trim(cancelar)=='detalleHistorial'){
		for(var i=0;i<nro_canastas_agregadas;i++){
			get('infoCanasta'+i).style.display="none";		
		}	
		get('infoCanasta'+id).style.display="";
	}else{
		get('infoCanasta'+id).style.display="none";	
	}	
}

function procNoQxCita(){
	validarTipoApartarCita();
	if(!get('procedimiento_noqx').checked){
		get('opc_proc_noqx').style.display="none";
		$("#stock").val("");	
		$("#id_stock").val("");
	}else{
		get('opc_proc_noqx').style.display="";	
		$("#stock").val($("#UserNomBodega").val());
		$("#id_stock").val($("#UserCodBodega").val());
	}	
	try{
		camposEsPlanEspecial();
	}catch(e){}
}


function aplazarCita(){
	var selCitaAplazar=0;
	var asunto=$("#nomservicio").val();
	try{
		selCitaAplazar=get('selCitaAplazar').value;
	}catch(e){}
	
	
	var estado_cita=get('estado_cita').value;
	var multiplescitas=get('confirmada_temp_hiden').value;
	
	if(estado_cita=='A'||(estado_cita=='CC'&& multiplescitas != 1)||estado_cita=='C'||estado_cita=='I'){
		notify("No se puede aplazar esta cita.","error");
//		return;
	}else if($.trim(asunto)==''){
		notify("Seleccione el asunto","error");
	}else{
		
		
		var id_cita_aplazar=get('id_cita_aplazar').value;
		var id_cita=get('id_cita').value;
		
		var sede=get('sede').value;
		var id_programacion=get('id_programacion').value;
	
		var tipo_id=get('tipo').value;
		var num_id=get('ident').value;
		var pnombre=get('pnombre').value;
		var snombre=get('snombre').value;
		var papellido=get('papellido').value;
		var sapellido=get('sapellido').value;
		var nombre_medico=get('medico').value;		
		var hora=get('hora').value;
		var minutos=get('minutos').value;
		var meridiano=get('meridiano').value;
		var fecha=get('fecha').value;	
		var codigoMedico = get('codigo_medico').value;
		
		if(get('estado_cita').value=='P' || (estado_cita=='CC'&& multiplescitas == 1)){
			get('txtCitaMarcada').innerHTML='<div align="center" class="citaApartar"><input type="hidden" name="selCitaAplazar" id="selCitaAplazar" value="ok"> APLAZAR CITA: Fecha: '+fecha+' '+hora+':'+minutos+' '+meridiano+' Paciente: '+tipo_id+' '+num_id+' '+pnombre+' '+snombre+' '+papellido+' '+sapellido+' Medico: '+nombre_medico+'</div>';
			get('id_cita_aplazar').value=get('id_cita').value;
			get('id_asunto_cita_aplazar').value=get('nomservicio').value;
			
		}else{
			if(get('id_cita_aplazar').value=='0'){
				get('txtCitaMarcada').innerHTML='<div align="center" class="citaApartarError">Debe seleccionar una cita ya asignada.</div>';		
			}else if(selCitaAplazar==0){
				notify("Primero seleccione la cita a aplazar","error");
			}else if(id_cita_aplazar!='0'&&id_cita==''){
				//alert("Apartando Cita.");	
				showMotivoAplazaCita(id_cita_aplazar,hora,minutos,meridiano,codigoMedico,fecha,sede,id_programacion,asunto);
				//deshabilitarToolBar();				//ajax("../../controlador/citas/citas.php?operacion=aplazarCita&id_cita_aplazar="+id_cita_aplazar+"&hora="+hora+"&minutos="+minutos+"&meridiano="+meridiano+'&codigoMedico='+codigoMedico+'&fecha='+fecha+'&sede='+sede+'&id_programacion='+id_programacion, "txtCitaMarcada","recargaAplazar('');habilitarToolBar();","");	
			}			
		}
	}
}


function showMotivoAplazaCita(id_cita_aplazar,hora,minutos,meridiano,codigoMedico,fecha,sede,id_programacion,asunto){
	
		var widget = new ModalContentZeusSalud("Aplazar Cita",
		"../../controlador/citas/citas.php?operacion=show_aplazar_cita&id_cita_aplazar="+id_cita_aplazar+'&hora='+hora+'&minutos='+minutos+'&meridiano='+meridiano+'&codigoMedico='+codigoMedico+'&fecha='+fecha+'&sede='+sede+'&id_programacion='+id_programacion+"&asunto="+asunto, {}, "general", "", "400px", "10%",null,null,true);
		
		widget.imgs_data = {	
								
								close_icon:{
								src:"../../ImagenesZeus/salir 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon close",
								_title:"Cerrar",
								id:"cerrar",
								fnAction:function(){
									$("#listaAut").parent(".ui-zs-modal").remove();
									$("#listaAut").remove();
								}
								},
								
								aplazar_icon:{
								src:"../../ImagenesZeus/32x32/aplazar-cita 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon",
								_title:"Aplazar Cita",
								id:"aplCita",
								fnAction:function(){
									//guardarAutCita();	
									fnAplazarCita();
								}
								},
								
						 };
			widget.process();	
			
}

function fnAplazarCita(){
	var motivoAplazaCita=getValue('txt_motivoAplazaCita');
	
	var id_cita_aplazar=getValue('Apl_id_cita_aplazar');
	var hora=getValue('Apl_hora');
	var minutos=getValue('Apl_minutos');
	var meridiano=getValue('Apl_meridiano');
	var codigoMedico=getValue('Apl_codigoMedico');
	var fecha=getValue('Apl_fecha');
	var sede=getValue('Apl_sede');
	var id_programacion=getValue('Apl_id_programacion');
	var asunto=$('#Apl_asunto').val();
	deshabilitarToolBar();
	ajax("../../controlador/citas/citas.php?operacion=aplazarCita&id_cita_aplazar="+id_cita_aplazar+"&hora="+hora+"&minutos="+minutos+"&meridiano="+meridiano+'&codigoMedico='+codigoMedico+'&fecha='+fecha+'&sede='+sede+'&id_programacion='+id_programacion+"&asunto="+asunto+"&motivoAplazaCita="+motivoAplazaCita, "txtCitaMarcada","recargaAplazar('');habilitarToolBar();closeModal();","");	
}

function recargaAplazar(){
	get('id_cita_aplazar').value="0";
	//get('cod_medico').value='';
	cargarProgramacion('');
}

function reestablecer(cadena){
	var cad=cadena.replace("Ý","&Aacute;"); 
	var cad=cadena.replace("á","&aacute;"); 
	var cad=cadena.replace("É","&Eacute;"); 
	var cad=cadena.replace("é","&eacute;"); 
	var cad=cadena.replace("Ý","&Iacute;"); 
	var cad=cadena.replace("í","&iacute;"); 
	var cad=cadena.replace("Ó","&Oacute;"); 
	var cad=cadena.replace("ó","&oacute;"); 
	var cad=cadena.replace("Ú","&Uacute;"); 
	var cad=cadena.replace("ú","&uacute;"); 
	var cad=cadena.replace("Ü","&Uuml;"); 
	var cad=cadena.replace("ü","&uuml;"); 
	var cad=cadena.replace("Ṅ","&Ntilde;"); 
	var cad=cadena.replace("ñ","&ntilde;"); 
	
	return cad;
	
}

var _callValidacionPassSuperUser2='';
var _callMetodo2='';
var _callMetodo='';
var _validaExistencia=true;

function guardarDatosPaciente(callValidacionPassSuperUser,callMetodo){
	
	if(callMetodo==undefined || callMetodo=="undefined"){
			_callMetodo="";
	}
	
	if(_validaExistencia){
		var _callValidacionPassSuperUser2=callValidacionPassSuperUser;
		var _callMetodo2=callMetodo;
		
		var tipo_id=$('#tipo').val();
		var num_id=$('#ident').val();
		var pnombre=$('#pnombre').val();
		var papellido=$('#papellido').val();
		var sapellido=$('#sapellido').val();
		var fecha_naci=$('#fechanaci').val();
		
		ajaxPOST("../../controlador/citas/citas.php", "","respValExistenciaPcte()","","operacion=validarExistenciaPcte&tipo_id="+tipo_id+"&num_id="+num_id+"&pnombre="+pnombre+"&papellido="+papellido+"&sapellido="+sapellido+"&fecha_naci="+fecha_naci);
	}else{
		alert("_validaExistencia: false");
	}
}

function respValExistenciaPcte(){	
	var resp=JSON.parse(_ResquestAJAX);
	if(resp["Autoid"]!="0"){
		if($("#user_superusuario").val()=='1'){
			//if(confirm("Ya existe un paciente con estos datos basicos desea seguir guardando los datos?")){
				//Aut_guardarDatosPaciente(_callValidacionPassSuperUser2,_callMetodo2);
				showPctesExistentes(_callValidacionPassSuperUser2,_callMetodo2);
			//}
		}else{
			notify("Ya existe un paciente con estos datos basicos.","error");
			_validaExistencia=true;
		}
	}else{
		Aut_guardarDatosPaciente(_callValidacionPassSuperUser2,_callMetodo2);
	}
}


function showPctesExistentes(_callValidacionPassSuperUser2,_callMetodo2,esQx){
	var tipo_id=$('#tipo').val();
	var num_id=$('#ident').val();
	var pnombre=$('#pnombre').val();
	var papellido=$('#papellido').val();
	var sapellido=$('#sapellido').val();
	var fecha_naci=$('#fechanaci').val();
	var widget = new ModalContentZeusSalud("Pacientes Existentes Relacionados",
						"../../controlador/citas/citas.php", {operacion:'getPctesExistentes',tipo_id:tipo_id,num_id:num_id,pnombre:pnombre,papellido:papellido,sapellido:sapellido,fecha_naci:fecha_naci,esQx:esQx}, "general",null,null,null,null,null,true);
			
	widget.imgs_data = {	
						
						close_icon:{
						src:"../../ImagenesZeus/salir 01.png", 
						_w:32, 
						_h:32, 
						_class:"icon close",
						_title:"Cerrar",
						id:"cerrar"
						},							
						confirmar_icon:{
						src:"../../ImagenesZeus/aceptar 01.png", 
						_w:32, 
						_h:32, 
						_class:"icon",
						_title:"Cargar datos del paciente seleccionado",
						id:"cargarDatosPcte",
						fnAction:function(){
							cargarDatosPcteSel();
						}
						}
				 };
	widget.process();		
}

function cargarDatosPcteSel(){
	var resp=JSON.parse($('input:radio[name=RadioPaciente]:checked').val());
	$("#tipo").val(resp["tipoDocumento"]);
	$("#ident").val(resp["numeroDocumento"]);
	traerPaciente();
	try{
		cargarProgramacion();
	}catch(e){}
	closeModal();
}
function AUT_guardarDatos(){
	Aut_guardarDatosPaciente(_callValidacionPassSuperUser2,_callMetodo2);
	closeModal();
}

function Aut_guardarDatosPaciente(callValidacionPassSuperUser,callMetodo){

		if(callValidacionPassSuperUser==undefined || callValidacionPassSuperUser=="undefined"){
			_callMetodo="";
		}
		
		var tipo_id=get('tipo').value;
		var num_id=get('ident').value;
		var nroCarnet=(get('nroCarnet').value);
		var pnombre=(get('pnombre').value);
		var snombre=(get('snombre').value);
		var papellido=(get('papellido').value);
		var sapellido=(get('sapellido').value);
		var sexo=get('sexo').value;
		var fecha_naci=get('fechanaci').value;
		var tipo_sangre=replaceTipoSangre(get('tipo_sangre').value);
		var tipo_afiliacion=$('#tipo_afiliacion').val();
		var departamento=get('departamento').value;
		var codigo_departamento=get('codigo_departamento').value;
		var codigo_ciudad=get('codigo_ciudad').value;
		var ciudad=get('ciudad').value;
		var codigo_barrio=get('codigo_barrio').value;
		var barrio=get('barrio').value;
		var direccion=(get('direccion').value);
		var telefono=(get('telefono').value);
		var autoid=get('autoid').value;
		var codigo_empresa=get('codigo_empresa').value;
		var contrato=get('id_contrato').value;
        var pcte_NivelPago=$("#pcte_NivelPago").val();
      
		var email=get('email').value;
		var id_stock=get('id_stock').value;
		var tipo_servicio=get('tipo_servicio').value;
		var asunto=get('nomservicio').value;
		var primera_vez_control=get('primera_vez_control').value;
		var fecha_usuario_desea_cita=$("#fecha_usuario_desea_cita").val();			
		var lugarAtencion=$("#lugarAtencion").val();			
		var formaSolicitud=$("#formaSolicitud").val();
		
		var responsable_nombre=(get('responsable_nombre').value);
		var responsable_apellido=(get('responsable_apellido').value);
		var responsable_parentesco=(get('responsable_parentesco').value);
		var responsable_direccion=(get('responsable_direccion').value);
		var responsable_telefono=(get('responsable_telefono').value);
		
		
		var paramPersonal='&tipo_id='+tipo_id+'&num_id='+num_id+'&pnombre='+pnombre+'&snombre='+snombre+'&papellido='+papellido+'&sapellido='+sapellido+'&sexo='+sexo+'&fecha_naci='+fecha_naci+'&direccion='+direccion+'&telefono='+telefono+'&tipo_sangre='+tipo_sangre+'&codigo_departamento='+codigo_departamento+'&codigo_ciudad='+codigo_ciudad+'&codigo_barrio='+codigo_barrio+"&codigo_empresa="+codigo_empresa+"&autoid="+autoid+"&contrato="+contrato+"&id_stock="+id_stock+"&tipo_servicio="+tipo_servicio+"&asunto="+asunto+"&primera_vez_control="+primera_vez_control+"&email="+email+'&responsable_nombre='+responsable_nombre+'&responsable_apellido='+responsable_apellido+'&responsable_parentesco='+responsable_parentesco+'&responsable_direccion='+responsable_direccion+'&responsable_telefono='+responsable_telefono+"&nroCarnet="+nroCarnet+"&tipo_afiliacion="+tipo_afiliacion+"&fecha_usuario_desea_cita="+fecha_usuario_desea_cita+"&lugarAtencion="+lugarAtencion+"&formaSolicitud="+formaSolicitud;
		
		var cod_medico=get('cod_medico').value;
		var fecha_cita=get('fecha_cita').value;
		var hora=get('hora').value;
		var minutos=get('minutos').value;
		var meridiano=get('meridiano').value;
		
		
		var estado='P';
		var asunto=get('nomservicio').value;
		var observacion=(get('obs').value);
		var id_procediento_realizar=get('id_procediento_realizar').value;
		
		var recordar_cita='0';
		
		if(get('recordar_cita').checked){
			recordar_cita='1';
		}
		
		var ObservacionUpdateCita='';
		var idSuperUser='';

		try{
			ObservacionUpdateCita=$("#ObservacionUpdateCita").val();
			idSuperUser=$("#idSuperUser").val();
		}catch(e){}
		
		
		
		/*alert(autoid+" | "+cod_medico+" | "+fecha_cita+" | "+hora+":"+minutos+" "+meridiano+" | "+estado+" | "+asunto+" | "+observacion+" | "+codigo_empresa);*/
		_validaExistencia=true;
		if(trim(tipo_id)==""){
			alert("Seleccione un tipo de documento");
		}else if(trim(num_id)==""){
			alert("Ingrese numero de documento");
			seleccionar('ident');
		}else if(trim(pnombre)==""){
			alert("Ingrese primer nombre del paciente");
			seleccionar('pnombre');
		}else if(trim(papellido)==""){
			alert("Ingrese primer apellido del paciente");
			seleccionar('papellido');
		}else if(trim(sexo)=="" && $("#permitirCitaDocNombre").val()!='S'){
			alert("Seleccione el sexo");
		}else if(trim(fecha_naci)=="" && $("#permitirCitaDocNombre").val()!='S'){
			alert("Seleccione la fecha de nacimiento del paciente");
			seleccionar('fechanaci');
		}else if(trim(tipo_afiliacion)==""&&get('citasRapidas').value!='S'){
			alert("Seleccione el tipo de afiliacion");
			seleccionar('tipo_afiliacion');
		}else if(trim(departamento)==""&&get('citasRapidas').value!='S'){
			alert("Seleccione el departamento");
		}else if(trim(ciudad)==""&&get('citasRapidas').value!='S'){
			alert("Seleccione la ciudad");
		}else if(trim(barrio)==""&&get('citasRapidas').value!='S'){
			alert("Seleccione el barrio");
		}else if(get('validar_telefono').value=='S'&&trim(get('telefono').value)==''){
			notify("Ingrese un numero telefonico","error");
		}else if(trim(codigo_empresa)=="" && $("#permitirCitaDocNombre").val()!='S'){
			notify("Seleccione una empresa","error");
		}else if(get('validar_correo').value=='S'&&trim(get('email').value)==''){
			notify("Ingrese un correo electronico","error");
		}else{
			_validaExistencia=false;
			var idCita=get('id_cita').value;
			var lugarAtencion=getValue('lugarAtencion');
			var guardar=true;
			if($.trim(idCita)!=''){
				var estado_cita=get('estado_cita').value;
				if(estado_cita!='P'){
					notify("No se pueden modificar los datos de la cita.","error");
					_validaExistencia=true;
					return false;
				}
				if($.trim(asunto)==''){
					notify("Seleccione el asunto de la cita","error");
					_validaExistencia=true;
					return false;
				}
				
				if($.trim($("#confirmarModificacionCitas").val())=='S'&&callValidacionPassSuperUser!=1){
					guardar=false;
					_validaExistencia=false;
					validDatosCitas(idCita);
					
				}else{
					if(!confirm("Se modificaran los datos de una cita asignada, desea continuar?")){
						guardar=false;
						_validaExistencia=true;
					}
				}
				
			}
			if(guardar){
				var getReferencias='';
				try{
					getReferencias=JSON.stringify($(this).ZS_get_referencias());
				}catch(e){}
				
				ajaxPOST("../../controlador/citas/citas.php", "res_paciente",'setAutoidPaci();'+((_callMetodo=='')?'cargarProgramacion("");':_callMetodo),'',"operacion=guardarPaciente"+paramPersonal+"&idCita="+idCita+'&lugarAtencion='+lugarAtencion+'&recordar_cita='+recordar_cita+'&observacion='+observacion+"&ObservacionUpdateCita="+ObservacionUpdateCita+"&idSuperUser="+idSuperUser+"&pcte_NivelPago="+pcte_NivelPago+"&JSON_Referencias="+getReferencias+'&NumeroPoliza='+$("#no_poliza").val());
			}
			_validaExistencia=true;
			
		
		}
	}

function validDatosCitas(idCita){

	_validaExistencia=false;
	var servicio=$("#tipo_servicio").val();
	var empresa=$("#codigo_empresa").val();
	var contrato=$("#id_contrato").val();
	var asunto=$("#nomservicio").val();
	var fecha_usuario_desea_cita=$("#fecha_usuario_desea_cita").val();	
	var formaSolicitud=$("#formaSolicitud").val();
	var lugarAtencion=$("#lugarAtencion").val();
	var primera_vez_control=$("#primera_vez_control").val();
	var obs=$.trim($("#obs").val());
	var getReferencias='';
	try{
		getReferencias=JSON.stringify($(this).ZS_get_referencias());
	}catch(e){}
	ajaxPOST("../../controlador/citas/citas.php","","getPasswordSuperUser()","","operacion=validarUpdateCita&idCita="+idCita+"&servicio="+servicio+"&empresa="+empresa+"&contrato="+contrato+"&asunto="+asunto+"&fecha_usuario_desea_cita="+fecha_usuario_desea_cita+"&formaSolicitud="+formaSolicitud+"&lugarAtencion="+lugarAtencion+"&primera_vez_control="+primera_vez_control+"&JSON_Referencias="+getReferencias+"&obs="+obs);
}




function getPasswordSuperUser(){
	
	_validaExistencia=true;
	if($.trim($("#nomservicio").val())==''){
		notify("Seleccione un asunto","error");
		return false;		
	}
	var resp=JSON.parse(_ResquestAJAX);

	if(resp["HuboModificacion"]==1){	

		var idCita=$('#id_cita').val();	
		var servicio=$("#tipo_servicio").val();
		var empresa=$("#codigo_empresa").val();
		var contrato=$("#id_contrato").val();
		var asunto=$("#nomservicio").val();	
		var fecha_usuario_desea_cita=$("#fecha_usuario_desea_cita").val();	
		var formaSolicitud=$("#formaSolicitud").val();
		var lugarAtencion=$("#lugarAtencion").val();
		var primera_vez_control=$("#primera_vez_control").val();
		var obs=$.trim($("#obs").val());
		var getReferencias='';
		try{
			getReferencias=JSON.stringify($(this).ZS_get_referencias());
		}catch(e){}
		var widget = new ModalContentZeusSalud("Modificar datos de Cita",
			"../../controlador/citas/citas.php?operacion=formPasswordSuperUser", {
					idCita:idCita,
					servicio:servicio,
					empresa:empresa,
					contrato:contrato,
					asunto:asunto,
					fecha_usuario_desea_cita:fecha_usuario_desea_cita,
					formaSolicitud:formaSolicitud,
					lugarAtencion:lugarAtencion,
					primera_vez_control:primera_vez_control,
					obs:obs,					
					VariablesReferencias_Update:resp["VariablesReferencias_Update"],
					Canastas_Update:resp["Canastas_Update"],
					Procedimientos_Update:resp["Procedimientos_Update"],
					Asunto_Update:resp["Asunto_Update"],
					Empresa_Update:resp["Empresa_Update"],
					Contrato_Update:resp["Contrato_Update"],
					Servicio_Update:resp["Servicio_Update"], 
					FechaUsuarioDeseaCita_Update:resp["FechaUsuarioDeseaCita_Update"], 
					FormaSolicitud_Update:resp["FormaSolicitud_Update"], 
					LugarAtencion_Update:resp["LugarAtencion_Update"], 
					PrimeraVezControl_Update:resp["PrimeraVezControl_Update"], 
					Observacion_Update:resp["Observacion_Update"],
					JSON_Referencias:getReferencias
					}, 
			"general", "", "", "2%",null,null,true,'idModalUpdateDatosCita');
	
			widget.imgs_data = {								
								close_icon:{
								src:"../../ImagenesZeus/salir 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon close",
								_title:"Cerrar",
								id:"cerrar"
								},		
													
								confirmar_icon:{
									src:"../../ImagenesZeus/confirmar citas 01.png", 
									_w:32, 
									_h:32, 
									_class:"icon",
									_title:"Confirmar Procedimientos",
									id:"confirmar",
									fnAction:function(){
										getDatosSuperUser();
									}
								}
						 };
			
		widget.process();	
	}else{
		if(_callMetodo=='confirmarCitas(null,1)'){
			eval('confirmarCitas(null,1)');
		}else{
			//notify("No se han detectado cambios en los datos de la cita seleccionada.","advise");
			get('id_cita').value='';
			Aut_guardarDatosPaciente(0);
		}
	}
}


function capturaPantalla(){
	if(validDatosSuperUser()){
		deshabilitarToolBar(true);
		crearLoadingJX();
		var canvas = document.querySelector("canvas");
		var ctx = canvas.getContext("2d");
		var idCita=$('#id_cita').val();
				
		html2canvas(document.querySelector("body"), {canvas: canvas}).then(function(canvas) {	
			setLetraDisplay();	
			var canvasData = canvas.toDataURL("image/png");			 
			var ajax_ = new XMLHttpRequest();
			ajax_.open("POST",'../../controlador/citas/citas.php?operacion=saveImageCapture&idCita='+idCita,false);
			ajax_.setRequestHeader('Content-Type', 'application/upload');
			ajax_.onreadystatechange = function() {
				if (ajax_.readyState == 4) {
					//habilitarToolBar(true);
					eliminaLoadingJX();
					Aut_guardarDatosPaciente(1,_callMetodo);
					closeModal();
				}
			}
			ajax_.send(canvasData);	
		}, false);
	}
}

function cargarDetalleLogCita(IdLog){
	var widget = new ModalContentZeusSalud("Detalle de Modificacion de Cita",
			"../../controlador/citas/citas.php?operacion=viewDetalleLog", {IdLog:IdLog},"general", "", "", "2%",null,null,true,'ModalViewLogCita',setPestaniasLogCitas);
	
		widget.imgs_data = {								
				close_icon:{
				src:"../../ImagenesZeus/salir 01.png", 
				_w:32, 
				_h:32, 
				_class:"icon close",
				_title:"Cerrar",
				id:"cerrar"
				},		
		 };
			
		widget.process();	
}

function setPestaniasLogCitas(){
	$("#ttabs li").click(function() {
		//	First remove class "active" from currently active tab
		$("#ttabs li").removeClass('tactive');

		//	Now add class "active" to the selected/clicked tab
		$(this).addClass("tactive");

		//	Hide all tab content
		$(".ttab_content").hide();

		//	Here we get the href value of the selected tab
		var selected_tab = $(this).find("a").attr("href");
		//	Show the selected tab content
		
		$(selected_tab).fadeIn();
		
		//	At the end, we add return false so that the click on the link is not executed
		return false;
	});
}

function setLetraDisplay(){
	$("table td").addClass("letraDisplay");		
	$("#medico").addClass("normDisabled");		
	$("#hora").addClass("normDisabled");		
	$("#minutos").addClass("normDisabled");		
	$("#meridiano").addClass("normDisabled");	
}

function getDatosSuperUser(){
	if(validDatosSuperUser()&&$("#UpdateCitasPassword").val()=='S'){
		var nameSuperUser=$("#nameSuperUser").val();
		var passwordSuperUser=md5($("#passwordSuperUser").val());
		deshabilitarToolBar(true);
		ajaxPOST("../../controlador/citas/citas.php","","returnDatosSuperUser();habilitarToolBar(true);","","operacion=validarSuperUser&nameSuperUser="+nameSuperUser+"&passwordSuperUser="+passwordSuperUser);
	}else if(validDatosSuperUser()){
		if($("#capturarPantallaUpdateCitas").val()=='S'){
			_validaExistencia=false;
			capturaPantalla();
		}else{
			Aut_guardarDatosPaciente(1,_callMetodo);
			closeModal();
		}
	}
}

function validDatosSuperUser(){
	var nameSuperUser=$("#nameSuperUser").val();
	var passwordSuperUser=md5($("#passwordSuperUser").val());
	var ObservacionUpdateCita=$("#ObservacionUpdateCita").val();
	var UpdateCitasPassword=$("#UpdateCitasPassword").val();
	$ok=true;
	if($.trim(nameSuperUser)==''&&UpdateCitasPassword=='S'){
		notify("Ingrese el nombre de usuario","error");
		$("#nameSuperUser").select();
		$ok=false;
	}else if($.trim($("#passwordSuperUser").val())==''&&UpdateCitasPassword=='S'){
		notify("Ingrese la clave del usuario","error");
		$("#passwordSuperUser").select();
		$ok=false;
	}else if($.trim(ObservacionUpdateCita)==''){
		notify("Ingrese una observacion","error");
		$("#ObservacionUpdateCita").select();
		$ok=false;
	}else if($.trim(ObservacionUpdateCita).length<10){
		notify("Observacion demasiado corta, favor detalle un poco mas la modificacion de cita.","error");
		$("#ObservacionUpdateCita").select();
		$ok=false;
	}
	return $ok;
}

function returnDatosSuperUser(){
	var resp=JSON.parse(_ResquestAJAX);
	if($.trim(resp[0])=='-1' || $.trim(resp[0])=='0'){
		notify(resp[1],"error");
	}else if($.trim(resp[0])=='1'){
		$("#idSuperUser").val(resp[2]);
		if($("#capturarPantallaUpdateCitas").val()=='S'){
			capturaPantalla();
		}else{
			guardarDatosPaciente(1,_callMetodo);
			closeModal();
		}
	}
}

function setAutoidPaci(){
	get('autoid').value=get('autoidPac').value;
}

function limpiarFiltro(id_filtro){
	get('tdNroCitaAsignadaMedico').style.display='none';
	if(id_filtro=="1"){
		get('cod_medico').value='';
		get('nom_medico').value='';
		cargarProgramacion('','',1,1);		
	}else if(id_filtro=="2"){
		get('cod_especialidad').value='';
		get('especialidad').value='';
		cargarProgramacion('','',1);		
	}else if(id_filtro=="3"){
		get('busqueda').value='';
		cargarProgramacion('','',1);		
	}
}

function verificarAccidente(){
	if(get('causa_ext').value=="1"){
		get('accidenteLaboral').style.display="";
	}else{
		get('accidenteLaboral').style.display="none";		
	}
}

function cargarHistorialCitas(){
	var tipo_id=get('tipo').value;
	var num_id=get('ident').value;
	estado_h_citas = "";
	if(get('estado_h_citas') != undefined){
		estado=get('estado_h_citas').value;
		if(estado != undefined && estado != ""){
			estado_h_citas = estado;
		}
	}

	filtroAsunto_h = 0;
	if(get('filtroAsunto_h') != undefined){
		asunto_h=get('filtroAsunto_h').value;
		if(asunto_h != undefined && asunto_h != "" && asunto_h != "0"){
			filtroAsunto_h = asunto_h;
		}
	}

	finicio_h_citas = "";
	if(get('fechainiHcta') != undefined){
		fini=get('fechainiHcta').value;
		if(fini != undefined && fini != ""){
			finicio_h_citas = fini;
		}
	}

	ffin_h_citas = "";
	if(get('fechafinHcta') != undefined){
		ffin=get('fechafinHcta').value;
		if(ffin != undefined && ffin != ""){
			ffin_h_citas = ffin;
		}
	}
	
	if(trim(tipo_id)==""){
		alert("Seleccione un tipo de documento");		
	}else if(trim(num_id)==""){
		alert("Ingrese numero de documento");
		seleccionar('ident');
	}else{
		ajax("../../controlador/citas/citas.php?operacion=historial_citas&tipo_id="+tipo_id+"&num_id="+num_id+"&estado_h_citas="+estado_h_citas+"&filtroAsunto_h="+filtroAsunto_h+"&fechainiHcta="+finicio_h_citas+"&fechafinHcta="+ffin_h_citas, "contenido_programacion","IniciarFechaHistoCita('"+finicio_h_citas+"','"+ffin_h_citas+"')",'');
	}
}

function IniciarFechaHistoCita(val_inicio = "", val_fin = "") {
	$("#fechainiHcta").datepicker({
		showOn: "both",
		buttonImage: "../../Imagenes/calendario 01.png",
		buttonImageOnly: true,
		buttonText: "Fecha de Nacimiento",
		changeMonth: true,
		changeYear: true,
		minDate: new Date(1900,1-1,1), 
		yearRange: "-110:+5",
		dateformat: "yy/mm/dd",
		showAnim: "fadeIn"
				 
	});
   $("#fechainiHcta").datepicker("option","dateFormat","yy/mm/dd");
   $("#fechainiHcta").val(val_inicio);

	$("#fechafinHcta").datepicker({
		showOn: "both",
		buttonImage: "../../Imagenes/calendario 01.png",
		buttonImageOnly: true,
		buttonText: "Fecha de Nacimiento",
		changeMonth: true,
		changeYear: true,
		minDate: new Date(1900,1-1,1), 
		yearRange: "-110:+5",
		dateformat: "yy/mm/dd",
		showAnim: "fadeIn"
				
	});
	$("#fechafinHcta").datepicker("option","dateFormat","yy/mm/dd");
	$("#fechafinHcta").val(val_fin);

	$("#fechainiHcta").change(function(){
		cargarHistorialCitas();
	})
	$("#fechafinHcta").change(function(){
		cargarHistorialCitas();
	})
}

function cargarHistorialCirugia(){
	var tipo_id=get('tipo').value;
	var num_id=get('ident').value;
	
		if(trim(tipo_id)==""){
			alert("Seleccione un tipo de documento");		
		}else if(trim(num_id)==""){
			alert("Ingrese numero de documento");
			seleccionar('ident');
		}else{
			ajax("../../controlador/quirofanos/quirofanos.php?operacion=historial_citas&tipo_id="+tipo_id+"&num_id="+num_id, "contenido_programacion",'','');
		}
}

function showDetalleCita(id_cita){
	
		var widget = new ModalContentZeusSalud("Detalle Cita",
		"../../controlador/citas/citas.php?operacion=show_detalle_cita&id_cita="+id_cita, {}, "general", "", "900px", "2%");
		
		widget.imgs_data = {								
							close_icon:{
							src:"../../ImagenesZeus/salir 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon close",
							_title:"Cerrar",
							id:"cerrar"
							},		
												
							imprimir_icon:{
							src:"../../ImagenesZeus/alante 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon",
							_title:"Ir a la Cita",
							id:"ir_cita",
							fnAction:function(){
								IrACita();	
							}
							} 
					 }
		
	widget.process();	

}

function IrACita(){
	var hist_estado_cita=$("#hist_estado_cita").val();
	var hist_fecha_cita=$("#hist_fecha_cita").val();
	var hist_pcte_cita=$("#hist_pcte_cita").val();
		
	$("#busqueda").val(hist_pcte_cita);
	$("#fecha").val(hist_fecha_cita);
	var numEstado=1;
	if(hist_estado_cita=='CC'){
		numEstado=5;
	}else if(hist_estado_cita=='A'){
		numEstado=6;
	}else if(hist_estado_cita=='C'){
		numEstado=4;
	}else if(hist_estado_cita=='P'){
		numEstado=3;
	}else if(hist_estado_cita=='I'){
		numEstado=8;
	}
	
	$("#mostrar").val(numEstado);
	cargarProgramacion('');
}

function verProgramacionMedico(){
	var cod_medico=get('codigo_medico').value;
	if($.trim(cod_medico)==''){
		cod_medico=$("#cod_medico").val();		
	}
	var sede=get('sede').value;
	var id_programacion=get('id_programacion').value;
	if(trim(cod_medico)==""){
		alert("Seleccione un medico");
	}else{
		var widget = new ModalContentZeusSalud("Programacion Medico",
		"../../controlador/citas/citas.php?operacion=showProgramacionMedico&cod_medico="+cod_medico+"&sede="+sede+"&id_programacion="+id_programacion, {}, "general", "", "700px", "10%");
		widget.process();	
	}
}

function verDisponibilidadMedico(){
	var cod_medico=$('#codigo_medico').val();
	if($.trim(cod_medico)==''){
		cod_medico=$("#cod_medico").val();		
	}
	var sede=$('#sede').val();
	var id_programacion=$('#id_programacion').val();
	if(trim(cod_medico)==""){
		notify("Seleccione un medico","error");
	}else{
		ajaxPOST("../../controlador/citas/citas.php","","setDisponibilidadCitas()","","operacion=showDisponibilidadMedico&cod_medico="+cod_medico+"&sede="+sede);
		
		/*var widget = new ModalContentZeusSalud("Disponibilidad Agenda Medica",
		"../../controlador/citas/citas.php?operacion=showDisponibilidadMedico&cod_medico="+cod_medico+"&sede="+sede, {}, "general", "", "", "2%");
		widget.imgs_data = {								
							close_icon:{
							src:"../../ImagenesZeus/salir 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon close",
							_title:"Cerrar",
							id:"cerrar"
							},		
												
							/*_apartar_icon:{
							src:"../../ImagenesZeus/32x32/apartar-cita 01.png",
							_w:32, 
							_h:32, 
							_class:"icon",
							_title:"Apartar Citas",
							id:"id_apartar",
							fnAction:function(){
								
							}
						  }
					 };
		widget.process();	*/
	}
}



function verAgendaMedica(){
	var cod_medico=get('codigo_medico').value;
	var sede=get('sede').value;
	var fecha=get('fecha').value;
	var medico=get('medico').value;
	if(trim(cod_medico)==""){
		alert("Seleccione un medico");
	}else{
		var widget = new ModalContentZeusSalud("Agenda Medico "+fecha,
		"../../controlador/citas/citas.php?operacion=showAgendaMedico&cod_medico="+cod_medico+"&sede="+sede+"&fecha="+fecha, {}, "general", "", "700px", "10%");		
		widget.imgs_data = {								
							close_icon:{
							src:"../../ImagenesZeus/salir 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon close",
							_title:"Cerrar",
							id:"cerrar"
							},		
												
							imprimir_icon:{
							src:"../../ImagenesZeus/imprecion 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon",
							_title:"Imprimir Agenda",
							id:"confirmar",
							fnAction:function(){
								imprimirAgendaMedico();	
							}
						  }
					 };
		widget.process();	
	}
}

function filtrarAgendaMedica(){
	deshabilitarToolBar(true);
	var cod_medico=get('codigo_medico').value;
	var sede=get('sede').value;
	var fecha=get('fecha').value;
	var medico=get('medico').value;
	var filtro=get('filtro').value;
	if(trim(cod_medico)==""){
		alert("Seleccione un medico");
	}else{
		ajax("../../controlador/citas/citas.php?operacion=agendaMedico&cod_medico="+cod_medico+"&sede="+sede+"&fecha="+fecha+"&filtro="+filtro, "divAutomatico",'habilitarToolBar(true)','');
	}
		
}

function imprimirAgendaMedico(){
	var cod_medico=get('codigo_medico').value;
	var filtro=get('filtro').value;
	var fecha=get('fecha').value;
	invocarReporte("estadisticas_citas_simplificado.php?cod_med="+cod_medico+ "&inicial=" 
				+ fecha + "&final=" 
				+ fecha+'&mostrar='+filtro, "consolidados");
	
}

function selectCama(codigoCama){
	if(get('cama'+codigoCama).className=="tabCamasSelected letraDisplay"){
		get('cama'+codigoCama).className="tabCamas letraDisplay";
	}else{
		get('cama'+codigoCama).className="tabCamasSelected letraDisplay";
	}
	var numCamasSelected=document.getElementsByClassName("tabCamasSelected letraDisplay");
	get('camasSeleccionadas').innerHTML=numCamasSelected.length;
}

function copiarCamas(){

	var numCamasSelected=document.getElementsByClassName("tabCamasSelected letraDisplay");
	var camas="";
	if(numCamasSelected.length==0){
		alert("Seleccione minimo una cama");
	}else{	
		try{
			get('contenidoMensaje').innerHTML='';
		}catch(e){}
		
		$("#btn").animate({"left": "+=40px"}, 100,function(){
					//$(".block").hide();	
					$("#btn").animate({"left": "-=40px"}, "fast"	);
		});
		
		
		for(i=0;i<numCamasSelected.length;i++){
			camas+=numCamasSelected.item(i).getAttribute("valor")+",";
		}
		var cama_a_copiar=get('cama_a_copiar').value;
	
		ajax("copiarCamas.php?metodo=copiarCamas&camas="+camas+"&cama_a_copiar="+cama_a_copiar, "respuestaCamas",'','');
	}
}


/********************* METODOS PARA EL NUEVO FICHERO ******************************************/


function consultaExterna(){
	var cedula=get('cedula').value;
	var nombre=get('nombre').value;
	var primer_ape=get('primer_ape').value;
	var segundo_ape=get('segundo_ape').value;
	var historia=get('historia').value;
	var estudio=get('estudio').value;
	var contrato=get('contrato').value;
	var ingreso=get('ingreso').value;
	var factura=get('factura').value;
	
	var parametros='&cedula='+cedula+"&nombre="+nombre+"&primer_ape="+primer_ape+"&segundo_ape="+segundo_ape+"&historia="+historia+"&estudio="+estudio+"&contrato="+contrato+"&ingreso="+ingreso+"&factura="+factura;

	var url="../controlador/facturacion/fichero.php?operacion=pacientesConsultaExterna"+parametros;
	ajax(url, "listadoPacientes","","");
}

function consultaUrgencia(){
	var cedula=get('cedula').value;
	var nombre=get('nombre').value;
	var primer_ape=get('primer_ape').value;
	var segundo_ape=get('segundo_ape').value;
	var historia=get('historia').value;
	var estudio=get('estudio').value;
	var contrato=get('contrato').value;
	var ingreso=get('ingreso').value;
	var factura=get('factura').value;
	
	var parametros='&cedula='+cedula+"&nombre="+nombre+"&primer_ape="+primer_ape+"&segundo_ape="+segundo_ape+"&historia="+historia+"&estudio="+estudio+"&contrato="+contrato+"&ingreso="+ingreso+"&factura="+factura;

	var url="../controlador/facturacion/fichero.php?operacion=pacientesUrgencia"+parametros;
	ajax(url, "listadoPacientes","","");
}

function consultaHospitalizacion(){
	var cedula=get('cedula').value;
	var nombre=get('nombre').value;
	var primer_ape=get('primer_ape').value;
	var segundo_ape=get('segundo_ape').value;
	var historia=get('historia').value;
	var estudio=get('estudio').value;
	var contrato=get('contrato').value;
	var ingreso=get('ingreso').value;
	var factura=get('factura').value;
	
	var parametros='&cedula='+cedula+"&nombre="+nombre+"&primer_ape="+primer_ape+"&segundo_ape="+segundo_ape+"&historia="+historia+"&estudio="+estudio+"&contrato="+contrato+"&ingreso="+ingreso+"&factura="+factura;

	var url="../controlador/facturacion/fichero.php?operacion=pacientesHospitalizacion"+parametros;
	ajax(url, "listadoPacientes","","");
}

function buscarTodosPacientes(){
	var cedula=get('cedula').value;
	var nombre=get('nombre').value;
	var primer_ape=get('primer_ape').value;
	var segundo_ape=get('segundo_ape').value;
	var historia=get('historia').value;
	var estudio=get('estudio').value;
	var contrato=get('contrato').value;
	var ingreso=get('ingreso').value;
	var factura=get('factura').value;
	
	var parametros='&cedula='+cedula+"&nombre="+nombre+"&primer_ape="+primer_ape+"&segundo_ape="+segundo_ape+"&historia="+historia+"&estudio="+estudio+"&contrato="+contrato+"&ingreso="+ingreso+"&factura="+factura;
	
	var url="../controlador/facturacion/fichero.php?operacion=buscarTodosPacientes"+parametros;
	ajax(url, "listadoPacientes","","");
}

function notify(msg,type){
		var speed=500;
		var speedOUT=10000;
		var fadeSpeed=3000;
		
       //Borra cualquier mensaje existente
       $('.notify').remove();

       //Si el temporizador para hacer desaparecer el mensaje está
       //activo, lo desactivamos.
       if (typeof fade != "undefined"){
           clearTimeout(fade);
       }
	   
	   var colorLetter="#21432B";
	   if(type=="error"){
			colorLetter="#FFFFFF";
	   }
       //Creamos la notificación con la clase (type) y el texto (msg)
       $('body').append('<div class="notify '+type+'" style="display:none;position:fixed;left:10"><p><font color="'+colorLetter+'"><b>'+msg+'</b></font></p></div>');

       //Calculamos la altura de la notificación.
       notifyHeight = $('.notify').outerHeight();

       //Creamos la animación en la notificación con la velocidad
       //que pasamos por el parametro speed
       $('.notify').css('top',-notifyHeight).animate({top:10,opacity:'toggle'},speed);

       //Creamos el temporizador para hacer desaparecer la notificación
       //con el tiempo almacenado en el parametro fadeSpeed
       fade = setTimeout(function(){

           $('.notify').animate({top:notifyHeight+10,opacity:'toggle'}, speed);

       }, speedOUT);

   }

/************************************************************************************************/

function cargarLotes(codMed,stock,cantidad,idDivLt,DocumentoRef,ingreso,ArticuloInventario){
	
	ArticuloInventario=get('ArtInv'+codMed).value;
	
	ajaxPOST("../../controlador/farmacia/farmacia.php", "subModal",'visibleDivLote();','',"operacion=cargarLotes&codMed="+codMed+'&stock='+stock+"&cantidad="+cantidad+"&idDivLt="+idDivLt+"&DocumentoRef="+DocumentoRef+"&ingreso="+ingreso+'&ArticuloInventario='+ArticuloInventario);
}
function visibleDivLote(){
	get('subModal').style.display="";	
}

function cerrarCargarLote(){
	get('subModal').style.display="none";		
}

function agregarLotes(){
	var nro_lotes=get('nro_lotes').value;
	var cantidadMaxDevolver=get('cantidadMaxDevolver').value;
	var cantidadADevolver=0.0;
	var idDivLtSel=get('idDivLtSel').value;
	var cajas='';
	var loteMedSel=get('loteMedSel').value;
	var loteStockSel=get('loteStockSel').value;	
	
	var loteArtInv=get('ArtInv'+loteMedSel).value;


	var error=false;
	for(i=0;i<nro_lotes;i++){
		if(trim(get('lt_cantidad'+i).value)!=''){
			
			cantidadADevolver=(parseFloat(cantidadADevolver)+parseFloat(get('lt_cantidad'+i).value));
			cajas+=get('codLt'+i).value+','+get('numLt'+i).value+','+get('lt_cantidad'+i).value+','+get('ltMed'+i).value+','+get('ltStock'+i).value+','+get('ltDocRef'+i).value+','+get('ltIngreso'+i).value+'|';
			if(parseFloat(cantidadADevolver)>parseFloat(cantidadMaxDevolver)){
				error=true;
				break;	
			}		
		}
	}

	if(error){
		notify("Verifique que las cantidades a devolver no superen las permitidas","error");	
	}else{
		ajaxPOST("../../controlador/farmacia/farmacia.php", 'divLt'+idDivLtSel,'cerrarCargarLote()','',"operacion=setCajasLotes&loteMedSel="+loteMedSel+"&loteStockSel="+loteStockSel+"&cajas="+cajas+"&idDivLtSel="+idDivLtSel+'&loteArtInv='+loteArtInv);		


	}
}

function eliminarCanLoteSel(idDivLt,codMed,codLote){
		ajaxPOST("../../controlador/farmacia/farmacia.php", 'divLt'+idDivLt,'','',"operacion=eliminarCanLoteSel&codMed="+codMed+"&idDivLt="+idDivLt+"&codLote="+codLote);			
}

function pagarProcedimientos(){
	var contrato=get('id_contrato').value;	
	var idCita=get('id_cita').value;
 	respGetTiposManuales=function(){
		var resp=JSON.parse(_ResquestAJAX);
	 
		var idprogramMultiplesCitas = "";
		if( $(".check_multiples").val()){
			$(".check_multiples").each(function(){
				if(this.value != "" && $(this).attr('checked')){
					idprogramMultiplesCitas += this.value +",";
				}
				
			});
		}

		if(resp.respuesta == 1){
			alert("No se puede generar el recibo para contrato particular, verifique que la factura este cerada.");
		}else{
		var contrato=get('id_contrato').value;	
		var estrato=get('id_estrato').value;
		var ufuncional=get('ufuncional').value;
	
		if(trim(estrato)==''){
			notify("Seleccione un estrato","error");
		}else if(trim(contrato)==''){
			notify("Seleccione un contrato","error");
		}else if(trim(ufuncional)==''){
			notify("Seleccione unidad funcional","error");
		}else{
			var subsidio=get('subsidio'+estrato).value;
			var vlr_maximo=get('maximo'+estrato).value;

			ajaxPOST("../../controlador/citas/citas.php", 'divPagos','verFormPagos();','',"operacion=cargarFormPagos&contrato="+contrato+'&estrato='+estrato+'&idCita='+idCita+'&subsidio='+subsidio+'&vlr_maximo='+vlr_maximo+'&ufuncional='+ufuncional+'&idprogramMultiplesCitas='+idprogramMultiplesCitas);	
		}
		}
 		
	}		
	ajaxPOST("../../Vistas/Citas/CtrlAplazarCita.php",'','respGetTiposManuales()','',"operacion=parametro_bloqueo_pagos&contrato="+contrato,'');


}
function verFormPagos(){

	get('mod').style.display="";
	get('divPagos').style.display="";
	calcularSaldo();
}

function cerrarDivPago(){
	get('mod').style.display="none";
	get('divPagos').style.display="none";
	ajaxPOST("../../controlador/citas/citas.php", 'contenidoPago','','',"operacion=mostrarPagos");			
}

function addPago(){
	var tipo_cita = $("#tipo_cita").val();
	var estrato = null;
	var subsidio = null;
	var vlr_maximo = null;
	var autoid = $("#autoid_pago").val();
	/* Para cita grupal. */
	if(tipo_cita == 2){
		
		estrato=$("#id_estrato_"+autoid).val();
		subsidio=get('subsidio_'+estrato+'_'+autoid).value;
		vlr_maximo=get('maximo_'+estrato+'_'+autoid).value;
	}else{
		estrato=get('id_estrato').value;	
		subsidio=get('subsidio'+estrato).value;
		vlr_maximo=get('maximo'+estrato).value;
	}

	id_cita=get('id_cita').value;
	IdAnticipoUsado=$("#IdAnticipoUsado").val();

	
	if(subsidio=="-1"&&vlr_maximo=="0"){
		$("#valorSaldoPagar").val(($.trim($("#ValorPago").val())==''?0:$("#ValorPago").val()));
		get('vlr_pagado'+autoid).value=0;
	}
		
		
	var valorSaldoPagar=get('valorSaldoPagar').value;
	var vlr_pagado=0;
	try{
		vlr_pagado=get('vlr_pagado'+autoid).value;
	}catch(e){}
	
	var tipoServPago='';
	try{
		tipoServPago=get('tipoServPago').value;
	}catch(e){}
		
	var cajaPago=get('cajaPago').value;
	var tipoPago=get('tipoPago').value;
	var valorPago=get('ValorPago').value;
	var tipoServPago=$("#tipoServPago").val();
	var observacion=$("#observacion").val();

	if(trim(cajaPago)==''){
		notify("Seleccione una caja","error");	
	}else if(trim(tipoPago)==''){
		notify("Seleccione un tipo de pago","error");
	}else if($.trim(tipoServPago)==''){
		notify("Seleccione un tipo de servicio","error");
	}else if(trim(valorPago)==''){
		notify("Ingrese el monto a pagar","error");
	}else{
		ajaxPOST("../../controlador/citas/citas.php", 'PagosListContainer',"pago_persona('"+autoid+"');"+((subsidio=="-1"&&vlr_maximo=="0")?'calcularSaldo();':'calcularSaldo();calcularCambio();'),'',
		"operacion=addPago&caja="+cajaPago+'&tipoPago='+tipoPago+'&valorPago='+valorPago+'&valorSaldoPagar='+valorSaldoPagar+
		'&vlr_pagado='+vlr_pagado+'&tipoServPago='+tipoServPago+"&id_cita="+id_cita+"&IdAnticipoUsado="+IdAnticipoUsado+
		"&observacion="+observacion+"&tipo_cita="+tipo_cita+"&autoid="+autoid);	
	}
}

function pago_persona(id){

	$("#vlr_pagado"+id).val();
	$("#pago_"+id).html("$"+$("#vlr_pagado"+id).val());
}

function calcularSaldo(){ 
	var grupal = $("#confirmada_agrupada").val();
	var autoid = null;
	if(grupal == 1){
		autoid = $("#autoid_pago").val();
		estrato=$("#id_estrato_"+autoid).val();
		subsidio=get('subsidio_'+estrato+'_'+autoid).value;
		vlr_maximo=get('maximo_'+estrato+'_'+autoid).value;
	}else{
		estrato=get('id_estrato').value;	
		subsidio=get('subsidio'+estrato).value;
		vlr_maximo=get('maximo'+estrato).value;
	}

	if(subsidio=="-1"&&vlr_maximo=="0" && grupal != 1){
		$("#valorSaldoPagar").val(get('vlr_pagado').value);
	}else if(subsidio=="-1"&&vlr_maximo=="0" && grupal == 1){
		$("#valorSaldoPagar").val(get('vlr_pagado'+autoid).value);
	}
 
	get('SaldoSpanHtml').innerHTML=parseFloat(get('valorSaldoPagar').value)-parseFloat(get('vlr_pagado'+autoid).value);
	get('ValorPago').select();
}

function calcularCambio(){
	get('CambioSpanHtml').innerHTML=get('cambio').value;
	get('ValorPago').value='';
	get('ValorPago').select();
}

function delItemPago(id){
	var tipo_cita = $("#tipo_cita").val();
 
	ajaxPOST("../../controlador/citas/citas.php", 'PagosListContainer','calcularSaldo();calcularCambio();','',"operacion=delPago&idPago="+id+"&tipo_cita="+tipo_cita);		
}

function finalizarPago(){
	var grupal = $("#confirmada_agrupada").val();
	var vlr_pagado = null ;
	var autoid = null;
	/* Si es una cita grupal*/
	if( grupal == 1 ) {
		autoid = $("#autoid_pago").val();
		vlr_pagado = get('vlr_pagado'+autoid).value;
	}else{
		vlr_pagado = get('vlr_pagado').value;
	}
	if(parseFloat(get('valorSaldoPagar').value)!=parseFloat(vlr_pagado)){
		notify("Paciente con saldo pendiente","error");
	}else{
		ajaxPOST("../../controlador/citas/citas.php", 'contenidoPago','cerrarDivPago();','',"operacion=mostrarPagos");			
	}
}


function guardarRipsNominal(){
	get('res_div_nominal').innerHTML='';
	var input_rips_nominal=document.getElementsByClassName('rips_nominal');
	var variables='';guardar=true;
	for(var i=0;i<input_rips_nominal.length;i++){
		if($('#'+input_rips_nominal.item(i).id).hasClass('requerido')){
			if(trim(input_rips_nominal.item(i).value)==''){
				$('#'+input_rips_nominal.item(i).id).addClass('errorRequerido');
				guardar=false;
			}else{
				$('#'+input_rips_nominal.item(i).id).removeClass('errorRequerido');
			}
		}
		//alert(input_rips_nominal.item(i).className);
		variables+='&'+input_rips_nominal.item(i).id+'='+input_rips_nominal.item(i).value;
	}
	if(guardar){
		
		ajaxPOST("../../controlador/HC/historiaClinica.php","res_div_nominal","respuestaRips()","","operacion=guardarRipsNominal"+variables);	
	}else{
		notify("Todos los campos marcados en rojo son obligatorios.","error");
	}
}
function respuestaRips(){
		$("#aplica_rips_nominal").attr('guardo','1');
	}
function verificarRelacion(id,name_input){
	var relacion=document.getElementsByClassName('relacion'+id);
	
	//alert(name_input);
	for(var i=0;i<relacion.length;i++){
		
		var arrayValor=relacion.item(i).getAttribute('cuando_valor').split("|"); 				
		for(var j=0;j<arrayValor.length;j++){
			if(get(name_input).value==arrayValor[j]){
				get(relacion.item(i).id).value=relacion.item(i).getAttribute('set_valor');
				get(relacion.item(i).id).disabled=true;
				
				break;
			}else{
					get(relacion.item(i).id).value='';
					get(relacion.item(i).id).disabled=false;
			}
		//	alert("'"+nro_dias[i]+"'");
		}
//		alert(relacion.item(i).id+' => '+relacion.item(i).getAttribute('cuando_valor'));
	}
}


var patron = new Array(4,2,2)
function mascara(d,sep,pat,nums){
if(d.valant != d.value){
	val = d.value
	largo = val.length
	val = val.split(sep)
	val2 = ''
	for(r=0;r<val.length;r++){
		val2 += val[r]	
	}
	if(nums){
		for(z=0;z<val2.length;z++){
			if(isNaN(val2.charAt(z))){
				letra = new RegExp(val2.charAt(z),"g")
				val2 = val2.replace(letra,"")
			}
		}
	}
	val = ''
	val3 = new Array()
	for(s=0; s<pat.length; s++){
		val3[s] = val2.substring(0,pat[s])
		val2 = val2.substr(pat[s])
	}
	for(q=0;q<val3.length; q++){
		if(q ==0){
			val = val3[q]
		}
		else{
			if(val3[q] != ""){
				val += sep + val3[q]
				}
		}
	}
	d.value = val
	d.valant = val
	}
}


function setearCalendario(id){
	Calendario=document.getElementsByClassName('Calendario');
	for(var i=0;i<Calendario.length;i++){
	$("#"+Calendario.item(i).id).datepicker({
			 showOn: "both",
			 buttonImage: "../../Imagenes/calendario 01.png",
			 buttonImageOnly: true,
			 changeMonth: true,
			 changeYear: true,
			 minDate: new Date(1800,1-1,1), 
			 yearRange: "-110:+5",
			 dateformat: "yy-mm-dd",
			 showAnim: "fadeIn"
	});
	$("#"+Calendario.item(i).id).datepicker("option","dateFormat","yy-mm-dd");
	$("#"+Calendario.item(i).id).val(Calendario.item(i).getAttribute('valorDefecto'));
	}
}

function setearCalendarioBarra(){
	Calendario=document.getElementsByClassName('Calendario');
	for(var i=0;i<Calendario.length;i++){
	$("#"+Calendario.item(i).id).datepicker({
			 showOn: "both",
			 buttonImage: "../../Imagenes/calendario 01.png",
			 buttonImageOnly: true,
			 changeMonth: true,
			 changeYear: true,
			 minDate: new Date(1800,1-1,1), 
			 yearRange: "-110:+5",
			 dateformat: "yy/mm/dd",
			 showAnim: "fadeIn"
	});
	$valCal=$("#"+Calendario.item(i).id).val();
	$("#"+Calendario.item(i).id).datepicker("option","dateFormat","yy/mm/dd");
	$("#"+Calendario.item(i).id).val($valCal);
	}
}


function guardarEncuestaSatisfaccion(){
	var encuesta=document.getElementsByClassName('encuesta');
	var guardar=true;
	var parametros='';
	for(var i=0;i<encuesta.length;i++){
		if(encuesta.item(i).type=='radio'){					
			if($("input:radio[name='"+encuesta.item(i).id+"']:checked").val()==undefined){
				if($("#"+encuesta.item(i).id).hasClass('requerido')){
					guardar=false;	
					$("#tab"+encuesta.item(i).id).addClass('require');
				}else{
					$("#tab"+encuesta.item(i).id).removeClass('require');	
				}				
			}else{
				parametros+='&'+encuesta.item(i).id+'='+$("input:radio[name='"+encuesta.item(i).id+"']:checked").val();
				$("#tab"+encuesta.item(i).id).removeClass('require');	
			}			
			
		}else if(encuesta.item(i).type=='select-one'){
			if(trim(encuesta.item(i).value)==''){
				if($("#"+encuesta.item(i).id).hasClass('requerido')){
					guardar=false;
					$("#"+encuesta.item(i).id).addClass('require');
				}
			}else{
				parametros+='&'+encuesta.item(i).id+'='+encuesta.item(i).value;
				$("#"+encuesta.item(i).id).removeClass('require');
			}
		}
		else if(encuesta.item(i).type=='textarea'){
			if(trim(encuesta.item(i).value)==''){
				if($("#"+encuesta.item(i).id).hasClass('requerido')){
					guardar=false;
					$("#"+encuesta.item(i).id).addClass('require');
				}
			}else{
				parametros+='&'+encuesta.item(i).id+'='+encuesta.item(i).value;
				$("#"+encuesta.item(i).id).removeClass('require');
			}
		}
	}
	
	if(guardar){
		get('guardarEncuesta').disabled=true;
		ajaxPOST("../../controlador/Estadistica/encuestaSatisfaccion.php","respuesta","enabledBtnGE()","","operacion=save"+parametros);
	}else{
		notify("Verique los campos obligatorios e intente nuevamente.","error");
	}
}

function enabledBtnGE(){
	get('guardarEncuesta').disabled=false;
	alert("Gracias por responder nuestra encuesta, seguiremos trabajando para ofrecerles un mejor servicio.");
	window.open('../../ctrl_usuario.php?oper=close', '_parent');
}


function guardarConfInicial(){
	
	var requeridos=document.getElementsByClassName('required');
	var ok=true;
	for(var i=0;i<requeridos.length;i++){
		if(trim(requeridos.item(i).value)==""){
			ok=false;
			$('#'+requeridos.item(i).id).addClass('requerido');
		}else{
			$('#'+requeridos.item(i).id).removeClass('requerido');
		}
	}
	if(ok){
		get('button').disabled=true;
		ajaxPOST("controlador/Configuracion/configuracionInicial.php","respuesta","nextConf('confParametrosGenerales');","","operacion=saveConfInicial&"+$("#form").serialize());
	}else{
		try{
			generate("error",'Verifique los campos obligatorios');			
		}catch(e){ocultarNotify();}
	}
}
function ocultarNotify(time){
	if(time==undefined){
		time=5000;
	}
	setTimeout("$('#noty_top_layout_container').remove();",time);
	
}
function guardarParametrosIniciales(){
	
	var requeridos=document.getElementsByClassName('required');
	var ok=true;
	for(var i=0;i<requeridos.length;i++){
		if(trim(requeridos.item(i).value)==""){
			ok=false;
			$('#'+requeridos.item(i).id).addClass('requerido');
		}else{
			$('#'+requeridos.item(i).id).removeClass('requerido');
		}
	}
	if(ok){
		get('button').disabled=true;
		ajaxPOST("controlador/Configuracion/configuracionInicial.php","respuesta","nextConf('salirIniciando')","","operacion=saveParametrosIniciales&"+$("#form").serialize());
	}else{
		try{
			generate("error",'Verifique los campos obligatorios');			
		}catch(e){ocultarNotify(10000);}
	}
}

function nextConf(metodo){
	get('button').disabled=false;
	var result=get('result').value;
	if(trim(result)==''){
		$('#button').val('Siguente');
		$('#button').attr('onClick','javascript:$("#pagina").fadeOut("slow",function(){'+metodo+'();});');
	}
}

function salirIniciando(){
	window.open("../index.php","_self");
}

function confParametrosGenerales(){
	ajaxPOST("validaciones/parametros.php","pagina",'$("#pagina").fadeIn("slow", function() {  });',"","");
	
}


function ocultarDivTime(nameDivRes){
	setTimeout('$("#'+nameDivRes+'").hide(1000,function(){get("'+nameDivRes+'").innerHTML="";$("#'+nameDivRes+'").show();});', 5000);	
}


/*******************************************************************************************/
/**************************** INICIO NUEVA NOTIFICACION ***************************************/
/*******************************************************************************************/
function generate(type,msg) {
	try{
		var colori='';
		var colorf='';
		if(type=='error'){
			var colori='<font style="color:#FFF">';
			var colorf='</font>';	
		}
		var n = noty({
			text: colori+msg+colorf,
			type: type,
			dismissQueue: true,
			layout: 'top',
			theme: 'defaultTheme',
			timeout: 5000, 
			maxVisible: 5000,		
			animation: {
			open: {height: 'toggle'},
			close: {height: 'toggle'},
			easing: 'swing',
			speed: 500,
			
		},
		});
		console.log('html: '+n.options.id);
	}catch(e){}
  }
  
  /**
 * noty - jQuery Notification Plugin v2.1.0
 * Contributors: https://github.com/needim/noty/graphs/contributors
 *
 * Examples and Documentation - http://needim.github.com/noty/
 *
 * Licensed under the MIT licenses:
 * http://www.opensource.org/licenses/mit-license.php
 *
 **/

if (typeof Object.create !== 'function') {
    Object.create = function (o) {
        function F() {
        }

        F.prototype = o;
        return new F();
    };
}

(function ($) {

    var NotyObject = {

        init:function (options) {

            // Mix in the passed in options with the default options
            this.options = $.extend({}, $.noty.defaults, options);

            this.options.layout = (this.options.custom) ? $.noty.layouts['inline'] : $.noty.layouts[this.options.layout];
            this.options.theme = $.noty.themes[this.options.theme];

            delete options.layout;
            delete options.theme;

            this.options = $.extend({}, this.options, this.options.layout.options);
            this.options.id = 'noty_' + (new Date().getTime() * Math.floor(Math.random() * 1000000));

            this.options = $.extend({}, this.options, options);

            // Build the noty dom initial structure
            this._build();

            // return this so we can chain/use the bridge with less code.
            return this;
        }, // end init

        _build:function () {

            // Generating noty bar
            var $bar = $('<div class="noty_bar"></div>').attr('id', this.options.id);
            $bar.append(this.options.template).find('.noty_text').html(this.options.text);

            this.$bar = (this.options.layout.parent.object !== null) ? $(this.options.layout.parent.object).css(this.options.layout.parent.css).append($bar) : $bar;

            // Set buttons if available
            if (this.options.buttons) {

                // If we have button disable closeWith & timeout options
                this.options.closeWith = [];
                this.options.timeout = false;

                var $buttons = $('<div/>').addClass('noty_buttons');

                (this.options.layout.parent.object !== null) ? this.$bar.find('.noty_bar').append($buttons) : this.$bar.append($buttons);

                var self = this;

                $.each(this.options.buttons, function (i, button) {
                    var $button = $('<button/>').addClass((button.addClass) ? button.addClass : 'gray').html(button.text)
                        .appendTo(self.$bar.find('.noty_buttons'))
                        .bind('click', function () {
                            if ($.isFunction(button.onClick)) {
                                button.onClick.call($button, self);
                            }
                        });
                });
            }

            // For easy access
            this.$message = this.$bar.find('.noty_message');
            this.$closeButton = this.$bar.find('.noty_close');
            this.$buttons = this.$bar.find('.noty_buttons');

            $.noty.store[this.options.id] = this; // store noty for api

        }, // end _build

        show:function () {

            var self = this;

            $(self.options.layout.container.selector).append(self.$bar);

            self.options.theme.style.apply(self);

            ($.type(self.options.layout.css) === 'function') ? this.options.layout.css.apply(self.$bar) : self.$bar.css(this.options.layout.css || {});

            self.$bar.addClass(self.options.layout.addClass);

            self.options.layout.container.style.apply($(self.options.layout.container.selector));

            self.options.theme.callback.onShow.apply(this);

            if ($.inArray('click', self.options.closeWith) > -1)
                self.$bar.css('cursor', 'pointer').one('click', function (evt) {
                    self.stopPropagation(evt);
                    if (self.options.callback.onCloseClick) {
                        self.options.callback.onCloseClick.apply(self);
                    }
                    self.close();
                });

            if ($.inArray('hover', self.options.closeWith) > -1)
                self.$bar.one('mouseenter', function () {
                    self.close();
                });

            if ($.inArray('button', self.options.closeWith) > -1)
                self.$closeButton.one('click', function (evt) {
                    self.stopPropagation(evt);
                    self.close();
                });

            if ($.inArray('button', self.options.closeWith) == -1)
                self.$closeButton.remove();

            if (self.options.callback.onShow)
                self.options.callback.onShow.apply(self);

            self.$bar.animate(
                self.options.animation.open,
                self.options.animation.speed,
                self.options.animation.easing,
                function () {
                    if (self.options.callback.afterShow) self.options.callback.afterShow.apply(self);
                    self.shown = true;
                });

            // If noty is have a timeout option
            if (self.options.timeout)
                self.$bar.delay(self.options.timeout).promise().done(function () {
                    self.close();
                });

            return this;

        }, // end show

        close:function () {

            if (this.closed) return;
            if (this.$bar && this.$bar.hasClass('i-am-closing-now')) return;

            var self = this;

            if (!this.shown) { // If we are still waiting in the queue just delete from queue
                var queue = [];
                $.each($.noty.queue, function (i, n) {
                    if (n.options.id != self.options.id) {
                        queue.push(n);
                    }
                });
                $.noty.queue = queue;
                return;
            }

            self.$bar.addClass('i-am-closing-now');

            if (self.options.callback.onClose) {
                self.options.callback.onClose.apply(self);
            }

            self.$bar.clearQueue().stop().animate(
                self.options.animation.close,
                self.options.animation.speed,
                self.options.animation.easing,
                function () {
                    if (self.options.callback.afterClose) self.options.callback.afterClose.apply(self);
                })
                .promise().done(function () {

                    // Modal Cleaning
                    if (self.options.modal) {
                        $.notyRenderer.setModalCount(-1);
                        if ($.notyRenderer.getModalCount() == 0) $('.noty_modal').fadeOut('fast', function () {
                            $(this).remove();
                        });
                    }

                    // Layout Cleaning
                    $.notyRenderer.setLayoutCountFor(self, -1);
                    if ($.notyRenderer.getLayoutCountFor(self) == 0) $(self.options.layout.container.selector).remove();

                    // Make sure self.$bar has not been removed before attempting to remove it
                    if (typeof self.$bar !== 'undefined' && self.$bar !== null ) {
                        self.$bar.remove();
                        self.$bar = null;
                        self.closed = true;
                    }

                    delete $.noty.store[self.options.id]; // deleting noty from store

                    self.options.theme.callback.onClose.apply(self);

                    if (!self.options.dismissQueue) {
                        // Queue render
                        $.noty.ontap = true;

                        $.notyRenderer.render();
                    }

					if (self.options.maxVisible > 0 && self.options.dismissQueue) {
						$.notyRenderer.render();
					}
                })

        }, // end close

        setText:function (text) {
            if (!this.closed) {
                this.options.text = text;
                this.$bar.find('.noty_text').html(text);
            }
            return this;
        },

        setType:function (type) {
            if (!this.closed) {
                this.options.type = type;
                this.options.theme.style.apply(this);
                this.options.theme.callback.onShow.apply(this);
            }
            return this;
        },

        setTimeout:function (time) {
            if (!this.closed) {
                var self = this;
                this.options.timeout = time;
                self.$bar.delay(self.options.timeout).promise().done(function () {
                    self.close();
                });
            }
            return this;
        },

        stopPropagation:function (evt) {
            evt = evt || window.event;
            if (typeof evt.stopPropagation !== "undefined") {
                evt.stopPropagation();
            } else {
                evt.cancelBubble = true;
            }
        },

        closed:false,
        shown:false

    }; // end NotyObject

    $.notyRenderer = {};

    $.notyRenderer.init = function (options) {

        // Renderer creates a new noty
        var notification = Object.create(NotyObject).init(options);

        (notification.options.force) ? $.noty.queue.unshift(notification) : $.noty.queue.push(notification);

        $.notyRenderer.render();

        return ($.noty.returns == 'object') ? notification : notification.options.id;
    };

    $.notyRenderer.render = function () {

        var instance = $.noty.queue[0];

        if ($.type(instance) === 'object') {
            if (instance.options.dismissQueue) {
				if (instance.options.maxVisible > 0) {
					if ($(instance.options.layout.container.selector + ' li').length < instance.options.maxVisible) {
						$.notyRenderer.show($.noty.queue.shift());
					} else {

					}
				} else {
					$.notyRenderer.show($.noty.queue.shift());
				}
            } else {
                if ($.noty.ontap) {
                    $.notyRenderer.show($.noty.queue.shift());
                    $.noty.ontap = false;
                }
            }
        } else {
            $.noty.ontap = true; // Queue is over
        }

    };

    $.notyRenderer.show = function (notification) {

        if (notification.options.modal) {
            $.notyRenderer.createModalFor(notification);
            $.notyRenderer.setModalCount(+1);
        }

        // Where is the container?
        if ($(notification.options.layout.container.selector).length == 0) {
            if (notification.options.custom) {
                notification.options.custom.append($(notification.options.layout.container.object).addClass('i-am-new'));
            } else {
                $('body').append($(notification.options.layout.container.object).addClass('i-am-new'));
            }
        } else {
            $(notification.options.layout.container.selector).removeClass('i-am-new');
        }

        $.notyRenderer.setLayoutCountFor(notification, +1);

        notification.show();
    };

    $.notyRenderer.createModalFor = function (notification) {
        if ($('.noty_modal').length == 0)
            $('<div/>').addClass('noty_modal').data('noty_modal_count', 0).css(notification.options.theme.modal.css).prependTo($('body')).fadeIn('fast');
    };

    $.notyRenderer.getLayoutCountFor = function (notification) {
        return $(notification.options.layout.container.selector).data('noty_layout_count') || 0;
    };

    $.notyRenderer.setLayoutCountFor = function (notification, arg) {
        return $(notification.options.layout.container.selector).data('noty_layout_count', $.notyRenderer.getLayoutCountFor(notification) + arg);
    };

    $.notyRenderer.getModalCount = function () {
        return $('.noty_modal').data('noty_modal_count') || 0;
    };

    $.notyRenderer.setModalCount = function (arg) {
        return $('.noty_modal').data('noty_modal_count', $.notyRenderer.getModalCount() + arg);
    };

    // This is for custom container
    $.fn.noty = function (options) {
        options.custom = $(this);
        return $.notyRenderer.init(options);
    };

    $.noty = {};
    $.noty.queue = [];
    $.noty.ontap = true;
    $.noty.layouts = {};
    $.noty.themes = {};
    $.noty.returns = 'object';
    $.noty.store = {};

    $.noty.get = function (id) {
        return $.noty.store.hasOwnProperty(id) ? $.noty.store[id] : false;
    };

    $.noty.close = function (id) {
        return $.noty.get(id) ? $.noty.get(id).close() : false;
    };

    $.noty.setText = function (id, text) {
        return $.noty.get(id) ? $.noty.get(id).setText(text) : false;
    };

    $.noty.setType = function (id, type) {
        return $.noty.get(id) ? $.noty.get(id).setType(type) : false;
    };

    $.noty.clearQueue = function () {
        $.noty.queue = [];
    };

    $.noty.closeAll = function () {
        $.noty.clearQueue();
        $.each($.noty.store, function (id, noty) {
            noty.close();
        });
    };

    var windowAlert = window.alert;

    $.noty.consumeAlert = function (options) {
        window.alert = function (text) {
            if (options)
                options.text = text;
            else
                options = {text:text};

            $.notyRenderer.init(options);
        };
    };

    $.noty.stopConsumeAlert = function () {
        window.alert = windowAlert;
    };

    $.noty.defaults = {
        layout:'top',
        theme:'defaultTheme',
        type:'alert',
        text:'',
        dismissQueue:true,
        template:'<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
        animation:{
            open:{height:'toggle'},
            close:{height:'toggle'},
            easing:'swing',
            speed:500
        },
        timeout:false,
        force:false,
        modal:false,
        maxVisible:5000,
        closeWith:['click'],
        callback:{
            onShow:function () {
            },
            afterShow:function () {
            },
            onClose:function () {
            },
            afterClose:function () {
            },
            onCloseClick:function () {
            }
        },
        buttons:false
    };

    $(window).resize(function () {
        $.each($.noty.layouts, function (index, layout) {
            layout.container.style.apply($(layout.container.selector));
        });
    });

})(jQuery);

// Helpers
window.noty = function noty(options) {

    // This is for BC  -  Will be deleted on v2.2.0
    var using_old = 0
        , old_to_new = {
            'animateOpen':'animation.open',
            'animateClose':'animation.close',
            'easing':'animation.easing',
            'speed':'animation.speed',
            'onShow':'callback.onShow',
            'onShown':'callback.afterShow',
            'onClose':'callback.onClose',
            'onCloseClick':'callback.onCloseClick',
            'onClosed':'callback.afterClose'
        };

    jQuery.each(options, function (key, value) {
        if (old_to_new[key]) {
            using_old++;
            var _new = old_to_new[key].split('.');

            if (!options[_new[0]]) options[_new[0]] = {};

            options[_new[0]][_new[1]] = (value) ? value : function () {
            };
            delete options[key];
        }
    });

    if (!options.closeWith) {
        options.closeWith = jQuery.noty.defaults.closeWith;
    }

    if (options.hasOwnProperty('closeButton')) {
        using_old++;
        if (options.closeButton) options.closeWith.push('button');
        delete options.closeButton;
    }

    if (options.hasOwnProperty('closeOnSelfClick')) {
        using_old++;
        if (options.closeOnSelfClick) options.closeWith.push('click');
        delete options.closeOnSelfClick;
    }

    if (options.hasOwnProperty('closeOnSelfOver')) {
        using_old++;
        if (options.closeOnSelfOver) options.closeWith.push('hover');
        delete options.closeOnSelfOver;
    }

    if (options.hasOwnProperty('custom')) {
        using_old++;
        if (options.custom.container != 'null') options.custom = options.custom.container;
    }

    if (options.hasOwnProperty('cssPrefix')) {
        using_old++;
        delete options.cssPrefix;
    }

    if (options.theme == 'noty_theme_default') {
        using_old++;
        options.theme = 'defaultTheme';
    }

    if (!options.hasOwnProperty('dismissQueue')) {
        options.dismissQueue = jQuery.noty.defaults.dismissQueue;
    }

    if (!options.hasOwnProperty('maxVisible')) {
        options.maxVisible = jQuery.noty.defaults.maxVisible;
    }

    if (options.buttons) {
        jQuery.each(options.buttons, function (i, button) {
            if (button.click) {
                using_old++;
                button.onClick = button.click;
                delete button.click;
            }
            if (button.type) {
                using_old++;
                button.addClass = button.type;
                delete button.type;
            }
        });
    }

    if (using_old) {
        if (typeof console !== "undefined" && console.warn) {
            console.warn('You are using noty v2 with v1.x.x options. @deprecated until v2.2.0 - Please update your options.');
        }
    }

    // console.log(options);
    // End of the BC

    return jQuery.notyRenderer.init(options);
}

;(function($) {

	$.noty.layouts.top = {
		name: 'top',
		options: {},
		container: {
			object: '<ul id="noty_top_layout_container" />',
			selector: 'ul#noty_top_layout_container',
			style: function() {
				$(this).css({
					top: 0,
					left: '5%',
					position: 'fixed',
					width: '90%',
					height: 'auto',
					margin: 0,
					padding: 0,
					listStyleType: 'none',
					zIndex: 9999999
				});
			}
		},
		parent: {
			object: '<li />',
			selector: 'li',
			css: {}
		},
		css: {
			display: 'none'
		},
		addClass: ''
	};

})(jQuery);

;(function($) {

	$.noty.themes.defaultTheme = {
		name: 'defaultTheme',
		helpers: {
			borderFix: function() {
				if (this.options.dismissQueue) {
					var selector = this.options.layout.container.selector + ' ' + this.options.layout.parent.selector;
					switch (this.options.layout.name) {
						case 'top':
							$(selector).css({borderRadius: '0px 0px 0px 0px'});
							$(selector).last().css({borderRadius: '0px 0px 5px 5px'}); break;
						case 'topCenter': case 'topLeft': case 'topRight':
						case 'bottomCenter': case 'bottomLeft': case 'bottomRight':
						case 'center': case 'centerLeft': case 'centerRight': case 'inline':
							$(selector).css({borderRadius: '0px 0px 0px 0px'});
							$(selector).first().css({'border-top-left-radius': '5px', 'border-top-right-radius': '5px'});
							$(selector).last().css({'border-bottom-left-radius': '5px', 'border-bottom-right-radius': '5px'}); break;
						case 'bottom':
							$(selector).css({borderRadius: '0px 0px 0px 0px'});
							$(selector).first().css({borderRadius: '5px 5px 0px 0px'}); break;
						default: break;
					}
				}
			}
		},
		modal: {
			css: {
				position: 'fixed',
				width: '100%',
				height: '100%',
				backgroundColor: '#000',
				zIndex: 10000,
				opacity: 0.6,
				display: 'none',
				left: 0,
				top: 0
			}
		},
		style: function() {

			this.$bar.css({
				overflow: 'hidden',
				background: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAYAAAAPOoFWAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAPZJREFUeNq81tsOgjAMANB2ov7/7ypaN7IlIwi9rGuT8QSc9EIDAsAznxvY4pXPKr05RUE5MEVB+TyWfCEl9LZApYopCmo9C4FKSMtYoI8Bwv79aQJU4l6hXXCZrQbokJEksxHo9KMOgc6w1atHXM8K9DVC7FQnJ0i8iK3QooGgbnyKgMDygBWyYFZoqx4qS27KqLZJjA1D0jK6QJcYEQEiWv9PGkTsbqxQ8oT+ZtZB6AkdsJnQDnMoHXHLGKOgDYuCWmYhEERCI5gaamW0bnHdA3k2ltlIN+2qKRyCND0bhqSYCyTB3CAOc4WusBEIpkeBuPgJMAAX8Hs1NfqHRgAAAABJRU5ErkJggg==') repeat-x scroll left top #fff"
			});

			this.$message.css({
				fontSize: '13px',
				lineHeight: '16px',
				textAlign: 'center',
				padding: '8px 10px 9px',
				width: 'auto',
				position: 'relative'
			});

			this.$closeButton.css({
				position: 'absolute',
				top: 4, right: 4,
				width: 10, height: 10,
				background: "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAATpJREFUeNoszrFqVFEUheG19zlz7sQ7ijMQBAvfYBqbpJCoZSAQbOwEE1IHGytbLQUJ8SUktW8gCCFJMSGSNxCmFBJO7j5rpXD6n5/P5vM53H3b3T9LOiB5AQDuDjM7BnA7DMPHDGBH0nuSzwHsRcRVRNRSysuU0i6AOwA/02w2+9Fae00SEbEh6SGAR5K+k3zWWptKepCm0+kpyRoRGyRBcpPkDsn1iEBr7drdP2VJZyQXERGSPpiZAViTBACXKaV9kqd5uVzCzO5KKb/d/UZSDwD/eyxqree1VqSu6zKAF2Z2RPJJaw0rAkjOJT0m+SuT/AbgDcmnkmBmfwAsJL1dXQ8lWY6IGwB1ZbrOOb8zs8thGP4COFwx/mE8Ho9Go9ErMzvJOW/1fY/JZIJSypqZfXX3L13X9fcDAKJct1sx3OiuAAAAAElFTkSuQmCC)",
				display: 'none',
				cursor: 'pointer'
			});

			this.$buttons.css({
				padding: 5,
				textAlign: 'right',
				borderTop: '1px solid #ccc',
				backgroundColor: '#fff'
			});

			this.$buttons.find('button').css({
				marginLeft: 5
			});

			this.$buttons.find('button:first').css({
				marginLeft: 0
			});

			this.$bar.bind({
				mouseenter: function() { $(this).find('.noty_close').stop().fadeTo('normal',1); },
				mouseleave: function() { $(this).find('.noty_close').stop().fadeTo('normal',0); }
			});

			switch (this.options.layout.name) {
				case 'top':
					this.$bar.css({
						borderRadius: '0px 0px 5px 5px',
						borderBottom: '2px solid #eee',
						borderLeft: '2px solid #eee',
						borderRight: '2px solid #eee',
						boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
					});
				break;
				case 'topCenter': case 'center': case 'bottomCenter': case 'inline':
					this.$bar.css({
						borderRadius: '5px',
						border: '1px solid #eee',
						boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
					});
					this.$message.css({fontSize: '13px', textAlign: 'center'});
				break;
				case 'topLeft': case 'topRight':
				case 'bottomLeft': case 'bottomRight':
				case 'centerLeft': case 'centerRight':
					this.$bar.css({
						borderRadius: '5px',
						border: '1px solid #eee',
						boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
					});
					this.$message.css({fontSize: '13px', textAlign: 'left'});
				break;
				case 'bottom':
					this.$bar.css({
						borderRadius: '5px 5px 0px 0px',
						borderTop: '2px solid #eee',
						borderLeft: '2px solid #eee',
						borderRight: '2px solid #eee',
						boxShadow: "0 -2px 4px rgba(0, 0, 0, 0.1)"
					});
				break;
				default:
					this.$bar.css({
						border: '2px solid #eee',
						boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
					});
				break;
			}

			switch (this.options.type) {
				case 'alert': case 'notification':
					this.$bar.css({backgroundColor: '#FFF', borderColor: '#CCC', color: '#444'}); break;
				case 'warning':
					this.$bar.css({backgroundColor: '#FFEAA8', borderColor: '#FFC237', color: '#826200'});
					this.$buttons.css({borderTop: '1px solid #FFC237'}); break;
				case 'error':
					this.$bar.css({backgroundColor: 'red', borderColor: 'darkred', color: '#FFF'});
					this.$message.css({fontWeight: 'bold'});
					this.$buttons.css({borderTop: '1px solid darkred'}); break;
				case 'information':
					this.$bar.css({backgroundColor: '#57B7E2', borderColor: '#0B90C4', color: '#FFF'});
					this.$buttons.css({borderTop: '1px solid #0B90C4'}); break;
				case 'success':
					this.$bar.css({backgroundColor: 'lightgreen', borderColor: '#50C24E', color: 'darkgreen'});
					this.$buttons.css({borderTop: '1px solid #50C24E'});break;
				default:
					this.$bar.css({backgroundColor: '#FFF', borderColor: '#CCC', color: '#444'}); break;
			}
		},
		callback: {
			onShow: function() { $.noty.themes.defaultTheme.helpers.borderFix.apply(this); },
			onClose: function() { $.noty.themes.defaultTheme.helpers.borderFix.apply(this); }
		}
	};

})(jQuery);
/*******************************************************************************************/
/**************************** FIN NUEVA NOTIFICACION ***************************************/
/*******************************************************************************************/

//$.noty.consumeAlert({layout: 'topRight', type: 'success', dismissQueue: true,maxVisible: 5000,});

function validarEmail( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ){
        return false;
	}
	
	return true;
}


function verQxSinAut(estudio){
	
	var widget = new ModalContentZeusSalud("Cirugias Sin Autorizaciones",
			"../../controlador/Configuracion/camasDisponibles.php?operacion=listaCirugiasSinAutorizaciones&estudio="+estudio, {}, "general", "", "", "2%",null,null,true);
			
			widget.imgs_data = {									
								close_icon:{
								src:"../../ImagenesZeus/salir 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon close",
								_title:"Cerrar",
								id:"cerrar"
								} ,
								 
						 };
			widget.process();		
}

function verAutorizacionesCTC(estudio){
	
	var widget = new ModalContentZeusSalud("Autorizaciones CTC",
			"../../controlador/Configuracion/camasDisponibles.php?operacion=listarAutorizacionesCTC&estudio="+estudio, {}, "general", "", "", "2%",null,null,true);
			
			widget.imgs_data = {									
								close_icon:{
								src:"../../ImagenesZeus/salir 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon close",
								_title:"Cerrar",
								id:"cerrar"
								} ,
								 
						 };
			widget.process();		
}


function verSolAutorizaciones(estudio){
	
	var widget = new ModalContentZeusSalud("Solicitud de Autorizaciones Pendientes",
			"../../controlador/Configuracion/camasDisponibles.php?operacion=listarSolicitudAutorizacionesPendientes&estudio="+estudio, {}, "general", "", "", "2%",null,null,true);
			
			widget.imgs_data = {									
								close_icon:{
								src:"../../ImagenesZeus/salir 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon close",
								_title:"Cerrar",
								id:"cerrar"
								} ,
								 
						 };
			widget.process();		
}

function ventanaAutoriza(accion,estudio,autoid,tipoAdmision,idCTC,idOmed,es_solicitudAut,idHojaCiruDeta){
	$("#quien_autorizo").val("");
	$("#semanas_cotizadas").val("");
	$("#bono_servicio").val("");
	$("#fecha_autorizacion").val("");
	$("#hora_sol").val("");
	$("#nro_autorizacion").val("");
	$("#servicio_autoriza").val("");
	$("#obs_autorizacion").val("");
	get('aut_estudio').value="";
	get('aut_autoid').value="";
	get('aut_tipoAdmision').value="";
	
	get('aut_estudio').value=estudio;
	get('aut_autoid').value=autoid;
	get('aut_tipoAdmision').value=tipoAdmision;
	get('aut_IdCTC').value=idCTC;
	get('aut_IdOmed').value=idOmed;
	get('aut_es_solicitudAut').value=es_solicitudAut;
	get('idHojaCiruDeta').value=idHojaCiruDeta;
	
	if(accion==0){
		$("#ventanaAutorizacion").hide(200);
	}else{
		$("#ventanaAutorizacion").show(200);
	}
}
function cerrarVentana(){
	setTimeout(function(){var ww = window.open(window.location, '_self'); ww.close(); }, 1000);
}


function alertGlobal(msg,title,btnclose,height){

	showBtnClose=true;
	if(btnclose=='0'){
		showBtnClose=false;
	}
	
	$title=title;
	try{
		if(trim($title)==''){
			$title='Notificacion';
		}
	}catch(e){$title='Notificacion';}
	
	var divStyleGlobal={
		textAlign: 'center',
		position:'fixed',
		top:'0%',
		left:'0%',
		width:'100%',
		height:'100%',
		zIndex:'100000',
		backgroundColor: '#FFF',
		opacity:'0.3',
		filter:'alpha(opacity=30)',
	}
	//alert(height)
	var divStyle={
		fontSize:'10px',
		textAlign: 'center',
		position:'fixed',
		top:'50%',
		left:'50%',
		marginLeft:'-200px',
		marginTop:'-35px',
		backgroundColor: '#FFF',		
		width: '400px',
		height:(($.trim(height)=='')?'70px':height),
		border:'solid',
		borderColor:'#355B89',
		borderWidth:'1px',
		zIndex:'100001',
	}
	
	var styleTitle={
		backgroundColor: '#34b0c4',
		color:'#ffffff',
		fontWeight:'bold',
		paddingLeft:'5px',
		paddingRight:'2px',
		fontSize:'12px',
		
	}
	
	var styleContent={
		color:'#336699',
		//fontWeight:'bold',
		textAlign: 'center',
		overflow:'auto',
		maxHeight:'100%',
	}
	
	$divGlobal=$('<div id="divAlertGlobal">').appendTo("body");
	$divGlobal.css(divStyleGlobal);
	$div=$('<div id="divAlertGlobalChild">').appendTo("body");
	$table=$('<table border="0" width="100%" cellspacing="0" cellspadding="0">').appendTo($div);
	$trTitle=$("<tr>").appendTo($table);
	$tdTitle=$("<td>").appendTo($trTitle);
	$tdIconClose=$('<td width="20px" align="center">').appendTo($trTitle);
	$tdTitle.append($title);
	if(showBtnClose){
	$tdIconClose.append('<a title="Cerrar" href="javascript:closeMsgGlobal();">[X]</a>');
	}
	$tdTitle.css(styleTitle);
	$tdIconClose.css(styleTitle);
	$trContent=$('<tr height="50px">').appendTo($table);
	$tdContent=$('<td colspan="2">').appendTo($trContent);
	$divContent=$('<div>').appendTo($tdContent);
	$divContent.append(msg);
	$divContent.css(styleContent);
	$div.css(divStyle);
	
}
function closeMsgGlobal(){
	$("#divAlertGlobalChild").remove();
	$("#divAlertGlobal").remove();
}


function llenarAutomaticoTerceroCliente(){
	if(get('llenar_automatico').checked){
		var autoid=get('autoid').value;
		ajax("../../controlador/busqueda/busqueda.php?operacion=consultarDatosPaciente&autoid="+autoid, "div_info_paciente",'setPacienteTercero()','');
	}else{
		get('res_tipo_identificacion').value='';
		get('res_identificacion').value='';
		get('res_primer_nom').value='';
		get('res_segundo_nom').value='';
		get('res_primer_ape').value='';
		get('res_segundo_ape').value='';
		get('res_ciudad').value='';
		get('res_direccion').value='';
		get('res_telefono').value='';
	}
	
}
function setPacienteTercero(){
	get('res_tipo_identificacion').value=get('rs_tipo_id2').value;
	get('res_identificacion').value=get('rs_num_id').value;
	get('res_primer_nom').value=get('rs_primer_nom').value;
	get('res_segundo_nom').value=get('rs_segundo_nom').value;
	get('res_primer_ape').value=get('rs_primer_ape').value;
	get('res_segundo_ape').value=get('rs_segundo_ape').value;
	get('res_ciudad').value=get('rs_ciudad').value;
	get('res_direccion').value=get('rs_direccion').value;
	get('res_telefono').value=get('rs_telefono').value;
}


/*MODULO DE TRASLADO DE AGENDA DE CITA MEDICA*/

function buscarMedicoA(){
	showWindowFind('codigo, nombre', 'sis_medi', 'es_medico | 1', 
		'Codigo, Nombre', '80, 300', 'codigoMedicoA#medicoA', 'M&eacute;dicos','','','','','winBuscarMed');
}
function buscarMedicoB(){
	showWindowFind('codigo, nombre', 'sis_medi', 'es_medico | 1', 
		'Codigo, Nombre', '80, 300', 'codigoMedicoB#medicoB', 'M&eacute;dicos','','','','','winBuscarMed')	
}

function cargarProgramacionMedicoA(){
	deshabilitarToolBar(true);
	ajax("../../controlador/citas/citas.php?operacion=trasladarAgendaMedica&fecha="+get('Fecha_Agenda').value+"&cod_medico="+get('codigoMedicoA').value+"&mostrar=1&sede="+get('sede').value+"&clase=TrasIzq", "divProgramacionA",'habilitarToolBar(true);','');
}
function cargarProgramacionMedicoB(){
	deshabilitarToolBar(true);
	ajax("../../controlador/citas/citas.php?operacion=trasladarAgendaMedica&fecha="+get('Fecha_Destino').value+"&cod_medico="+get('codigoMedicoB').value+"&mostrar=1&sede="+get('sede').value+"&clase=TrasDer", "divProgramacionB",'habilitarToolBar(true);','');
}

function invertirMedicosTraslado(){
	var medicoA=get('medicoA').value;	
	var codMedicoA=get('codigoMedicoA').value;	
	var medicoB=get('medicoB').value;	
	var codMedicoB=get('codigoMedicoB').value;	
	
	get('medicoB').value=medicoA;
	get('codigoMedicoB').value=codMedicoA;
	
	get('medicoA').value=medicoB;
	get('codigoMedicoA').value=codMedicoB;
	
	cargarProgramacionMedicoA();
	cargarProgramacionMedicoB();
}

function seleccionarTodosTraslado(tipo,Trasladar){
	if(tipo==1){
		var trasladar=document.getElementsByClassName(Trasladar);
		for(var i=0;i<trasladar.length;i++){
			$("#"+trasladar.item(i).id).addClass('borderSelTraslado');
			$("#"+trasladar.item(i).id).removeClass('citaPendiente');
		}
	}else{
		var trasladar=document.getElementsByClassName(Trasladar);
		for(var i=0;i<trasladar.length;i++){
			$("#"+trasladar.item(i).id).removeClass('borderSelTraslado');
			$("#"+trasladar.item(i).id).addClass('citaPendiente');
		}
	}
		
}

/*FIN MODULO DE TRASLADO DE AGENDA CITA MEDICA*/


/*AUTORIZACION MODULO DE CITAS*/
function ventanaAutorizaciones(ident,campo){
	if(ident=='id_cirugia'){
		try{
		id=get('id_cirugia'+get('id_quirofano_seleccionado').value).value;
		}catch(e){id='';}
		title='Cirugia';
	}else{
		id=get(ident).value;
		title='Cita';
	}
	
	if(id==''){
		notify("Debe seleccionar una "+title+".","error");
		return false;
	}
	var widget = new ModalContentZeusSalud("Autorizacion",
			"../../controlador/citas/citas.php?operacion=VentanaAutorizacion&id="+id+"&campo="+campo, {}, "general", "", "750px", "2%",null,setFechaAutCitas,null,'listaAut');

			widget.imgs_data = {	
								
								close_icon:{
								src:"../../ImagenesZeus/salir 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon close",
								_title:"Cerrar",
								id:"cerrar",
								fnAction:function(){
									$("#listaAut").parent(".ui-zs-modal").remove();
									$("#listaAut").remove();
								}
								},
								
								imprimir_icon:{
								src:"../../ImagenesZeus/imprimir 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon",
								_title:"Imprimir Autorizaciones",
								id:"imprimirAut",
								fnAction:function(){
									//guardarAutCita();	
									imprimirAutorizaciones();
								}
								},
								nuevo_icon:{
								src:"../../ImagenesZeus/agregar 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon",
								_title:"Nueva Autorizacion",
								id:"imprimirAut",
								fnAction:function(){
									//guardarAutCita();	
									//imprimirAutorizaciones();
									nuevaAurorizacionCita(ident);
								}
								}
						 };
			widget.process();		
}


function nuevaAurorizacionCita(ident,idAutorizacion){
	
	if(idAutorizacion==undefined||idAutorizacion=='undefined'){
		idAutorizacion='';
	}
	if(ident=='id_cirugia'){
		var estadoCirugia=get('estadoCirugia').value;
		if(estadoCirugia!='P'){
			notify("No se pueden agregar autorizaciones.","error")
			return false;
		}
		try{
		id=getValue('id_cirugia'+get('id_quirofano_seleccionado').value);
		}catch(e){id='';}
		title='Cirugia';
	}else{
		id=getValue(ident);
		title='Cita';
	}
	var widget = new ModalContentZeusSalud("Nueva Autorizacion",
			"../../controlador/citas/citas.php?operacion=NuevaAutorizacion&id="+id+'&idAutorizacion='+idAutorizacion, {}, "general", "", "750px", "2%",null,setFechaAutCitas,null,'winAut');
			
			widget.imgs_data = {	
								
								close_icon:{
								src:"../../ImagenesZeus/salir 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon close",
								_title:"Cerrar",
								id:"cerrar",
								fnAction:function(){
									$("#winAut").parent(".ui-zs-modal").remove();
									$("#winAut").remove();
								}
								},
								
								guardar_icon:{
								src:"../../ImagenesZeus/guardar 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon",
								_title:"Guardar Autorizacion",
								id:"imprimirAut",
								fnAction:function(){
									guardarAutCita(ident);	
								}
								}
						 };
			widget.process();		
}

function seleccionarTodasAut(){
	var selectAll=false;
	if(get('selectAll').checked){
		selectAll=true;
	}
	selectAutorizacion=document.getElementsByClassName('selectAutorizacion');
	
	for(var i=0;i<selectAutorizacion.length;i++){
		selectAutorizacion.item(i).checked=selectAll;
	}
	
	
}

function imprimirAutorizaciones(){
	
	selectAutorizacion=document.getElementsByClassName('selectAutorizacion');
	var ids='';
	for(var i=0;i<selectAutorizacion.length;i++){
		if(selectAutorizacion.item(i).checked){
			ids+=selectAutorizacion.item(i).value+',';
		}
	}
	invocarReporte('reporte_autorizacion.php?ids='+ids)
}


function setFechaAutCitas(){
	setearCalendario('fecha_autorizacion')	;
}

function guardarAutCita(){
	
	var id_cita='';
	var id_cirugia='';
	var estudio='';
	try{
		id_cita=get('id_cita').value;
		estudio=$(".seleccionado").attr("estudio");
	}catch(e){}
	try{
		if($.trim(estudio)==''){
			estudio=$(".seleccionarCirugia").attr("estudio");
		}		
	}catch(e){}
	//alert(estudio);
	try{
		id_cirugia=get('id_cirugia'+get('id_quirofano_seleccionado').value).value;
	}catch(e){}
	var somos = $("#quien_autorizo").val();
	var semanas = $("#semanas_cotizadas").val();
	var bono = $("#bono_servicio").val();
	var fecha_servi = $("#fecha_autorizacion").val();
	var hora = $("#hora_sol").val();
	var user = $("#usuario_receptor").val();
	var num_autoriza = $("#nro_autorizacion").val();
	var servicio = $("#servicio_autoriza").val();
	var obs = $("#obs_autorizacion").val();
	if($.trim(estudio)==''){
		estudio = get('aut_estudio').value;
	}
	var autoid = get('aut_autoid').value;
	var ambito = get('aut_tipoAdmision').value;
	var IdCTC = get('aut_IdCTC').value;
	var IdOmed = get('aut_IdOmed').value;
	var idAutorizacion=get('idAutorizacion').value;
	var es_solicitudAut='0';
	var idHojaCiruDeta='0';
	
	var operacion="GAutorizacion";
	if(trim(idAutorizacion)!=''){
		operacion="UpdateAutorizaciones";
	}
	
	try{
		es_solicitudAut=get('aut_es_solicitudAut').value;
	}catch(e){}
	
	try{
		idHojaCiruDeta=get('idHojaCiruDeta').value;
	}catch(e){}
	
	
	/*alert(somos+' '+semanas+' '+bono+' '+fecha_servi+' '+hora+' '+user+' '+num_autoriza+' '+servicio+' '+obs+' '+estudio+' '+autoid+' '+ambito);*/
	
	if(trim(somos)==''){
		notify("Ingrese campo 'Quien Autoriz&oacute;'","error");
	}else if(trim(fecha_servi)==""){
		notify("Ingrese campo 'Fecha'","error");
	}else if(trim(hora)==""){
		notify("Ingrese campo 'Hora'","error");
	}else if(trim(servicio)==""){
		notify("Ingrese campo 'Servicio Autoriza'","error");
	}else if($.trim(num_autoriza) == ""){
		notify("Ingrese campo 'Numero de Autorizacion'","error");
	}else{
		$.ajax({
				type: "POST",
				url: "../Facturacion/CtrlAutorizacion.php",
				data: {
					somos:somos,
					semanas:semanas,
					bono:bono,
					fecha_servi:fecha_servi,
					hora:hora,
					user:user,
					num_autoriza:num_autoriza,
					servicio:servicio,
					obs:obs,
					estudio:estudio,
					autoid:autoid,
					ambito:ambito,
					idCTC:IdCTC,
					idOmed:IdOmed,
					es_solicitud:es_solicitudAut,
					idHojaCiruDeta:idHojaCiruDeta,
					idCita:id_cita,
					idCirugia:id_cirugia,
					id:idAutorizacion,
					op:operacion
					},
					success: function(respuesta){
						console.log(respuesta);
						var obj = JSON.parse(respuesta);
						console.log(obj);
						alert(obj["msg"]);
						//ReacargarAutorizaciones(respuesta);
						if(obj["error"]==0){
							closeModal();
						}			
					}
			 });
		}
		try{
			$("#ventanaAutorizacion").hide();
		}catch(e){}
}

/*FIN AUTORIZACION CITAS*/

function archivosCargar(nameId,dir,metodo){
	deshabilitarToolBar(true);
	var archivosCitas = document.getElementById(nameId);//Damos el valor del input tipo file
	var archivo = archivosCitas.files; //Obtenemos el valor del input (los arcchivos) en modo de arreglo
	
	/* Creamos el objeto que hara la petición AJAX al servidor, debemos de validar 
	si existe el objeto “ XMLHttpRequest” ya que en internet explorer viejito no esta,
	y si no esta usamos “ActiveXObject” */ 
	
	 if(window.XMLHttpRequest) { 
	 var Req = new XMLHttpRequest(); 
	 }else if(window.ActiveXObject) { 
	 var Req = new ActiveXObject("Microsoft.XMLHTTP"); 
	 }
	
	//El objeto FormData nos permite crear un formulario pasandole clave/valor para poder enviarlo, 
	//este tipo de objeto ya tiene la propiedad multipart/form-data para poder subir archivos
	var data = new FormData();
	
	//Como no sabemos cuantos archivos subira el usuario, iteramos la variable y al
	//objeto de FormData con el metodo "append" le pasamos calve/valor, usamos el indice "i" para
	//que no se repita, si no lo usamos solo tendra el valor de la ultima iteración
	for(i=0; i<archivo.length; i++){
	   data.append('archivo'+i,archivo[i]);
	}
	
	//Pasándole la url a la que haremos la petición
	Req.open("POST", "../../controlador/archivo/cargar.php?dir="+dir, true);
	
	/* Le damos un evento al request, esto quiere decir que cuando termine de hacer la petición,
	se ejecutara este fragmento de código */ 
	
	Req.onload = function(Event) {
	//Validamos que el status http sea ok 
	if (Req.status == 200) { 
	  //Recibimos la respuesta de php
	  var msg = Req.responseText;
	  if(metodo!=''){
			setTimeout(metodo);
			habilitarToolBar(true);
	  }
	//  $("#cargados").append(msg);
	} else { 
	  console.log(Req.status); //Vemos que paso. 
	} 
	};
	
	 //Enviamos la petición 
	 Req.send(data); 
}

function res_archivos(){
	if(get('errorAC').value=='OK'){
		notify("Se ha modificado el nombre del archivo correctamente.");
	}else{
		notify("Error al modificar nombre del archivo.","error");	
	}
}
$_DivGlobal=null;
function ModalGlobal(url,fnAfter,param,H,W,fullTop,fullWidth,typeHeight,backgroundColor){

	if (!$('#divInformes').length){
		var $divWidth = (fullWidth)?'99.8%':'95%';
		var $divHeight = ($.trim(typeHeight)!='')?typeHeight:'80%';
		var $colorStyleDiv='#398EC7';
		
		var cssDiv={
			color:'#FFF',
			backgroundColor:($.trim(backgroundColor)!='')?backgroundColor:'#E6EDEF',
			width:$divWidth,
			position:'absolute',
			top:(fullTop)?'0px':'50%',
			left:(fullTop)?'0px':'50%',
			display:'none',
			height:$divHeight,		
			borderLeft:'solid',
			borderRight:'solid',
			borderTop:'solid',
			borderBottom:'solid',
			borderLeftWidth:'1px',
			borderRightWidth:'1px',
			borderTopWidth:'1px',
			borderBottomWidth:'1px',
			borderLeftColor:$colorStyleDiv,
			borderRightColor:$colorStyleDiv,
			borderTopColor:$colorStyleDiv,
			borderBottomColor:$colorStyleDiv,
			borderTopLeftRadius: '10px 5px',
			borderTopRightRadius: '10px 5px',
			overflow:(fullWidth)?'none':'auto'
		};
	
		
		$div=$("<div id='divInformes'>" ).appendTo("body");
		//$toolbar.appendTo($div);
		$div.css(cssDiv);
	
		$w=parseInt($div.width()/2);
		$h=parseInt($div.height()/2);
		if(fullTop){}else{
		$div.css({ marginLeft : "-"+$w+"px",marginTop: "-"+$h+"px"});
		}
		
		$div.show(300, null, function(){
			ajaxPOST(getValue('ruta')+url,"divInformes","afterModal();"+fnAfter,"",param);
		});
		
		$_DivGlobal=$div;
		
	}
}

function afterModal(){
	//alert($("#divInformes").innerWidth()+' - '+$("#divInformes").outerWidth());

}

function globaltop(url,after,param){
	window.top.ModalGlobal(url,after,param);	
}


function showInformes(){
	ModalGlobal("Vistas/Informes/informes.php","cargarCategorias();","","","");
}

function showHistoricoHC(Autoid,estudio){
	ModalGlobal("Vistas/Hc/historicoHC.php","cargarEstudios('"+Autoid+"','"+estudio+"');eventoHC();","","","",true,true,'99%','#C9CBCB');
}
$_maximizado=100;
function maximizarHC(){
	$_minimizado=1;
	if($_maximizado>50){
		$_DivGlobal.css('height','50%');
		$_maximizado=50;
	}else{
		$_DivGlobal.css('height','99%');
		$_maximizado=100;
	}
}
$_minimizado=1;
function minimizarHC(){
	if($_minimizado==1){
		$_DivGlobal.css('height','40px');
		$_minimizado=0;
		$_maximizado=50;
	}
}
function cargarEstudios(Autoid,estudioselect){

	respGetEstudios=function(){
		var resp=JSON.parse(_ResquestAJAX);
		if(resp.length == 0){
			cerrarInformes();
			alert("Paciente sin Historias diligenciadas.");
			return 0;
		}
		resp.reverse();
		for(var i in resp){
		//	console.log(resp[i]);
			$div=$('<div class="CssDiv HCNotView MenuDiv'+resp[i]["Estudio"]+'" Estudio="'+resp[i]["Estudio"]+'">');
			
			$div.ZS_tabla('width="100%"');
			$tabla.ZS_tr('');
			$tr.ZS_td('bgColor="#34b0c4" class="letraCaptionLink"','<b>Ingreso : '+resp[i]["ingreso"]+'</b> <b>Estudio : '+resp[i]["Estudio"]+'</b>');
			$tabla.ZS_tr('');
			$tr.ZS_td('class="letraDisplay"','<b>Fecha Ingreso: </b>'+resp[i]["FechaIngreso"]);
			$tabla.ZS_tr('');
			$tr.ZS_td('class="letraDisplay"','<b>Hora Ingreso: </b>'+resp[i]["HoraIngreso"]);
			$tabla.ZS_tr('');
			$tr.ZS_td('class="letraDisplay"','<b>Medico: </b>'+resp[i]["Medico"]);
			$div.data('Estudio',resp[i]["Estudio"]);
			$div.click(function(){
				estudios = $(this).data('Estudio');
				$(".CssDivSelectedPintar").removeClass("CssDivSelectedPintar");
				$('#DivHistorico_Der .Estudio'+estudios).addClass("CssDivSelectedPintar");
				
				if(/*$(this).hasClass("HCNotView")*/true){				
					$(this).removeClass("HCNotView");
					$(".CssDivSelected").removeClass("CssDivSelected");
					$(this).addClass("CssDivSelected");
					//crearLoadingJX();
					var data = new Object();
					data.estudio = $(this).data('Estudio');
					data.ruta = $('#HTTP_HOST').val();
					data.RutaCarpeta = $('#CarpetaArchivosRead').val();
					data.rptdatasource = $('#host').val();
					data.rptdatabase = $('#database').val();
					data.rptuser = $('#cliente').val();
					data.rptpassword = $('#clave').val();
					data.id_sede = $('#id_sede').val();
					data.pdf = 1;
					data.rutapdf = $("#CarpetaArchivosSave").val() + 'HistoricoHC/';
					data.nombreArchivoPdf = $(this).data('Estudio')+'.pdf';
					data.rptName = "HCFASTREPORT";
					
					
							
				
					//_url = "http://" + _location + "/ZeusSalud/Reportes/FastReport/Pages/c_informe_view.aspx";
				
				
					var estudioClick=$(this).data('Estudio');
					//procesador.procesar = function(response){
						//$("#iframePDF").attr('src',$('#CarpetaArchivosRead').val()+ 'HistoricoHC/'+estudioClick+'.pdf')
						cargarPageHC(estudioClick);
						
					//	eliminaLoadingJX();
					//}
				
				
					//deshabilitarToolBar();
				
					//requestAjaxGeneral(data, _url, procesador, "POST", false, "", 1);
				}else{
					estudio=$(this).data('Estudio');
					$('#DivHistorico_Der').animate({
						scrollTop:(($("#DivHistorico_Der").scrollTop())+($('#DivHistorico_Der .Estudio'+estudio).offset().top))
					}, 1000);
					
					
					
					$(".CssDivSelected").removeClass("CssDivSelected");
					$(this).addClass("CssDivSelected");
					
		
					
				}
			})
			
			$("#DivHistorico_Izq").append($div);

		}
		
			$("#DivHistorico_Izq  .MenuDiv"+estudioselect).click();
			//alert('modifico');
			cargarEstudiosDescargados(Autoid,estudioselect);
		
	}
	ajaxPOST(getValue('ruta')+"controlador/HC/historicoHC.php","","respGetEstudios();","","Op=GetEstudios&Autoid="+Autoid);	
	
	
	

	
	
}




function cargarEstudiosDescargados(Autoid,estudioselect){


	//------------------------------------ DESCARGADO REMOTO ------------------------------------------------------------------------
	
	
	
	respGetEstudiosDescargados=function(){
		var resp=JSON.parse(_ResquestAJAX);

		
		for(var i in resp){
		
		
		
		
		//	console.log(resp[i]);
			$div=$('<div class="CssDiv HCNotView MenuDiv'+resp[i]["Estudio"]+'" Estudio="'+resp[i]["Estudio"]+'">');
			
			$div.ZS_tabla('width="100%"');
			$tabla.ZS_tr('');
			$tr.ZS_td('bgColor="#75D975" class="letraCaptionLink"','<b>Ingreso : '+resp[i]["ingreso"]+'</b> <b>Estudio : '+resp[i]["Estudio"]+'</b>');
			$tabla.ZS_tr('');
			$tr.ZS_td('class="letraDisplay"','<b>Fecha Ingreso: </b>'+resp[i]["FechaIngreso"]);
			$tabla.ZS_tr('');
			$tr.ZS_td('class="letraDisplay"','<b>Hora Ingreso: </b>'+resp[i]["HoraIngreso"]);
			$tabla.ZS_tr('');
			$tr.ZS_td('class="letraDisplay"','<b>Medico: </b>'+resp[i]["Medico"]);
			$div.data('Estudio',resp[i]["Estudio"]);
			$div.click(function(){
				estudios = $(this).data('Estudio');
				$(".CssDivSelectedPintar").removeClass("CssDivSelectedPintar");
				$('#DivHistorico_Der .Estudio'+estudios).addClass("CssDivSelectedPintar");
				
				if(/*$(this).hasClass("HCNotView")*/true){				
					$(this).removeClass("HCNotView");
					$(".CssDivSelected").removeClass("CssDivSelected");
					$(this).addClass("CssDivSelected");
					//crearLoadingJX();
					var data = new Object();
					data.estudio = $(this).data('Estudio');
					data.ruta = $('#HTTP_HOST').val();
					data.RutaCarpeta = $('#CarpetaArchivosRead').val();
					data.rptdatasource = $('#host').val();
					data.rptdatabase = $('#database').val();
					data.rptuser = $('#cliente').val();
					data.rptpassword = $('#clave').val();
					data.id_sede = $('#id_sede').val();
					data.pdf = 1;
					data.rutapdf = $("#CarpetaArchivosSave").val() + 'HistoricoHC/';
					data.nombreArchivoPdf = $(this).data('Estudio')+'.pdf';
					data.rptName = "HCFASTREPORT";
					
					
							
				
					//_url = "http://" + _location + "/ZeusSalud/Reportes/FastReport/Pages/c_informe_view.aspx";
				
				
					var estudioClick=$(this).data('Estudio');
					//procesador.procesar = function(response){
						//$("#iframePDF").attr('src',$('#CarpetaArchivosRead').val()+ 'HistoricoHC/'+estudioClick+'.pdf')
						cargarPageHCDescarga(estudioClick);
						
					//	eliminaLoadingJX();
					//}
				
				
					//deshabilitarToolBar();
				
					//requestAjaxGeneral(data, _url, procesador, "POST", false, "", 1);
				}else{
					estudio=$(this).data('Estudio');
					$('#DivHistorico_Der').animate({
						scrollTop:(($("#DivHistorico_Der").scrollTop())+($('#DivHistorico_Der .Estudio'+estudio).offset().top))
					}, 1000);
					
					
					
					$(".CssDivSelected").removeClass("CssDivSelected");
					$(this).addClass("CssDivSelected");
					
		
					
				}
			})
			
			$("#DivHistorico_Izq").append($div);

		}
		
			$("#DivHistorico_Izq  .MenuDiv"+estudioselect).click();
		
		
	}
	ajaxPOST(getValue('ruta')+"controlador/HC/historicoHC.php","","respGetEstudiosDescargados();","","Op=GetEstudiosDescargados&Autoid="+Autoid);	
	
	
	



}

function cerrarInformes(){
	$("#divInformes").hide(300,null,function(){
		$("#divInformes").remove();	
	});
}

function cargarCategorias(){
	
	eventosTabsInformes();

	cargarCategoriasSistema=function(){
		
		var resp=JSON.parse(_ResquestAJAX);

		$divCategorias=$("#divCategoriasSistema");
		$divCategorias.empty();
		$divCategorias.ZS_tabla('width="100%"');
		
		for(var i in resp){			
			$tabla.ZS_tr('');
			$tr.ZS_td('onclick="javascript:cargarReportesCat(\''+resp[i]["id"]+'\');"','<a href="javascript:void(0)">'+resp[i]["descripcion"]+'</a>');
		}

		cargarCategoriasCliente=function(){
			var resp=JSON.parse(_ResquestAJAX);

			$divCategoriasCliente=$("#divCategoriasCliente");
			$divCategoriasCliente.empty();
			$divCategoriasCliente.ZS_tabla('width="100%"');
			
			for(var i in resp){			
				$tabla.ZS_tr('');
				$tr.ZS_td('onclick="javascript:cargarReportesCat(\''+resp[i]["id"]+'\');"','<a href="javascript:void(0)">'+resp[i]["descripcion"]+'</a>');
			}

		}

		ajaxPOST(getValue('ruta')+"controlador/Informes/informes.php","","cargarCategoriasCliente()","",
				$.param({Op:"cargarCategorias",TipoInforme:"Cliente"}));
	}
	
	ajaxPOST(getValue('ruta')+"controlador/Informes/informes.php?Op=cargarCategorias&TipoInforme=Sistema","","cargarCategoriasSistema()","",
			'');
}

function eventosTabsInformes(){

	$("#ttabs li").click(function() {
		//	First remove class "active" from currently active tab
		$("#ttabs li").removeClass('tactive');

		//	Now add class "active" to the selected/clicked tab
		$(this).addClass("tactive");

		//	Hide all tab content
		$(".ttab_content").hide();

		//	Here we get the href value of the selected tab
		var selected_tab = $(this).find("a").attr("href");
		//	Show the selected tab content
				
		$(selected_tab).fadeIn();
		
		//	At the end, we add return false so that the click on the link is not executed
		return false;
	});
}

function cargarReportesCat(id){
	var TipoInforme = $(".tactive").find("a").attr("tipoInforme");
	
	$('#InformeDescripcion'+TipoInforme).val();
	$('#buscarInforme'+TipoInforme).val();
	ajaxPOST(getValue('ruta')+"controlador/Informes/informes.php","divListaReportes"+TipoInforme,"","",$.param({Op:'cargarReportesCat',
	id:id,TipoInforme:TipoInforme}));
}

function cargarPlantillaInforme(url,resumen){
	var TipoInforme = $(".tactive").find("a").attr("tipoInforme");
	get('InformeDescripcion'+TipoInforme).value=resumen;
	ajaxPOST(getValue('ruta')+"Vistas/Estadisticas/"+url,"divFormularioReporte"+TipoInforme,"setCal();","","");
}

function cargarPlantillaPersonalizada(url,resumen){
	var TipoInforme = $(".tactive").find("a").attr("tipoInforme");
	get('InformeDescripcion'+TipoInforme).value=resumen;
	var urlPlantilla = getValue('ruta')+"Vistas/Estadisticas/"+url;
	procesador.procesar = function(content){
		$("#divFormularioReporte"+TipoInforme).html(content);
		setearCalendarioBarra();
	}
	requestAjaxGeneral({}, urlPlantilla, procesador, "GET", false, "");
	//ajaxPOST(getValue('ruta')+"Vistas/Estadisticas/"+url,"divFormularioReporte","setearCalendarioBarra();","","");
}

function buscarInformes(){
	var descripcion=getValue('buscarInforme');
	ajaxPOST(getValue('ruta')+"controlador/Informes/informes.php","divListaReportes","","","Op=buscarReportes&descripcion="+descripcion);	
}

function setCal(){
	var id_camposTipoFecha=getValue('id_camposTipoFecha');
	var campos=id_camposTipoFecha.split("|"); 
	var fec='';
	for(var i=0;i<campos.length;i++){

		if($.trim(campos[i])!=''){
			fec=getValue(""+campos[i]);
			$("#formularioInforme #"+campos[i]).datepicker({
					 showOn: "both",
					 buttonImage: "../../Imagenes/calendario 01.png",
					 buttonImageOnly: true,
					 changeMonth: true,
					 changeYear: true,
					 minDate: new Date(1800,1-1,1), 
					 yearRange: "-110:+5",
					 dateformat: "yy-mm-dd",
					 showAnim: "fadeIn"
			});
			$("#formularioInforme #"+campos[i]).datepicker("option","dateFormat","yy/mm/dd");
			$("#formularioInforme #"+campos[i]).val(fec);
		}
	}
	setearCalendarioBarra();
}

function imprimirInforme(){
	if(validarInforme()){
		$('#formularioInforme').attr("action", "http://"+getValue('rutaHTTP')+"/sismasalud/Reportes/FastReport/Pages/c_informe_view.aspx");
		$('#formularioInforme')[0].submit();
	}else{
		notify("Verifique los campos obligatorios","error");
	}
}


function imprimir(object) {
	
	var Informe=document.getElementsByClassName("Informe");
	var variables='&TipoReporte='+$(object).attr("tiporeporte");
	var imprimir=true;
	for(var i=0;i<Informe.length;i++){
		variables+='&'+Informe.item(i).id+'='+Informe.item(i).value;
		if(trim(Informe.item(i).value)==''){
			imprimir=false;	
		}
	}
	texto = get('nombre_reporte').value;
	
	if(texto.indexOf('?') != -1){
	 $("input[ajaxreport='report'][type='hidden']").remove();
            
            var ReportTitle = $(object).attr("title");
            var ReportName = $(object).attr("reportname");


            if (typeof (ReportName) == "undefined" || ReportName == '') { alert("No se especifico el nombre del Reporte."); return false; }
            
            var QueryStringData = '<input type="hidden" id="rptName" name="rptName" value="' + ReportName + '" ajaxreport="report" />';
            QueryStringData += '<input type="hidden" id="rptitle" name="rptitle" value="' + ReportTitle + '" ajaxreport="report" />';
            jQuery.each($("[ajaxrecord=param]").serializeArray(), function () { QueryStringData += '<input type="hidden" id="' + this.name + '" name="' + this.name + '" value="' + this.value + '" ajaxreport="report" />'; });

            $("#formulario").attr("action", "http://zeussalud08:8089/Pages/c_informe_view.aspx")
            $("#formulario").append(QueryStringData);
            $("#formulario")[0].submit();
}else{
	invocarReporte(get('nombre_reporte').value+"?dinamico=1"+variables, get('carpeta_reporte').value);
}
	
}
function validarInforme(){
	var requeridos=document.getElementsByClassName('required');
	var ok=true;
	for(var i=0;i<requeridos.length;i++){
		if(trim(requeridos.item(i).value)==""){
			ok=false;
			$('#'+requeridos.item(i).id).addClass('requerido');
		}else{
			$('#'+requeridos.item(i).id).removeClass('requerido');
		}
	}
	return ok;
}


function fnAtenderPYP2(plan,fuente_servicio,estado_his,hc_codigonombreconsulta,hc_valorconsulta,hc_nombreconsultaPYP,estudio,numero){

	var widget = new ModalContentZeusSalud("Atencion PYP",
			"PYPFormato.php", {plan:plan,fuente_servicio:fuente_servicio,estado_his:estado_his,hc_codigonombreconsulta:hc_codigonombreconsulta,hc_valorconsulta:hc_valorconsulta,hc_nombreconsultaPYP:hc_nombreconsultaPYP,estudio:estudio,numero:numero}, "general", "", "90%", null,null,null, false,'format_pyp33');
			
		widget.imgs_data = {	
							
							close_icon:{
							src:"../../ImagenesZeus/salir 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon close",
							_title:"Cerrar",
							id:"cerrar"
							},
							
							guardar:{
							src:"../../ImagenesZeus/guardar 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon",
							_title:"Guardar",
							id:"guardarPYP",
							fnAction:function(){
								guardarPYP(plan);
							}
							},
							imprimir:{
							src:"../../ImagenesZeus/imprimir 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon",
							_title:"Imprimir",
							id:"imprimirPYP",
							fnAction:function(){
								imprimirPYP(plan);
							}
							}
					 	};
		widget.process();
}


function imprimirPYP(plan){
	
	var numPYP=getValue('numPYP');
	invocarReporte('reportePYP.php?numPYP='+numPYP+'&plan='+plan);
	
}


function ordenarProcsAgrupadosPYP(){
	//alert("hola");
	var hc=1;
	limpiarAgrupadosPYP();
	if (getValue("fuente_servicioPYP").length == 0) {
		alert("Primero debe seleccionar un servicio");
		nextFocus("fuente_servicio");
		return;
	}
	
	
	var xfuente = getValue('fuente_servicioPYP');
	var estudio = getValue('estudio');

	dt = "fuente="+getValue("fuente_servicioPYP")+"&estudio="+estudio+"&multipleSelect=0"+'&hc='+hc; //$("f").serialize();
	//alert(dt)
	$.ajax({
		type: "POST",
		url: "OrdenarProcsAgrupadosPYP.php",
		data: dt,
		success: function(datos2){
			
			document.getElementById('divProcsAgrupadosPYP').innerHTML = datos2;
		
			document.getElementById('divProcsAgrupadosPYP').style.display="";
		}
	});
}

function limpiarAgrupadosPYP(){
	document.getElementById('divProcsAgrupadosPYP').innerHTML = "";
	document.getElementById('divProcsAgrupadosPYP').style.display = "none";
	document.getElementById('lista_codigos_ordenadosPYP').value="";
	document.getElementById('lista_nombres_ordenadosPYP').value="";
}

function enviarAgrupadosPYP(isGeneral) {
	isGeneral = isGeneral || false;
	if(getValue("fuente_servicioPYP") == ""){
		notify("Seleccione un servicio antes de continuar", "error");
		return false;
	}
	
	var complemento = "";
	if (false/*"<?php echo $_REQUEST["update"]; ?>" == "no"*/) {
		alert("No se puede realizar esta operacion");
	}
	else {
		
		/*if(getValue("codigo_diagnostico_principal") == ""){
			notify("Debe selecionar un diagnostico antes de continuar", "error");
		}*/
		
		if(isGeneral){
			cod_proc = getValue("hc_codigonombreconsultaPYP");
			
			complemento = "&cod_proc=" + cod_proc;
		}else{
			complemento = "";
		}
		
		procesador.procesar = traerconsultaPYP; 
		
		getJSONAjax("formularioPYP", "CtrlDatosHistoria.php?operacion=protocolosPYP"+complemento, procesador, "POST", 0, "");
		
	}
}


function traerconsultaPYP(dt){

	if(dt.codigo == null) return false;
	
	get("hc_codigonombreconsultaPYP").value = dt.codigo;
	get("hc_nombreconsultaPYP").value = utf8_decode(dt.desc);
	get("hc_valorconsultaPYP").value = dt.valor;	
	document.getElementById('divProcsAgrupadosPYP').innerHTML = "";
	document.getElementById('divProcsAgrupadosPYP').style.display = "none";
	document.getElementById('lista_codigos_ordenadosPYP').value="";
	document.getElementById('lista_nombres_ordenadosPYP').value="";
	$("#cnt-listaProcsPYP").html(dt.payload);
	/*$("#codigo_diagnostico_principal").val("");
	$("#diagnostico_principal").val("");*/
}

function guardarPYP(plan){
	procesador.procesar = respuestaSavePYP;
	getJSONAjax("formularioPYP", "../../controlador/HC/historiaClinica.php?operacion=guardarDatosPYP&plan="+getValue("planPYP")+'&paginita='+getValue('paginitaPYP'), procesador, "POST", 0, "");
	
}

function respuestaSavePYP(data){
	console.log(data);
	if(data.TipoRespuesta!='OK'){
		notify(data.Respuesta,"error");
	}else{
		$('#numPYP').val(data.numPYP);
		$("#estudio_pyp_aux").val(data.estudioPYP);
		notify("Se ha guardado correctamente");

	}
}

function tabDefecto(){
	changeTabPYP(getValue("tabDefecto"));
	if($.trim(getValue("numPYP"))!=''){
		fnProcsFacturadosPYP();
	}
}


function cargarGraficaPYP(estudio){

	var widget = new ModalContentZeusSalud("GRAFICA PYP",
			getValue('ruta')+"/Vistas/Hc/GraficaPYP.php", {estudio:estudio}, "general", "", "700px", "",null,null,null,null,setDatosGrafica);
		widget.imgs_data = {	
							
							close_icon:{
							src:"../../ImagenesZeus/salir 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon close",
							_title:"Cerrar",
							id:"cerrar"
							},
							
							imprimir:{
							src:"../../ImagenesZeus/guardar 01.png", 
							_w:32, 
							_h:32, 
							_class:"icon",
							_title:"Guardar",
							id:"guardarPYP",
							fnAction:function(){
								
							}
							}
					 	};
		widget.process();
}

function setDatosGrafica(){
	
	var obj = JSON.parse(get('jsonCad').value);
	// console.log(obj);
	// alert(obj);

	//jQuery.globalEval($("#general").html());
        $('#divGrafica').highcharts({
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'INDICE DE MASA CORPORAL'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['0', '10', '20', '30', '40', '50',
                    '60', '70', '80', '90', '100', '110'],
				labels: {
                    enabled: false
                },
                title: {
                    text: 'EDAD'
                }
            }],
			
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'ALTURA',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'PESO',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: false,
				 format: '{value}',
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
			series:obj
            /*series: [{
                name: '',
                type: 'spline',
                yAxis: 1,
                data: [[0, 175], [25, 25], [90, 90]],
                tooltip: {
                    valueSuffix: ''
                }
    
            }, {
                name: '',
                type: 'spline',
                 data: [[0, 175], [25, 25], [90, 90]],
                tooltip: {
                    valueSuffix: ''
                },
				
            }]*/
        });

}

function showDiagnosticosPYP(diagnostico){	
		showWindowFind('codigo, descripcion', "sis_diags","codigo in (select * from dbo.split('"+diagnostico+"',',')) or '"+diagnostico+"'='' ", 'Codigo, Descripcion', '80, 300', 'codDiagnosticoPYP#DiagnosticoPYP', 'Diagnosticos PyP', "fnCheckDiagsFaltantesPYP()");
}

function fnCheckDiagsFaltantesPYP(){
	procesador.procesar = function(response){
		$("#cnt-listaProcsPYP").html(response.payload);
	}
	var diagnostico = getValue("codDiagnosticoPYP");
	getJSONAjax({codigo_diagnostico_principal:diagnostico}, "CtrlDatosHistoria.php?operacion=checkDiagsFaltantesPYP", procesador, "POST", 0, "");
}

function consultaPYP(espyp, cups) {
	if (getValue("fuente_servicioPYP").length == 0) {
		alert("Primero debe seleccionar un servicio");
		nextFocus("fuente_servicioPYP");
		return;
	}
	var manual = getValue("codigoManualPYP");
	var tipo = getValue("tipoManualPYP");
	var estudio = getValue("estudioPYP");
	var fuente = getValue("fuente_servicioPYP");
	/*
	showWindowFind(
		"p.codigo, p.nombreve, p.manual_" + manual,
		"sis_proc AS p, servicios AS s, sis_maes AS m",
		"s.contrato | m.contrato AND s.codigo | " + fuente + " AND p.tipo | !" + tipo 
		+ "! AND p.codigo | s.cod_proc AND s.A = 1 AND m.con_estudio | " + estudio,
		"C&oacute;digo, Nombre, Valor",
		"80, 300, 80",
		"hc_codigonombreconsulta#hc_nombreconsulta#hc_valorconsulta",
		"Nombre de la Consulta")*/
		//alert(manual + " - " + tipo);

		var mi_cups = '';
		if((espyp == 1) && (cups != '')){
			mi_cups = " AND p.cups in (select items from dbo.Split(!"+cups+"!, !,!))";
		}

	/*showWindowFind(
		"p.codigo, p.nombreve, p.manual_" + manual,
		"sis_proc AS p",
		"p.rips | !AC! AND p.tipo | !" + tipo 
		+ "! AND p.manual_" + manual +" > 0" + mi_cups,
		"C&oacute;digo, Nombre, Valor",
		"80, 300, 80",
		"hc_codigonombreconsultaPYP#hc_nombreconsultaPYP#hc_valorconsultaPYP",
		"Nombre de la Consulta","fnAddProcedimientoGeneralPYP()")*/

		showWindowFind(
			"p.codigo, p.nombreve, p.manual_" + manual,
			"sis_proc AS p",
			"p.tipo | !" + tipo 
			+ "! AND p.manual_" + manual +" > 0" + mi_cups,
			"C&oacute;digo, Nombre, Valor",
			"80, 300, 80",
			"hc_codigonombreconsultaPYP#hc_nombreconsultaPYP#hc_valorconsultaPYP",
			"Nombre de la Consulta","fnAddProcedimientoGeneralPYP()")
}


function fnAddProcedimientoGeneralPYP(){	
	enviarAgrupadosPYP(true);
}

function fnProcsFacturadosPYP(){

	var ingreso = getValue("numPYP");
	
	var data = new Object();
	data.ingreso = ingreso;
	
	procesador.procesar = function(response){
		$("#cnt-listaProcsPYP").html(response.payload);			
	}
	getJSONAjax(data,"CtrlDatosHistoria.php?operacion=procsFacturadosPYP", procesador, "POST", true, "");
}

function fnBusquedasCostoOT(Tipo){
		if(Tipo == 'Servicios'){
			showWindowFind('id,nombre', 'sis_tipo', '', 'Codigo,Nombre', '100,300', 'servicio#nombreServicio', 'Servicios','');
			return 0;
		}
		
		if(Tipo == 'Medicos'){
			showWindowFind('codigo,nombre', 'sis_medi', 'estado | 1', 'Codigo,Nombre', '100,300', 'medico#nombreMedico', 'Medicos','');
			return 0;
		}
		
		if(Tipo == 'Especialidades'){
			showWindowFind('id,nombre', 'sis_especialidades', '', 'Codigo,Nombre', '100,300', 'especialidad#nombreEspecialidad', 'Especialidades','');
			return 0;
		}
		
		if(Tipo == 'PrefijosServicios'){
			showWindowFind('Id,Nombre', 'Prefijos', 'Activo | 1', 'Codigo,Nombre', '100,300', 'prefijo#nombrePrefijo', 'Prefijos servicio','');
			return 0;
		}

		if(Tipo == 'PrefijosServicios_CostosPrefijoServicios'){
			showWindowFind('Id,Nombre', 'Prefijos', 'Activo | 1 And InformeCostosPrefijoSer | 1', 'Codigo,Nombre', '100,300', 'prefijo#nombrePrefijo', 'Prefijos servicio','');
			return 0;
		}
		
		if(Tipo == 'PuntosAtencion'){
			showWindowFind('Codigo,Nombre,Id', 'puntoAtencion', 'Activo | 1', 'Codigo,Nombre', '100,300', 'CodigoPuntoAtencion#nombrePuntoAtencion#PuntoAtencion', 'Puntos de atencion','');
			return 0;
		}

		if(Tipo == 'PuntosAtencionNoProveedor'){
			showWindowFind('Codigo,Nombre,Id', 'puntoAtencion', 'Activo | 1 And IsProveedor | 0', 'Codigo,Nombre', '100,300', 'CodigoPuntoAtencion#nombrePuntoAtencion#PuntoAtencion', 'Puntos de atencion','');
			return 0;
		}
		
		if(Tipo == 'TiposConsulta'){
			showWindowFind('Id,Nombre', 'TipoConsulta', 'Activa | 1', 'Codigo,Nombre', '100,300', 'tipoConsulta#nombreTipoConsulta', 'Tipos de consulta','');
			return 0;
		}
		
		if(Tipo == 'AsuntosCita'){
			showWindowFind('Id,Nombre', 'sis_asunto', 'activo | 1', 'Codigo,Nombre', '100,300', 'asunto#nombreAsunto', 'Asuntos cita','');
			return 0;
		}
		
		if(Tipo == 'Pacientes'){
			showWindowFind('autoid,NombreCompleto', 'sis_paci', '', 'Codigo,Nombre', '100,300', 'paciente#nombrePaciente', 'Pacientes','');
			return 0;
		}
		
		if(Tipo == 'Cajeros'){
			showWindowFind('usu.id,usu.nombre', 'dbo.pagos p,dbo.usuario usu', 'usu.cedula|p.cod_usuario AND usu.status | 1 GROUP BY usu.id,usu.nombre', 'Codigo,Nombre', '100,300', 'cajero#nombreCajero', 'Cajeros','');
			return 0;
		}
		
		
}

function fnBusquedaServiciosxTipoRips(tipo){
	showWindowFind('id,nombre', 'sis_tipo', 'filerips | !'+tipo+'!', 'Codigo,Nombre', '100,300', 'servicio#nombreServicio', 'Servicios','');
}

function fnBusquedaManualesTarifarios(elementId){
	showWindowFind('codigo, nombre','sis_manual','','Codigo, Nombre','80, 300',elementId,'Manuales','');
}

function fnResetBusquedasOT(Tipo){
	if(Tipo == "Servicios"){
		$("#servicio").val(0);
		$("#nombreServicio").val("TODOS");	
		return 0;
	}
	
	if(Tipo == "Medicos"){
		$("#medico").val(0);
		$("#nombreMedico").val("TODOS");	
		return 0;	
	}
	
	if(Tipo == "Especialidades"){
		$("#especialidad").val(0);
		$("#nombreEspecialidad").val("TODAS");
		return 0;
	}
	
	if(Tipo == "PrefijosServicios"){
		$("#prefijo").val(0);
		$("#nombrePrefijo").val("TODOS");		
		return 0;
	}
	
	if(Tipo == "AsuntosCita"){
		$("#asunto").val(0);
		$("#nombreAsunto").val("TODOS");		
		return 0;
	}
	
	
	if(Tipo == "TiposConsulta"){
		$("#tipoConsulta").val(0);
		$("#nombreTipoConsulta").val("TODOS");		
		return 0;
	}
	
	if(Tipo == "PuntosAtencion"){
		$("#PuntoAtencion").val(0);
		$("#CodigoPuntoAtencion").val("");
		$("#nombrePuntoAtencion").val("TODOS");		
		return 0;
	}
	
	if(Tipo == "Pacientes"){
		$("#paciente").val(0);	
		$("#nombrePaciente").val("TODOS");
		return 0;
	}
	
	if(Tipo == "Cajeros"){
		$("#cajero").val(0);	
		$("#nombreCajero").val("TODOS");
		return 0;
	}
	
		if(Tipo == "ufuncional"){
			$("#ufuncional").val(0);	
			$("#nombreUfuncional").val("TODOS");
			chageUnidadFuncionalRemove();
			return 0;
		}
}





function fnBusquedaUfuncional(){
		showWindowFind('id,descripcion', 'ufuncionales', 'habilitada=1', 'id,descripcion', '100,300', 'ufuncional#nombreUfuncional', 
		'Ufuncional','chageUnidadFuncional()');	
}


function fnBusquedaEmpresas(idsSet){
	idsSet = idsSet || 'empresa#nombreEmpresa';
	showWindowFind('codigo,nombre', 'sis_empre', '', 'Codigo,Nombre', '100,300', idsSet, 
		'Empresas','');
	
}

function fnResetBusquedaEmpresas(ids){
	ids = ids || "#empresa;#nombreEmpresa";
	var a = ids.split(';');	
	$(a[0]).val("");
	$(a[1]).val("TODAS");
}

function fnBusquedaContratos(filtroxEmpresa,idCampo){
	var filtroxEmpresa = filtroxEmpresa || 0;
	if(filtroxEmpresa == 0){
		showWindowFind('codigo,nombre', 'contratos', 'activo | 1', 'Codigo,Nombre', '100,300', 'contrato#nombreContrato', 
		'Contrato','');
	}else{
		showWindowFind('codigo,nombre', 'contratos', 'activo | 1 and empresa | !'+$(idCampo).val()+'!', 'Codigo,Nombre', '100,300', 'contrato#nombreContrato', 
		'Contrato','');
	}
}

function fnResetBusquedaContratos(){
	$("#contrato").val(0);
	$("#nombreContrato").val("TODOS");
}

function fnImprimirInformeCostoOT(){
	var flag = 0;
	// Reporte resumido por servicios
	if($("#medico").val() == 0 && $("#detallarMedicos").val() == 0){
		$("#rptName").val("InformeCostoOrd_TransxServicio_Resumido");
		flag = 1;
	}
	
	if($("#resumenMedicos").val() == 1 && $("#detallarMedicos").val() == 0){
		$("#rptName").val("InformeCostoOrd_TransxServicioxMedico_Resumido");
		flag = 1;
	}
	
	// Reporte resumido por medico
	if(($("#medico").val() > 0 && $("#detallarMedicos").val() == 0) && flag == 0){
		$("#rptName").val("InformeCostoOrd_TransxServicioxMedico_Resumido");
		flag = 1;
	}
	
	// Reporte detallado por medico
	if(($("#detallarMedicos").val() > 0) && flag == 0){
		$("#rptName").val("InformeCostoOrd_TransxServicioxMedico_Detallado");
		flag = 1;
	}
	
	imprimirInforme();	

}

function fnImprimirInformeConsultasxMedicoCostos(){
	var flag = 0;
	// Reporte resumido por servicios
	if($("#medico").val() == 0){
		$("#rptName").val("InformeConsultasRealizadasxMedicoCostos");
		flag = 1;
	}
	
	if($("#medico").val() > 0 || $("#detallarMedicos").val() == 1){
		$("#rptName").val("InformeConsultasRealizadasxMedicoCostosDetalle");
		flag = 1;
	}
	
	imprimirInforme();	
}

function ImprimirEstadisticaProductividadMedico(){
		invocarReporte("estadisticaProductividadMedico.php?fechaini=" 
			+ getValue("inicial") + "&fechafin=" 
			+ getValue("final"), "");
			
	}

function ImprimirEstadisticaCostosServiciosResumidos(){
		invocarReporte("EstadisticaCostoServiciosResumido.php?fechaini=" 
			+ getValue("inicial") + "&fechafin=" 
			+ getValue("final"), "");
	}
	
function ImprimirEstadisticaConsumoGeneralMedicamentos(){
		invocarReporte("estadisticaConsumoGeneralMedicamentos.php?fechaini=" 
			+ getValue("ano")+''+getValue("mesInicial") + "&fechafin=" 
			+ getValue("ano")+''+getValue("mesFinal")+"&conceptoConsumo="+getValue("conceptosGastos"), "");
	}	


function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}

function notificacionGlobal(tipo,mensaje){
	if(tipo=='success'||tipo==undefined||tipo=="undefined"){
		alertify.success(mensaje);	
	}
}


function cargarManual(url){

	if (!$('#divUrl').length){
		var $divWidth='95%';
		var $divHeight='550px';
		var $colorStyleDiv='#398EC7';
		
		var cssDiv={
			color:'#FFF',
			backgroundColor:'#E6EDEF',
			width:$divWidth,
			position:'absolute',
			top:'50%',
			left:'50%',
			display:'none',
			height:$divHeight,		
			borderLeft:'solid',
			borderRight:'solid',
			borderTop:'solid',
			borderBottom:'solid',
			borderLeftWidth:'1px',
			borderRightWidth:'1px',
			borderTopWidth:'1px',
			borderBottomWidth:'1px',
			borderLeftColor:$colorStyleDiv,
			borderRightColor:$colorStyleDiv,
			borderTopColor:$colorStyleDiv,
			borderBottomColor:$colorStyleDiv,
			borderTopLeftRadius: '10px 5px',
			borderTopRightRadius: '10px 5px',
		};
		
		$div=$("<div id='divInformes'>" ).appendTo("body");
		$div.css(cssDiv);
		//alert($div.width());
		$w=parseInt($div.width()/2);
		$h=parseInt($div.height()/2);
		$div.css({ marginLeft : "-"+$w+"px",marginTop: "-"+$h+"px"});
		$div.show(300);
		ajaxPOST(getValue('ruta')+url,"divInformes","cargarCategorias();","","");
	}
}

function fnImprimirInformeConsumoPlanillasMctos(){
	$("#rptName").val($("#nombreReporteImprimir").val());
	imprimirInforme();
}

function fnBusquedaConsumoPlanillasMctos(Tipo){
	var municipio = "";
	if(Tipo == 'Municipios'){
		showWindowFind('codigo,nomMuni', 'municipiosView', '', 'Codigo,Nombre', '100,300', 'Municipio#nombreMunicipio', 'Municipios','');
	}
	
	if(Tipo == 'PuntoAtenciones'){
		municipio = $("#Municipio").val();
		if(municipio == ""){
			showWindowFind('Codigo,Nombre,Id', 'puntoAtencion', 'GeneraPlanilla | 1 And Activo | 1', 'Codigo,Nombre', '100,300', 'CodigoPuntoAtencion#nombrePuntoAtencion#PuntoAtencion', 'Puntos de atencion','');
		}else{
			showWindowFind('Codigo,Nombre,Id', 'puntoAtencion', 'GeneraPlanilla | 1 And dbo.fnTrim(Departamento) + dbo.fnTrim(Municipio) | !' + municipio + '! And Activo | 1', 'Codigo,Nombre', '100,300', 'CodigoPuntoAtencion#nombrePuntoAtencion#PuntoAtencion', 'Puntos de atencion','');
		}
	}
}

function fnResetBusquedaConsumoPlanillasMctos(Tipo){
	if(Tipo == 'Municipios'){
		$("#Municipio").val("");
		$("#nombreMunicipio").val("TODOS");
	}
	
	if(Tipo == 'PuntoAtenciones'){
		$("#PuntoAtencion").val(0);
		$("#CodigoPuntoAtencion").val("");
		$("#nombrePuntoAtencion").val("TODOS");
	}
}

function fnImprimirProcsRealizadosxServicioxMedicos(){
	if($("#manualRef").val() == ""){
		alert("Debe seleccionar un manual de referencia antes de continuar");
	}else{
		imprimirInforme();
	}
}

function fnGetGuiaMedica(){
	var tipo_servicio=$("#tipo_servicio").val();
	var id_contrato=$("#id_contrato").val();
	var actividadCheck=$("#actividadCheck").val();
	if($.trim(id_contrato)==''){
		notify("Seleccione un contrato","error");
	}else if($.trim(tipo_servicio)==''){
		notify("Seleccione un tipo de servicio","error");
	}else if($("#esMontoPcte").val()==1&&$("#tipoMontoPcte").val()==2&&$("#actividadCheck").val()==''){
		notify("Seleccione una actividad","error");
	}else{
		
		var widget = new ModalContentZeusSalud("Guia Medica",
			"../../controlador/HC/historiaClinica.php", {operacion:'getGuiasMedicas',tipo_servicio:tipo_servicio,id_contrato:id_contrato,actividad:actividadCheck,autoid:$("#autoid").val()}, "general", "", "700px", "2%",null,null,true);
			
			/* ModalContentZeusSalud(_title, url_content, dt, _type, validar, _width, _margin, target, callBackFn, _fixed, _id,callBackFnAfter)*/
			widget.imgs_data = {	
								
								close_icon:{
								src:"../../ImagenesZeus/salir 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon close",
								_title:"Cerrar",
								id:"cerrar"
								},
								
								confirmar_icon:{
								src:"../../ImagenesZeus/confirmar citas 01.png", 
								_w:32, 
								_h:32, 
								_class:"icon",
								_title:"Confirmar Procedimientos",
								id:"confirmar",
								fnAction:function(){
									confirmarGuiasMedicas();	
								}
								}
						 };
			widget.process();
	}
}


function confirmarGuiasMedicas(){
	var ids_procedimientos='';
	$('.guiaMedica:checked').each(function(indice, elemento) {
		//console.log(""+$(this).attr("procedimiento")+" "+$("#id_contrato").val());
		ids_procedimientos+="'"+$(this).attr("procedimiento")+"',";
		//cargarProcedimientos();
	});
	
	var id_contrato=get('id_contrato').value;
	var id_servicio=get('tipo_servicio').value;
	
	ajax("../../controlador/citas/citas.php?operacion=cargar_procedimientos_guia&ids_procedimientos="+ids_procedimientos+"&id_contrato="+id_contrato+"&id_servicio="+id_servicio, "contenidoProcedimientos",'closeModal();cargarProcedimientos();','');
	//	cargarProcedimientos(){
//		var id_procedimiento=get('id_procediento_realizar').value;
//    	var id_contrato=get('id_contrato').value;	
}



function checkAllGuia(codigo){
	if(get("guia"+codigo).checked){
		$(".guiaMedica_"+codigo).attr("checked",true);
	}else{
		$(".guiaMedica_"+codigo).attr("checked",false);	
	}
}

function showLoadingZS(title){
	if($.trim(title)==""){
		title="Cargando...";   		  
	}
	$css={
			position:"absolute",
			//width:"100px",
			//height:"100px",
			top:"50%",
			left:"50%",
			//marginTop:"-50px",
			//marginLeft:"-50px",
			border:"solid",
			backgroundColor:"#FFF",
			borderColor:"#5C70AF",
			borderWidth:"1px",
		}
	$div=$('<div class="divLoading-ZS">');
	$div.append(title);
	$div.css($css);
	$(document.body).append($div);
	
}

function hideLoadingZS(){
	$(".divLoading-ZS").remove();
}

function mostrarImg(img){
	
	window.open(img);
	
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}


function fnGetTiposCamaConfirmarQx(){	
	var codigo_contrato = $("#codContrato").val();
	var tipoAdmision = $("#tipo_admision").val();
	var uf = $("#ufuncional").val();
	var codigo_cama = $("#id_cama").val();
	
	if(codigo_contrato != "" && tipoAdmision != "" && uf != "" && codigo_cama != ""){
		ajaxPOST("../Facturacion/tipo_camas.php?operacion=listarCamas&codigo_contrato="+codigo_contrato
			+"&estudio=&uf="+uf+"&tipoAdmision="+tipoAdmision+"&codigo_cama="+codigo_cama+"&moduloQx=1", 'divTipo_de_camas', '');
	}
	
}

/*ACTIVANDO OPCIONES AVANZADAS ALT+F7*/
var ctrlPressed = false;
var teclaCtrl = 18, teclaC = 118

$(document).keydown(function(e){
	//alert(e.keyCode);
  if($("#ConfigEditParameter").length>0){
	  if (e.keyCode == teclaCtrl)
		ctrlPressed = true;
	
	  if (ctrlPressed && (e.keyCode == teclaC)){
		if($(".class_ModoDesarrollador").length>0){
			$(".class_ModoDesarrollador").remove();
		}else if(confirm("Mostrar opciones avanzadas de configuracion?")){
			addEditParams($("#ConfigEditParameter").attr("modulo"));
		}
	  }	  
  }
});

$(document).keyup(function(e){
	if($("#ConfigEditParameter").length>0){
	  if (e.keyCode == teclaCtrl){
		ctrlPressed = false;
	  }
	}
});

function addEditParams(Modulo){
	$css={
		color:'#FFF',
		fontSize:'18px',
		position:'fixed',
		top:'2px',
		backgroundColor:'#000',
		zIndex:'9999'
	}
	$labelCss={
			color:'#FFF',
			cursor:'pointer'
	}
	$div=$('<div class="class_ModoDesarrollador">');
	$a=$('<a href="javascript:void(0)" id="#editModuleParams" modulo="'+Modulo+'">')
	$label=$('<label>');
	$label.append('{Parametros}');
	$label.css($labelCss);
	$a.append($label);
	$a.click(function(){
		fnEditModuleParams($(this).attr("modulo"));
	});
	$div.append($a);
	$div.css($css);
	$(document.body).append($div);
}



function fnEditModuleParams(Modulo){	
	var widget = new ModalContentZeusSalud("Editar Parametros Modulo("+Modulo+")",
				"http://"+_location+"/SismaSalud/ips/App/controlador/Configuracion/EditModuleParams.php", {Op:"editModuleParams",Modulo:Modulo}, "general", "", "750px", "2%",null,null,true,'idShowEditModuleParams');
				
	widget.imgs_data = {	
						
						close_icon:{
						src:"../../ImagenesZeus/salir 01.png", 
						_w:32, 
						_h:32, 
						_class:"icon close",
						_title:"Cerrar",
						id:"cerrar"
						},
						
						/*guardar_icon:{
						src:"../../ImagenesZeus/guardar 01.png", 
						_w:32, 
						_h:32, 
						_class:"icon",
						_title:"Guardar Parametros",
						id:"guardar",
						fnAction:function(){
							
						}
						}*/
				 };
	widget.process();
		
}
/**************** PUGLINS ZEUSSALUD ***********************/
$(document).ready(function(e) {	
	
	/**************************************************************************************/
	$.fn.ZS_input=function(atributos,value){
		strAtributos='';strValue=''
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}
		if(value!=undefined && value!='undefined' && $.trim(value)!=''){
			strValue=value;
		}
		$input=$('<input '+strAtributos+' '+($.trim(strValue)!=''?'value="'+strValue+'"':'')+'>');
		$(this).append($input);
	}
	
	/**************************************************************************************/
	$.fn.ZS_textarea=function(atributos,content){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}
		$textarea=$('<textarea '+strAtributos+'>');
		if($.trim(content)!=''){
			$textarea.append(content);
		}
		$(this).append($textarea);
	}
	
	/**************************************************************************************/
	$.fn.ZS_select=function(atributos,option){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}
		$select=$('<select '+strAtributos+'>');
		$(option).each(function(index, element) {
			selected='';
			if(element.selected!=undefined && element.selected!='undefined' && $.trim(element.selected)=='1'){
				selected=' selected="selected" ';	
			}
			$select.append('<option '+selected+' value="'+element.value+'">'+element.name);
		});
		
		$(this).append($select);
	}	
	
	/**************************************************************************************/
	$.fn.ZS_br=function(){
		$(this).append($('<br>'));
	}
	/**************************************************************************************/
	$.fn.ZS_tabla=function(atributos){		
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}
		$tabla=$('<table '+atributos+'>');
		$(this).append($tabla);
	};
	/**************************************************************************************/
	$.fn.ZS_div=function(atributos){		
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}
		$div=$('<div '+atributos+'>');
		$(this).append($div);
	};
	/**************************************************************************************/
	$.fn.ZS_label=function(atributos,content){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}
		$label=$('<label '+strAtributos+'>');
		if($.trim(content)!=''){
			$label.append(content);
		}
		$(this).append($label);
	}
	
	/**************************************************************************************/
	$.fn.ZS_span=function(atributos,content){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}
		$span=$('<span '+strAtributos+'>');
		if($.trim(content)!=''){
			$span.append(content);
		}
		$(this).append($span);
	}

	/**************************************************************************************/
	$.fn.ZS_fieldset=function(atributos,legend,legendAttr){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}
		$fieldset=$('<fieldset '+strAtributos+'>');

		if(legend!=undefined && legend!='undefined' && $.trim(legend)!=''){
			$fieldset.append('<legend '+((legendAttr!=undefined && legendAttr!='undefined' && $.trim(legendAttr)!='')?legendAttr:'')+'>'+legend);
		}
		$(this).append($fieldset);
	};
	/**************************************************************************************/
	$.fn.ZS_tr=function(atributos){		
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}
		$tr=$('<tr '+strAtributos+'>');
		$(this).append($tr);
	};
	/**************************************************************************************/
	$.fn.ZS_td=function(atributos,content){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}		
		$td=$('<td '+strAtributos+'>');$td.append(content);
		$(this).append($td);
	};
	/**************************************************************************************/
	$.fn.ZS_th=function(atributos,content){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}		
		$th=$('<th '+strAtributos+'>');$th.append(content);
		$(this).append($th);
	};
	/**************************************************************************************/
	$.fn.ZS_ul=function(atributos,content){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}		
		$ul=$('<ul '+strAtributos+'>');$ul.append(content);
		$(this).append($ul);
	};
	/**************************************************************************************/
	$.fn.ZS_li=function(atributos,content){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}		
		$li=$('<li '+strAtributos+'>');$li.append(content);
		$(this).append($li);
	};
	/**************************************************************************************/
	$.fn.ZS_href=function(atributos,content){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}		
		$href=$('<a '+strAtributos+'>');$href.append(content);
		$(this).append($href);
	};
	/**************************************************************************************/
	$.fn.ZS_p=function(atributos,content){
		strAtributos='';
		if(atributos!=undefined && atributos!='undefined' && $.trim(atributos)!=''){
			strAtributos=atributos;
		}		
		$p=$('<p '+strAtributos+'>');$p.append(content);
		$(this).append($p);
	};
	/**************************************************************************************/
	$.fn.ZS_FlechaAbajo=function(url,_div,metodoAlCerrar,metodoAlAbrir,parametros){
		$cssImg={
			cursor:'pointer'
		}
		$div=$('<div>');
		$img=$('<img class="img_down" src="'+"http://"+_location+'/SismaSalud/ips/App/ImagenesZeus/flecha_azul_abajo.png" width="15">');
		$img.click(function (){	
			$varImg=$(this);		
			if($(this).hasClass("img_down")){	
				
				$.ajax({
					data:  parametros,
					url:   url,
					type:  'post',
					beforeSend: function () {
						crearLoadingJX();
					},
					success:  function (response) {
						_ResquestAJAX=response;
						
						$varImg.removeClass("img_down");
						$varImg.addClass("img_up");
						$varImg.attr("src","http://"+_location+"/SismaSalud/ips/App/ImagenesZeus/flecha_azul_arriba.png");	
												
						if($.trim(_div)!=''){
							try{
								$("#"+_div).html(response);						
							}catch(e){}
							$("#"+_div).css("display","");
						}
						
						eliminaLoadingJX();
						if($.trim(metodoAlAbrir)!=''){
							setTimeout(metodoAlAbrir,0)
						}
					}
				});
			}else{				
				$varImg.removeClass("img_up");
				$varImg.addClass("img_down");
				$varImg.attr("src","http://"+_location+"/SismaSalud/ips/App/ImagenesZeus/flecha_azul_abajo.png");
				if($.trim(_div)!=''){
					$("#"+_div).html("");
					$("#"+_div).css("display","none");
				}
				if($.trim(metodoAlCerrar)!=''){
					setTimeout(metodoAlCerrar,0)
				}
			}
		});
		$img.css($cssImg);
		$div.append($img);
		$(this).append($div);
	}
	/**************************************************************************************/
	$_ObjTemp=null; $_arrayObjectTemp=new Array();
	$.fn.ZS_set_referencia=function(){
		//console.log($_arrayObjectTemp);
		$cssLink={
			color:"#1a8090",
			fontWeight:'bold',
			cursor:'pointer',
			fontStyle:'italic',
		}
		$('.set-attr-referencia').each(function(index, element) {
		   $existe=false;
		   for(var i in $_arrayObjectTemp) {
				if($_arrayObjectTemp[i]._codigo==$(this).data('codigo_unico')){
					$existe=true;
					break;
				}
		   }
		    
			   $hidden=$('<input type="hidden" name="temp_id_attr" id="temp_id_attr"><input type="hidden" name="temp_name_attr" id="temp_name_attr">');
			   $(this).append($hidden);
		   if(!$existe){
			  
			   $link=$("<a>");
			   $link.css($cssLink);
			   $link.append("[Agregar Referencias]");
			   $div=$('<div>');
			   $div.data("codigo_unico",$(this).data("codigo_unico"));
			   $($link).data("div",$div);
			   $($link).data("divPpal",$(this));
			   $(this).data("div",$div);
			   $link.click(function(){
				   $_ObjTemp=$(this).data("div");
					showWindowFind('descripcion,id', 'variables_internas', 'estado | 1 AND Tipo IN ('+$(this).data('divPpal').data('referencia')+')', 
			'Nombre', '300', 'temp_name_attr#temp_id_attr', 'Referencias','fnSetDataRef()','',null,null,'idShowModalReferencia')

					
			   });
			   $_arrayObjectTemp.push({_codigo:$(this).data('codigo_unico'),_link:$link,_div:$div});
			   
		   }else{
		       $link=$_arrayObjectTemp[i]._link;
			   $div=$_arrayObjectTemp[i]._div;
		   }
		   
		   $(this).append($link); 
		   $(this).append($div);
        });
	};
	
	/**************************************************************************************/
	$.fn.ZS_get_referencias=function(){
		$object=new Array();
		$(".get-referencias-variables").each(function(index, element) {
        	$object.push({codigo_unico:$(this).data('codigo_unico_item'),codigo_referencia:$(this).data('codigo_referencia')});
        });
		return $object;
	}
	/**************************************************************************************/
	$.fn.ZS_CleanReferencias=function(){
		$_arrayObjectTemp=new Array();
	}
	
	
});
	/**************************************************************************************/
function fnSetDataRef(){
	$cssDiv={
		
		backgroundColor:'#FFF',
		border:'solid',
		float:'left',
		margin:'2px',
		borderWidth:'1px',
		borderColor:"#1a8090",
		padding:'2px',
		
	}
	$cssLabel={
		fontWeight:'bold'
	}
	
	$cssDelete={
		fontWeight:'bold',
		color:'#920000',
		cursor:'pointer',
	}
	
	$div=$('<div class="get-referencias-variables" data-codigo_referencia="'+$("#temp_id_attr").val()+'" data-codigo_unico_item="'+$_ObjTemp.data('codigo_unico')+'">');
	$label=$('<label class="letraDisplay">');
	$label.append($("#temp_name_attr").val()+' ');
	$delete=$('<span title="Eliminar">');
	$delete.append('[X]');
	$delete.data('div',$div);
	$delete.click(function(){
		$(this).data('div').remove();
	});
	$delete.css($cssDelete);
	$label.append($delete);
	$label.css($cssLabel);
	$div.append($label);
	$div.css($cssDiv);	
	$_ObjTemp.append($div);
}

//window.top.eventoMouse();

$(document).ready(function(event){	
	fechaSesion=localStorage.getItem('fechaSesion');
	if(fechaSesion!=null){
		ActionInactividad()
		setInterval(function(){
			verificarInactividad();
		}, 1000); // loop drawScene		
	}
	
});



function verificarInactividad(){
	var d = new Date();
	//
	fechaSesion=localStorage.getItem('fechaSesion');
	if(fechaSesion!=null){
		fechaAnt=addMinutes(fechaSesion, localStorage.getItem('tiempoLimiteSesion'))
		if(fechaAnt<d){
			/*window.top.alertGlobal('Ha sobrepasado el tiempo máximo de inactividad, <a href="#" onclick="javascript:window.open(\'http://\'+_location+\'/SismaSalud/ips/App/ctrl_usuario.php?oper=closeWindow\', \'_parent\')">inicie sesion</a> nuevamente.','Tiempo De Sesion Expirado',0);*/
			localStorage.removeItem('fechaSesion');
			localStorage.removeItem('tiempoLimiteSesion');
			localStorage.setItem('sesionFinalizado','1');
			window.open('http://'+_location+'/SismaSalud/ips/App/ctrl_usuario.php?oper=closeWindow', '_parent');
			
		}
	}
}
function ActionInactividad(){
	$('iframe').each(function(index) {
		$(this).contents().mousemove( function(e) {
			var d = new Date();
			//
			fechaSesion=localStorage.getItem('fechaSesion');
			if(fechaSesion!=null){
				fechaAnt=addMinutes(fechaSesion, localStorage.getItem('tiempoLimiteSesion'))
				if(fechaAnt<d){
					/*window.top.alertGlobal('Ha sobrepasado el tiempo máximo de inactividad, <a href="#" onclick="javascript:window.open(\'http://\'+_location+\'/SismaSalud/ips/App/ctrl_usuario.php?oper=closeWindow\', \'_parent\')">inicie sesion</a> nuevamente.','Tiempo De Sesion Expirado',0);*/
					localStorage.removeItem('fechaSesion');
					localStorage.removeItem('tiempoLimiteSesion');
					localStorage.setItem('sesionFinalizado','1');
					window.open('http://'+_location+'/SismaSalud/ips/App/ctrl_usuario.php?oper=closeWindow', '_parent');
					
				}else{
					localStorage.setItem('fechaSesion',d);
				}
			}
		});
		
		$(this).contents().keyup( function(e) {
			var d = new Date();
			//
			fechaSesion=localStorage.getItem('fechaSesion');
			if(fechaSesion!=null){
				fechaAnt=addMinutes(fechaSesion, localStorage.getItem('tiempoLimiteSesion'))
				if(fechaAnt<d){
					/*window.top.alertGlobal('Ha sobrepasado el tiempo máximo de inactividad, <a href="#" onclick="javascript:window.open(\'http://\'+_location+\'/SismaSalud/ips/App/ctrl_usuario.php?oper=closeWindow\', \'_parent\')">inicie sesion</a> nuevamente.','Tiempo De Sesion Expirado',0);*/
					localStorage.removeItem('fechaSesion');
					localStorage.removeItem('tiempoLimiteSesion');
					localStorage.setItem('sesionFinalizado','1');
					window.open('http://'+_location+'/SismaSalud/ips/App/ctrl_usuario.php?oper=closeWindow', '_parent');
					
				}else{
					localStorage.setItem('fechaSesion',d);
				}
			}
		});
	});
	$(document).contents().mousemove( function(e) {
			var d = new Date();
			//localStorage.setItem('fechaSesion',d);
			fechaSesion=localStorage.getItem('fechaSesion');
			if(fechaSesion!=null){
				fechaAnt=addMinutes(fechaSesion, localStorage.getItem('tiempoLimiteSesion'))
				if(fechaAnt<d){
					/*window.top.alertGlobal('Ha sobrepasado el tiempo máximo de inactividad, <a href="#" onclick="javascript:window.open(\'http://\'+_location+\'/SismaSalud/ips/App/ctrl_usuario.php?oper=closeWindow\', \'_parent\')">inicie sesion</a> nuevamente.','Tiempo De Sesion Expirado');*/
					localStorage.removeItem('fechaSesion');
					localStorage.removeItem('tiempoLimiteSesion',0);
					localStorage.setItem('sesionFinalizado','1');
					window.open('http://'+_location+'/SismaSalud/ips/App/ctrl_usuario.php?oper=closeWindow', '_parent');					
				}else{
					localStorage.setItem('fechaSesion',d);
				}
			}
	});	
	$(document).contents().keyup( function(e) {
			var d = new Date();
			//localStorage.setItem('fechaSesion',d);
			fechaSesion=localStorage.getItem('fechaSesion');
			if(fechaSesion!=null){
				fechaAnt=addMinutes(fechaSesion, localStorage.getItem('tiempoLimiteSesion'))
				if(fechaAnt<d){
					/*window.top.alertGlobal('Ha sobrepasado el tiempo máximo de inactividad, <a href="#" onclick="javascript:window.open(\'http://\'+_location+\'/SismaSalud/ips/App/ctrl_usuario.php?oper=closeWindow\', \'_parent\')">inicie sesion</a> nuevamente.','Tiempo De Sesion Expirado');*/
					localStorage.removeItem('fechaSesion');
					localStorage.removeItem('tiempoLimiteSesion',0);
					localStorage.setItem('sesionFinalizado','1');
					window.open('http://'+_location+'/SismaSalud/ips/App/ctrl_usuario.php?oper=closeWindow', '_parent');					
				}else{
					localStorage.setItem('fechaSesion',d);
				}
			}
	});	
}
function addMinutes(date, minutes) {
	var newDateObj = new Date(date);
	return new Date(newDateObj.getTime() + minutes*60000);
}
var imgDown = 'http://' + _location + '/SismaSalud/ips/App/Imagenes/down.gif';
var imgRight = 'http://' + _location + '/SismaSalud/ips/App/Imagenes/right.gif';
var arrowimages={down:['downarrowclass', imgDown, 23], right:['rightarrowclass', imgRight]}

var jqueryslidemenu={

animateduration: {over: 100, out: 100}, //duration of slide in/ out animation, in milliseconds

buildmenu:function(menuid, arrowsvar){
	jQuery(document).ready(function($){
		var $mainmenu=$("#"+menuid+">ul")
		var $headers=$mainmenu.find("ul").parent()
		$headers.each(function(i){
			var $curobj=$(this)
			var $subul=$(this).find('ul:eq(0)')
			this._dimensions={w:this.offsetWidth, h:this.offsetHeight, subulw:$subul.outerWidth(), subulh:$subul.outerHeight()}
			this.istopheader=$curobj.parents("ul").length==1? true : false
			$subul.css({top:this.istopheader? this._dimensions.h+"px" : 0});
			$curobj.children("a:eq(0)").css(this.istopheader? {paddingRight: arrowsvar.down[2]} : {}).append(
				'<img src="'+ (this.istopheader? arrowsvar.down[1] : arrowsvar.right[1])
				+'" class="' + (this.istopheader? arrowsvar.down[0] : arrowsvar.right[0])
				+ '" style="border:0;" />'
			);			
			$curobj.hover(
				function(e){
					var $targetul=$(this).children("ul:eq(0)")
					this._offsets={left:$(this).offset().left, top:$(this).offset().top}
					var menuleft=this.istopheader? 0 : this._dimensions.w
					menuleft=(this._offsets.left+menuleft+this._dimensions.subulw>$(window).width())? (this.istopheader? -this._dimensions.subulw+this._dimensions.w : -this._dimensions.w) : menuleft
					if ($targetul.queue().length<=1) //if 1 or less queued animations
						$targetul.css({left:menuleft+"px", width:this._dimensions.subulw+'px'}).slideDown(jqueryslidemenu.animateduration.over)
				},
				function(e){
					var $targetul=$(this).children("ul:eq(0)")
					$targetul.slideUp(jqueryslidemenu.animateduration.out)
				}
			) //end hover
			$curobj.click(function(){
				$(this).children("ul:eq(0)").hide()
			})
		}) //end $headers.each()
		$mainmenu.find("ul").css({display:'none', visibility:'visible',border:'3px'})
		$mainmenu.find("ul").addClass('selected');
		
		iframeBottom=$("#zs-piepagina-appinfo").attr("nombre");
		
		$("#"+iframeBottom).css("height",'calc(100% - '+($("#table-zs-menubar").height()+75)+'px)');
		
		ActionLinkModulo();
	}) //end document.ready
}
}

function ActionLinkModulo(){
	if($("#div_ModalModule").length>0){
		return false;
	}
	
	window.top.showMenuModulos()
	
	$(".IconModulo").click(function(){
		if($("#div_ModalModule").is(":hidden")){
			$("#div_ModalModule").css("display","none");
			$("#div_ModalModule").slideDown();	
			$(".IconModulo").addClass("IconModuloActive");
		}else{
			$(".IconModulo").removeClass("IconModuloActive");
			$("#div_ModalModule").slideUp();
		}
	});
}

function showMenuModulos(){
	if($("#div_ModalModule").length>0){
		return false;
	}
	
	respModulos=function(){
		var resp=JSON.parse(_ResquestAJAX);
		$divGral=$('<div id="div_ModalModule">');		
		$divGral.ZS_tabla('width="100%" height="20px" cellpadding="0" cellspaing="0" bgColor="#34b0c4"');
		$tabla.ZS_tr('');
		$tr.ZS_td('width="25px"','<img style="width:20px;height:20px;" src="http://'+_location+'/SismaSalud/ips/App/ImagenesZeus/Simbolo_Zeus.png">');
		$tr.ZS_td('align="left" class="letraCaptionLink"','<b>Modulos Sisma Asistencial</b>');
		$img=$('<img title="Maximizar Men&uacute; M&oacute;dulos" style="cursor:pointer;width:13px;height:13px;" src="http://'+_location+'/SismaSalud/ips/App/ImagenesZeus/aumentar.png">');
		$tr.ZS_td('width="25px" align="center"',$img);
		$img.click(function(){
			window.top.redireccionarMenu('','',1);
		});
		
		
		$tr.ZS_td('width="50px" align="center" class="letraCaptionLink"','<b>[Cerrar]</b>');
		$td.addClass('tdIconModulo');
		$td.attr('title','Cerrar');
		$td.click(function(){
			$(".IconModulo").removeClass("IconModuloActive");
			$("#div_ModalModule").slideUp();
		});
		
		$divGral.ZS_div('id="div_ModalModule2"');
		
		for(var i in resp){			
			$divItem=$('<div class="DivItemModulo" '+((resp[i]["javascript"]=="1")?'onclick="'+resp[i]["link"]+'"':'')+'>');
			$divItem.ZS_tabla('width="100%" cellpadding="0" cellspacing="0"');
			$tabla.ZS_tr('height="50px"');
			$tr.ZS_td('width="50px" style="border-right:solid;border-right-width: 1px;border-right-color: #355C6E;" align="center" bgcolor="#E7E7E7" valign="center"','<img style="width:50px;height:50px;" src="http://'+_location+'/SismaSalud/ips/App/ImagenesZeus/Modulos/'+resp[i]["ImagenModulo"]+'">');
			$tr.ZS_td('width="200px"  class="letraDisplay" style="padding:10px"','<b>'+resp[i]["etiqueta"]+'</b><br>'+resp[i]["DescripcionModulo"]);
			$divItem.data('hijo',resp[i]["hijo"]);
			$divItem.data('link',$.trim(resp[i]["link"])+''+(($.trim(resp[i]["parametro"])!='')?'?'+resp[i]["parametro"]:''));
			
			if(resp[i]["javascript"]!="1"){
				// console.log(resp[i]["externo"], resp[i]["linkModuloExterno"]);
				if(resp[i]["externo"]=="1" && resp[i]["linkModuloExterno"]!=null){
					$divItem.click(function(){
						redireccionarMenuExterno($(this).data('hijo'));
					});
				}else{
					$divItem.click(function(){
						window.top.redireccionarMenu($(this).data('link'),$(this).data('hijo'));
					});
				}				
			}
			$div.append($divItem);
			
		}
		$divGral.css("max-height",'calc(100% - '+($("#table-zs-menubar").height()+79)+'px)');

		//
		$divGral.appendTo("body");
		$div.css("max-height",$divGral.height()-7);

		
		
	}
	ajaxPOST('http://'+_location+'/SismaSalud/ips/App/controlador/sistemas/MenuModular.php',"","respModulos();","",$.param({operacion:'GetModulos'}));
	
}

function redireccionarMenuExterno(idmenu){
	reloadPageExterna=function(){
		var resp=JSON.parse(_ResquestAJAX);
		window.open(resp.path+resp.link+"?modulo="+resp.modulo+"&conexion="+resp.conexion+"&punto="+resp.idPunto+"&user="+resp.user+"&contra="+resp.pass, '_blank');
	}
	ajaxPOST('http://'+_location+'/SismaSalud/ips/App/controlador/sistemas/MenuModular.php',"","reloadPageExterna()","",$.param({operacion:"GetVarsRedirectExterno",idmenu:idmenu}));
}

function redireccionarMenu(url,hijo,volverMenu){
	reloadPageInicial=function(){
		window.top.location.reload();
	}
	
	ajaxPOST('http://'+_location+'/SismaSalud/ips/App/controlador/sistemas/MenuModular.php',"","reloadPageInicial()","",$.param({operacion:"SetMenuHijo",CodigoMenu:hijo,Url:url,volverMenu:$.trim(volverMenu)}));
}

function cargarMctosSuspendidosPcte(dataRow){
	console.log(dataRow);

	$divGral=$("<div>");
	$divGral.ZS_tabla('class="intercalarColorTab" width="100%"');
	
	var resp=dataRow.MctosSuspender;
	for(var i in resp){
	$tabla.ZS_tr('');
	$tr.ZS_td('width="120px" align="left" class="letraDisplay"','<label style="color:red">'+resp[i]["CodMedicamento"]+'</label>');	
	$tr.ZS_td('align="left" class="letraDisplay"','<label style="color:red">'+resp[i]["NomMedicamento"]+'</label>');	
	}

	var widget = new ModalContentZeusSalud("Los siguientes medicamentos estan suspendidos:",
				$divGral, {}, "content", "", "700px", "",null,null,false,'idShowModalMctosSuspendidos');
				
				
				widget.imgs_data = {	
									
									close_icon:{
									src:"../../ImagenesZeus/salir 01.png", 
									_w:32, 
									_h:32, 
									_class:"icon close",
									_title:"Cerrar",
									id:"cerrar"
									}
							 };
				widget.process();	
}

//Desarrollado en Sisma Corporation.
function ImprimirReporte(ruta, formdata, excel = true){
	form = $('<form>');
	// form.attr( "enctype", 'text/plain');
	name = '/public';
	form.attr('action', name+"/generarReporte");
	// form.attr('target', '_blank');
	form.attr('class', 'formTempReport')
	form.attr('method', 'GET');
	input = $('<input>');
	input.attr('name', 'ruta');
	input.val(ruta);
  
	input2 = $('<input>');
	input2.attr('name', 'data');
	input2.val(JSON.stringify(formdata));
  
	if(excel){
	  input4 = $('<input>');
	  input4.attr('name', 'excel');
	  input4.val(excel);
	  form.append(input4);
	}
  
	
  
	btn = $('<input>');
	btn.attr('type', 'submit');
	form.append(input);
	form.append(input2);
	
	form.append(btn);
	$(document.body).append(form);
	console.log($('#path_new_sisma').val() + name+"/generarReporte?");
	
	window.open('http://zeussalud.csm.net.co:82/sismanuevo' + name+"/generarReporte?"+ form.serialize())
	$('.formTempReport').remove();
}

function ChecksCitasMultiplesProcs(){
	var esCitaMultiple = get('esCitaMultiple').value;
	if(esCitaMultiple == 1){
		$('.checkEsCitaMultiple').show(); 
		$('.checkEsCitaMultiple').removeAttr('disabled');
		$('input[name=canasta]').attr('disabled', true);
		$('input[name=canasta]').addClass('normDisabled');
		$('input[name=canasta]').val("");
		$('input[name=stock]').attr('disabled', true);
		$('input[name=stock]').addClass('normDisabled');
		$('input[name=stock]').val("");
		$('#procedimiento_noqx').attr('checked', true); 
		get('opc_proc_noqx').style.display="";
		$("#btnAsignar").show();
		//$("#forms_cita").css("width", "");
	}else{
		$(".esCitaMultiple").attr("checked",false);
		$('.checkEsCitaMultiple').hide(); 
		$('.checkEsCitaMultiple').attr('disabled', true);
		$('input[name=canasta]').removeAttr('disabled');
		$('input[name=canasta]').removeClass('normDisabled');
		$('input[name=stock]').removeAttr('disabled');
		$('input[name=stock]').removeClass('normDisabled');
		$("#tbodyy").html('');
		$("input[name=checkEsCitaMultiple]").attr('checked', false);
		$('#procedimiento_noqx').attr('checked', false);
 		$("#stock").val("");	
		$("#id_stock").val("");
		get('opc_proc_noqx').style.display="none";
		deleteAllCitaMultiple();
		ajaxPOST("../../controlador/citas/citas.php", '','','',"operacion=limpiarCitasMultiples");		
		$('#procedimiento_noqx').attr('checked', false);
		$("#btnAsignar").hide();
	}
	if( esCitaMultiple == 2 ){
		get('tabla_cita_agrupada').style.display="";
		$('#procedimiento_noqx').attr('checked', false)
		$("#procedimiento_noqx").attr('disabled','disabled');
	}else{
		get('tabla_cita_agrupada').style.display="none";
		$("#procedimiento_noqx").removeAttr('disabled');
	}

	cod_contrato = $("#id_contrato").val();
	id_medico = get('codigo_medico').value;
	cargarAsuntosMedico(id_medico,'',cod_contrato);
}

function SeleccionarAsuntos(select, numid ) {
	var  cita = $(select).val();

	ajaxPOST("../../controlador/citas/citas.php","",'obtenerid_medico('+numid+');',"","operacion=obtener_cod_medico&cita="+cita);
 
}

function obtenerid_medico(numid){
	var resp=JSON.parse(_ResquestAJAX);
	
	var id_medico= resp.id_medico;
	cod_contrato = $("#id_contrato").val();

	if($('#esCitaMultiple').val() == 1 ){
		cargarAsuntosMedicoMultiple(id_medico,'',cod_contrato, numid);
	}
	
}

function cargarAsuntosMedicoMultiple(id_medico,asuntoCita,cod_contrato){
	deshabilitarToolBar();
	ajaxPOST("../../controlador/citas/citas.php","","setAsuntosMedicoMultiple('"+asuntoCita+"' );habilitarToolBar();","","operacion=cargarAsuntosMedico&id_medico="+id_medico+"&asuntoCita="+asuntoCita+"&contrato_paci="+cod_contrato+"&c_multiple="+1);
}

function setAsuntosMedicoMultiple(asuntoCita, Asuntos_multiple){
	$("#Asuntos_multiple"+numid+" option").remove();
	var resp=JSON.parse(_ResquestAJAX);

	$("#Asuntos_multiple"+numid).append('<option value="">[SELECCIONE]</option>');
	for(var i in resp) {
		$("#Asuntos_multiple"+numid).append('<option value="'+resp[i]["id"]+'">'+resp[i]["nombre"]+'</option>');
	}
	
	$("#Asuntos_multiple"+numid).val(asuntoCita);
	
	if($.trim(asuntoCita)==''){
		$("#Asuntos_multiple"+numid).val($_asuntoTemp);
	}
	
	try{
		if(get('estado_cita').value!='P'&&get('id_cita_aplazar').value!='0'){
			$("#Asuntos_multiple"+numid).val($('#id_asunto_cita_aplazar').val());
		}
	}catch(e){}
	
	validarContrato();
}


function selectcheked_cita_multiple(id){

	if( !$('#checkEsCitaMultiple_'+id).attr('checked')  ){
		$("#checkEsCitaMultiple_"+id).attr("checked",true );		
		sesioncheckEsCitaMultiple(id, 0);
		
	}
}

// Respuesta de la reversion de nota
function verificar_revertir_nota(){
	var resp=JSON.parse(_ResquestAJAX);

	//console.log(resp);
	
	if($.trim(resp["ERRORMSG"])==''){
		alert('Nota Revertida con exito');
		closeModal(); // Cerramos el modal
		location.reload(); // Recargamos la vista
		//preview();
		//$("#trPoliza").css({display:''});
		// window.top.fnCargarSelectPoliza($("#autoid").val(),$("#codigo_empresa").val(),$("#no_poliza"),resp[0]["NumeroPoliza"]);
	}else{
		alert($.trim(resp["ERRORMSG"]));
	}
}

//Mandamos la petición al controlador Nota credito
function guardar_reversion_nota(documento, fuente){
	var motivo_revertir_items = $('#motivoNotarevertir').val();
	ajaxPOST("../../Vistas/Facturacion/CtrlNotaCredito.php","","habilitarToolBar();verificar_revertir_nota()","","operacion=revertiritems&onrm_documento_="+documento+"&ofuente_="+fuente+"&omotivo_="+motivo_revertir_items);
} 

// Creamos la función para revertir items 
function revertir_items(documento, fuente){
	alertify.confirm('&#191;Esta seguro de revertir la Nota Credito?', function (e) {
		if(e){
			//Creamos el contenedor de motivo
			var $div = $(document.createElement("div"));				

			var $text = $(document.createElement("textarea"));
			$text.attr("id", "motivoNotarevertir");
			$text.attr("name", "motivoNotarevertir");
			$text.addClass("norm");
			$text.css({width:"800px", height:"100px", textTransform:"uppercase"});
			$text.appendTo($div);
			
			var w = new ModalContentZeusSalud("Motivo De reversion", $div, {}, "content", "", null, null, null,function(){$("#motivoNota").focus()},false, 'motivo_revertiNota');
			var icons = {	revertir_icon:{
							src:"../../ImagenesZeus/guardar 01.png",
							_w:32,
							_h:32,
							_class:"icon save",
							_title:"Revertir",
							id:"revertirBtn",
							fnAction: function (){guardar_reversion_nota(documento, fuente)} // Boton guardar
							}
					}
			$.extend(w.imgs_data, icons);
			w.process();
		}
	});
}


function Agrupar_citas_multiples (){
	
	var codigo_medico = $("#codigo_medico").val();
	var medico = $("#medico").val();
	var numeroservicio = $("#nomservicio").val();
	var servicio = $("#nomservicio").find('option:selected').text();
	var fecha_hora = $("#fecha_cita").val()+' '+$("#hora").val()+':'+$("#minutos").val()+' '+$("#meridiano").val();
	var id_programacion = $("#id_programacion").val();
	var IdProgMedDeta = $("#IdProgMedDeta").val();
	var tipo_servicio = $("#tipo_servicio").val();
	var tipo_servicio_nombre = $("#tipo_servicio").find('option:selected').text();

	if(codigo_medico == ""){
		notify("Seleccione un Medico","error");
		return false;
	}else if( numeroservicio == ""){ 
		notify("Seleccione un Asunto","error");
		return false;
	}

	datos_agrupar_cita_multiple=function(){
		resp=JSON.parse(_ResquestAJAX);
		setear_citas_agrupadas(resp);
	}

	ajaxPOST("../../controlador/citas/citas.php","","datos_agrupar_cita_multiple()",""
	,"operacion=Agrupar_citas_multiples&codigo_medico="+codigo_medico+"&medico="+medico+"&numeroservicio="+numeroservicio+"&fecha_hora="+fecha_hora+"&servicio="+servicio+"&id_programacion="+id_programacion+"&IdProgMedDeta="+IdProgMedDeta+"&tipo_servicio="+tipo_servicio+"&tipo_servicio_nombre="+tipo_servicio_nombre);
}

function delete_agrupar_cita_multiple(id=null){

	delete_datos = function(){
		resp=JSON.parse(_ResquestAJAX);
		setear_citas_agrupadas(resp);
	}

	ajaxPOST("../../controlador/citas/citas.php","","delete_datos()",""
	,"operacion=delete_agrupar_cita_multiple&id="+id);

}

function setear_citas_agrupadas( resp, ){
	if(resp.msg == "Error" ){
		notify("Error Debe cargar un procedimiento.","error");
		return false;
	}else{
	
		cleanProcsSelected();
	  	$("#codigo_medico").val("");
		$("#medico").val("");
		$("#nomservicio").val("");
		$("#fecha_cita").val("");
		$("#hora").val("");
		$("#minutos").val("");
		$("#meridiano").val("");

		var cita = resp.msg;
		var string_table =""; 
		var procedimiento;
		for (let index = 0; index < cita.length; index++) {
			procedimiento = cita[index];
	
			for (let j = 0; j  < procedimiento.length; j++) {

				var cadena = procedimiento[j]['codigo_proc']+"  "+procedimiento[j]['nombreve'];
				var sub_cadena = cadena.substr(0,18);
				sub_cadena+="...";

				var procedimientos = procedimiento[j]['tipo_servicio_nombre'];
				var sub_procedimientos = procedimientos.substr(0, 18);
				sub_procedimientos +="...";
				string_table += "<tr>"
				string_table += "<td  class='letraDisplay'>"+procedimiento[j]['fecha_hora']+"</td>"+"<td  class='letraDisplay'  >"+procedimiento[j]['medico']+"</td>"+"<td  class='letraDisplay' ><p  class='letraDisplay' title='"+cadena+"'>"
				+sub_cadena+"</p></td>"+"<td  class='letraDisplay' ><p class='letraDisplay' title='"+procedimientos+"'>"+sub_procedimientos+"</td>"+"<td  class='letraDisplay' >"+procedimiento[j]['servicio']+"</td><td  class='letraDisplay' > <a href='javascript:delete_agrupar_cita_multiple("+procedimiento[j]['codigo_proc']+");'><img src='../../Imagenes/Eliminar.png' border='0'></a></td>";	
				string_table += "</tr>";
			}		
		}
	}
	
	$("#tbodyy").html(string_table);
}



function addPacienteAgrupada (){
	var tipo_identificacion = $("#tipo").val();
	var numero_id = $("input[name=ident]").val();
	var contrato = $("#contrato").val();
	var id_contrato = $("#id_contrato").val();
	var autoid = $("#autoid").val();
	var pnombre = $("#pnombre").val();
	var snombre = $("#snombre").val();
	var papellido = $("#papellido").val();
	var sapellido = $("#sapellido").val();
	var nombre_paciente = pnombre+"  "+snombre+"  "+papellido+" "+sapellido;

	pacinete_grupal = function (){
		resp=JSON.parse(_ResquestAJAX);
		setear_pacinete_grupal(resp);
	}

	if(contrato != ""){
		ajaxPOST("../../controlador/citas/citas.php","","pacinete_grupal();limpiar_crear_cita();",""
		,"operacion=addPacienteAgrupada&tipo_identificacion="+tipo_identificacion+"&autoid="+autoid+
		"&numero_id="+numero_id+"&contrato="+contrato+"&id_contrato="+id_contrato+"&nombre_paciente="+nombre_paciente);
	
	}else{
		notify("Seleccione un contrato","error");
		return 0;
	}
	
}

function setear_pacinete_grupal(data){
 
	var pacientes = data.msg;
	var error = data.error;
	if(pacientes != null ){
		string ="";
		if(pacientes.length > 0 ){
			for (let index = 0; index < pacientes.length; index++) {
				
				string += "<tr> <td class='letraDisplay' > "+pacientes[index]['tipo_identificacion']+"   "+pacientes[index]['numero_id']+"</td>"
				+"<td class='letraDisplay' > "+pacientes[index]['nombre_paciente']+"</td>"
				+"<td class='letraDisplay' > "+pacientes[index]['contrato']+"</td>"
				+"<td class='letraDisplay' >  <a href='javascript:delete_cita_grupal("+pacientes[index]['autoid']+");'><img src='../../Imagenes/Eliminar.png' border='0'></a></td><tr>";
			}
		}
	}
	if(error != undefined && error !=""){
		notify(error,"error");
	}

	$("#tbody-cita_agrupada").html(string);
}


function delete_cita_grupal(autoid){


	delete_pacinete_grupal = function(){
		resp=JSON.parse(_ResquestAJAX);
		
		setear_pacinete_grupal(resp);
	}
	ajaxPOST("../../controlador/citas/citas.php","","delete_pacinete_grupal()",""
	,"operacion=delete_cita_grupal&autoid="+autoid);

}
 


function ver_pacientes_cita_Agrupada(id){


	respGetPacientesCita=function(){
		resp=JSON.parse(_ResquestAJAX);

		try{
		$divGral=$('<div style="max-height:400px; overflow:auto; width:100%">');
		$divGral.ZS_tabla('width="100%" class="intercalarColorTab"');
		$tabla.ZS_tr('');
		$tr.ZS_td('colspan="8" align="center" bgColor="#34b0c4" class="letraCaptionLink"','Pacientes');
		$tabla.ZS_tr('');
 
		$tr.ZS_td('align="center" bgColor="#34b0c4" class="letraCaptionLink"','Paciente');
		$tr.ZS_td('align="center" width="80px" bgColor="#34b0c4" class="letraCaptionLink"','Contrato');
		$tr.ZS_td('align="center" width="60px" bgColor="#34b0c4" class="letraCaptionLink"','Estudio');
		$tr.ZS_td('align="center" width="60px" bgColor="#34b0c4" class="letraCaptionLink"','Estado');
		$tr.ZS_td('align="center" width="80px" bgColor="#34b0c4" class="letraCaptionLink"','Recordatorio de Cita ');
		$tr.ZS_td('align="center" width="80px" bgColor="#34b0c4" class="letraCaptionLink"','Reportes ');
		$tr.ZS_td('align="center" width="80px" bgColor="#34b0c4" class="letraCaptionLink"','Cancelar Cita ');
		$tr.ZS_td('align="center" width="80px" bgColor="#34b0c4" class="letraCaptionLink"','Caja ');
		}catch(e){}
		var id = null;
		var  cancelar_cita ="";
		var  id_cita ="";
		var boton_imprimir = "";
		var boton_pago = "";
		var reportes = "";
		for(var i in resp){

			if(resp[i]["estado"] != 'CC' && resp[i]["estado"] != 'C' && resp[i]["estado"] != 'A'){
				boton_imprimir = "<input  type='button'  class='norm'  value='Imprimir' onclick=\"invocarReporte('Recordatorio_Cita.php?id="+resp[i]["id"]+"&tipocita=2&autoid="+resp[i]["autoid"]+"', 'citas')\" >";
			}
			if(resp[i]["estado"] != 'C' && resp[i]["estado"] != 'A' ){
				cancelar_cita = "<input type='checkbox'   class='check_autoid'  value='"+resp[i]["autoid"]+"'  >";
			}
			if(resp[i]["estado"] != 'A'){
				id_cita = "<input   type='hidden'  id='id_cita_grupal'  value='"+resp[i]["id"]+"'  >";
			}
			
			var caja = "<a  onclick=\"javascript:irAcaja_grupal('"+resp[i]['num_id']+"','"+resp[i]['tipo_id']+"') \" alt='Copagos'"
				+"onmouseout=\"cambiar(0,'IMGCopagos');\" onmouseover=\"cambiar(1,'IMGCopagos');\" " 
				+"style='  cursor: pointer; !important;padding:initial !important;margin:0px !important'>"
				+"<img style='outline: none;' src='../../ImagenesZeus/copagos%2002.png' id='IMGCopagos'"
				+" title='Copagos' alt='Copagos' border='0' name='IMGCopagos' width='32' height='32'></a>";
			if(resp[i]["estado"] == 'CC'){
				reportes = "<a href='javascript:void(0);' onclick=\"invocarReporte('reporte_certificacion2.php?estudio="+resp[i]['estudio']+"')\">"
					+"<img src='../../ImagenesZeus/imprimir 01.png' width='20'></a>"
				+"<a href='javascript:void(0);' onclick=\"invocarReporte('reporte_certificacion.php?estudio="+resp[i]['estudio']+"')\">"
					+"<img src='../../ImagenesZeus/imprimir 01.png' width='20'></a>"
				+"<a href='javascript:void(0);' onclick=\"invocarReporte('reporte_admision.php?estudio="+resp[i]['estudio']+"')\">"
					+"<img src='../../ImagenesZeus/imprimir 01.png' width='20'></a>";
			}

			
 			$tabla.ZS_tr('');
			$tr.ZS_td('class="letraDisplay"  align="center"',resp[i]["NombreCompleto"]);
			$tr.ZS_td('class="letraDisplay"  align="center"',resp[i]["contrato"]);
			$tr.ZS_td('class="letraDisplay"  align="center"',resp[i]["estudio"]);
			$tr.ZS_td('class="letraDisplay"  align="center" '+pintarEstadoCita(resp[i]["estado"],null,resp[i]["fechaCita"])+' align="center"',"");
			$tr.ZS_td('class="letraDisplay"  align="center"',boton_imprimir);
			$tr.ZS_td('class="letraDisplay"  align="center"',reportes);
			$tr.ZS_td('class="letraDisplay"  align="center"',cancelar_cita+id_cita);
			$tr.ZS_td('class="letraDisplay"  align="center"',caja);
			cancelar_cita = "";
			boton_imprimir = "";
		}
		 
		var widget = new ModalContentZeusSalud("Pacientes asociados",
			$divGral, {}, "content", "", "700px", "3%",null,null,false);
			
			widget.imgs_data = {	
								
				close_icon:{
				src:"../../ImagenesZeus/salir 01.png", 
				_w:32, 
				_h:32, 
				_class:"icon close",
				_title:"Cerrar",
				id:"cerrar"
				},
								
				confirmar_icon:{
					src:"../../ImagenesZeus/eliminar 02.png",  
					_w:32,  
					_h:32,  
					_class:"icon", 
					_title:"Cancelar", 
					id:"Guardar", 
					fnAction:function(){ 
						var id = $("#id_cita_grupal").val();
						var pacentes = new Array();
						/*Obtiene el autoid del paciente */
						$("input[class=check_autoid]:checked").each(function(){	
							pacentes.push($(this).val()); 
						});
						 
						if( pacentes.length > 0 ){
							cancela_cita_agrupada( id );
						}else{
							notify("Seleccione por lo menos un paciente.","error");
							return false;
						} 					
					}
				},
				reporte_pacientes:{
					src:"../../ImagenesZeus/ordenHC 02.png",  
					_w:32,  
					_h:32,  
					_class:"icon", 
					_title:"Pacientes cita asociada", 
					id:"Guardar", 
					fnAction:function(){ 
						var id = $("#id_cita_grupal").val();
						invocarReporte('Recordatorio_Cita_pacientes.php?id='+id,'citas');			
					}
				}
			};
			widget.process();
	}

	ajaxPOST("../../controlador/citas/citas.php","","respGetPacientesCita();habilitarToolBar();","","operacion=ver_pacientes_cita_Agrupada&IdCita="+id);
	 
}

function setar_tipo_cita(tipo_cita){
	var confirmada_agrupada= null;
	if( tipo_cita == 2 ){
		confirmada_agrupada = $("#confirmada_agrupada").val(1);
	}
	/* Setea el select de tipo de cita*/
	$("#esCitaMultiple").val(tipo_cita);
	$("#tipo_cita").val(tipo_cita);

}

function pintarEstadoCita(estado,cadena=null,fechaCita=null){
	var hoy             = new Date();
	var fechaFormulario = new Date(fechaCita);
	hoy.setHours(0,0,0,0); 
	var bgcolor = "";
	
	if( estado == 'P'  && fechaCita != null && fechaFormulario < hoy ){
		bgcolor= 'bgcolor="#FAED16"'; 
	}else if(estado=="P"&&cadena==null){
		bgcolor=  'bgcolor="#00FF00"';    
	}else if(estado=="C"&&cadena==null){
		bgcolor=  'bgcolor="#FF0000"';    
	}else if(estado=="A"&&cadena==null){
		bgcolor=  'bgcolor="#6EBCE5"';    
	}else if(estado=="CC"&&cadena==null){
		bgcolor=  'bgcolor="#0000FF"';    
	}else if(estado=="bgcolorBlueDark"){
		bgcolor=  'bgcolor="#7291C5"';    
	}
	
	return bgcolor;
}

 
function cancela_cita_agrupada(id ){
	var widget = new ModalContentZeusSalud("Cancelar Cita",
	"../../controlador/citas/citas.php?operacion=show_cancelar_cita_grupal&id="+id
	 , {}, "general", "", "400px", "10%",null,null,true, '#id_cancela');
	widget.process();	
}

function cancelarCitaGrupal(){
    
	var motivo=get('motivo').value;
	var id =get('id_cita_grupal').value;
	var motivoCancelaCita=get('motivoCancelaCita').value;
	var estado_cita=get('estado_cita').value;
	var pacientes = new Array();
	$("input[class=check_autoid]:checked").each(function(){	
		pacientes.push($(this).val()); 
	});
 
	if(trim(id)!=""){		
		ajax("../../controlador/citas/citas.php?operacion=cancelar_cita_grupal&id_cita="+id+"&motivo="+motivo+
		"&motivoCancelaCita="+motivoCancelaCita+"&pacientes="+JSON.stringify(pacientes),
		"txtCitaMarcada","cerrarDiv();cargarProgramacion('');",'');
	}
}


function pago_grupal(idCita, autoid){
	var contrato = 	$("#id_contrato_"+autoid).val();	
	
 	respGetTiposManuales=function(){
		var resp=JSON.parse(_ResquestAJAX);
	
		if(resp.respuesta == 1){
			alert("No se puede generar el recibo para contrato particular, verifique que la factura este cerada.");
		}else{
			var contrato=$("#id_contrato_"+autoid).val();
			var estrato=$("#id_estrato_"+autoid).val();
			var ufuncional=get('ufuncional').value;
			$("#autoid_pago").val(autoid);
			if(trim(estrato)==''){
				notify("Seleccione un estrato","error");
			}else if(trim(contrato)==''){
				notify("Seleccione un contrato","error");
			}else if(trim(ufuncional)==''){
				notify("Seleccione unidad funcional","error");
			}else{

				var subsidio=get('subsidio_'+estrato+'_'+autoid).value;
				var vlr_maximo=get('maximo_'+estrato+'_'+autoid).value;

				ajaxPOST("../../controlador/citas/citas.php", 'divPagos','verFormPagos();','',
				"operacion=cargarFormPagos&contrato="+contrato+'&estrato='+estrato+'&idCita='+idCita+'&subsidio='+subsidio+
				'&vlr_maximo='+vlr_maximo+'&ufuncional='+ufuncional+"&autoid="+autoid+"&pago_grupal=1");	
			}
		}
 		
	}		
	ajaxPOST("../../Vistas/Citas/CtrlAplazarCita.php",'','respGetTiposManuales()','',
	"operacion=parametro_bloqueo_pagos&contrato="+contrato,'');


}


function limpiar_crear_cita(){
	$('#ident').val("");
	$('#tipo').val("");
	$('#autoid').val("");
	buscarPacientes();	
}
 

function respuesta_PYP(){
	resp=JSON.parse(_ResquestAJAX);
	$('#procedientosHc').html(resp.payload);
}

function fnEliminarProcedimiento(indice_pyp_procedimiento){
	console.log('Eliminar Procs js');
	ajaxPOST("../../Vistas/Hc/CtrlDatosHistoria.php","","respuesta_PYP()",""
	,"operacion=eliminarProcPYP&key="+indice_pyp_procedimiento);
}
function cargarBateriaRutasAtencion(autoid) {
	$("#divalerta-ruta-atencion").hide();
	var procs = {};
	if($("input[name='id_riesgos_asign_procs[]']").length > 0){
		$("input[name='id_riesgos_asign_procs[]']").each(function(key, value){
			procs[key] = $(value).val();
		});
	}else{
		procs = null;
	}

	var asuntoscero = {};
	if($("input[name='asuntos_riesgoscero_asignados[]']").length > 0){
		$("input[name='asuntos_riesgoscero_asignados[]']").each(function(key, value){
			asuntoscero[key] = $(value).val();
		});
	}else{
		asuntoscero = null;
	}
	
	var asuntos = {};
	if($("input[name='asuntos_riesgos_asignados[]']").length > 0){
		$("input[name='asuntos_riesgos_asignados[]']").each(function(key, value){
			asuntos[key] = $(value).val();
		});
	}else{
		asuntos = null;
	}
    
	cargarbateria = function () {
		if(_ResquestAJAX != ""){
			res = JSON.parse(_ResquestAJAX);

            if(res.Riesgos != ""){
                $("#divalerta-ruta-atencion").show();
                setInterval(function(){
                    $('#Btnalerta-ruta-atencion').animate({
                    opacity: 'toggle'
                    },800);
                },900);
			
                var widget = new ModalContentZeusSalud("Ruta de Atenci&oacute;n del paciente",
                res.Riesgos,
                {}, "content", "", "80%", "5px",null,null,false,"id_test");
				widget.imgs_data = {
					close_icon:{
					src:"../../ImagenesZeus/salir 01.png", 
					_w:32, 
					_h:32, 
					_class:"icon close",
					_title:"Cerrar",
					id:"cerrar"
					},					
					confirmar_icon:{
						src:"../../ImagenesZeus/confirmar citas 01.png", 
						_w:32, 
						_h:32, 
						_class:"icon",
						_title:"Guardar",
						id:"confirmar",
						fnAction:function(){							
							$("#procs_riesgos_asignados").html("");
							$("#asuntos_riesgoscero_asignados").html("");
							$('#nomservicio option').removeAttr('disabled');

							if(res.CargarRutaCita == 'S' && $("input[name=cups_riesgos]:checked").filter(function() {
								return !this.disabled;
							}).length > 0){
								$('#procedimiento_noqx').attr('checked',true);
								get('opc_proc_noqx').style.display="";
							}
							
							if($("input[name=cups_riesgos]:checked").length == 0){
								cargarProcedimientos(1);
							}

							$("input[name=cups_riesgos]:checked").each(function(index){
                                if($(this).attr('disabled') != true){
                                    ident = $(this).attr('item-riesgo');
                                    input_cup = $('<input>');
                                    input_cup.attr("id", "id_riesgos_asign_procs");
                                    input_cup.attr("class", "procs_riesgo_ruta");
                                    input_cup.attr("name", "id_riesgos_asign_procs[]");
                                    input_cup.val(ident);
									$("#procs_riesgos_asignados").append(input_cup);
									
									if(res.CargarRutaCita == 'S'){
										// get('id_procediento_realizar').value = $(this).val();
										get('id_procediento_realizar').value = $(this).attr('data-codigo');
										get('tipo_servicio').value = $(this).attr('data-servicio');
										if(get('tipo_servicio').value != ""){
											if(index == 0){
												cargarProcedimientos(1);												
											}else{
												cargarProcedimientos();												
											}
											$("#nomservicio").focus();
										}else{
											// notify('No existe un servicio seleccionado.','error');

											notify('El servicio relacionado al procedimiento no se encuentra en citas.','error');
											$('#id_riesgos_asign_procs').remove();											
											$("#tipo_servicio").focus();
										}								
									}									
                                }
							});
							$("input[name=asuntos_riesgoini]:checked").each(function(){
								if($(this).attr('disabled') != true){
									ident = $(this).attr('item-riesgo');
									idasunto = $(this).attr('data-id_asunto');
                                    input_asun = $('<input>');
                                    input_asun.attr("id", "asuntos_riesgoscero_asignados");
                                    input_asun.attr("class", "asuntos_riesgocero_ruta");
                                    input_asun.attr("name", "asuntos_riesgoscero_asignados[]");
                                    input_asun.val(ident);
									$("#asuntos_riesgoscero_asignados").append(input_asun);
									// console.log($("#nomservicio option[value="+ idasunto +"]"));
									if(trim(get("codigo_medico").value) != ""){
										if($("#nomservicio option[value="+ idasunto +"]").length != 0){
											$("#nomservicio").val(idasunto);
											// $("#nomservicio").attr('readonly', 'readonly');
											$('#nomservicio option:not(:selected)').attr('disabled',true);
											$("#nomservicio").focus();
										}else{
											notify('El medico no posee el asunto del riesgo.','error');
										}
									}else{
										notify('No existe un Medico seleccionado.','error');
										$('#asuntos_riesgoscero_asignados').remove();
									}									
                                }
							});
							closeModal("#id_test");
						}
					}
				};
				widget.process();
			}
		}
	}
    ajaxPOST('../../Vistas/Configuracion/CtrlConfigurarRiesgos.php?operacion=CargarBateriaPendienteCita&autoid='+autoid+'&tipo_manual='+getValue("tipoManual")+'&baterias='+JSON.stringify(procs)+'&asuntocero='+JSON.stringify(asuntoscero)+'&asuntos='+JSON.stringify(asuntos)+'&cod_contrato='+getValue("id_contrato")+'&modulo=citas'//+'&es_cero=0'
    ,'','cargarbateria()', '');
}

function ProcRealizadoRutas(iditem){
	ajaxPOST('../../Vistas/Configuracion/CtrlConfigurarRiesgos.php?operacion=MarcarProcRealizado&id_item='+iditem+'&modulo=citas'
    ,'','MarcarRealizadoRes()', '');
}

function MarcarRealizadoRes() {
	if(_ResquestAJAX != ""){
		res = JSON.parse(_ResquestAJAX);
		console.log(res);
		if(res.estado == 1){
			notify(res.msg);
			autoid = $('#autoid').val();
			ActualizarModalRutas(autoid);
		}else{
			notify(res.msg,'error');
		}		
	}
}

function ActualizarModalRutas(autoid) {
	$("#divalerta-ruta-atencion").hide();
	var procs = {};
	if($("input[name='id_riesgos_asign_procs[]']").length > 0){
		$("input[name='id_riesgos_asign_procs[]']").each(function(key, value){
			procs[key] = $(value).val();
		});
	}else{
		procs = null;
	}

	var asuntoscero = {};
	if($("input[name='asuntos_riesgoscero_asignados[]']").length > 0){
		$("input[name='asuntos_riesgoscero_asignados[]']").each(function(key, value){
			asuntoscero[key] = $(value).val();
		});
	}else{
		asuntoscero = null;
	}
	
	var asuntos = {};
	if($("input[name='asuntos_riesgos_asignados[]']").length > 0){
		$("input[name='asuntos_riesgos_asignados[]']").each(function(key, value){
			asuntos[key] = $(value).val();
		});
	}else{
		asuntos = null;
	}

	cargarRutaModal = function () {
		res = JSON.parse(_ResquestAJAX);
		if(res.Riesgos != ""){
			$("#divalerta-ruta-atencion").show();
			setInterval(function(){
				$('#Btnalerta-ruta-atencion').animate({
				opacity: 'toggle'
				},800);
			},900);
		}
		$("#id_test .zs-body").html(res.Riesgos);
	}

	ajaxPOST('../../Vistas/Configuracion/CtrlConfigurarRiesgos.php?operacion=CargarBateriaPendienteCita&autoid='+autoid+'&tipo_manual='+getValue("tipoManual")+'&baterias='+JSON.stringify(procs)+'&asuntocero='+JSON.stringify(asuntoscero)+'&asuntos='+JSON.stringify(asuntos)+'&cod_contrato='+getValue("id_contrato")+'&modulo=citas'//+'&es_cero=0'
    ,'','cargarRutaModal()', '');
}


function irAcaja_grupal(ident,tipo ){
	$("#tipo").val(tipo);
	$("#ident").val(ident);
	abrirOpcion('../Caja/CtrlCajaCopago.php?tipo='+tipo+'&ident='+ident, 'f');	
}

function msgSuccess(msg,cerrar = false){
	toastr.remove();
	if(cerrar){
		toastr.success(msg,'',{
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "0",
			"hideDuration": "0",
			"timeOut": "0",
			"extendedTimeOut": "0",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		});
	}else{
		toastr.success(msg);
	}

}

function msgWarning(msg){
	toastr.remove();
	toastr.warning(msg);
}

function msgDanger(msg){
	toastr.remove();
	toastr.error(msg,'',{
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "0",
		"hideDuration": "0",
		"timeOut": "0",
		"extendedTimeOut": "0",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut",
		"tapToDismiss": false
	  })


}

function alertaModal(msg,tipo){
    title = 'INFORMACI\u00d3N IMPORTANTE';
	if(tipo == 'success'){
		title = 'Operacion Exitosa';
	}else if(tipo == 'error'){
		title = 'Ha ocurrido un error';
	}
	alertify.alert(title,msg).set('closable', false);
}