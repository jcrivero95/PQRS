<?php
@session_start();
include_once("../Model/Model.php");
include_once("../Vistas/General.php");
include_once("../Model/Utils/mail.php");

extract($_REQUEST);
$model=new Model();
$json=new PHP_JSON_AJAX();

$logueado = $model->statusUserLogueado();
$json->addParam('stauts', $logueado);

if(!$logueado){
	$json->encode();
	die();
}

switch($operacion){

    case 'solicitarOrden':
		$subida_adjuntos = array();
		$arrayTipos = array();
        $errorTran = false;

        foreach($tipoSolicitud as $key => $tipo){
            $arrayTipos[$key]['id'] =  $tipo;
        }
        $xmlTipos=$model->ArrayToXml($arrayTipos,'item');

        $model->iniciarTransaccion();
        $solicitud = $model->RSAsociativo("EXEC spPortalUsuarios @op='solicitarOrden'
            ,@autoid = '".$_SESSION["IdUsuario"]."',@observacion='".$observacion."'
            ,@puntoAtencion = '".$_SESSION['punto_atencion_pcte']."',@tipoAtencion='".$tipoAtencion."'
            ,@es_medicamento = '".$tipoMedicamento."', @es_orden = '".$tipoOrden."'
            ,@idSolicitudAutorizacion = '".$idSolicitudAutorizacion."'
            ,@xmlTipos = '".$xmlTipos."'
            ,@manejaLaTransaccion = 0
            ,@llamadaSp = 'N'");

        $solicitud = $solicitud[0];
        if(isset($solicitud['idSolicitud'])){
            $adjuntos = array();
            $ext = array('jpg','JPG','png','PNG','pdf','PDF','jpeg','JPEG');
            $nombre_archivo = $_FILES['archivos']['name']; 
            $tipo_archivo = $_FILES['archivos']['type']; 
            $tamano_archivo = $_FILES['archivos']['size']; 
            $tmp_name = $_FILES['archivos']['tmp_name'];

            if($nombre_archivo[0] != ''){
                $respuestaArchivo = adjuntarArchivosSolicitud($solicitud['idSolicitud'],$nombre_archivo,$ext,$tmp_name,$tipo_archivo);;
                if($respuestaArchivo["estado"]){
                    $nombre_archivo = $_FILES['archivosH']['name']; 
                    $tipo_archivo = $_FILES['archivosH']['type']; 
                    $tamano_archivo = $_FILES['archivosH']['size']; 
                    $tmp_name = $_FILES['archivosH']['tmp_name'];
                    if($nombre_archivo[0] != ''){
                        $respuestaArchivo = adjuntarArchivosSolicitud($solicitud['idSolicitud'],$nombre_archivo,$ext,$tmp_name,$tipo_archivo);
                        if($respuestaArchivo["estado"]){
                            $ERRORMSG = NULL;
                            $errorTran =false;
                            
                        }else{
                            $ERRORMSG= $respuestaArchivo["mensaje"];
                            $errorTran = true;
                        }
                    }else{
                        $ERRORMSG = NULL;
                        $errorTran =false;
                    }

                }else{
                    $ERRORMSG= $respuestaArchivo["mensaje"];
                    $errorTran =true;
                }			
            }else{
                $errorTran =false;
            }
        }else{
            $errorTran = true;
            $ERRORMSG =  $solicitud['ERRORMSG'];
        }
        $model->finTransaccion($errorTran);
        
        $json->addParam('ERRORMSG',$ERRORMSG);
        $json->addParam('id_hidden',$solicitud['idSolicitud']);
        if($modal == 1){
            $gestiones = $model->RSAsociativo("EXEC spPortalUsuarios @op='verGestionesSolicitudOrden', @idSolicitudPortal='".$solicitud['idSolicitud']."',@idSolicitudAutorizacion='0' ");
            $payload = listarGestionesSolicitud($gestiones,$solicitud['idSolicitud'],0);
            $json->addParam('payload',$payload);
        }
        $json->encode();
    break;

    case 'visualizarDevolucionSolicitud':
        $gestiones = $model->RSAsociativo("EXEC spPortalUsuarios @op='verGestionesSolicitudOrden', @idSolicitudPortal='".$idSolicitud."',@idSolicitudAutorizacion='".$idSolicitudAutorizacion."' ");
        $payload = listarGestionesSolicitud($gestiones,$idSolicitud,$idSolicitudAutorizacion);
        $json->addParam('payload',$payload);
        $json->encode();
    break;

    case "verArchivosSolicitud":
		$archivos = $model->RSAsociativo("EXEC spPortalUsuarios @op='verArchivosSolicitud', @idSolicitud = '".$idSolicitud."', @tipoAdjunto='solicitud'");
		$json->addParam('archivos',$archivos);
		$json->encode();
    break;
    
    case "adjuntarArchivosSolicitud":
		$ext = array('jpg','JPG','png','PNG','pdf','PDF','jpeg','JPEG');
		$nombre_archivo = $_FILES['archivosSol']['name']; 
		$tmp_name = $_FILES['archivosSol']['tmp_name'];
		$tipo_archivo = $_FILES['archivosSol']['type']; 
		// $tamano_archivo = $_FILES["'".$archivos."'"]['size'];
		if($nombre_archivo[0] != ''){
			$respuesta = adjuntarArchivosSolicitud($idSolicitud,$nombre_archivo,$ext,$tmp_name,$tipo_archivo);

		}else{
			$respuesta = ["estado"=>false,"mensaje"=>"No se han seleccionado archivos a cargar"];
		}
		$json->addParam('subida_adjuntos',$respuesta["estado"]);
		$json->addParam('mensaje_adjuntos',$respuesta["mensaje"]);
		$json->encode();
	break;
}

function listarGestionesSolicitud($gestiones,$idSolicitud,$idSolicitudAutorizacion){
    global $model;
    $tabla = '<form id="form_'.$idSolicitud.'" name="form_'.$idSolicitud.'" enctype="multipart/form-data" method="post" novalidate>
                <button type="button" class="btn btn-info" style="font-size: 10px;"><input name="archivosSol[]" id="archivosSol" type="file" class="file" multiple accept="" data-msg-placeholder="Selecciona los archivos...">
                <div class="invalid-feedback"></div>
                </button>
                <button type="button" onclick="validarArchivosSolicitudesOrden('.$idSolicitud.')" class="btn btn-sm btn-portal2">
                    <i class="fas fa-upload"></i> Cargar
                </button>   
                <button type="button" onclick="verArchivosSolicitudOrden('.$idSolicitud.')" class="btn btn-sm btn-portal2">
                    <i class="fas fa-eye"></i> Ver Archivos cargados
                </button>
            </form>';
    if($idSolicitud == 0){
        $tipoSolicitud = 6;
        $tabla .= '<form id="enviarSolicitud">
            <br/>
            <input type="hidden" id="idSolicitudAutorizacionHidden" value="'.$idSolicitudAutorizacion.'">
            <input type="hidden" id="tipoSolicitud" value="'.$tipoSolicitud.'">
            <div class="row">
            <div class="col-md-12 form-group">
                <label for="descripcionSol">Descripci&oacute;n</label>
                <textarea class="form-control form-control-sm" id="descripcionSol" name="descripcionSol"></textarea>    
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-md-12">
                <button type="button" class="btn btn-sm btn-portal2" onclick="enviarSolicitud()">Enviar solicitud</button>
            </div>
            </div>
        </form>
        ';
    }
    $tabla .= '<div id="filesSol" class="row"></div>
            <br>
            <table id="gestionesSolicitud" class="table table-striped table-bordered table-sm">
                <thead>
                    <th>Usuario</th>
                    <th>Observacion</th>
                    <th>Fecha</th>
                    <th>Adjuntos</th>
                </thead>
                <tbody>';
    if(count($gestiones) > 0){
        foreach($gestiones as $g){
            if($g["usuario"] == '0'){
                $g["usuario"] = $_SESSION['nombre_pcte_portal'];
            }
            $ruta = "portalUsuarios/Archivos/solicitudesOrden/".$idSolicitud."/".$g["id"]."/";
            $adjuntos = listFiles($ruta);
            $tabla .= '<tr>
                        <td>'.$g["usuario"].'</td>
                        <td>'.$g["observacion"].'</td>
                        <td>'.$g["fecha"].'</td>
                        <td>'.$adjuntos.'</td>
                    </tr>';
        }
    }else{
        $tabla .= '<tr><td colspan="3" class="text-center">No se encontraron registros</td></tr></tbody>';
    }
    return $tabla;
}

function adjuntarArchivosSolicitud($solicitud,$nombre_archivo,$ext,$tmp_name,$tipo_archivo){
	$subida_adjuntos = array();
	$adjuntos = array();
	$respuesta = ["estado" => true];
	$model=new Model();
	if($nombre_archivo[0] != ''){
        $directorio = "C:/inetpub/wwwroot/SismaSalud/portalUsuarios/Vistas/autorizaciones/adjuntos/";//por corregir ## portalUsuarios/
        if (!file_exists($directorio)) {
            mkdir($directorio, 0777, true);
        }
		foreach($nombre_archivo as $key => $a){
            $aux = explode("/",$tipo_archivo[$key]);
			if(in_array($aux[1],$ext)){
				$nombre = uniqid().".".$aux[1];
				$ruta = $directorio.$nombre;
				$subido=move_uploaded_file($tmp_name[$key], $ruta);
				array_push($subida_adjuntos, $subido);
				array_push($adjuntos, $nombre);

			}else{
				array_push($subida_adjuntos, false);
				$respuesta = ["estado"=>false, "mensaje"=>'Solo se admiten archivos con extension '.implode(",", $ext)];
				break;
			}
		}	
		if(!in_array(false,$subido)){
			$adjuntos = implode(",",$adjuntos);
			$arch= $model->RSAsociativo("EXEC spPortalUsuarios @op='asociarAdjuntos', @idSolicitud = '".$solicitud."',@adjuntos = '".$adjuntos."',@tipoAdjunto = 'solicitud'");

		}			
	}
	return $respuesta;
}

function listFiles($ruta){ //La función recibira como parametro un directorio
	$directorio = $_SESSION["Document_root_portal"]."/".$_SESSION['Dir_app_main_portal']."/".$ruta;
	$dir2 = "http".$_SESSION['protocolo_c']."://".$_SESSION['http_host_portal']."/".$_SESSION['Dir_app_main_portal']."/".$ruta;
    $respuesta = '';
	if (is_dir($directorio)) { //Comprovamos que sea un directorio Valido
		if ($dir = opendir($directorio)) {//Abrimos el directorio

			$respuesta .= '<ul>'; //Abrimos una lista HTML para mostrar los archivos

			while (($archivo = readdir($dir)) !== false){ //Comenzamos a leer archivo por archivo

				if ($archivo != '.' && $archivo != '..'){//Omitimos los archivos del sistema . y ..
					$respuesta .= '<li><a href="'.$dir2.$archivo.'" target="_blank">'.$archivo.'</a></li>';

				}

			}//finaliza While
			$respuesta .= '</ul>';//Se cierra la lista

			closedir($dir);//Se cierra el archivo
		}
	}else{//Finaliza el If de la linea 12, si no es un directorio valido, muestra el siguiente mensaje
		$respuesta .= '';
	}
	return $respuesta;				
}

?>