<?php 
	class FileUtil{
	
		function isDir($path)
		{
			return is_dir($path);
		}
		
		function fileExists($path)
		{
			return file_exists($path);
		}

		public function isFile($path)
		{
			return is_file($path);
		}

		public function isReadable($path)
		{
			return is_readable($path);
		}
		
		function deleteDir($path)
		{
			foreach(glob($path . "/*") as $p){
				if (is_dir($p))
				{
					$this->deleteDir($p);
				}
				else
				{
					unlink($p);
				}
			}
		 
			rmdir($path);
		}
		
		function createDir($path)
		{
			return mkdir($path, 0777,true);
		}
		
		function createFile($path, $mode)
		{
			return fopen($path, $mode);
		}
		
		function writeInFile($gestor, $content)
		{
			return fwrite($gestor, $content);
		}
		
		function closeFile($gestor)
		{
			return fclose($gestor);
		}
	
	}
?>