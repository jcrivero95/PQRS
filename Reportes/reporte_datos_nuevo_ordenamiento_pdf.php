<?php
@session_start();
include_once("{$_SESSION['root']}/Model/Model.php");
require_once('/mpdf/mpdf.php');
$model = new Model();
header('Content-Type: text/html; charset=UTF-8');
if(!isset($ordenes)){
	$ordenes = explode(",", $_REQUEST["ordenesGeneradas"]);
}
// if(count($ordenes) > 1){
// 	foreach($ordenes as $numero){
// 		$_REQUEST['numero'] = $numero;
// 		include("reporte_datos_orden.php");
// 	}
// }else{
// 	$numero = $_REQUEST["ordenesGeneradas"];
// 	$_REQUEST['numero'] = $numero;
// 	include("reporte_datos_orden.php");
// }
$html = '';
$mpdf = new Mpdf($mode = '', 
$format = 'letter', 
$default_font_size = 0,
$default_font = '', 
$mgl = 2, $mgr = 2, $mgt = 3, 
$mgb = 16, $mgh = 2, $mgf = 2, 
$orientation = 'L');

if(count($ordenes) > 1){
	// ob_start();
	foreach($ordenes as $key => $numero){
		$_REQUEST['numero'] = $numero;
		$req_aut = $model->getDato('requiere_aut', 'ordenes', 'numero ='.$numero);
		// ob_start();
		// 	include("reporte_datos_orden.php");
		// 	$html .= ob_get_contents();
		// ob_end_clean();
		// if($key+1 != count($ordenes)){
		// 	$html .= "<p class='SaltoDePagina'></p>";
		// }
		if($req_aut == 1){		
			$impresa=$model->getDato("impresa","ordenes","numero='".$numero."'");
			$imprimeOriginalCopia=$model->getParametroGeneral("imprimeOriginalCopia","CONFIGURACION");
	
			if($impresa=="0"||$imprimeOriginalCopia!='S'){
				ob_start();
				$mpdf->SetWatermarkImage($_SESSION["path"].'/ImagenesZeus/requiere_aut_mod.png',1,'',array(0,0));
				$mpdf->watermarkImageAlpha = 0.2;
				$mpdf->showWatermarkImage = true;
				include("reporte_datos_nueva_orden.php");
				$html = ob_get_contents();
				$mpdf->WriteHTML($html);
				ob_end_clean();
				// if($key != count($ordenes)-1){
				// 	$mpdf->AddPage();
				// }
			}
			if($impresa=="0" && $imprimeOriginalCopia=='S'){
				$mpdf->AddPage();
			}
			if($imprimeOriginalCopia=='S'){
				ob_start();
				// $mpdf->AddPage();
				$mpdf->SetWatermarkImage($_SESSION["path"].'/ImagenesZeus/requiere_aut_copia.png',1,'',array(0,0));
				$mpdf->watermarkImageAlpha = 0.2;
				$mpdf->showWatermarkImage = true;
				include("reporte_datos_nueva_orden.php");
				$html = ob_get_contents();
				$mpdf->WriteHTML($html);
				ob_end_clean();
				// if($key != count($ordenes)-1){
				// 	$mpdf->AddPage();
				// }
			}
			
		}else{
			$impresa=$model->getDato("impresa","ordenes","numero='".$numero."'");
			$imprimeOriginalCopia=$model->getParametroGeneral("imprimeOriginalCopia","CONFIGURACION");
			if($impresa=="0"||$imprimeOriginalCopia!='S'){
				ob_start();
				$mpdf->showWatermarkImage = false;
				include("reporte_datos_nueva_orden.php");
				$html = ob_get_contents();
				$mpdf->WriteHTML($html);
				ob_end_clean();
			}
			if($impresa=="0" && $imprimeOriginalCopia=='S'){
				$mpdf->AddPage();
			}
			if($imprimeOriginalCopia=='S'){
				ob_start();
				// $mpdf->AddPage();
				$mpdf->SetWatermarkImage($_SESSION["path"].'/ImagenesZeus/copia2.png',1,'',array(0,0));
				$mpdf->watermarkImageAlpha = 0.2;
				$mpdf->showWatermarkImage = true;
				include("reporte_datos_nueva_orden.php");
				$html = ob_get_contents();
				$mpdf->WriteHTML($html);
				ob_end_clean();
			}
			
		}
		
		if($key != count($ordenes)-1){
			$mpdf->AddPage();
		}
	}
	// ob_end_clean();
	
}else{
	ob_start();
	$numero = $_REQUEST["ordenesGeneradas"];
	$_REQUEST['numero'] = $numero;

	$req_aut = $model->getDato('requiere_aut', 'ordenes', 'numero ='.$numero);

	if($req_aut == 1){		
		$impresa=$model->getDato("impresa","ordenes","numero='".$numero."'");
		$imprimeOriginalCopia=$model->getParametroGeneral("imprimeOriginalCopia","CONFIGURACION");

		if($impresa=="0"||$imprimeOriginalCopia!='S'){
			// print 'primera impresion';
			$mpdf->SetWatermarkImage($_SESSION["path"].'/ImagenesZeus/requiere_aut_mod.png',1,'',array(0,0));
			$mpdf->watermarkImageAlpha = 0.2;
			$mpdf->showWatermarkImage = true;
			include("reporte_datos_nueva_orden.php");
			$html = ob_get_contents();
			$mpdf->WriteHTML($html);
		}
		if($imprimeOriginalCopia=='S'){
			$mpdf->AddPage();
			$mpdf->SetWatermarkImage($_SESSION["path"].'/ImagenesZeus/requiere_aut_copia.png',1,'',array(0,0));
			$mpdf->watermarkImageAlpha = 0.2;
			$mpdf->showWatermarkImage = true;
			include("reporte_datos_nueva_orden.php");
			$html = ob_get_contents();
			$mpdf->WriteHTML($html);
		}
		
	}else{
		
		$impresa=$model->getDato("impresa","ordenes","numero='".$numero."'");
		$imprimeOriginalCopia=$model->getParametroGeneral("imprimeOriginalCopia","CONFIGURACION");
		if($impresa=="0"||$imprimeOriginalCopia!='S'){
			$mpdf->showWatermarkImage = false;
			include("reporte_datos_nueva_orden.php");
			$html = ob_get_contents();
			$mpdf->WriteHTML($html);
		}
		if($imprimeOriginalCopia=='S'){
			$mpdf->AddPage();
			$mpdf->SetWatermarkImage($_SESSION["path"].'/ImagenesZeus/copia2.png',1,'',array(0,0));
			$mpdf->watermarkImageAlpha = 0.2;
			$mpdf->showWatermarkImage = true;
			include("reporte_datos_nueva_orden.php");
			$html = ob_get_contents();
			$mpdf->WriteHTML($html);
		}
		
	}
	ob_end_clean();
}

//$mpdf->Output();

$mpdf->Output($_SESSION["CarpetaArchivosSave"].'/afiliaciones/Adjuntos/ordenamiento-'.$ordenes[0].'.pdf', 'F');
//exit;
//echo $html;
?>