<?php
@session_start();
// require_once("configurar.php");
define("CONECTADO", 1);
define("DESCONECTADO", 2);

error_reporting(E_ERROR | E_WARNING);
set_error_handler("BDController_error_handler");


class BDController {
	
	var $host;
	var $database;
	var $user;
	var $password;
	var $errorMessage;
	var $mongodburl;
	var $mongo;
	
	var $con = NULL;
	var $estado = DESCONECTADO;
	
	function BDController($host, $database, $user, $password) {
		$this->host = $host;
		$this->database = $database;
		$this->user = $user;
		$this->password = $password;
		$this->mongodburl = "mongodb://130.103.99.240:27017";
	}
	
	function connect() {
		/*print ("HOST: ".$this->host.
			", DATABASE: ".$this->database.
			", USER: ".$this->user.
			", PASSWORD: ".$this->password);*/
		if(false){
		$this->mongo = new MongoClient($this->mongodburl);
		}
		
		if (($this->con = mssql_connect($this->host, $this->user,$this->password)) == true) {	
			$this->changeDatabase($this->database);
			$this->errorMessage = $this->con ? "" : mssql_get_last_message();
			$this->estado = CONECTADO;
			
			//session_register("var_conexion");
			$_SESSION["var_conexion_vacunacion"]=$this->con;
			return true;
		} else {
			$this->estado = DESCONECTADO;
			return false;
		}
		
		
	}
	function mongolog(){
		return $this->mongo ;
		
	}
	
	function Mongoinsert($collecion,$array,$base="zeussalud"){
		$collection = $this->mongo->selectCollection($base,$collecion);
		$collection->insert($array);	
	}
	
	function Mongoupdate($collecion,$array,$condicion,$base="zeussalud"){
		$collection = $this->mongo->selectCollection($base,$collecion);
		$nuevosdatos = array('$set' => $array);
		$collection->update($condicion, $nuevosdatos);
	}
	
	
	

	
	function disconnect() {
		mssql_close($this->con);
		$this->estado = DESCONECTADO;
	}
	
	function changeDatabase($database) {
		mssql_select_db($database, $this->con);
		$this->database = $database;
	}
	
	function query($SQL) {
		//echo $SQL;
		$rs = @mssql_query($SQL, $this->con);
		$this->errorMessage = $rs ? "" : mssql_get_last_message();
        
		return new Result($rs);
	}
	
	function limitQuery($SQL, $index, $offset = 0) {
		$SQL = trim($SQL);
		$SQL = substr($SQL, 0, 6)." TOP $index ".substr($SQL, 7, strlen($SQL));
		//echo $SQL."<br>";
		$rs = mssql_query($SQL, $this->con);
		$this->errorMessage = $rs ? "" : mssql_get_last_message();
		return new Result($rs);
	}
	
	function select($fields, $table, $where = NULL, $show = NULL) {
		$SQL = "SELECT $fields FROM $table".($where == NULL || $where == "" ? "" : " WHERE $where");
		
		if ($show != NULL) {
			print $SQL;
		}
		$rs = $this->query($SQL);
		
		return $rs;
	}
	
	function consulta($campos, $tabla, $where = NULL, $show = NULL, $tipo = NULL) {
		$SQL = "SELECT $campos FROM $tabla".
			($where == NULL || $where == "" ? "" : " WHERE $where");
	
		if ($show != NULL) {
			echo $SQL;
		}
		
		if ($tipo != NULL) {
			//echo $SQL;
			$result = $this->query($SQL);
			$row = $result->nextRow();
			$result->free();
			return $row;
		} else {
			return $this->query($SQL);
		}
	}
	
	function execute($SQL) {
		$this->query($SQL);
		return $this->affectedRows();
	}
	
	function affectedRows() {
		return mssql_rows_affected($this->con);
	}
	
	function getMessage() {
		return $this->errorMessage;
	}
	
	function getHost() {
		return $this->host;
	}
	
	function getDatabase() {
		return $this->database;
	}
	
	function getUser() {
		return $this->user;
	}
	
	function getPassword() {
		return $this->password;
	}
	
	function setHost($host) {
		$this->host = $host;
	}
	
	function setDatabase($database) {
		$this->database = $database;
	}
	
	function setUser($user) {
		$this->user = $user;
	}
	
	function setPassword($password) {
		$this->password = $password;
	}
}

class Result {
	var $rs = NULL;
	
	function Result($rs) {
		$this->rs = $rs;
	}
	
	function nextRow() {
		return @mssql_fetch_array($this->rs);
	}

	function nextRowAssoc() {
		return mssql_fetch_array($this->rs, MSSQL_ASSOC);
	}

	function numRows() {
		return mssql_num_rows($this->rs);
	}
	
	function numFields() {
		return mssql_num_fields($this->rs);
	}
	
	function free() {
		@mssql_free_result($this->rs);
	}
	
	function getResult() {
		return $this->rs;
	}
	
	function getResultToArray() {
		$max = $this->numRows();
		$rows = array();
		
		for ($i = 0; $i < $max; $i++) {
			$rows[$i] = mssql_fetch_assoc($this->rs);
		}
		
		return $rows;
	}
	
	function getResultToObject() {
		$max = $this->numRows();
		$rows = array();
		
		for ($i = 0; $i < $max; $i++) {
			$rows[$i] = mssql_fetch_object($this->rs);
		}
		
		return $rows;
	}
	
	function nextResult(){
		return mssql_next_result($this->rs);	
	}
	
}

function miGestorDeErrores($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        // Este código de error no está incluido en error_reporting
        return;
    }

    switch ($errno) {
    case E_USER_ERROR:
        echo "<b>Mi ERROR</b> [$errno] $errstr<br />\n";
        echo "  Error fatal en la línea $errline en el archivo $errfile";
        echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        echo "Abortando...<br />\n";
        exit(1);
        break;

    case E_USER_WARNING:
        echo "<b>Mi WARNING</b> [$errno] $errstr<br />\n";
        break;

    case E_USER_NOTICE:
        echo "<b>Mi NOTICE</b> [$errno] $errstr<br />\n";
        break;

    default:
        echo "Tipo de error desconocido: [$errno] $errstr<br />\n";
		
		 echo "<b>Mi ERROR</b> [$errno] $errstr<br />\n";
        echo "  Error fatal en la línea $errline en el archivo $errfile";
        echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        echo "Abortando...<br />\n";

        break;
    }

    /* No ejecutar el gestor de errores interno de PHP */
    return true;
}


function BDController_error_handler($severity, $msg, $filename, $linenum) {
	static $error_handled = false;
	$errores = array("mssql_select_db", "mssql_connect", "mssql_pconnect", "session_start");
	
	if ($error_handled == false) {
		switch ($severity) {
			case E_ERROR:
			case E_WARNING:
				for ($i = 0, $handler = false; $i < sizeof($errores); $i++) {
					if (stripos($msg, $errores[$i]) !== false) {
						$handler = true;
					}
				}
				if ($handler != false) {
					$error_handled = true;
					//$dir_base = "http://{$_SERVER['HTTP_HOST']}/SismaSalud/ips";
					$dir_base =$_SESSION['site_name_vacunacion'];
					session_unset();
					session_destroy();

				}
				break;
		}
	}
	
	restore_error_handler();
}
?>