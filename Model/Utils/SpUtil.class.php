<?php
include_once("../Entities/Entity.class.php");
class SpUtil{

	static function EntityToSpParams(Entity $e, array $extraData = array()){
		$e_params = array_merge(is_null($e)? array() : $e->getValuesAsArray($e), $extraData);
		
		foreach($e_params as $p => $v){
			if($v === NULL) continue;
			
			if($v instanceof Entity) continue;
			
			if(is_array($v)) continue;
			
			$params[] = "@{$p} = '{$v}'";
			
			/*if(is_numeric($v)){
				$params[] = "@{$p} = {$v}";
			}else{
				$params[] = "@{$p} = '{$v}'";
			}*/
		}
		
		return implode(", ",$params);
	}

}