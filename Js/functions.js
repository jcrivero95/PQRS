function cargarPaciente() {
    var tipoId = document.getElementById("id_tipo").value;
    var identificacion = document.getElementById("id").value;
    var operacion = "cargar_paciente";
    var url = "../Controlador/ctrlPQRS.php";


    if (identificacion == null || identificacion == "") {
        toastr["warning"]("Digite numero de identificación", "");
        $('#id').focus();
        return false;
    }

    $.ajax({
        url: url,
        method: "POST",
        data: {
            "tipoId": tipoId,
            "identificacion": identificacion,
            "operacion": operacion
        },
        success: function (res) {
                    if (res.datos[0].respuesta == "false") {
                        Swal.fire({
                            icon: 'info',
                            html: 'Esta seguro que este es su numero de identificación'+' '+identificacion,
                            showCancelButton: false,
                            showDenyButton: true,
                            confirmButtonText: 'Si',
                            denyButtonText: 'No',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                /*  document.getElementById("id").value = "";
                                  document.getElementById("nombres").value = "";
                                  document.getElementById("apellidos").value = "";
                                  document.getElementById("direccion").value = "";
                                  document.getElementById("correo").value = "";
                                  document.getElementById("telefono").value = "";
                                  document.getElementById("telefono2").value = "";*/
                                document.getElementById("entidad").value = "";
                                document.getElementById("id_sede").value = "";
                                document.getElementById("PuntoAtencion").value = "";
                                document.getElementById("autoid").value = "";
                                document.getElementById("cod_dep").value = "";
                                document.getElementById("cod_muni").value = "";
                            } else {
                                document.getElementById("id").value = "";
                            }
                        })
                    }else {
                        Swal.fire({
                            icon: 'info',
                            html: 'Esta seguro que este es su numero de identificación'+' '+ res.datos[0].num_id,
                            showCancelButton: false,
                            showDenyButton: true,
                            confirmButtonText: 'Si',
                            denyButtonText: 'No',
                        }).then((result) => {
                            if (result.isConfirmed) {
                               /* $('#nombres').val(res.datos[0].nombre + " " + res.datos[0].s_nombre);
                                $('#apellidos').val(res.datos[0].apellido + " " + res.datos[0].s_apellido);
                                $('#direccion').val(res.datos[0].direccion);
                                $('#correo').val(res.datos[0].correo);
                                $('#telefono').val(res.datos[0].telefono);
                                $('#telefono2').val(res.datos[0].telefono_2);*/
                                $('#entidad').val(res.datos[0].entidad);
                                $('#id_sede').val(res.datos[0].id_sede);
                                $('#PuntoAtencion').val(res.datos[0].PuntoAtencion);
                                $('#autoid').val(res.datos[0].autoid);
                                $('#cod_dep').val(res.datos[0].cod_dep);
                                $('#cod_muni').val(res.datos[0].cod_muni);
                            }else{
                                document.getElementById("id").value = "";
                               /* document.getElementById("nombres").value = "";
                                document.getElementById("apellidos").value = "";
                                document.getElementById("direccion").value = "";
                                document.getElementById("correo").value = "";
                                document.getElementById("telefono").value = "";
                                document.getElementById("telefono2").value = "";*/
                                document.getElementById("entidad").value = "";
                                document.getElementById("id_sede").value = "";
                                document.getElementById("PuntoAtencion").value = "";
                                document.getElementById("autoid").value = "";
                                document.getElementById("cod_dep").value = "";
                                document.getElementById("cod_muni").value = "";
                            }
                        })

                    }

        },
        error: function (r) {
            alert("algo salio mal");
        },
        complete: () => {

        }
    })

}


function cargarPaciente_AF() {
    var tipoId = document.getElementById("id_tipo_AF").value;
    var identificacion = document.getElementById("id_AF").value;
    var operacion = "cargar_paciente";
    var url = "../Controlador/ctrlPQRS.php";


    if (identificacion == null || identificacion == "") {
        toastr["warning"]("Digite numero de identificación", "");
        $('#id').focus();
        return false;
    }

    $.ajax({
        url: url,
        method: "POST",
        data: {
            "tipoId": tipoId,
            "identificacion": identificacion,
            "operacion": operacion
        },
        success: function (res) {
            if (res.datos[0].respuesta == "false") {
                Swal.fire({
                    icon: 'info',
                    html: 'Esta seguro que este es el numero de identificacion del paciente'+' '+identificacion,
                    showCancelButton: false,
                    showDenyButton: true,
                    confirmButtonText: 'Si',
                    denyButtonText: 'No',
                }).then((result) => {
                    if (result.isConfirmed) {
                       /* document.getElementById("id_AF").value = "";
                        document.getElementById("nombres_AF").value = "";
                        document.getElementById("apellidos_AF").value = "";
                        document.getElementById("direccion_AF").value = "";
                        document.getElementById("correo_AF").value = "";
                        document.getElementById("telefono_AF").value = "";
                        document.getElementById("telefono2_AF").value = "";*/
                        document.getElementById("entidad_AF").value = "";
                        document.getElementById("id_sede_AF").value = "";
                        document.getElementById("PuntoAtencion_AF").value = "";
                        document.getElementById("autoid_AF").value = "";
                        document.getElementById("cod_dep_AF").value = "";
                        document.getElementById("cod_muni_AF").value = "";
                    } else {
                        document.getElementById("id_AF").value = "";
                    }
                })
            }else {
                Swal.fire({
                    icon: 'info',
                    html: 'Esta seguro que este es el numero de identificacion del paciente'+' '+ res.datos[0].num_id,
                    showCancelButton: false,
                    showDenyButton: true,
                    confirmButtonText: 'Si',
                    denyButtonText: 'No',
                }).then((result) => {
                    if (result.isConfirmed) {
                        /*$('#nombres_AF').val(res.datos[0].nombre + " " + res.datos[0].s_nombre);
                        $('#apellidos_AF').val(res.datos[0].apellido + " " + res.datos[0].s_apellido);
                        $('#direccion_AF').val(res.datos[0].direccion);
                        $('#correo_AF').val(res.datos[0].correo);
                        $('#telefono_AF').val(res.datos[0].telefono);
                        $('#telefono2_AF').val(res.datos[0].telefono_2);*/
                        $('#entidad_AF').val(res.datos[0].entidad);
                        $('#id_sede_AF').val(res.datos[0].id_sede);
                        $('#PuntoAtencion_AF').val(res.datos[0].PuntoAtencion);
                        $('#autoid_AF').val(res.datos[0].autoid);
                        $('#cod_dep_AF').val(res.datos[0].cod_dep);
                        $('#cod_muni_AF').val(res.datos[0].cod_muni);
                    }else{
                        document.getElementById("id_AF").value = "";
                       /* document.getElementById("nombres_AF").value = "";
                        document.getElementById("apellidos_AF").value = "";
                        document.getElementById("direccion_AF").value = "";
                        document.getElementById("correo_AF").value = "";
                        document.getElementById("telefono_AF").value = "";
                        document.getElementById("telefono2_AF").value = "";*/
                        document.getElementById("entidad_AF").value = "";
                        document.getElementById("id_sede_AF").value = "";
                        document.getElementById("PuntoAtencion_AF").value = "";
                        document.getElementById("autoid_AF").value = "";
                        document.getElementById("cod_dep_AF").value = "";
                        document.getElementById("cod_muni_AF").value = "";
                    }
                })

            }

        },
        error: function (r) {
            alert("algo salio mal");
        },
        complete: () => {

        }
    })

}