<style>
	table td,.head{
		font-size:10px !important;	
	}
	
	
	
</style>
<br />
<?php
@session_start();
extract($_REQUEST);
include_once("../Model/Model.php");
$model = new Model();
require_once("template.php");


encabezado("CITA No. ".$model->rellenar($_REQUEST['id'], $digs=6,$caracter='0'), "");

$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

//echo $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
if($_REQUEST['tipocita'] !=2){
$rs = $model->select("c.nombre_tipo_examen,p.full_name_tipo_id_inv as paciente, isnull(contratos.nombre,se.nombre) AS contrato,
 sis_medi.nombre AS medico, sis_asunto.nombre AS motivo, CONVERT(VARCHAR(10),c.fecha,111) fecha, c.hora, c.meridiano,case when (c.estado='C') 
  then 'CANCELADO' 
  when (c.estado='A') 
  then 'ATENDIDO' 
  when (c.estado='CC') 
  then 'CONFIRMADA'  
  when (c.estado='P') 
  then 'PENDIENTE POR CONFIRMAR' END as estado, c.estado As estadoCita,c.estudio,sis_estrato.nombre as nombreEstrato,dbo.fnGetCopagoPagado(c.estudio) pagoPaciente,sis_medi.direccion,sis_medi.citaExterna,sis_medi.telefono,c.observacion as obsCita,sis_medi.leyendaConfirmarMedico,
  case 
  when c.tipocita = 2 then 'GRUPAL'
  when c.tipocita = 3 then 'TERAPIA'
  when c.tipocita = 4 then 'TELEMEDICINA'
  ELSE 'PRESENCIAL'
  END as tipocitadato,
  c.tipocita",
	"citas AS c LEFT JOIN
                      pacientesView AS p ON c.autoid = p.autoid LEFT JOIN
                      sis_medi ON c.cod_medi = sis_medi.codigo LEFT JOIN
                      sis_asunto ON c.asunto = sis_asunto.id LEFT JOIN
                      contratos ON c.contrato = contratos.codigo
					  left join sis_empre se on se.codigo=c.empresa
					  left join sis_maes on con_estudio = c.estudio
					  left join sis_estrato on cod_clasi = sis_estrato.codigo", 
  "c.id= {$_REQUEST['id']}");
}else{
  $rs = $model->select("c.nombre_tipo_examen,p.full_name_tipo_id_inv as paciente, isnull(contratos.nombre,se.nombre) AS contrato,
 sis_medi.nombre AS medico, sis_asunto.nombre AS motivo, CONVERT(VARCHAR(10),c.fecha,111) fecha, c.hora, c.meridiano,case when (c.estado='C') 
  then 'CANCELADO' 
  when (c.estado='A') 
  then 'ATENDIDO' 
  when (c.estado='CC') 
  then 'CONFIRMADA'  
  when (c.estado='P') 
  then 'PENDIENTE POR CONFIRMAR' END as estado, c.estado As estadoCita,c.estudio,sis_estrato.nombre as nombreEstrato,dbo.fnGetCopagoPagado(c.estudio) pagoPaciente,sis_medi.direccion,sis_medi.citaExterna,sis_medi.telefono,c.observacion as obsCita,sis_medi.leyendaConfirmarMedico,
  case 
  when c.teleconsulta = 1 then 'TELEMEDICINA'
  END as tipocitadato,
  c.tipocita",
  "citas AS c 
  inner join citas_agrupada on c.id = citas_agrupada.id_cita
  LEFT JOIN pacientesView AS p ON citas_agrupada.autoid = p.autoid
  LEFT JOIN sis_medi ON c.cod_medi = sis_medi.codigo
  LEFT JOIN sis_asunto ON c.asunto = sis_asunto.id
  LEFT JOIN contratos ON citas_agrupada.contrato = contratos.codigo
  left join sis_empre se on se.codigo= contratos.empresa
  left join sis_maes on con_estudio = citas_agrupada.estudio
  left join sis_estrato on cod_clasi = sis_estrato.codigo", 
  "c.id= {$_REQUEST['id']} and citas_agrupada.autoid = {$_REQUEST['autoid']} ");
}
$row = $model->nextRow($rs);
 
?>

<div class="block">
  <table class="external_border">
  	<?php 
		if($row["leyendaConfirmarMedico"]=="1"){?>
			<tr>
            	<td colspan="2"><b>CONFIRMAR CON EL MEDICO</b></td>
            </tr>		
	<?php
		}
	?>
	<?php if($row["leyendaConfirmarMedico"]=="0"){?>
    <tr>
      <td width="120px">FECHA DE LA CITA:</td>
      <td><strong><?php 
	//  echo $dias[date('w',)]; 
	  echo strtoupper($dias[date('w',strtotime($row['fecha']))]." ".date('d',strtotime($row['fecha']))." de ".$meses[date('n',strtotime($row['fecha']))-1]. " de ".date('Y',strtotime($row['fecha'])));
	  //echo $row['fecha']; 
	  ?></strong></td>
     </tr>
     
     <tr> 
      <td>HORA DE LA CITA: </td>
      <td><strong><?php echo $row['hora'].' '.$row['meridiano']; ?></strong></td>
    </tr>
    <?php }?>
    <tr>
      <td>PACIENTE:</td>
      <td colspan=3><strong><?php echo $row['paciente']; ?></strong></td>
    </tr>
    <tr>
      <td>EPS:</td>
      <td><strong><?php echo $row['contrato']; ?></strong></td>
      </tr>
      <tr>
      <td>PROFESIONAL DE LA SALUD:</td>
      <td><strong><?php echo $row['medico']; ?></strong></td>
    </tr>
    <?php 
		if($row["citaExterna"]=="1"){?>
			<tr>
            	<td>TELEFONO: </td>
                <td><b><?php echo $row["telefono"];?></b></td>
            </tr>		
	<?php
		}
	?>
    <tr>
      <td>DIRECCI&Oacute;N:</td>
      <td><strong><?php echo $row['direccion']; ?></strong></td>
    </tr>
    <tr>
      <td>MOTIVO:</td>
      <td><strong><?php echo $row['motivo']; ?></strong></td>
    </tr>
	<tr>
      <td>ESTADO:</td>
      <td><strong><?php echo $row['estado']; ?></strong></td>
    </tr>
    <tr>
		<td>TIPO DE EXAMEN:</td>
		<td><STRONG><?php echo $row['nombre_tipo_examen']?></STRONG></td>
  </tr>
	<tr>
    <td>TIPO DE CITA:</td>
    <td><b><?php echo $row['tipocitadato'];?></b></td>
  </tr>		


  <?php 
  $proc=$model->RSAsociativo("SELECT sp.codigo,sp.nombreve,cp.Cantidad,cp.NroOrden 
  FROM dbo.citas_procedimientos cp,dbo.sis_proc sp  
  WHERE cp.id_cita=".$_REQUEST['id']."
   AND sp.codigo=cp.id_procedimiento
   AND sp.tipo=cp.tipo");
   $proc=$proc[0];
   if($proc['codigo']!=null){ ?>
  <tr>
      <td>PROCEDIMIENTO:</td>
      <td><strong><?php echo $proc['codigo'].' - '.$proc['nombreve']; ?></strong></td>
    </tr>
  <?php
   }
  ?>
  
  
    <?php
    if(trim($row['obsCita'])!=''){
	?>
    <tr>
      <td>OBSERVACION:</td>
      <td><strong><?php echo $row['obsCita']; ?></strong></td>
    </tr>    
    <?php
	}
	?>
    <tr>
      <td></td>
      </tr>
	<?php if($row["estadoCita"] == 'P'): ?>
    <tr>
      <td><STRONG>RECUERDE:</STRONG></td>
      </tr>
		  <?php if($row["tipocitadato"]=="TELEMEDICINA"){
        $link_teleconsulta = $model->getDato("value", "parametrosGenerales", "parametro = 'servidorVideoConsultas'");
        $link_teleconsulta = $link_teleconsulta.'?id='.$_REQUEST['id'];
			?>
        <tr>
          <td colspan="2">
            <ul>
              <li>SI SU CONSULTA ES PARA <a>MEDICINA GENERAL</a>, SU M&Eacute;DICO LO CONTACTAR&Aacute; A TRAV&Eacute;S DE LLAMADA TELEF&Oacute;NICA Y/O VIDEOLLAMADA. POR FAVOR INGRESE AL LINK : 
              <a href=" <?php echo $link_teleconsulta ?>"><?php echo $link_teleconsulta ?></a> 
              A LA FECHA Y HORA DE LA CITA.
              </li>
            </ul>
          </td>
        </tr>
      <?php
        }
			?>
      <tr>
        <td colspan="2">
            <?php 
              $row["tipocita"] = ($row["tipocita"] == NULL)? 0 :$row["tipocita"];
              $texto_recordatorio = $model->getDato("texto_recordatorio", "tipos_citas","codigo = '".$row["tipocita"]."'");
              if($texto_recordatorio != '' && $texto_recordatorio != NULL){
                echo $texto_recordatorio;
              }else{
                echo  ($model->getDato("convert(varchar(MAX),value)","parametrosGenerales","parametro='textoRecordatorioCitas'"));
              } 
            ?>
        </td>
      </tr>
      
<?php else:	?>
	<tr>
		<td>NIVEL/ESTRATO:</td>
		<td><STRONG><?php echo $row["nombreEstrato"]; ?></STRONG></td>
	</tr>
	<tr>
		<td>VALOR PAGADO:</td>
		<td><STRONG><?php echo number_format($row["pagoPaciente"]); ?></STRONG></td>
	</tr>
	<tr>
		<td>IMPRESO POR:</td>
		<td><STRONG><?php echo $model->getDato("nombre", "usuario", "id = ".$_SESSION["id_user_sistema"]); ?></STRONG></td>
	</tr>
	<tr>
		<td>FECHA Y HORA IMP.:</td>
		<td><STRONG><?php echo date("Y/m/d H:i"); ?></STRONG></td>
	</tr>

<?php endif; ?>
  </table>
</div>
<?php
pie();
?>