<?php
require_once("dompdf/dompdf_config.inc.php");
class reporte{
	public function reporte(){
	}
	
	public function pdf($html, $nombre, $or="v", $hoja="a4",$savePdfRuta="",$showPdf=true){
		$orientacion="portair";
		if($or=="h"){
			$orientacion="landscape";
		}
		//$nombre='PDFs/'.$nombre.".pdf";
		$dompdf = new DOMPDF();
		$dompdf->set_paper($hoja, $orientacion); 
		$dompdf->load_html($html);
		$dompdf->render();
		if($savePdfRuta!=""){
			if(!is_dir($savePdfRuta)){
				mkdir($savePdfRuta, 0777, true);
			}
			
			file_put_contents($savePdfRuta.'/'.$nombre.".pdf", $dompdf->output());
		}
		if($showPdf){
			$dompdf->stream($nombre, array("Attachment" => 0));
		}
		
	}
}
?>
