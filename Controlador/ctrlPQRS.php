<?php
@session_start();
include_once("../Model/Model.php");
include_once("../Vistas/General.php");
include_once("../Model/Utils/mail.php");


extract($_REQUEST);
$model=new Model();
$json=new PHP_JSON_AJAX();

// if($model->statusConexion == false){
//$json->addParam('status',$model->statusUserLogueado);
// }

switch($operacion){

	case 'municipio':

		$res = $model->RSAsociativo("select * from sis_muni where id_dep = '".$id_depar."' ");
		$options = '<option value="">Seleccione...</option>';
		foreach ($res as $h) {
			$options .= '<option  value="' . $h['codigo'] . '">' . $h['nombre'] . '</option>';
		}									
		$json->addParam('datos',$res);
		$json->addParam('muni',$options);
		$json->encode();

	break;

    case 'cargarSolicitudes':
        $solicitudes = $model->RSAsociativo("EXEC SismaEps.dbo.Sp_PQRS @op='" . $operacion . "', @estado='" . $estado . "', @autoid='".$_SESSION["IdUsuario"]."'");
        $json->addParam('datos', $solicitudes);
        $json->encode();
        break;

    case "visualizarArchivo":
        $solicitudes = $model->RSAsociativo("EXEC SismaEps.dbo.Sp_PQRS @op = '".$operacion."', @id='" . $Radicado . "'");

        $html = '<br/>
				<div id="filesSol" class="row"></div>
				<br>
				<table id="gestionesSolicitud" class="table table-striped table-bordered">
					<thead class="text-center">
						<th >Nombre del archivo</th>
						<th>Acción</th>
					</thead><tbody>';
        if (count($solicitudes) > 0) {
            $btnAdjuntar = '';
            foreach ($solicitudes as $solicitud) {
                $html .= '<tr>
					<td>' . $solicitud["Archivo"] . '</td>
					<td align="center"><a class="btn btn-link" style="color: #3f65f5; font-size:18px;" download="' . $solicitud["Archivo"] . '" href="'.$_SESSION['site_name_portal'].'/Archivos/archivosGestionPQRS/' . $solicitud['Archivo'] . '">
                            <i class="fa fa-download" aria-hidden="true"></i>
                        </a>
                    </td>
				</tr>';
            }
        } else {
            $html .= '<tr><td colspan="3" class="text-center">No se encontraron resultados</td></tr>';
        }
        $html .= '</tbody></table>';
        $json->addParam('payload', $html);
        $json->encode();
        break;


	case 'guardarFormulario':
		if($igualAfectado==1){
			$id_tipo_AF = null;
			$nombres_AF = null;
			$apellidos_AF = null;
			$direccion_AF = null;
			$id_AF = null;
			$correo_AF = null;
			$telefono_AF = null;
			$telefono2_AF = null;
			$eps_AF = null;
			$autoid_AF = null;
			$entidad_AF = null;
			$PuntoAtencion_AF = null;

		}
		
		$res = $model->RSAsociativo("EXEC SismaEps_Pruebas.dbo.Sp_PQRS @op = 'GuardarEXT',
										@nomUsuario = '". $nombres." ".$apellidos."',
										@identificacion = '".$id."',
										@tipo_id = '".$id_tipo."',
										@direccion = '".$direccion."',
										@correo = '".$correo."',
										@telefono = '".$telefono."',
										@telefono2 = '".$telefono2."',
										@tipoRequerimiento = '".$tipo_req."',
										@subCategoria = '".$sub_tipo."',
										@es_externa = '0',
										@en_gestion = '1',
										@estado = 'PENDIENTE',
										@el_mismo_AF ='".$igualAfectado."',
										@resumenCausa = '".$descripcion."',
										@tipo_af = '".$id_tipo_AF."',
										@nombres_af = '".$nombres_AF."',
										@apellidos_af = '".$apellidos_AF."',
										@identificacion_af = '".$id_AF."',
										@direccion_af = '".$direccion_AF."',
										@entidad = '".$entidad."',
										@id_punto_atencion = '".$PuntoAtencion."',
										@autoid = '".$autoid."',
										@entidad_AF = '".$entidad_AF."',
										@id_punto_atencion_AF = '".$PuntoAtencion_AF."',
										@autoid_AF = '".$autoid_AF."',
										@correo_af = '".$correo_AF."',
										@eps = '".$eps."',
										@telefono_AF = '".$telefono_AF."',
										@telefono2_AF = '".$telefono2_AF."',
										@eps_AF = '".$eps_AF."'
										");

		$adjuntos = array();
		$ext = array('jpg','JPG','png','PNG','pdf','PDF','jpeg','JPEG');
		$nombre_archivo = $_FILES['archivos']['name']; 
		$tipo_archivo = $_FILES['archivos']['type']; 
		$tamano_archivo = $_FILES['archivos']['size']; 
		$tmp_name = $_FILES['archivos']['tmp_name'];
		
		if($nombre_archivo[0] != ''){
			$respuestaArchivo = adjuntarArchivosSolicitud($res[0]['id_consecutivo'],$nombre_archivo,$ext,$tmp_name,$tipo_archivo,$id);
			//$model->finTransaccion(true);
			if($respuestaArchivo["estado"]){
				$ERRORMSG = NULL;
				$errorTran = false;
			}else{
				$ERRORMSG= $respuestaArchivo["mensaje"];
				$errorTran = true;
			}		
		}else{
			$errorTran = false;
		}
		
		$json->addParam('datos',$res );
		$json->encode();

	break;

	case 'validarPaciente':


		$res = $model->RSAsociativo("EXEC SpActualizarDatosPortal @op = 'validarPaciente',
										@id_tipo = '".$tipo_id."',
										@documento = '".$documento."'");

		$json->addParam('datos',$res);
		$json->encode();

	break;


	case "enviarCorreo":
		$json = new PHP_JSON_AJAX();
		
		$mail = new Mail();

		$nombre_sede = $model->getDato("nombre","puntoAtencion", "id = ".$sede);


		$mail->ASUNTO = "Respuesta de tamizaje";
		$content = "		
		<table style='width:90%' style='font-size: 25px'>
			<tbody>				
				<tr>
					<td>
						<b>Cordial saludo</b>,
						<p></p>
					</td>
				</tr>
				<tr>
					<td>
						Señor(a) ".$nombre.",  Medicina Integral notifica que su información ha sido diligenciada y enviada correctamente.
						<p></p>
					</td>
				</tr>
				<tr>
					<td>
						Puede acercarse a <b>".$nombre_sede."</b>,  teniendo en cuenta las siguientes recomendaciones.
						<p></p>
					</td>
				</tr>
			</tbody>
		</table>
		<div>
          <div class='row'>
             <div class='col-md-4'>
             </div>
             <div class=col-md-4>
                 <img class='img-fluid' alt='Responsive image' src='../Imagenes/recomendaciones_tamizaje.jpg'>
             </div>
             <div class='col-md-4'>
             </div>
          </div>
          
       </div>";


		$mail->MENSAJE = $content;
		$mail->REMITENTE = $model->getParametroGeneral("mail_from", "CORREO");
		$r_social = $model->getDato("r_social","seriales", "id = 1");

		$mail->NOMBRE_REMITENTE = utf8_decode($r_social);
		
		$mail->DESTINATARIO = $model->getDato("email","pacientesView", "autoid =".$autoid);


		$mail->CC = substr($mail->CC, 0, (strlen($mail->CC) - 1));
		$json->addParam("CC", $mail->CC);
		$statusDestinos = array("email" => $mail->DESTINATARIO, "statusTran" => $mail->sendMail(true));
			
		
		
		$json->addParam("destinosInfo", $statusDestinos);
		if($statusDestinos["statusTran"] == "success"){
			$json->addParam("status", "success");
			$json->addParam("statusInfo", "Correos enviados satisfactoriamente");
		}else{
			$json->addParam("status", "error");
			$json->addParam("statusInfo", $statusDestinos["statusTran"]);
		}


		if ($datos_correo != null ||  $datos_correo != ''){


			$content = "		
			<table style='width:90%' style='font-size: 25px'>
				<tbody>				
					<tr>
						<td>
							<b>Cordial saludo</b>,
							<p></p>
						</td>
					</tr>
					<tr>
						<td>
							Señor(a) ".$datos_nombre.",  Medicina Integral notifica que su información ha sido diligenciada y enviada correctamente.
							<p></p>
						</td>
					</tr>
					<tr>
						<td>
							Puede acercarse a <b>".$nombre_sede."</b>,  teniendo en cuenta las siguientes recomendaciones.
							<p></p>
						</td>
					</tr>
				</tbody>
			</table>
			<div>
	          <div class='row'>
	             <div class='col-md-4'>
	             </div>
	             <div class=col-md-4>
	                 <img class='img-fluid' alt='Responsive image' src='../Imagenes/recomendaciones_tamizaje.jpg'>
	             </div>
	             <div class='col-md-4'>
	             </div>
	          </div>
	          
	       </div>";


			$mail->MENSAJE = $content;
			$mail->REMITENTE = $model->getParametroGeneral("mail_from", "CORREO");
			$r_social = $model->getDato("r_social","seriales", "id = 1");

			$mail->NOMBRE_REMITENTE = utf8_decode($r_social);
			
			$mail->DESTINATARIO = $datos_correo;


			$mail->CC = substr($mail->CC, 0, (strlen($mail->CC) - 1));
			$json->addParam("CC", $mail->CC);
			$statusDestinos = array("email" => $mail->DESTINATARIO, "statusTran" => $mail->sendMail(true));
				
			
			
			$json->addParam("destinosInfo", $statusDestinos);
			if($statusDestinos["statusTran"] == "success"){
				$json->addParam("status", "success");
				$json->addParam("statusInfo", "Correos enviados satisfactoriamente");
			}else{
				$json->addParam("status", "error");
				$json->addParam("statusInfo", $statusDestinos["statusTran"]);
			}

		}
		
		$json->encode();

	break;

    case "cargar_paciente":

        $res = $model->RSAsociativo("EXEC SpActualizarDatosPortal @op = 'cargarPaciente',
										@id_tipo = '".$tipoId."',
										@documento = '".$identificacion."'");

        $json->addParam('datos',$res);
        $json->encode();

    break;
	
}


function adjuntarArchivosSolicitud($solicitud,$nombre_archivo,$ext,$tmp_name,$tipo_archivo,$documento){
    $subida_adjuntos = array();
    $adjuntos = array();
    $respuesta = ["estado" => true];
    $model=new Model();
    $link = 'http://localhost:93/archivosPQRS/';

    if($nombre_archivo[0] != ''){
        foreach($nombre_archivo as $key => $a){
            $aux = explode("/",$tipo_archivo[$key]);
            if(in_array($aux[1],$ext)){
                $nombre = uniqid().".".$aux[1];
                $ruta = "C:\inetpub\wwwroot\PQRS\archivosPQRS/".$nombre;
                $subido=move_uploaded_file($tmp_name[$key], $ruta);

                exec('icacls "C:\inetpub\wwwroot\PQRS\archivosPQRS\\'.$nombre.'" /grant Todos:F /Q /C /T',$output);
                array_push($subida_adjuntos, $subido);
                array_push($adjuntos, $nombre);

            }else{
                array_push($subida_adjuntos, false);
                $respuesta = ["estado"=>false, "mensaje"=>'Solo se admiten archivos con extension '.implode(",", $ext)];
                break;
            }

            if(!in_array(false,$subido)){
                $adjuntos = implode(",",$adjuntos);
                $arch= $model->RSAsociativo("EXEC SismaEps_Pruebas.dbo.Sp_PQRS @op='subirArchivo', @idSolicitud = '".$solicitud."',@adjuntos = '".$nombre."',@ruta = '".$link."',@identificacion = '".$documento."'");

            }
        }

    }
    return $respuesta;
}

?>