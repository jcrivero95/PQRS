<?php 
	@session_start();	
	include_once("../Model/Model.php");
	require_once('/mpdf/mpdf.php');
	extract($_REQUEST);	
	
	$model = new Model();
	$html = '';
	$mpdf = new Mpdf($mode = '', 
	$format = 'letter', 
	$default_font_size = 0,
	$default_font = '', 
	$mgl = 2, $mgr = 2, $mgt = 3, 
	$mgb = 16, $mgh = 2, $mgf = 2, 
	$orientation = 'L');

	$datosEmpresa = $model->select("Sede.r_social As NombreEmpresa,Sede.tipoid,Sede.nit,Sede.direccion,Sede.telefono,Sede.ciudad,Sede.imagen", "seriales As Sede", "Sede.id = ".(int)$sede, NULL, 1);
	$dctoAutorizacion = $model->RSAsociativo("Exec dbo.spRptDctoAutorizacion @TipoDocumento = '".$tipoDocumento."', @Consecutivo = ".$consecutivo);
	$datosDcto = $dctoAutorizacion[0];
	
	if($datosDcto["TipoEstado"] == "Anulacion"){
		$path =  $_SESSION["site_name_portal"]."/Imagenes/anulado.png";
		//echo $path;
		$cssAnulado = ".dctoAnulado{ background-image:url('".$path."');background-repeat:no-repeat;background-position:center;}";
	}else{
		$cssAnulado = "";
	}
	$firmaUsuario = "{$_SESSION['Dir_app_main_portal']}/Archivos/Cliente/Firmas/Usuarios/{$datosDcto['FirmaUsuario']}";

	if (file_exists($firmaUsuario)) {		
		copy("{$_SESSION['CarpetaArchivosRead']}/Archivos/Cliente/Firmas/Usuarios/{$datosDcto['FirmaUsuario']}", "./{$datosDcto['FirmaUsuario']}");
		$firmaUsuario = "<img src='{$datosDcto['FirmaUsuario']}' style='width:80px;height:auto' />";
	} else {
		$firmaUsuario='';
	}
	
	copy("{$_SESSION['Dir_app_main_portal']}Imagenes/{$datosEmpresa['imagen']}", "./{$datosEmpresa['imagen']}");
	
	$pathCopia =  $_SESSION["Dir_app_main_portal"]."/Imagenes/COPIA.png";
	$cssCopia= ".dctoCopia{ background-image:url('".$pathCopia."');background-repeat:no-repeat;background-position:center;}";	
	
	$img = "<img src='".$_SESSION["site_name_portal"]."/Archivos/Imagenes/".$datosEmpresa["imagen"]."' style='width:74px;height:76px' />";
	$html = "<html>
			<head>
			<style>
				".$cssAnulado."
			</style>
			</head>
			<body style='font-size:8pt;font-family:helvetica'>
		
			<div class='dctoAnulado'>
			<table style='margin:auto;width:649px;font-size: 10px !important'>
				<tr>
					<td style='vertical-align:center;width:60px'>".$img."</td>
					<td style='text-align:right'>
						<table cellpadding='1' cellspacing='-0.1' style='width:100%;font-size: 12px !important'>
							<tr>
								<td style='text-align:left'><b>".$datosEmpresa["NombreEmpresa"]."</b><br/>"
								.$datosEmpresa["tipoid"].": ".$datosEmpresa["nit"]."</td>
								<td style='text-align:right;font-size:8pt;font-family:codigo barra;'>"."*".(is_numeric($datosDcto["NumeroReceta"]) ? ($datosDcto["NumeroReceta"]) : rellenar($datosDcto["Consecutivo"], 10))."*"."</td>
							</tr>
							<!-- <tr>
								<td style='text-align:left'>".$datosEmpresa["tipoid"].": ".$datosEmpresa["nit"]."</td>
							</tr> -->
							<tr>
								<td style='text-align:left'>".$datosEmpresa["direccion"]."<br />TEL:".$datosEmpresa["telefono"]."<br />".$datosEmpresa["ciudad"]."</td>
								<td><span style='font-size:11pt;'><b>".($datosDcto["TipoDocumento"] == "AS" ? "Autorizaci&oacute;n No.:" : "")."</b><span style='padding-left:30px'>&nbsp;</span><b>".(rellenar($datosDcto["Consecutivo"], 10))."</b></span>
								<span style='display:block'>Fecha Autorizaci&oacute;n:&nbsp;&nbsp;{$datosDcto['Fecha']}</span><br />"
								."<span>Impreso Por:&nbsp;&nbsp;</span><b>{$_SESSION['user']}</b></td>
							</tr>
							<tr>
								<td style='text-align:left'>"."</td>
								<td rowspan='2' style='text-align:right;'><b style='text-transform:uppercase;'>".$datosDcto["NombreTipoDcto"]."</b></td>
							</tr>
							<tr>
								<td style='text-align:left'></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align:center'>
						<div style='border:solid 0.5px black;padding:2px'>							
							<table style='width:100%;text-align:left;font-size: 12px !important'>
								<tr>
									<td style='width:60px'>Identificaci&oacute;n:</td>
									<td>".$datosDcto["IdPcte"]."</td>
									<td style='width:50px'>Nombre:</td>
									<td>".$datosDcto["NombrePcte"]."</td>																		
								</tr>
								<tr>
									<td>Fecha Nac.:</td>
									<td>".$datosDcto["FechaNacPcte"]."</td>
									<td colspan='2'>
										<table cellpadding='0' cellspacing='0'>
											<tr>
												<td style='width:36px'>Edad:</td>
												<td>".calcularEdad($datosDcto["FechaNacPcte"])."</td>
												<td style='width:36px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sexo:</td>
												<td>".$datosDcto["SexoPcte"]."</td>
												<td style='width:76px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Diagnostico:</td>
												<td>".$datosDcto["DiagPcte"]."</td>
											</tr>
										</table>
									</td>
								</tr>
								<!-- <tr>
									<td>Sede Afiliado:</td>
									<td>".$datosDcto["PuntoAtencionPcte"]."</td>
									<td>F. Afiliaci&oacute;n:</td>
									<td>".convertirFecha($datosDcto["FechaAfiliaPcte"])."</td>
								</tr> -->
								<tr>
									<td>Direcci&oacute;n:</td>
									<td>".$datosDcto["DireccionPcte"]."</td>
									<td>Telefono:</td>
									<td>".$datosDcto["TelefonoPcte"]."</td>			
								</tr>
								<tr>
									<td>Administradora:</td>
									<td colspan='3'>".$datosDcto["Administradora"]."</td>												
								</tr>
								<tr>
									<td>Plan:</td>
									<td colspan='3'>".$datosDcto["Plan"]."</td>
								</tr>
								<!-- <tr>
									<td>Departamento</td>
									<td>".$datosDcto["DepartamentoPcte"]."</td>
									<td>Municipio:</td>
									<td>".$datosDcto["MunicipioPcte"]."</td>			
								</tr> -->
								<tr>
									<td>E-mail:</td>
									<td colspan='3'>".$datosDcto["EmailPcte"]."</td>		
								</tr>
							</table>
						</div>
					</td>
				</tr>";
// if($datosDcto["TipoDocumento"] == "AS"){
	if(true){
	$html .=		"<tr>
						<td colspan='2'>
							<table style='width:100%;font-size: 12px !important'>
								<tr>
									<td>Prestador:</td>
									<td>".$datosDcto["Prestador"]."</td>
									<td>Direccion:</td>
									<td>".(($datosDcto["direccion_prov"] == null)?$datosDcto["DirPrestador"]:$datosDcto["DirPrestador".$datosDcto["direccion_prov"]])."</td>
								</tr>
								<tr>
									<td>Telefonos:</td>
									<td>".$datosDcto["TelPrestador"]."</td>
									<td>Servicio:</td>
									<td>".$datosDcto["Servicio"]."</td>
								</tr>
								<tr>
									<td>Mdco. Trant:</td>
									<td colspan='".($datosDcto['ValorPagadoPcte'] == 0 && $datosDcto['ValorPagarPcte'] > 0 ? 1 : 3)."'>"
									.$datosDcto["MedicoTratante"]."</td>
								".
								(
									$datosDcto['ValorPagadoPcte'] == 0 && $datosDcto['ValorPagarPcte'] > 0 ?
									"<td>Pendiente por pagar:</td>
									<td><strong>".number_format($datosDcto['ValorPagarPcte'])."</strong></td>" 
									:
									""
								)
								."</tr>
								".(trim($datosDcto["Anotaciones"]) != "" ? ("<tr>
									<td>Observaciones:</td>
									<td colspan='3'>".$datosDcto["Anotaciones"]."</td>
								</tr>") : "")."
							</table>
						</td>
					</tr>
					<tr>
						<td colspan='2'>Procedimientos/Medicamentos Autorizados</td>
					</tr>
					<tr>
						<td colspan='2'>
							<div style='border: solid 0.5px black;padding:2px'>
							<table style='width:100%;font-size: 10px !important' cellspacing='0' cellpadding='0'>
								<tr>
									<td>Codigo</td>
									<td>Descripci&oacute;n</td>
									<td>Cantidad</td>
									<td>Obs</td>
								</tr>";
	foreach($dctoAutorizacion as $k => $item){
		$html .= "<tr><td style='width:60px;border-top: solid 0.5px black;margin-top:1px'>".$item["Codigo"]."</td>"
				."<td style='width:auto;border-top: solid 0.5px black;margin-top:1px'>".($item["Descripcion"])."</td>"
				."<td style='width:70px;border-top: solid 0.5px black;margin-top:1px'>".$item["Cantidad"]."</td>"
				."<td style='width:145px;border-top: solid 0.5px black;margin-top:1px'>".utf8_decode($item["Observacion"])."</td></tr>";
	}
	$html .=			"</table>
						<div>
						</td>
					</tr>";
}if($datosDcto["TipoDocumento"] == "AS"){
	$html .=		"<tr>
						<td style='text-align:center;padding-top:10px'>".$firmaUsuario."<br />{$datosDcto['Usuario']}</td>
						<td style='text-align:center;padding-top:10px;'>&nbsp;</td>
					</tr>
					<tr>
						<td style='text-align:center;'>----------------------------<br />Autorizado Por</td>
						<td style='text-align:center;'>---------------------------<br />Firma Afiliado</td>
					</tr>";
}if($datosDcto["TipoDocumento"] != "AS"){
	switch($datosDcto["TipoDocumento"]){
		case "NS":
			$titulo1 = 'JUSTIFICACION';
			$titulo2 = "ALTERNATIVA";
		break;
		
		case "DV":
			$titulo1 = "JUSTIFICACION";
			$titulo2 = "RESPUESTA";
		break;
	}
$html .= 	"
			<tr>
				<td>".$titulo1.":</td>
				<td style='text-transform:uppercase;font-weight:bold'>".utf8_decode($datosDcto["JustificacionDV"])."</td>
			</tr>
			<tr>
				<td>".$titulo2.":</td>
				<td style='text-transform:uppercase;font-weight:bold'>".utf8_decode($datosDcto["RespuestaDV"])."</td>
			</tr>";
}
$html .=	"</table>
			</div>
			
			</body>
			</htmt>";

		


/********************************************************************************/
  /****************************************************************************/


$html2 = "<html>
			<head>
			<style>
				".$cssAnulado." ".$cssCopia."
			</style>
			</head>
			<body style='font-size:8pt;font-family:helvetica' class='dctoCopia'>
		
			<div class='dctoAnulado'>
			<table style='margin:auto;width:80%'>
				<tr>
					<td style='vertical-align:center;width:60px'>".$img."</td>
					<td style='text-align:right'>
						<table cellpadding='1' cellspacing='-0.1' style='width:100%'>
							<tr>
								<td style='text-align:left'><b>".$datosEmpresa["NombreEmpresa"]."</b><br/>"
								.$datosEmpresa["tipoid"].": ".$datosEmpresa["nit"]."</td>
								<td style='text-align:right;font-size:8pt;font-family:codigo barra;'>"."*".(is_numeric($datosDcto["NumeroReceta"]) ? ($datosDcto["NumeroReceta"]) : rellenar($datosDcto["Consecutivo"], 10))."*"."</td>
							</tr>
							<!-- <tr>
								<td style='text-align:left'>".$datosEmpresa["tipoid"].": ".$datosEmpresa["nit"]."</td>
							</tr> -->
							<tr>
								<td style='text-align:left'>".$datosEmpresa["direccion"]."<br />TEL: ".$datosEmpresa["telefono"]."<br />".$datosEmpresa["ciudad"]."</td>
								<td><span style='font-size:12pt;'>".($datosDcto["TipoDocumento"] == "AS" ? "Autorizaci&oacute;n No.:" : "")."<span style='padding-left:30px'>&nbsp;</span>".(rellenar($datosDcto["Consecutivo"], 10))."</span>
								<span style='display:block'>Fecha Autorizaci&oacute;n:&nbsp;&nbsp;{$datosDcto['Fecha']}</span>"
								."<span>Impreso Por:&nbsp;&nbsp;</span><b>{$_SESSION['user']}</b></td>
							</tr>
							<tr>
								<td style='text-align:left'>"."</td>
								<td rowspan='2' style='text-align:right;'><b style='text-transform:uppercase;'>".$datosDcto["NombreTipoDcto"]."</b></td>
							</tr>
							<tr>
								<td style='text-align:left'></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align:center'>
						<div style='border:solid 0.5px black;padding:2px'>							
							<table style='width:100%;text-align:left'>
								<tr>
									<td style='width:60px'>Identificaci&oacute;n:</td>
									<td>".$datosDcto["IdPcte"]."</td>
									<td style='width:50px'>Nombre:</td>
									<td>".$datosDcto["NombrePcte"]."</td>																		
								</tr>
								<tr>
									<td>Fecha Nac.:</td>
									<td>".$datosDcto["FechaNacPcte"]."</td>
									<td colspan='2'>
										<table cellpadding='0' cellspacing='0'>
											<tr>
												<td style='width:36px'>Edad:</td>
												<td>".calcularEdad($datosDcto["FechaNacPcte"])."</td>
												<td style='width:36px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sexo:</td>
												<td>".$datosDcto["SexoPcte"]."</td>
												<td style='width:76px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Diagnostico:</td>
												<td>".$datosDcto["DiagPcte"]."</td>
											</tr>
										</table>
									</td>
								</tr>
								<!-- <tr>
									<td>Sede Afiliado:</td>
									<td>".$datosDcto["PuntoAtencionPcte"]."</td>
									<td>F. Afiliaci&oacute;n:</td>
									<td>".convertirFecha($datosDcto["FechaAfiliaPcte"])."</td>
								</tr> -->
								<tr>
									<td>Direcci&oacute;n:</td>
									<td>".$datosDcto["DireccionPcte"]."</td>
									<td>Telefono:</td>
									<td>".$datosDcto["TelefonoPcte"]."</td>			
								</tr>
								<tr>
									<td>Administradora:</td>
									<td colspan='3'>".$datosDcto["Administradora"]."</td>												
								</tr>
								<tr>
									<td>Plan:</td>
									<td colspan='3'>".$datosDcto["Plan"]."</td>
								</tr>
								<!-- <tr>
									<td>Departamento</td>
									<td>".$datosDcto["DepartamentoPcte"]."</td>
									<td>Municipio:</td>
									<td>".$datosDcto["MunicipioPcte"]."</td>			
								</tr> -->
								<tr>
									<td>E-mail:</td>
									<td colspan='3'>".$datosDcto["EmailPcte"]."</td>		
								</tr>
							</table>
						</div>
					</td>
				</tr>";
if($datosDcto["TipoDocumento"] == "AS"){
	$html2 .=		"<tr>
						<td colspan='2'>
							<table style='width:100%'>
								<tr>
									<td>Prestador:</td>
									<td>".$datosDcto["Prestador"]."</td>
									<td>Direccion:</td>
									<td>".$datosDcto["DirPrestador"]."</td>
								</tr>
								<tr>
									<td>Telefonos:</td>
									<td>".$datosDcto["TelPrestador"]."</td>
									<td>Servicio:</td>
									<td>".$datosDcto["Servicio"]."</td>
								</tr>
								<tr>
									<td>Mdco. Trant.:</td>
									<td colspan='".($datosDcto['ValorPagadoPcte'] == 0 && $datosDcto['ValorPagarPcte'] > 0 ? 1 : 3)."'>"
									.$datosDcto["MedicoTratante"]."</td>"
								.
								(
									$datosDcto['ValorPagadoPcte'] == 0 && $datosDcto['ValorPagarPcte'] > 0 ?
									"<td>Pendiente por pagar:</td>
									<td><strong>".number_format($datosDcto['ValorPagarPcte'])."</strong></td>" 
									:
									""
								)
								."</tr>
								".(trim($datosDcto["Anotaciones"]) != "" ? ("<tr>
									<td>Observaciones:</td>
									<td colspan='3'>".$datosDcto["Anotaciones"]."</td>
								</tr>") : "")."
							</table>
						</td>
					</tr>
					<tr>
						<td colspan='2'>Procedimientos/Medicamentos Autorizados</td>
					</tr>
					<tr>
						<td colspan='2'>
							<div style='border: solid 0.5px black;padding:2px'>
							<table style='width:100%' cellspacing='-0.1' cellpadding='-0.1'>
								<tr>
									<td>Codigo</td>
									<td>Descripci&oacute;n</td>
									<td>Cantidad</td>
									<td>Obs</td>
								</tr>";
	foreach($dctoAutorizacion as $k => $item){
		$html2 .= "<tr><td style='width:60px'><div style='border-top: solid 0.5px black;margin-top:1px'>".$item["Codigo"]."</div></td><td style='width:auto'><div style='border-top: solid 0.5px black;margin-top:1px'>".utf8_decode($item["Descripcion"])."</div></td><td style='width:70px'><div style='border-top: solid 0.5px black;margin-top:1px'>".$item["Cantidad"]."</div></td><td><div style='border-top: solid 0.5px black;margin-top:1px'>".utf8_decode($item["Observacion"])."</div></td></tr>";
	}
	$html2 .=			"</table>
						<div>
						</td>
					</tr>
					<tr>
						<td style='text-align:center;padding-top:10px'>".$firmaUsuario."<br />{$datosDcto['Usuario']}</td>
						<td style='text-align:center;padding-top:10px;'>&nbsp;</td>
					</tr>
					<tr>
						<td style='text-align:center;'>----------------------------<br />Autorizado Por</td>
						<td style='text-align:center;'>---------------------------<br />Firma Afiliado</td>
					</tr>";
}else{
	switch($datosDcto["TipoDocumento"]){
		case "NS":
			$titulo1 = 'JUSTIFICACION';
			$titulo2 = "ALTERNATIVA";
		break;
		
		case "DV":
			$titulo1 = "JUSTIFICACION";
			$titulo2 = "RESPUESTA";
		break;
	}
$html2 .= 	"
			<tr>
				<td>".$titulo1.":</td>
				<td style='text-transform:uppercase;font-weight:bold'>".utf8_decode($datosDcto["JustificacionDV"])."</td>
			</tr>
			<tr>
				<td>".$titulo2.":</td>
				<td style='text-transform:uppercase;font-weight:bold'>".utf8_decode($datosDcto["RespuestaDV"])."</td>
			</tr>";
}
$html2 .=	"</table>
			</div>
			
			</body>
			</htmt>";

  /*****************************************************************************/
/********************************************************************************/

			
	//echo $html;
	

	$impresa=$model->getDato("impresa","AutorizacionServicio","consecutivo='".$_REQUEST["consecutivo"]."' AND TipoDocumento='".$_REQUEST["tipoDocumento"]."'");
	$imprimeOriginalCopia=$model->getParametroGeneral("imprimeOriginalCopia","CONFIGURACION");
	
	if($impresa=="0" || $imprimeOriginalCopia!='S'){
		$imprimir.=$html;
	} else if($impresa == 1) {
		$imprimir.=$html2;
	}

	if($imprimeOriginalCopia=='S' && !isset($generar)){			
		$model->insertar("update AutorizacionServicio set impresa=1 where consecutivo='".$_REQUEST["consecutivo"]."' AND TipoDocumento='".$_REQUEST["tipoDocumento"]."'");
	}
	
	
	$nombreArchivo = $datosDcto["TipoDocumento"]."-".rellenar($datosDcto["Consecutivo"], 10);
	

	$mpdf->WriteHTML(utf8_encode($imprimir));
		
	if($visualizar == 0){
		$mpdf->Output($rutaAdjunto, 'F');
	}else{
		$mpdf->Output();		
		// $rpt->pdf($imprimir, $nombreArchivo);	
		//echo $imprimir;	
	}
	
	
?>
