<?php	
header('Content-Type: text/html; charset=UTF-8');
require_once("template_nuevo_ordenamiento.php");
$tipoManual = $model->tipoManual($estudio);
$codigoManual = $model->codigoManual($estudio);
$MostrarValorReporteDatosOrden=$model->getParametroGeneral("MostrarValorReporteDatosOrden","FACTURACION");

if($model->getParametroGeneral("mostrarCuadroImpresion","CONFIGURACION")=='S'){
	?>
<HTML>
	<HEAD>
		<SCRIPT language="javascript"> 
			function imprimir(){ 
				if ((navigator.appName == "Netscape")) { window.print() ; 
				} 
				else{ 
					var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>'; 
					document.body.insertAdjacentHTML('beforeEnd', WebBrowser); WebBrowser1.ExecWB(6, -1); WebBrowser1.outerHTML = "";
				}
			}
		</SCRIPT> 
	</HEAD>
	<BODY onLoad="imprimir();">
	<?php
	}
	?>

<STYLE>
	section.SaltoDePagina
	{
	PAGE-BREAK-AFTER: always
	}
	
</STYLE>
	<section class="SaltoDePagina">
	<?php
			switch($_REQUEST['tipo_orden']){
			case "nqx":
				$titulo_tipo_orden = "PROCEDIMIENTOS NQx";
			break;
			case "qx":
				$titulo_tipo_orden = "PROCEDIMIENTOS Qx";

			break;
			case "lab":
				$titulo_tipo_orden = "LABORATORIOS";
			break;
			case "pat":
				$titulo_tipo_orden = "PATOLOGIAS";

			break;
			case "ext":
			$tipo_ext = $model->getDato("tipo_ext", 
				"ordenes Left Join dbo.sis_especialidades As Especialidad On dbo.ordenes.Especialidad = Especialidad.id", 
				"numero = $numero AND tipo = '{$_REQUEST['tipo_orden']}'");
				$titulo_tipo_orden = "RECETA MEDICA";
				if($tipo_ext=='medicamentos'){
					$titulo_tipo_orden = "RECETA MEDICA";	 
				}else{
					$titulo_tipo_orden = "ORDENES EXTERNAS";
				}

			break;
			case "exm":
				$titulo_tipo_orden = "FORMULA MEDICA";

			break;
			case "RME":
				$titulo_tipo_orden = "REMISI&Oacute;N A ESPECIALIDAD";

			break;
			default:
				$titulo_tipo_orden =  strtoupper($model->getDato('Desplegable','sismaelm',"tabla = 'CONCEP' AND nombre = '".$_REQUEST['tipo_orden']."'"),1);

			}

			$tamanoLetraInformeReceta=$model->getParametroGeneral("tamanoLetraInformeReceta","CONFIGURACION");
			$tamanoLetraInformeReceta=(trim($tamanoLetraInformeReceta)==''?0:$tamanoLetraInformeReceta);

		?>
		<style>
			
			table td,.head{
				font-size:12px !important;	
			}	
			table.border td {
				font-size: 6px !important;
				/* border: #000000 1px solid; */
				border: #afafaf 1px solid;
				border-right: #afafaf 1px solid;
				font-style: normal;
				padding: 2px 5px 2px 5px;
			}
		</style>
		<?php	
		encabezado($titulo_tipo_orden, $titulo_tipo_orden." del Paciente");

		$rs = $model->select("s.id,o.requiere_aut,con.nombre as nomProveedor,stip.nombre as nomServicio,Solicitud.IdSolicitud", 
			"ordenes o LEFT JOIN contratos con ON con.codigo=o.proveedor Left Join SolicitudAutorizacion As Solicitud On o.numero = Solicitud.NumeroOrden LEFT JOIN sis_deta_temp s ON o.num_servicio= s.num_servicio LEFT JOIN sis_tipo stip ON stip.id=s.fuente_tips", 
			"o.numero = $numero AND o.tipo = '{$_REQUEST['tipo_orden']}'", NULL,1);

		$id=$rs['id'];
		$nomServicio=$rs['nomServicio'];
		$OrdenRequiereAut=$rs['requiere_aut'];
		$NomProveedor=$rs['nomProveedor'];
		$solicitud = $rs["IdSolicitud"];

		require_once("complemento/datos_paciente.php"); 
		?>
		<div class="block" id="ordenNro<?php echo $numero ?>">
			<?php


			/*$rs = $model->select("fecha, hora, medico, resumen, orden_receta,cod_med, Especialidad.id As codigoEspecialidad, Especialidad.nombre As nombreEspecialidad,tipo_ext, ordenes.MedicoEspInterconsulta", 
				"sis_especialidades AS Especialidad INNER JOIN
								ordenes INNER JOIN
								sis_medi ON ordenes.cod_med = sis_medi.codigo ON Especialidad.codigo = sis_medi.especialidad", 
				"numero = $numero AND ordenes.tipo = '{$_REQUEST['tipo_orden']}'");*/
				
			$rs = $model->select("distinct ord.fecha, ord.hora, ord.medico, ord.resumen,esp2.nombre AS esp, 
			orden_receta,cod_med, Especialidad.id As codigoEspecialidad, Especialidad2.nombre As nombreEspecialidad,
			tipo_ext, ord.MedicoEspInterconsulta", 
					"ordenes ord
			left JOIN sis_especialidades AS Especialidad on ord.Especialidad=Especialidad.codigo
			Left Join dbo.sis_especialidades As Especialidad2 On ord.Especialidad = Especialidad2.id 
			left JOIN sis_medi ON ord.cod_med = sis_medi.codigo and Especialidad.codigo = sis_medi.especialidad 
			left JOIN (SELECT nombre,sis_especialidades.id FROM sis_especialidades,ordenes 
			WHERE ordenes.Especialidad=dbo.sis_especialidades.id ) AS esp2 ON esp2.id=ord.Especialidad", 
					"numero = $numero AND ord.tipo = '{$_REQUEST['tipo_orden']}'");

			$r = $model->nextRow($rs);
			$model->freeResult($rs);
			
			$row_servicio = $model->select("u.codigo, u.descripcion,con.nombre as NomContrato",
				"sis_maes AS m 
				inner join ordenes o on o.ingreso = con_estudio
				left join sis_deta sd on sd.num_servicio = o.num_servicio
				INNER JOIN ufuncionales AS u ON case when o.ufuncional is not null then o.ufuncional else 
				case when sd.ufuncional is not null then sd.ufuncional else m.ufuncional end end = u.id,contratos con", "m.con_estudio = '$estudio' and m.contrato=con.codigo and o.numero = $numero",NULL, 1);
				
			$sql =	"SELECT (SELECT TOP 1 descripcion FROM sis_diags WHERE codigo = m.diagno_ing) AS diagno_ing, m.diagno_ing AS cod_diagno_ing
						FROM sis_maes AS m WHERE con_estudio = '$estudio'";
			/*			
			$sql =  "select top 1 codigo_diagnostico_principal as cod_diagno_ing,max(fecha_evo),max(hora_evo),max(dg.descripcion) as diagno_ing from nueva_evolucion
			inner join sis_diags as dg 
			 dg.codigo = codigo_diagnostico_principal
			 ingreso = '$estudio'
			group by codigo_diagnostico_principal 
			order by max(fecha_evo),max(hora_evo) desc";
			*/	
			$rs = $model->query($sql);
			$row2 = $model->nextRow($rs);
			$model->freeResult($rs);
			?>
			<table class="noborder" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100">Dx Principal:</td>
					<td><b><?php echo $row2['cod_diagno_ing'];?> - <?php echo $row2['diagno_ing'];?></b></td>
					<td width="30px">Edad:</td>
					<td align="left" width="80px">
						<b><?php echo $model->getDato("dbo.fnEdad(sp.fecha_naci) as edad","sis_maes sm, sis_paci sp","sm.con_estudio = '".$estudio."' and sp.autoid=sm.autoid");?></b>
					</td>
					<?php if(trim($nomServicio) != ''){	?>
						<td width="100">Servicio.:</td>
						<td><b><?php echo $nomServicio;?></b></td>
					<?php } ?>
		
				</tr>
				<tr>
					<td>Contrato:</td><td><?php echo $row_servicio["NomContrato"];?></td>
				</tr>
				<?php 
					$municipio = $model->RSAsociativo("select sis_muni.nombre as muni  from sis_paci, sis_muni,sis_maes where
									cod_dep = id_dep and cod_muni = codigo 
									and con_estudio = '".$estudio."'
									and sis_maes.autoid = sis_paci.autoid");
				?>
				<tr>
					<td>Municipio:</td><td><?php echo $municipio[0]["muni"];?></td>
				</tr>
			</table>
			<?php if($OrdenRequiereAut == 0 && is_numeric($solicitud)): ?>
			<span>ORDEN DE SERVICIO No. <?php echo $solicitud; ?></span>
			<?php else: ?>
			<span><?php echo ($r['tipo_ext']=='medicamentos')?'Solicitud De Medicamentos N&ordm;:&nbsp;':'Procedimiento N&ordm;:&nbsp;';?><?php echo $numero; ?></span>
			<?php endif; ?>
			<table class="border" style='width:100%'>
				<tr>
					<td width="100">Unidad Funcional:</td>
					<td><b><?php echo $row_servicio['codigo'];?> - <?php echo $row_servicio['descripcion'];?></b></td>
				</tr>
			</table>
			<table cellspacing="0" cellpadding="0">
				<tr>
					<td width="151">Fecha:</td>
					<td width="100"><strong><?php echo convertirFecha($r['fecha']); ?></strong></td>
					<td width="150">Hora:</td>
					<td width="90"><strong><?php echo $r['hora']; ?></strong></td>
					<!--<td>Es:</td>
					<td><strong><?php /*
					if($_REQUEST["tipo_orden"] == "RME"){
						echo "Solcitiud Remisi&oacute;n" ;
					}else{
						if($r['orden_receta']==2){
							echo "Orden";
						}else{
							if(trim($_REQUEST['tipo_orden'])=="ext"){
								echo "Receta";
							}
							
						}
					}

				*/

					?></strong></td>-->
				</tr>  
				<tr>
					<td>M&eacute;dico:</td>
					<td colspan="5"><strong><?php echo $r['medico']; ?></strong></td>
				</tr>
				<tr>
					<?php
					if(trim($model->getParametroGeneral('ManejaProcesosEPS','EPS'))=='S'){
					?>
					<tr>
						<td>Proveedor:</td>
						<td colspan="5"><strong><?php echo $NomProveedor; ?></strong></td>
					</tr>
					<?php
						}
					?>
					<tr>
						<td> </td>
					</tr>
					<?php if($_REQUEST['tipo_orden'] == "RME" || trim($r["nombreEspecialidad"])!=''): ?>
					<tr>
						<td>Especialidad:</td>
						<td colspan="3"><strong><?php echo $r['nombreEspecialidad']?></strong></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td colspan="6"  align="center"><strong><?php echo $_REQUEST['tipo_orden'] =='exm'? 'FORMULA MEDICA' : ($r['tipo_ext']=='medicamentos')?'Datos de Medicamentos':'Datos del Procedimiento:'; ?></strong></td>
					</tr>
					<tr>
						<td colspan="6"><strong><?php echo nl2br( $r['resumen'] ); ?></strong></td>
					</tr>
					<?php if($r['orden_receta']==2||$_REQUEST["tipo_orden"]=="ext"){ ?>
					<tr>
					<td colspan="8" class="border">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="border" >
							<tr>
								<td width="70" height="19" align="center"><strong>C&oacute;digo</strong></td>
								<td width="700" align="center"><strong>Descripci&oacute;n</strong></td>
								<td align="center"><strong>Cantidad</strong></td>
								<?php
								if($r['tipo_ext']=='medicamentos'){?>
								<td width="690" align="center"><strong>Posologia</strong></td>
								<td align="center"><strong>Dias</strong></td>
								<?php 
								} else if($MostrarValorReporteDatosOrden=='S') {
								?>
								<td align="center" width="80"><strong>Valor</strong></td>
								<?php } ?>
							</tr>
							<?php
							$ref=$model->RSAsociativo("EXEC spOrdenesExternas @Op='S_Get_ReferenciasOrden',@NroOrden=".$numero);
							/*$rs2 = $model->query("SELECT codigo, detalle,motivo_autorizacion, Cantidad,Posologia,Dias FROM ordenes_detalle WHERE numero_orden = '$numero'");*/
						
							$rs2 = $model->query("SELECT od.codigo, od.detalle,od.motivo_autorizacion, od.Cantidad,od.Posologia,od.Dias,o.Servicio,st.nombre as 								NomServicio ,o.tipo_ext,
														CASE	WHEN(o.tipo_ext = '02') THEN UPPER('Solicitud De Procedimientos De Diagnosticos')					WHEN(o.tipo_ext = '03') THEN UPPER('Proc. Terapeuticos No Quirurgicos')
														WHEN(o.tipo_ext = '04') THEN UPPER('Solicitud De Procedimientos Quirurgicos')
														END AS NomTipoOrden, dbo.fnGetValorProcedimento(od.codigo, '{$codigoManual}', '$tipoManual') Total
												FROM 	ordenes_detalle od,ordenes o left join sis_tipo st on st.id=o.Servicio  
												WHERE od.numero_orden = o.numero and o.numero=".$numero."								
												ORDER BY o.tipo_ext,o.Servicio");
							$NomTipoOrden='';$NomServicio='';			
							while (($row2 = $model->nextRow($rs2))) {
								if($NomTipoOrden!=$row2["tipo_ext"]){
									?>
									<tr>
										<td colspan="4" class="head row2" style=""><?php echo $row2["NomTipoOrden"];?></td>
									</tr>
									<?php
									$NomTipoOrden=$row2["tipo_ext"];
								}
								if($NomServicio!=$row2["Servicio"]){
									?>
									<tr>
										<td colspan="3" class="row2"><b><?php echo $row2["NomServicio"];?></b></td>
									</tr>
									<?php
										$NomServicio=$row2["Servicio"];
										}
									?>
									<tr>
										<td width="70" height="10" align="center" ><?php echo $row2[0]; ?></td>
										<td width="690" align="justify" >
											<?php echo $row2[1].(trim($row2["motivo_autorizacion"])==''?'':'<br><b>Justificacion: </b>'.$row2["motivo_autorizacion"]);
											if(count($ref)>0){
												echo '<br>';
											}
											foreach($ref as $r=>$ro){
												if($row2[0]==$ro["CodigoUnicoItem"]){
													echo '['.$ro["Descripcion"].'] &nbsp;&nbsp;&nbsp;';
												}							
											}	
										
											?>
										</td>
										<td align="center" ><?php echo $row2["Cantidad"]; ?></td>
										<?php
											if($r['tipo_ext']=='medicamentos'){?>
											<td align="center" ><?php echo $row2["Posologia"]; ?></td>
											<td align="center" ><?php echo $row2["Dias"]; ?></td>	
											<?php
											} else if($MostrarValorReporteDatosOrden=='S') {

										?>
										<td align="center" ><?php echo '$ ' .number_format($row2["Total"], 0, ',', ','); ?></td>
										<?php } ?>
									</tr>
								<?php
									}
								?>
						</table>
					</td>
				</tr>
				<?php } ?>
			</table>
			<table style="width: 100%">
				<tr>
					<td>
						<?php 
							$codigo=$r['cod_med'];
							include("complemento/firma_medico_nuevo.php"); 
						?>
					</td>
					<td>
						<?php 
							if (is_numeric($r['MedicoEspInterconsulta']) && $r['MedicoEspInterconsulta'] > 0) {
								$codigo=$r['MedicoEspInterconsulta'];
								include("complemento/firma_medico_nuevo.php");
							}				 
						?>
					</td>
				</tr>
			</table>

			<!-- <table width="100%" align="center" border="0">
			
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			
			<tr>
				<td width="29%">
				<?php
			//require_once("complemento/firma_medico.php?codigo={$r['cod_med']}"); 
			?></td>
				<td>&nbsp;</td>
			</tr>
			</table> -->
		</div>
		<?php
			pie();
		?>
	</section>
	<?php	
		$impresa=$model->getDato("impresa","ordenes","numero='".$numero."'");
		$imprimeOriginalCopia=$model->getParametroGeneral("imprimeOriginalCopia","CONFIGURACION");

		if($impresa=="0"||$imprimeOriginalCopia!='S'){
			$model->insertar("update ordenes set impresa=1 where numero='".$numero."'");
		}//if impresa==0

	?>

<?php
if($model->getParametroGeneral("mostrarCuadroImpresion","CONFIGURACION")=='S'){
?>
</BODY>
</HTML>
<?php
}
?>