<?php
@session_start();

$estudio = $_REQUEST['estudio'];
$numero = $_REQUEST['numero'];
$codigo_paquete = $_REQUEST['codigo_paquete'];
$paquete = $_REQUEST['paquete'];

function encabezado($title, $descripcion, $display_desc = true) {
	global $model, $estudio, $serial, $numero, $epicrisis, $reporte_cet;
	
  $root = $_SESSION['site_name_portal'] ."/Reportes";	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $root; ?>/css/printable.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $root; ?>/js/print.js" language="javascript"></script>
<title><?php echo $title; ?></title>
<style type='text/css'>
<?php if($reporte_cet == 1): ?>
	body{
		font-family:Arial;
	}
	.borde td{
		border-bottom:#000 solid 1px;
	}
<?php endif; ?>
</style>
<script defer>

function Print() {
	//factory.printing.header = "";
	//factory.printing.footer = "";
	//factory.printing.portrait = true;
	//factory.printing.leftMargin = 2;
	//factory.printing.topMargin = 2;
	//factory.printing.rightMargin = 2;
	//factory.printing.bottomMargin = 2;
	//factory.printing.Print(true) // print with prompt
}
function verMas(recibo){
	e = document.getElementById("extra_" + recibo);
	e.style.display = e.style.display == 'none'? 'block' : 'none';
}
</script>
<script src="<?php echo $_SESSION['site_name_portal']; ?>/Scripts/AC_ActiveX.js" type="text/javascript"></script>
<script src="<?php echo $_SESSION['site_name_portal']; ?>/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="<?php echo $_SESSION['site_name_portal']; ?>/Js/MyFunciones.js" type="text/javascript"></script>
</head>

<body onload="Print()">
<script type="text/javascript">
AC_AX_RunContent( 'id','factory','viewastext','viewastext','style','display:none','classid','clsid:1663ed61-23eb-11d2-b92f-008048fdd814','codebase','<?php echo $_SESSION['root'].'/Comun'; ?>/smsx.cab#Version=6,3,435,20' ); //end AC code
</script><noscript><object id="factory" viewastext  style="display:none"
  classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814"
  codebase="<?php echo $_SESSION['root'].'/Comun'; ?>/smsx.cab#Version=6,3,435,20">
</object></noscript>
<div class="margen">
<div class="content" <?php echo (($reporte_cet == 1)? "style='width:2080px'" : "");?> >
  <div class="header">
    <?php
    $id_sede = $model->getdato("pa.sede", "solicitudautorizacion sa inner join puntoatencion pa on pa.id = sa.puntoatencion", "sa.idSolicitud='".$_REQUEST['idSolicitud']."'");
	  $serial = $model->select("*", "seriales", "id = ".$id_sede, NULL, 1);
    ?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="664"><font size="+1"><?php echo $serial['r_social']; ?></font></td>
        </tr>
      <tr>
        <td>C&oacute;digo del Prestador:&nbsp;&nbsp;<?php echo $serial['codigo_eps']; ?>&nbsp;&nbsp;&nbsp;<strong>Nit:</strong>&nbsp;&nbsp;<?php echo $serial['nit']; ?></td>
        </tr>
      <tr>
        </tr>
      <tr>
        <td>Tel&eacute;fono:&nbsp;&nbsp;<?php echo $serial['telefono']; ?></td>
	  <tr>
	  
	  <tr>
        <td>Tel&eacute;fono consulta externa:&nbsp;&nbsp;2741306-3126154943</td>
	  <tr>
        <td colspan="2" height="5"></td>
      </tr>
    </table>
	<?php
		if ($display_desc) {
	?>
    <div class="subheader">
	  <table width="100%">
        <tr>
          <td>Descripci&oacute;n</td>
          <td rowspan="2" class="sub">Fecha de Impresi&oacute;n:&nbsp;<strong><?php echo date("Y/m/d H:i:s"); ?></strong><br />
            Impreso por:&nbsp;<strong><?php echo $_SESSION['user']; ?></strong> </td>
        </tr>
        <tr>
          <td class="desc"><strong><?php echo $title; ?></strong></td>
          </tr>
        <tr>
          <td colspan="2" class="desc2"><strong><?php echo $descripcion; ?></strong></td>
          </tr>
      </table>
	</div>
	<?php
		}
	?>
  </div>
  <div class="info">
    <?php
}

function bloque($bloque) {
	global $model, $estudio, $serial, $numero, $epicrisis;
?>
<div class="block">
      <?php require_once($bloque); ?>
	</div>
<?php
}

function pie() {
	global $model, $estudio, $serial, $numero, $epicrisis;
?>
  </div>
  <div class="footer"><?php echo /*$serial['coment']*/"2019 Sisma Corporation. Todos los Derechos Reservados."; ?></div>
</div>
</div>
<!-- <center>
<br />
<input type="button" size="100%" value="Imprimir" name="imprimi" id="imprimir" align="center" onclick="window.print()" />
</center> -->
</body>
</html>
<?php
}
?>