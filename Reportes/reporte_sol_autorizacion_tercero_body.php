<?php
include_once('../Model/Model.php');
include_once('../Model/funciones.php');
extract($_REQUEST);
$model = new Model();

require_once("template_2.php");

$tamanoLetraInformeReceta=$model->getParametroGeneral("tamanoLetraInformeReceta","CONFIGURACION");
$tamanoLetraInformeReceta=(trim($tamanoLetraInformeReceta)==''?0:$tamanoLetraInformeReceta);

?>
<style>
	table td,.head{
		font-size:<?php echo $tamanoLetraInformeReceta;?>px !important;	
	}	
</style>
<?php

$tipoSolicitud=$model->getDato("sat.Nombre","dbo.SolicitudAutorizacion sa,dbo.SolicitudAutorizacionTipos sat","sa.IdSolicitud=".$idSolicitud." AND sat.Id=sa.TipoSolicitud");

encabezado("Autorizacion ".$tipoSolicitud, "Reporte de Solicitud de Autorizacion ".ucwords(strtolower($tipoSolicitud)));


$rs=$model->select("pv.tipo_id,pv.num_id,CONVERT(DATE,sa.FechaSolicitud) AS FechaSolicitud,selm.desplegable AS OrigenAtencion,
					sap.Nombre AS Prioridad,CASE when sa.UbicacionPcte='H' THEN 'Hospitalizacion' WHEN sa.UbicacionPcte='U' 
					THEN 'Urgencia'	WHEN sa.UbicacionPcte='A' THEN 'Ambulatorio' END AS UbicacionPcte,sa.Solicitante,
					sa.Solicitante,sdia.descripcion AS DiagP,sdiar.descripcion AS DiagR,stip.nombre AS 
					Servicio,sa.DetalleSolicitud,ea.Nombre as EstadoSolicitud, usu.cedula as cedulaUsuario, usu.nombre As nombreUsuario, sa.Usuario					
					",
					"dbo.SolicitudAutorizacion sa
					LEFT JOIN dbo.sis_diags sdia ON sa.DiagnosticoP=sdia.codigo
					LEFT JOIN dbo.sis_diags sdiar ON sa.DiagnosticoR=sdiar.codigo
					,EstadoAutorizacion ea,dbo.pacientesView pv 
					LEFT JOIN dbo.contratos con ON convert(varchar(MAX),con.codigo)=pv.contrato 
					LEFT JOIN dbo.sis_estrato sest ON sest.codigo=pv.nivel,sis_muni smuni,dbo.EstadoAfiliado estAfil,
					sismaelm selm,SolicitudAutorizacionPrioridades sap,sis_tipo stip, usuario usu",
					"ltrim(rtrim(sa.IdSolicitud)) like '%%' AND pv.autoid=sa.Afiliado 
					AND ea.Id=sa.EstadoSolicitud and smuni.id_dep=pv.cod_dep AND smuni.codigo=pv.cod_muni 
					AND estAfil.Id=pv.EstadoAfiliado 
					AND selm.tabla='CAUSA' AND selm.tipo='EXTERNA'
					AND selm.valor=sa.OrigenAtencion
					AND sa.Prioridad=sap.Id
					AND stip.id=sa.Servicio
					AND usu.id = sa.Usuario
					AND sa.IdSolicitud=".$idSolicitud." 
					ORDER BY IdSolicitud DESC");

$row=$model->nextRow($rs);	
$ident=$row["num_id"]; $tipo=$row["tipo_id"];

$cedulaUsuario = $row["cedulaUsuario"];
$nombreUsuario = $row["nombreUsuario"];
$rutaImgUsuario = $_SESSION["CarpetaArchivosRead"]."/firmas/Usuarios/".$model->getDato("firma", "usuario", "id = ".$row["Usuario"]);
$file_headers = @get_headers($rutaImgUsuario);

require_once("complemento/datos_paciente2.php"); 
	
?>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
    	<td>
        	<b>Datos de la solicitud</b><br /><br />
		</td>
		<!-- <td colspan="" align="center"><img style="position:absolute; left:25%;opacity: 0.5;" src="<!?php echo $_SESSION["site_name_portal"].'/Imagenes/requiere_aut.png' ;?>" /></td> -->
    </tr>
    <tr>
    	<td>
        	<table width="100%" class="border">
            	<tr>
                	<td width="150px"><b>Estado Solicitud: </b></td>
                    <td colspan="3"><b><?php echo $row["EstadoSolicitud"];?></b></td>
                    
                </tr>
                <tr>
                	<td width="150px"><b>Fecha Solicitud: </b></td><td><?php echo $row["FechaSolicitud"];?></td>
                    <td width="120px"><b>Origen Atencion: </b></td><td><?php echo $row["OrigenAtencion"];?></td>
                </tr>
                <tr>
                	<td><b>Prioridad: </b></td><td><?php echo $row["Prioridad"];?></td>
                    <td><b>Ubicacion Paciente: </b></td><td><?php echo $row["UbicacionPcte"];?></td>
                </tr>
                <tr>
                	<td><b>Solicitante: </b></td><td colspan="3"><?php echo $row["Solicitante"];?></td>
                </tr>
                <tr>
                	<td><b>Diagnostico Principal: </b></td><td colspan="3"><?php echo $row["DiagP"];?></td>
                </tr>
                <tr>
                	<td><b>Diagnostico Relacionado: </b></td><td colspan="3"><?php echo $row["DiagR"];?></td>
                </tr>
                <tr>
                	<td><b>Servicio: </b></td><td colspan="3"><?php echo $row["Servicio"];?></td>
                </tr>
                <tr>
                	<td colspan="4"><b>Observacion: </b><br /><?php echo $row["DetalleSolicitud"];?><br /><br /></td>
                </tr>
                
                <tr>
                	<td colspan="4">
                    	<table width="100%">
                        	<tr>
                            	<td class="head">Codigo</td>
                                <td class="head">Detalle</td>
                                <td class="head">Posologia</td>
                                <td class="head">Cantidad</td>
                                <td class="head">Observacion</td>
                            </tr>
                    <?php
                    	$rs=$model->select("Codigo,Detalle,Cantidad,TipoItem, Posologia, ObsItem","SolicitudAutorizacionItem","Solicitud=".$idSolicitud);
							while($row=$model->nextRow($rs)){?>
								<tr>
                                	<td><?php echo $row["Codigo"];?></td>
                                    <td><?php echo utf8_decode(utf8_encode($row["Detalle"]));?></td>
                                    <td><?php echo utf8_decode(utf8_encode($row["Posologia"]));?></td>
                                    <td class="row"><?php echo $row["Cantidad"];?></td>
                                    <td><?php echo utf8_decode(utf8_encode($row["ObsItem"]));?></td>
                                </tr>
							
					<?php 	}
					?>
	                    </table>
                    </td>
                </tr>
                <tr>
                	<td colspan="4" style="text-align:center">
                    <?php 
						if(strpos($file_headers[0], 'Not Found') !== false){
							echo "<div style='margin-top:60px'>&nbsp;</div>";
						} else if (strpos($file_headers[0], 'Not Found') !== false && strpos($file_headers[7], 'Not Found') !== false){
							echo "<div style='margin-top:60px'>&nbsp;</div>";
						} else {
							echo "<img src='".$rutaImgUsuario."' width='200px' height='100px' alt='Firma usuario' /><br />";
						}
					?>
                    -----------------------------------------
                    <br />
					<?php echo $nombreUsuario; ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?php
pie();
?>
