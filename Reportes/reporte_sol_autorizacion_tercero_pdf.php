<?php
@session_start();
include_once("../Model/Model.php");
require_once('/mpdf/mpdf.php');
include_once('../Model/funciones.php');
$model = new Model();
header('Content-Type: text/html; charset=UTF-8');
extract($_REQUEST);

$html = '';
$mpdf = new Mpdf($mode = '', 
$format = 'letter', 
$default_font_size = 0,
$default_font = '', 
$mgl = 2, $mgr = 2, $mgt = 3, 
$mgb = 16, $mgh = 2, $mgf = 2, 
$orientation = 'L');


	ob_start();
		$mpdf->AddPage();
		$mpdf->SetWatermarkImage($_SESSION["site_name_portal"].'/Imagenes/requiere_aut.png',1,'',array(0,0));
		$mpdf->watermarkImageAlpha = 0.2;
		$mpdf->showWatermarkImage = true;
		include("reporte_sol_autorizacion_tercero_body.php");
		$html = ob_get_contents();
		$mpdf->WriteHTML(utf8_encode($html));
		
	ob_end_clean();

if($visualizar == 0){
	$mpdf->Output($rutaAdjunto, 'F');
}else{
	$mpdf->Output();
}

//exit;
// echo $html;
?>