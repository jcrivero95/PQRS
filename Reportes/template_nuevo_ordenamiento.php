<?php
@session_start();
include_once("{$_SESSION['site_name_portal']}/Model/Model.php");
include_once("{$_SESSION['site_name_portal']}/Model/WebModel.php");
$model = new Model();
$estudio = $_REQUEST['estudio'];
$numero = $_REQUEST['numero'];

$font_size = $model->getDato("h.tam_letra","hctiphis AS h","h.codigo = (SELECT TOP 1 i.tipo_histo FROM hcingres AS i WHERE i.con_estudio = '{$_REQUEST['estudio']}')");

function encabezado($title, $descripcion, $display_desc = true,$class="subheader") {
	global $model, $estudio, $serial, $numero, $epicrisis, $font_size;
	
	$path = "{$_SESSION['path']}/Reportes/html";
	
	$root = "{$_SESSION['path']}/../../Reportes/".$model->getParametroGeneral('nit_ftp_cliente','ARCHIVOS SISTEMA')."/html";	


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?php echo $root; ?>/printable_nuevo_ordenamiento.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $root; ?>/print.js" language="javascript"></script>
<title><?php echo $title; ?></title>
<script defer>
/*
function Print() {
	factory.printing.header = "";
	factory.printing.footer = "";
	factory.printing.portrait = true;
	//factory.printing.leftMargin = 2;
	//factory.printing.topMargin = 2;
	//factory.printing.rightMargin = 2;
	//factory.printing.bottomMargin = 2;
	//factory.printing.Print(true) // print with prompt
}*/
</script>
<style>
 WordSection1
	{size:612.0pt 792.0pt;
	margin:70.9pt 3.0cm 70.9pt 3.0cm;
	mso-header-margin:35.45pt;
	mso-footer-margin:35.45pt;
	mso-paper-source:0;}
	
td, .border tr td, .noborder tr td{
	font-size:<?php echo $font_size; ?>px;
}

</style>
</head>

<body>

<object id="factory" viewastext  style="display:none"
  classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814"
  codebase="<?php echo $_SESSION['path'].'/Comun'; ?>/smsx.cab#Version=6,3,435,20">
</object>
<div class="margen">
<div class="content">
  <div class="header">
    <?php
    if (is_numeric($estudio)) {
    	$serial = $model->select("*", "seriales", "id in (select top(1) id_sede from sis_maes where con_estudio = '$estudio') ", NULL, 1);
    } else {
    	$serial = $model->select("*", "seriales", "id = '{$_SESSION['id_sede']}'", NULL, 1);
    }	 
	  
	  $Parametro = $model->getParametroGeneral("reporte_punto_atencion","CONFIGURACION");
	  if($Parametro == 'N'){
		 if (is_numeric($estudio)) {
			$serial = $model->select("*", "seriales", "id in (select top(1) id_sede from sis_maes where con_estudio = '$estudio') ", NULL, 1);
		} else {
			$serial = $model->select("*", "seriales", "id = '{$_SESSION['id_sede']}'", NULL, 1);
		}
		$imagenruta =  $_SESSION["CarpetaArchivosRead"].'/Imagenes/'.$serial["imagen"];
	 }else{
		$s = $model->select("*", "puntoAtencion", "Id='".$_SESSION["IdpuntoAtencion"]."'", NULL, 1); 
		
		$serial = array();
	
		$serial["imagen"]	 	= $s["Codigo"].'.png';
		$serial['r_social'] 	= $s["Nombre"];
		$serial['codigo_eps'] 	= $s["Codigo"];
		$serial['e_mail'] 		= $s["Email"];
		$serial['telefono'] 	= $s['Telefono'];
		$serial['direccion'] 	= $s['Direccion'];
		$serial['web']			= $s['Web'];
		$serial['nit']			= $s['Nit'];
		$imagenruta = $_SESSION["CarpetaArchivosRead"].'/PuntosDeAtencion/'.$serial["imagen"];
	 }
	 
    ?>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td colspan="2" align="center"><strong><font size="+1"><?php echo $serial['r_social']; ?></font></strong></td>
    </tr>
    <tr><td valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      
      <tr>
        <td width="102" rowspan="5" align="center" valign="middle"><img src="<?php echo $imagenruta ?>" height="50px" /></td>
        <td width="500"><strong>C&oacute;digo del Prestador:</strong>&nbsp;&nbsp;<?php echo $serial['codigo_eps']; ?>&nbsp;&nbsp;&nbsp;<strong>Nit:</strong>&nbsp;&nbsp;<?php echo $serial['nit']; ?></td>

        
      </tr>
      <tr>
        <td><strong>Direcci&oacute;n:</strong>&nbsp;&nbsp;<?php echo $serial['direccion']; ?>&nbsp;&nbsp;&nbsp;</td>
		</tr>
      <tr>
        <td><strong>Tel&eacute;fono:</strong>&nbsp;&nbsp;<?php echo $serial['telefono']; ?></td>
		</tr>
      <tr>
        <td><strong>Web:</strong>&nbsp;&nbsp;<?php echo $serial['web']; ?>&nbsp;&nbsp;&nbsp;</td>
		</tr>
      <tr>
        <td><strong>Email:</strong>&nbsp;&nbsp;<?php echo $serial['e_mail']; ?></td>
		</tr>
      <tr>
        <td colspan="2" height="5"></td>
		<td width="198"></td>		
      </tr>
    </table>
    </td><td>
    <td width="198"  valign="bottom" valign="top" >
		<?php if ($display_desc) { ?>
			<div class="<?php echo $class;?>">
			  <table width="100%" cellpadding="0" cellspacing="0">
				<tr>
				  <td class="sub">
				  Fecha de Impresi&oacute;n:&nbsp;<strong><?php echo date("Y/m/d H:i:s"); ?></strong><br />					</td>
				</tr>
				<tr>
				  <td class="desc"><strong><?php echo $title; ?></strong></td>
			    </tr>
			  </table>
			</div>
		<?php }	?>		</td>
    </td></tr></table>
  </div>
  <div class="info">
    <div>
    <!--< ?php require_once("complemento/datos_paciente.php"); ?>-->
	</div>
<?php
}

function bloque($bloque,$requireOnce=true) {
	global $model, $estudio, $serial, $numero, $epicrisis;
?>
    <div class="block">
      <?php 
      if($requireOnce){
      	require_once($bloque); 
	  }else{
	  	require($bloque); 
	  }
      	?>
	</div>
<?php
}

function bloqueImpresionHC($bloque, $fechaInicio, $fechaFin) {
	global 	$model, $estudio, $serial, $numero, $epicrisis, $fechaInicioImpHC, $fechaFinImpHC;
	$fechaInicioImpHC = $fechaInicio;
	$fechaFinImpHC = $fechaFin;
?>
    <div class="block">
      <?php require_once($bloque); ?>
	</div>
<?php
}

function pie() {
	global $model, $estudio, $serial, $numero, $epicrisis;
?>
  </div>
</div>
</div>
</body>
</html>
<?php
}

function firma($medico){?>
<br /><br />
<table width="300" class="info" cellpadding="0" cellspacing="0">
	<tr>
		<td class="firma_sup">Firma del M&eacute;dico: <strong><?php echo $medico['nombre']; ?></strong></td>
	</tr>
	<tr>
		<td>Registro: <strong><?php echo $medico['registro']; ?></strong></td>
	</tr>
	<tr>
		<td>Cꥵla: <strong><?php echo $medico['cedula']; ?></strong></td>
	</tr>
</table>	
<?php 
}
?>