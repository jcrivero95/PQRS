<?php

require_once("template.php");
?>
<?php
$LeyendaReporteReceta = $model->getParametroGeneral("LeyendaReporteReceta", "HISTORIA CLINICA");
if($model->getParametroGeneral("mostrarCuadroImpresion","CONFIGURACION")=='S'){
?>
<HTML>
<HEAD>
<SCRIPT language="javascript"> 
function imprimir()
{ if ((navigator.appName == "Netscape")) { window.print() ; 
} 
else
{ var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>'; 
document.body.insertAdjacentHTML('beforeEnd', WebBrowser); WebBrowser1.ExecWB(6, -1); WebBrowser1.outerHTML = "";
}
}
</SCRIPT> 
</HEAD>
<BODY onLoad="imprimir();">
<?php
}
?>

<STYLE>
section.SaltoDePagina
{
PAGE-BREAK-AFTER: always
}

/* @font-face { 
  font-family: idautomationhc39m;  
  src: url('fonts/IDAutomationHC39M.ttf');
} */
.codigo-barra{
	font-family:idautomationhc39m !important;font-size:10pt
}

</STYLE>

<?php	
$proveedorMctos = $model->getDato("Contrato.nombre", "AutorizacionServicio As Autorizacion Inner Join contratos As Contrato On Autorizacion.Contrato = Contrato.codigo", "Autorizacion.NumeroReceta = '{$_REQUEST["numero"]}'");
$autorizacionServicio = $model->getDato("Autorizacion.TipoDocumento + '-' + dbo.rellenar(Autorizacion.Consecutivo,'0',10)", "AutorizacionServicio As Autorizacion Inner Join contratos As Contrato On Autorizacion.Contrato = Contrato.codigo", "Autorizacion.NumeroReceta = '{$_REQUEST["numero"]}'");
$horaAten = $model->getDato("fecha_atencion+' '+hora_atencion","hcingres","con_estudio = '{$_REQUEST['estudio']}'");
$valorPagarPcte = $model
  ->getDato('dbo.fnGetCopagoPagar(m.con_estudio) valorPagarPcte', "sis_maes m", "m.con_estudio = '{$_REQUEST['estudio']}'");
$digitadoPor = $model
  ->getDato("u.nombre", "AutorizacionServicio a INNER JOIN usuario u ON a.Usuario = u.id", "a.NumeroReceta = '{$_REQUEST['numero']}'");
?>
<?php
encabezado('<table width="100%" align="center"><tr><td align="center">Receta de Medicamentos</td></tr><tr><td align="center"><barcode code="'.$numero.'" type="C39" size="1" height="2.0" /><span class="codigo-barra" style="font-family:idautomation !important;font-size:10pt">*'.$numero.'*</span></td></tr></table>', "Receta de Medicamentos al Paciente <br> ",true,'');

$tamanoLetraInformeReceta=$model->getParametroGeneral("tamanoLetraInformeReceta","CONFIGURACION");
$tamanoLetraInformeReceta=(trim($tamanoLetraInformeReceta)==''?0:$tamanoLetraInformeReceta);
$rs = $model->select("*", "recetas", "numero = $numero");
$r = $model->nextRow($rs);
?>
<style>
	table td,.head{
		font-size:<?php echo $tamanoLetraInformeReceta;?>px !important;	
	}	
</style>
<div class="block">
<?php if(strtolower($r["estado"]) == "anulada"):?>
<table style="width:100%">
	<tr>
    	<td style="text-align:center"><strong style="font-size:12pt !important;color:red">(RECETA ANULADA)</strong></td>
    </tr>
</table>
<?php endif; ?>
<?php
	require_once("complemento/datos_paciente_4.php"); 
	
	$recomendacion=$r['recomendacion'];
	$cod_medico = $r['id_medico'];
	$model->freeResult($rs);
	
	$nombre_med = $model->getDato("s.nombre +'. Reg Medico :'+ s.registro", "sis_medi AS s", "s.codigo = $cod_medico");
	$diagnostico = $model->getDato("sd.codigo", "sis_maes sm,sis_diags sd", "sm.con_estudio=".$_REQUEST["estudio"]."
and sd.codigo=sm.diagno_ing");
	$fechasRs = $model->select("convert(varchar(10), fecha, 111) as Fecha, convert(varchar(10), FechaVencimiento, 111) As FechaVencimiento", "recetas", "numero = $numero",  NULL, 1);

	$MostrarVencimientoReceta = $model->getParametroGeneral("MostrarVencimientoReceta", 'HISTORIA CLINICA');
  ?>
  <table cellspacing="0" cellpadding="0" width="80%">
  	<tr>
    	<td>Diagnostico Principal: <?php echo $diagnostico;?></td>
        
        <td align="center" ><strong> Receta: <?php echo $numero ?></strong></td>
  	</tr>
  </table>
<table cellspacing="0" cellpadding="0" width="100%">
	<tr>
    	<td colspan="2">Fecha Receta:<strong style="margin-left:6px"><?php echo $fechasRs["Fecha"]?></strong></td>
        <?php if($fechasRs["FechaVencimiento"] != "" && $MostrarVencimientoReceta == "S"):?>
        <td colspan="2" style="text-align:right">Vence:<strong style="margin-left:6px"><?php echo $fechasRs["FechaVencimiento"]?></strong></td>
        <?php endif; ?>
    </tr>
    <?php if(trim($proveedorMctos) != ""): ?>
    <tr>
      <td colspan="2">Proveedor:&#160;&#160;<strong><?php echo $proveedorMctos; ?></strong></td>
      <td colspan="2" style="text-align: right;">Autorizaci&oacute;n:&#160;&#160;<strong><?php echo $autorizacionServicio; ?></strong></td>
    </tr>
    <?php endif; ?>
    <?php if($valorPagarPcte > 0 && trim($autorizacionServicio) != ''): ?>
    <tr>
      <td colspan="4" style="text-align: right;">Copago / C.M:&nbsp;<strong><?php echo number_format($valorPagarPcte); ?></strong></td>
    </tr>
    <?php endif; ?> 
	<tr height="20px">
    	<td class="head" align="left">
        	<div align="left">&#160;&#160;MEDICAMENTO</div>
        </td>
    	<td class="head" width="60px" align="center">
        	CANT.
        </td>
    	<td class="head" width="120px">
        	POS. Y VIA DE ADMIN.
        </td>
    	<td class="head" width="60px">
        	D&Iacute;AS
        </td>
    </tr>
    <?php
		$rs = $model->select("rd.*", 
		"recetas_deta as rd", 
		"rd.numero=$numero",NULL);
		while($r = $model->nextRow($rs)){
	?>
	<tr height="15px">
    	<td align="left">
        	<?php echo $r['cod_prod'];
				if(trim($r['cod_prod'])!=""){echo ' - ';}
				echo $r['nombre']; ?>
        </td>
    	<td width="60px" align="center">
        	<?php echo (int)($r['cantidad']); ?>
        </td>
    	<td width="120px">
        	<?php echo ($r['posologia']); ?>
        </td>
    	<td width="60px" align="center">
        	<?php echo (int)($r['dias']); ?>
        </td>
    </tr>
    <?php if(trim($r["Obs"]) != ""): ?>
    <tr>
    	<td colspan="4">Obs: <strong><?php echo $r["Obs"]; ?></strong></td>
    </tr>
    <?php endif; ?>
    <?php
		}
	?>
    <tr >
    	<td align="left"  colspan="4" >
        	&#160;
        </td>
    </tr>
    <?php 
    if(trim($_REQUEST["esTriage"])!="1"){
	?>
	<tr >
    	<td align="left"  colspan="4" style="border-top:#000000 1px solid">
        	<b>RECOMENDACION:</b>
        </td>
    </tr>

	<tr>
    	<td align="left" colspan="4">
        	<?php echo $recomendacion; ?>
        </td>
    </tr>
    <?php
    }
		if($model->getParametroGeneral('es_ips','CONFIGURACION')=="S"){
	?>
	<tr >
    	<td align="center"  colspan="4" style="border-top:#000000 1px solid; font-size:8px;">
        	NO VALIDO PARA FACTURAR, RECLAME SUS MEDICAMENTOS ANTES DE 72 HORAS DE LA FECHA DE EMISION
        </td>
    </tr>
    <?php }?>
</table>
<br/>
<table cellspacing="0" cellpadding="0" style="width:100%">
	<tr>
 	<td  width="45%" style="border-bottom: #000000 2px solid" class="row">
    	<?php
			$cedula_med = $model->getDato( "cedula", "sis_medi", "codigo=".$cod_medico."" );
			
			$firma = $wmodel->getFirmaMedico($cedula_med,120,50);
			echo (empty($firma))? "&nbsp;" : $firma;
		 ?>
    </td>
    <td>&nbsp;</td>
    <?php if(trim($digitadoPor) != ''): ?>
    <td style="border-bottom: #000000 2px solid" class="row"><b><?php echo $digitadoPor; ?></b></td>    
    <?php else: ?>
    <td>&nbsp;</td>
    <?php endif; ?>
  </tr>
  <tr>
  <?php  
	$nombre_med = $model->getDato( "nombre+'. Reg medico: '+registro", "sis_medi", "codigo=".$cod_medico."" );
  ?>
    <td class="row">MEDICO</td>
    <td>&nbsp;</td>
    <?php if(trim($digitadoPor) != ''): ?>
    <td style="text-align: center;">DIGITADO POR</td>    
    <?php endif; ?>
  </tr>
  <tr>
    <td class="row"><b><?php echo $nombre_med ?></b></td>
    <td>&nbsp;</td>    
  </tr>
  <?php if(trim($LeyendaReporteReceta) != ""): ?>
   <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
  	<td colspan="3" style="text-align:center"><strong><?php echo $LeyendaReporteReceta; ?></strong></td>
  </tr>
  <?php endif; ?>
  </table>
<?php
if($info_hctiphis['ImprimeOdon'] == 1){
	include("complemento/disclaimer.php");
}

pie();
?>

<?php
$model->insertar("update recetas set impresa=0 where numero='".$_REQUEST["numero"]."'");
?>
</body>
</html>