<span>Datos del Paciente</span>
<?php
	$datos = $model->datosPaciente($ident, $tipo, "*");
?>
<table border="0" cellpadding="1" cellspacing="1">
  
  <tr>
    <td width="123">Identificaci&oacute;n:</td>
    <td width="92"><strong><?php echo "{$datos['tipo_id']} - {$datos['num_id']}"; ?></strong></td>
    <td width="70">Paciente:</td>
    <td colspan="5"><strong><?php echo "{$datos['primer_ape']} {$datos['segundo_ape']}, {$datos['primer_nom']} {$datos['segundo_nom']}"; ?></strong></td>
  </tr>
  <tr>
    <td>Fecha de Nacimiento:</td>
    <td><strong><?php echo convertirFecha($datos['fecha_naci']); ?></strong></td>
    <td>Edad:</td>
    <td width="90"><strong><?php echo calcularEdad(convertirFecha($datos['fecha_naci'])); ?></strong></td>
    <td width="110">R&eacute;gimen:</td>
    <td width="160" colspan="2"><strong><?php echo $model->getDato("UPPER(nombre)", "sismaelm", "tabla = 'GRL' AND tipo = 'RGMN' AND valor = {$datos['tipo_usuario']}"); ?></strong></td>
    <td width="80">&nbsp;</td>
  </tr>
  <tr>
    <td>Direcci&oacute;n:</td>
    <td colspan="3"><strong><?php echo $datos[15]; ?></strong></td>
    <td> Tel&eacute;fono: </td>
    <td colspan="2"><strong><?php echo $datos['telefono']; ?></strong></td>
    <td>&nbsp;</td>
  </tr>
</table>
